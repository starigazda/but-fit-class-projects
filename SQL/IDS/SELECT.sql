-- tabtop = 2 !

DROP TABLE Rozhodci CASCADE CONSTRAINTS;
DROP TABLE Tym			CASCADE CONSTRAINTS;
DROP TABLE Skupina	CASCADE CONSTRAINTS;
DROP TABLE Zapas		CASCADE CONSTRAINTS;
DROP TABLE Hrac			CASCADE CONSTRAINTS;
DROP TABLE Udalost	CASCADE CONSTRAINTS;
DROP TABLE Hrac_Zapas;
DROP TABLE Hrac_Udalost;
DROP TABLE Skupina_Tym;

--DROP SEQUENCE seq_IDRozhodci;
--DROP SEQUENCE seq_IDZapasu;
--DROP SEQUENCE seq_IDHrace;
--DROP SEQUENCE seq_IDUdalosti;

CREATE TABLE Rozhodci (
	IDRozhodci	INTEGER NOT NULL,				-- PK
	Jmeno 			VARCHAR(30),
	Prijmeni 		VARCHAR(30),
	Stat 				VARCHAR(35)				
);
--CREATE SEQUENCE seq_IDRozhodci;

CREATE TABLE Tym (
	Stat 							VARCHAR(35),			-- PK
	JmenoTrener				VARCHAR(30),
	PrijmeniTrener		VARCHAR(30),
	JmenoAsistent1 		VARCHAR(30),
	PrijmeniAsistent1 VARCHAR(30),
	JmenoAsistent2 		VARCHAR(30),
	PrijmeniAsistent2	VARCHAR(30)
);

CREATE TABLE Skupina (
	-- nazev bych dal pouze A, B, .... i treba finale by se hralo ve skupine C a
	-- ze je to finale by rikala az uroven
	NazevSkupiny 	CHARACTER(1),				-- PK		
	-- zakladni, ctvrtfinale(8 tymu => 4 postupuji), semifinale(4 tymy = > 2
	-- postupuji do finale), finale
	Uroven 				VARCHAR(15)
);

CREATE TABLE Zapas (
	IDZapasu				INTEGER NOT NULL,	-- PK
	Datum						DATE,							-- DD-MM-YYYY
	Misto						VARCHAR(30),
	GolyTym1 				INTEGER,				
	GolyTym2				INTEGER,
	-- pocet minut
	NastavenyCas		INTEGER,				
	PocetDivaku 		INTEGER,
	Tym1						VARCHAR(35),			-- FK
	Tym2						VARCHAR(35),			-- FK
	HlavniRozhodci	INTEGER,					-- FK
	Rozhodci2				INTEGER,					-- FK
	Rozhodci3				INTEGER,					-- FK
	Rozhodci4				INTEGER,					-- FK
	NazevSkupiny		CHARACTER(1)			-- FK
);
--CREATE SEQUENCE seq_IDZapasu;

CREATE TABLE Hrac (
	IDHrace				INTEGER NOT NULL,		-- PK
	Jmeno 				VARCHAR(30),
	Prijmeni 			VARCHAR(30),
	Pozice 				VARCHAR(30),
	vek						INTEGER,						
	DomaciKlub 		VARCHAR(30),
	-- tym => PK stat
	tym					VARCHAR(35)					-- FK
);
--CREATE SEQUENCE seq_IDHrace;


CREATE TABLE Udalost (
	IDUdalosti	INTEGER NOT NULL,			-- PK
	Minuta 			INTEGER,
	Typ 				VARCHAR(50),
	IDZapasu		INTEGER NOT NULL			-- FK
);
--CREATE SEQUENCE seq_IDUdalosti;

CREATE TABLE Hrac_Zapas (
	Jako 				VARCHAR(30),
	Od 					INTEGER,
	Do 					INTEGER,
	CisloDresu 	INTEGER,
	IDZapasu 		INTEGER	NOT NULL,			-- FK
	IDHrace			INTEGER NOT NULL			-- FK
);

CREATE TABLE Hrac_Udalost (
	Jako 				VARCHAR(30),
	IDHrace			INTEGER NOT NULL,			-- FK
	IDUdalosti 	INTEGER NOT NULL			-- FK
);

CREATE TABLE Skupina_Tym (
	NazevSkupiny 	CHARACTER(1),				-- FK
	Stat 					VARCHAR(35)					-- FK
);
--

ALTER TABLE Rozhodci ADD CONSTRAINT PK_Rozhodci PRIMARY KEY (IDRozhodci);
ALTER TABLE Tym ADD CONSTRAINT PK_Stat PRIMARY KEY (Stat);
ALTER TABLE Skupina ADD CONSTRAINT PK_NazevSkupiny PRIMARY KEY (NazevSkupiny);
--
ALTER TABLE Zapas ADD CONSTRAINT PK_Zapasu PRIMARY KEY (IDZapasu);
ALTER TABLE Zapas ADD CONSTRAINT FK_zapas_Tym1 FOREIGN KEY (Tym1) REFERENCES Tym;
ALTER TABLE Zapas ADD CONSTRAINT FK_zapas_Tym2 FOREIGN KEY (Tym2) REFERENCES Tym;
ALTER TABLE Zapas ADD CONSTRAINT FK_zapas_skupina FOREIGN KEY (NazevSkupiny) REFERENCES Skupina;
ALTER TABLE Zapas ADD CONSTRAINT FK_zapas_HlavniRozhodci FOREIGN KEY (HlavniRozhodci) REFERENCES Rozhodci;
ALTER TABLE Zapas ADD CONSTRAINT FK_zapas_Rozhodci2 FOREIGN KEY (Rozhodci2) REFERENCES Rozhodci;
ALTER TABLE Zapas ADD CONSTRAINT FK_zapas_Rozhodci3 FOREIGN KEY (Rozhodci3) REFERENCES Rozhodci;
ALTER TABLE Zapas ADD CONSTRAINT FK_zapas_Rozhodci4 FOREIGN KEY (Rozhodci4) REFERENCES Rozhodci;
--
ALTER TABLE Hrac ADD CONSTRAINT PK_Hrace PRIMARY KEY (IDHrace);
ALTER TABLE Hrac ADD CONSTRAINT FK_Hrac_tym FOREIGN KEY (tym) REFERENCES Tym;
--
ALTER TABLE Udalost ADD CONSTRAINT PK_Udalosti PRIMARY KEY (IDUdalosti);
ALTER TABLE Udalost ADD CONSTRAINT FK_Udalost_Zapas FOREIGN KEY (IDZapasu) REFERENCES Zapas;
--
ALTER TABLE Hrac_Zapas ADD CONSTRAINT FK_Hrac_Zapas_Zapas FOREIGN KEY (IDZapasu) REFERENCES Zapas;
ALTER TABLE Hrac_Zapas ADD CONSTRAINT FK_Hrac_Zapas_Hrac FOREIGN KEY (IDHrace) REFERENCES Hrac;
--
ALTER TABLE Hrac_Udalost ADD CONSTRAINT FK_Hrac_Udalost_Hrac FOREIGN KEY (IDHrace) REFERENCES Hrac;
ALTER TABLE Hrac_Udalost ADD CONSTRAINT FK_Hrac_Udalost_Udalost FOREIGN KEY (IDUdalosti) REFERENCES Udalost ON DELETE CASCADE;
--
ALTER TABLE Skupina_Tym ADD CONSTRAINT FK_Skupina_Tym_Skupina FOREIGN KEY (NazevSkupiny) REFERENCES Skupina;
ALTER TABLE Skupina_Tym ADD CONSTRAINT FK_Skupina_Tym_Tym FOREIGN KEY (Stat) REFERENCES Tym;

--
ALTER SESSION SET NLS_DATE_FORMAT = 'DD-MM-YYYY';

------------------------------------------------------------------------
-- OPRAVA: doplnenie chybajucich primarnych klucov
------------------------------------------------------------------------
ALTER TABLE Hrac_Zapas ADD CONSTRAINT PK_Hrac_Zapas PRIMARY KEY (Jako,IDZapasu,IDHrace);
ALTER TABLE Hrac_Udalost ADD CONSTRAINT PK_Hrac_Udalost PRIMARY KEY (IDHrace,IDUdalosti);
ALTER TABLE Skupina_Tym ADD CONSTRAINT PK_Skupina_Tym PRIMARY KEY (NazevSkupiny,Stat);
------------------------------------------------------------------------

INSERT INTO Rozhodci VALUES(0,'Jan',		'Cejl',				'Czech Republic');
INSERT INTO Rozhodci VALUES(1,'Michal',	'Afsad',			'United States');
INSERT INTO Rozhodci VALUES(2,'Diego',	'Macao',			'Brazil');
INSERT INTO Rozhodci VALUES(3,'Pierre',	'DArtagnan',	'French');
INSERT INTO Rozhodci VALUES(4,'Jan',		'Pisek',			'Czech Republic');
INSERT INTO Rozhodci VALUES(5,'Johan',	'Baumann',		'Germany');
INSERT INTO Rozhodci VALUES(6,'Ivan',		'Lokomotov',	'Russia');

INSERT INTO Tym
VALUES('Czech Republic','Stefan', 'Markita', 'Prvy', 'Asistent', 'Druhy', 'Asistent');
INSERT INTO Tym
VALUES('Slovak Republic','Miro', 'Smajda', 'Jozef', 'Hruska', 'Robert', 'Drab');
INSERT INTO Tym
VALUES('Poland','Jurij', 'Bojka', 'Jozef', 'Lustgarten', 'Tadeuzs', 'Kuchar');

INSERT INTO Skupina VALUES('A','Finale');
INSERT INTO Skupina	VALUES('B','Semifinale');

INSERT INTO Zapas
VALUES(400,'21-03-2012','Banik','3','2','1','30323','Czech Republic','Slovak Republic',0,1,2,3,'A');
INSERT INTO Zapas
VALUES(401,'02-03-2012','Varsava','0','1','7','25556','Poland','Slovak Republic',2,3,0,1,'B');

-- Slovak Republic
INSERT INTO Hrac VALUES(100,'Jan',		'Mucha',		'brankar',	28,'Everton FC', 					'Slovak Republic'); 
INSERT INTO Hrac VALUES(101,'Marek',	'Cech',			'obrance',	28,'Trabzonspor', 				'Slovak Republic'); 
INSERT INTO Hrac VALUES(102,'Peter',	'Pekarik',	'obranca',	24,'Kayserispor', 				'Slovak Republic'); 
INSERT INTO Hrac VALUES(103,'Jan',		'Durica',		'obranca',	29,'FC Lokomotiv Moskva', 'Slovak Republic'); 
INSERT INTO Hrac VALUES(104,'Martin',	'Skrtel',		'obranca',	26,'Liverpool FC', 				'Slovak Republic'); 
INSERT INTO Hrac VALUES(105,'Juraj',	'Kucka',		'zaloznik',	24,'Janov FC', 						'Slovak Republic'); 
INSERT INTO Hrac VALUES(106,'Marek',	'Hamsik',		'zaloznik',	24,'SSC Neapol', 					'Slovak Republic'); 
INSERT INTO Hrac VALUES(107,'Vladimir','Weiss',		'zaloznik',	21,'RCD Espanyol', 				'Slovak Republic'); 
INSERT INTO Hrac VALUES(108,'Miroslav','Stoch',		'zaloznik',	21,'Fenerbahce SK', 			'Slovak Republic'); 
INSERT INTO Hrac VALUES(109,'Filip',	'Sebo',			'utocnik',	27,'SK Slovan Bratislava', 'Slovak Republic'); 
INSERT INTO Hrac VALUES(110,'Robert',	'Vittek',		'utocnik',	28,'Trabzonspor', 				'Slovak Republic'); 
INSERT INTO Hrac VALUES(111,'Erik',		'Jendrisek','utocnik',	24,'FC Freiburg', 				'Slovak Republic'); 
-- Czech Republic
INSERT INTO Hrac VALUES(150,'Petr',		'Cech',			'brankar',	NULL,'Chelsea FC', 				'Czech Republic'); 
INSERT INTO Hrac VALUES(151,'Ondrej',	'Kusnir',		'obranca',	NULL,'Sparta Praha', 			'Czech Republic'); 
INSERT INTO Hrac VALUES(152,'Ondrej',	'Mazuch',		'obranca',	NULL,'R.S.C Anderlecht', 	'Czech Republic'); 
INSERT INTO Hrac VALUES(153,'Tomas',	'Sivok',		'obranca',	NULL,'Besiktas', 					'Czech Republic'); 
INSERT INTO Hrac VALUES(154,'Radek',	'Sirl',			'zaloznik',	NULL,'Zenit Petrohrad', 	'Czech Republic'); 
INSERT INTO Hrac VALUES(155,'Libor',	'Sionko',		'zaloznik',	NULL,'FC Kodan', 					'Czech Republic'); 
INSERT INTO Hrac VALUES(156,'Marek',	'Matejovsky','zaloznik',NULL,'FC Reading',			 	'Czech Republic'); 
INSERT INTO Hrac VALUES(157,'Tomas',	'Rosicky',	'zaloznik',	NULL,'Arsenal FC', 				'Czech Republic'); 
INSERT INTO Hrac VALUES(158,'Jan',		'Polak',		'zaloznik',	NULL,'Anderlecht Brusel', 'Czech Republic'); 
INSERT INTO Hrac VALUES(159,'Martin',	'Fenin',		'utocnik',	NULL,'Eintracht Frankfurt', 'Czech Republic'); 
INSERT INTO Hrac VALUES(160,'Milan',	'Baros',		'utocnik',	NULL,'Galatasary Istanbul', 'Czech Republic'); 
INSERT INTO Hrac VALUES(161,'Tomas',	'Necid',		'utocnik',	NULL,'CSKA Moskva', 			'Czech Republic'); 
-- Poland
INSERT INTO Hrac VALUES(200,'Lukasz',	'Fabiansky','brankar',	26,'Arsenal FC', 					'Poland'); 
INSERT INTO Hrac VALUES(201,'Wojciech','Szczesny','brankar',	21,'Arsenal FC', 					'Poland'); 
INSERT INTO Hrac VALUES(202,'Marcin','Wasilewsky','obranca',	31,'R.S.C Anderlecht',		'Poland'); 
INSERT INTO Hrac VALUES(203,'Jakub','Wawrzyniak',	'obranca',	28,'Legia Warsaw',				'Poland'); 
INSERT INTO Hrac VALUES(204,'Damien',	'Perquis',	'obranca',	27,'Sochaux',							'Poland'); 
INSERT INTO Hrac VALUES(205,'Sebastian','Boenisch','obranca',	27,'Werder Bremen',				'Poland'); 
INSERT INTO Hrac VALUES(206,'Sebastian','Boenisch','obranca',	25,'Werder Bremen',				'Poland'); 
INSERT INTO Hrac VALUES(207,'Ludovic','Obraniak',	'zaloznik',	27,'Bordeaux',						'Poland'); 
INSERT INTO Hrac VALUES(208,'Maciej',	'Rybus',		'zaloznik',	22,'Terek Grozny',				'Poland'); 
INSERT INTO Hrac VALUES(209,'Eugen',	'Polanski',	'zaloznik',	26,'Mainz 05',						'Poland'); 
INSERT INTO Hrac VALUES(210,'Ireneuzs',	'Jelen',	'utocnik',	30,'Lille',								'Poland'); 
INSERT INTO Hrac VALUES(211,'Kamil',	'Grosicki',	'utocnik',	23,'Sivasspor',						'Poland'); 

-- Czech Republic vs Slovak Republic
-- SVK
INSERT INTO Hrac_Zapas VALUES('Aktivne',0,91,1,400,100);
INSERT INTO Hrac_Zapas VALUES('Aktivne',0,91,2,400,101);
INSERT INTO Hrac_Zapas VALUES('Aktivne',0,91,3,400,102);
INSERT INTO Hrac_Zapas VALUES('Aktivne',0,91,4,400,103);
INSERT INTO Hrac_Zapas VALUES('Aktivne',0,91,5,400,104);
INSERT INTO Hrac_Zapas VALUES('Aktivne',0,91,6,400,105);
INSERT INTO Hrac_Zapas VALUES('Aktivne',0,91,7,400,106);
INSERT INTO Hrac_Zapas VALUES('Aktivne',0,70,8,400,107);
INSERT INTO Hrac_Zapas VALUES('Aktivne',0,91,9,400,108);
INSERT INTO Hrac_Zapas VALUES('Aktivne',0,91,10,400,109);
INSERT INTO Hrac_Zapas VALUES('Aktivne',0,91,11,400,110);
-- CZE
INSERT INTO Hrac_Zapas VALUES('Aktivne',0,91,1,400,150);
INSERT INTO Hrac_Zapas VALUES('Aktivne',0,91,2,400,151);
INSERT INTO Hrac_Zapas VALUES('Aktivne',0,91,3,400,152);
INSERT INTO Hrac_Zapas VALUES('Aktivne',0,91,4,400,153);
INSERT INTO Hrac_Zapas VALUES('Aktivne',0,91,5,400,154);
INSERT INTO Hrac_Zapas VALUES('Aktivne',0,91,6,400,155);
INSERT INTO Hrac_Zapas VALUES('Aktivne',0,91,7,400,156);
INSERT INTO Hrac_Zapas VALUES('Aktivne',0,91,8,400,157);
INSERT INTO Hrac_Zapas VALUES('Aktivne',0,91,9,400,158);
INSERT INTO Hrac_Zapas VALUES('Aktivne',0,91,10,400,159);
INSERT INTO Hrac_Zapas VALUES('Aktivne',0,91,11,400,160);
--
INSERT INTO Hrac_Zapas VALUES('Nahradnik',0,70,16,400,111);
INSERT INTO Hrac_Zapas VALUES('Aktivne',70,91,16,400,111);

-- Czech Republic vs Slovak Republic
INSERT INTO Udalost VALUES(500,10,'gol',400);
INSERT INTO Udalost VALUES(501,38,'gol',400);
INSERT INTO Udalost VALUES(502,65,'gol',400);
INSERT INTO Udalost VALUES(503,70,'stridani',400);
INSERT INTO Udalost VALUES(504,85,'faul',400);
INSERT INTO Udalost VALUES(505,89,'gol',400);
INSERT INTO Udalost VALUES(506,89,'gol',400);
INSERT INTO Udalost VALUES(507,5, 'out',400);
INSERT INTO Udalost VALUES(508,34,'out',400);
INSERT INTO Udalost VALUES(509,35,'rohovy kop',400);
INSERT INTO Udalost VALUES(510,59,'rohovy kop',400);

INSERT INTO Hrac_Udalost VALUES('Strelec golu',160,500);
INSERT INTO Hrac_Udalost VALUES('Inkasoval gol',100,500);
INSERT INTO Hrac_Udalost VALUES('Strelec golu',160,501);
INSERT INTO Hrac_Udalost VALUES('Inkasoval gol',100,501);
INSERT INTO Hrac_Udalost VALUES('Strelec golu',159,502);
INSERT INTO Hrac_Udalost VALUES('Inkasoval gol',100,502);
INSERT INTO Hrac_Udalost VALUES('stridani down',107,503);
INSERT INTO Hrac_Udalost VALUES('stridani up',111,503);
INSERT INTO Hrac_Udalost VALUES('fauloval',108,504);
INSERT INTO Hrac_Udalost VALUES('obet',150,504);
INSERT INTO Hrac_Udalost VALUES('Strelec golu',106,505);
INSERT INTO Hrac_Udalost VALUES('Inkasoval gol',150,505);
INSERT INTO Hrac_Udalost VALUES('Strelec golu',109,506);
INSERT INTO Hrac_Udalost VALUES('Inkasoval gol',150,506);
INSERT INTO Hrac_Udalost VALUES('Zahral roh',153,506);
INSERT INTO Hrac_Udalost VALUES('Zahral roh',105,506);
------------------------------------------------------
-- Poland vs Slovak Republic
-- PL
INSERT INTO Hrac_Zapas VALUES('Aktivne',0,50,1,401,201);
INSERT INTO Hrac_Zapas VALUES('Aktivne',0,97,2,401,202);
INSERT INTO Hrac_Zapas VALUES('Aktivne',0,97,3,401,203);
INSERT INTO Hrac_Zapas VALUES('Aktivne',0,97,4,401,204);
INSERT INTO Hrac_Zapas VALUES('Aktivne',0,97,5,401,205);
INSERT INTO Hrac_Zapas VALUES('Aktivne',0,97,6,401,206);
INSERT INTO Hrac_Zapas VALUES('Aktivne',0,97,7,401,207);
INSERT INTO Hrac_Zapas VALUES('Aktivne',0,97,8,401,208);
INSERT INTO Hrac_Zapas VALUES('Aktivne',0,97,9,401,209);
INSERT INTO Hrac_Zapas VALUES('Aktivne',0,97,10,401,210);
INSERT INTO Hrac_Zapas VALUES('Aktivne',0,97,11,401,211);
-- SVK
INSERT INTO Hrac_Zapas VALUES('Aktivne',0,97,1,401,100);
INSERT INTO Hrac_Zapas VALUES('Aktivne',0,97,2,401,101);
INSERT INTO Hrac_Zapas VALUES('Aktivne',0,97,3,401,102);
INSERT INTO Hrac_Zapas VALUES('Aktivne',0,97,4,401,103);
INSERT INTO Hrac_Zapas VALUES('Aktivne',0,97,5,401,104);
INSERT INTO Hrac_Zapas VALUES('Aktivne',0,97,6,401,105);
INSERT INTO Hrac_Zapas VALUES('Aktivne',0,97,7,401,106);
INSERT INTO Hrac_Zapas VALUES('Aktivne',0,97,8,401,107);
INSERT INTO Hrac_Zapas VALUES('Aktivne',0,97,9,401,108);
INSERT INTO Hrac_Zapas VALUES('Aktivne',0,97,10,401,109);
INSERT INTO Hrac_Zapas VALUES('Aktivne',0,97,11,401,110);
--
INSERT INTO Hrac_Zapas VALUES('Nahradnik',0,50,12,401,200);
INSERT INTO Hrac_Zapas VALUES('Aktivne',50,97,12,401,200);

-- Poland vs Slovak Republic
INSERT INTO Udalost VALUES(600,5,	'out',401);
INSERT INTO Udalost VALUES(601,13,'faul',401);
INSERT INTO Udalost VALUES(602,29,'primi kop',401);
INSERT INTO Udalost VALUES(603,40,'rohovy kop',401);
INSERT INTO Udalost VALUES(604,50,'faul',401);
INSERT INTO Udalost VALUES(605,50,'zraneni',401);
INSERT INTO Udalost VALUES(606,50,'stridani',401);
INSERT INTO Udalost VALUES(607,70,'out',401);
INSERT INTO Udalost VALUES(608,81,'out',401);
INSERT INTO Udalost VALUES(609,90,'rohovy kop',401);
INSERT INTO Udalost VALUES(610,95,'gol',401);

INSERT INTO Hrac_Udalost VALUES('fauloval',105,601);
INSERT INTO Hrac_Udalost VALUES('obet',211,601);
INSERT INTO Hrac_Udalost VALUES('Zahral primi kop',211,602);
INSERT INTO Hrac_Udalost VALUES('Zahral roh',211,603);
INSERT INTO Hrac_Udalost VALUES('fauloval',108,604);
INSERT INTO Hrac_Udalost VALUES('obet',201,604);
INSERT INTO Hrac_Udalost VALUES('zraneny',201,605);
INSERT INTO Hrac_Udalost VALUES('stridani down',201,606);
INSERT INTO Hrac_Udalost VALUES('stridani up',200,606);
INSERT INTO Hrac_Udalost VALUES('Zahral roh',108,609);
INSERT INTO Hrac_Udalost VALUES('Strelec golu',110,610);
INSERT INTO Hrac_Udalost VALUES('Inkasoval gol',200,610);
------------------------------------------------------

INSERT INTO Skupina_Tym VALUES('A', 'Czech Republic');
INSERT INTO Skupina_Tym VALUES('A', 'Slovak Republic');
INSERT INTO Skupina_Tym VALUES('B', 'Slovak Republic');
INSERT INTO Skupina_Tym VALUES('B', 'Poland');

COMMIT;

-- UPDATE:
-- =============================================================================
INSERT INTO Udalost VALUES(611,20,'faul',401);
INSERT INTO Udalost VALUES(511,30,'faul',400);

INSERT INTO Hrac_Udalost VALUES('fauloval',207,611);
INSERT INTO Hrac_Udalost VALUES('obet',109,611);
INSERT INTO Hrac_Udalost VALUES('fauloval',158,511);
INSERT INTO Hrac_Udalost VALUES('obet',100,511);

INSERT INTO Hrac VALUES(112,'Dusan','Nahradnik','zaloznik',	18,'TJ Dvorianky', 'Slovak Republic'); 
INSERT INTO Hrac_Zapas VALUES('Nahradnik',0,97,13,401,112);

COMMIT; 

-- SELECTS:
-- =============================================================================
-- 1. Dva dotazy vyuzivajici spojeni dvou tabulek:
-- 		- vybere vsechny hrace jejichz jmeno je 'Ondrej' a jmeno jejich  trenera je 'Stefan'
SELECT H.Jmeno, H.Prijmeni, T.JmenoTrener FROM Hrac H, Tym T WHERE H.Tym=T.Stat
AND T.JmenoTrener='Stefan' AND H.Jmeno='Ondrej';
--		- zobrazi jmeno, prijmeni, tym, vsech hracu, kteri nastoupili do nejakeho zapasu s cislem dresu 7
SELECT Jmeno, Prijmeni, Tym, CisloDresu, Tym1 || ' vs ' || Tym2 AS Zapas 
FROM Hrac NATURAL JOIN Hrac_Zapas NATURAL JOIN Zapas
WHERE CisloDresu=7;

-- 2. Jeden dotaz vyuzivajici spojeni tri tabulek:
--		- vybere vsechny udalosti ktere byly odpiskany rozhodcim ze statu "Brazil"
SELECT IDUdalosti, Typ FROM Udalost NATURAL JOIN Zapas NATURAL JOIN Rozhodci
WHERE Stat='Brazil';

-- 3. Dva dotazy s klauzulí GROUP BY a agreagcní funki
--		- pocet odohranych zapasov v skupinach
SELECT NazevSkupiny || '-' || Uroven AS Skupina, COUNT(*) Pocet_Odehranych_Zapasu 
FROM Zapas NATURAL JOIN Skupina
GROUP BY NazevSkupiny || '-' || Uroven;
--		- ktory tym bol najagresivnejsi a s kolkymi faulami?
SELECT Stat, pocet_faulov 
FROM (SELECT Stat, count(*) pocet_faulov
      FROM Tym, Hrac NATURAL JOIN Hrac_Udalost  
      WHERE Stat=tym AND jako='fauloval'
      GROUP BY Stat
      ORDER BY pocet_faulov DESC)
WHERE ROWNUM=1;

-- 		- ktory 3 Kluby, maju najaviac hracov na majstrovstvach, kolkych?
SELECT DomaciKlub, Pocet_hracov
FROM (SELECT DomaciKlub, count(*) Pocet_hracov
      FROM Hrac
      GROUP BY DomaciKlub
      ORDER BY Pocet_hracov DESC)
WHERE ROWNUM<=3;

-- 4. Jeden dotaz s predikatem EXIST.
-- 		- ktory hraci sa nezucastnili ziadneho zapasu? ani ako nahradnici
SELECT T.Stat, H.Jmeno, H.Prijmeni, H.DomaciKlub
FROM Tym T, Hrac H
WHERE H.tym = T.Stat AND NOT EXISTS (SELECT * FROM Hrac_Zapas
                                    WHERE H.IDHrace=IDHrace);
                                    
-- 5. Jeden dotaz s predikatem IN.
-- 		- ktory hraci sa zucastnili aspon jedneho zapasu ako nahradnici
SELECT *
FROM Hrac
WHERE IDHrace IN (SELECT IDHrace
                  FROM Hrac_Zapas 
                  WHERE Jako='Nahradnik');
