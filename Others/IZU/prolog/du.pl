%delka linearniho seznamu
delka([],0).
delka([_|T],S) :- delka(T,SS), S is SS + 1.

%je prvek clenem lin. seznamu?
jePrvek([X|_],X).
jePrvek([_|T],X) :- jePrvek(T,X). 

%spojeni dvou linearnich seznamu
spoj([],L,L).
spoj([H|T],L,[H|TT]) :- spoj(T,L,TT).


%doplnte nasledujici predikaty:

% Vytvorte funkci pro rozdeleni linearniho seznamu na poloviny
% divide_half(INPUT,HALF1,HALF2)
divide_half(L,LA,LB):-spoj(LA,LB,L), delka(LA,S1), delka(LB,S2), S1 - S2 >= 0, S1 - S2 =< 1,!.

% Vytvorte funkci pro odstraneni obecneho prvku z obecneho seznamu
% remove_item_GEN(INPUT,ITEM,OUTPUT)
remove_item_GEN([],_,[]).
remove_item_GEN([H|T],H,NT):-remove_item_GEN(T,H,NT).
remove_item_GEN([H|T],I,[NH|NT]):-is_list(H),remove_item_GEN(H,I,NH),remove_item_GEN(T,I,NT).
remove_item_GEN([H|T],I,[H|NT]):-remove_item_GEN(T,I,NT),!.

% Vytvorte funkci pro reverzaci obecneho seznamu
% reverse_GEN(INPUT,OUTPUT)
reverse_GEN([],[]).
reverse_GEN([H|T],R):-is_list(H),reverse_GEN(H,RH),reverse_GEN(T,RT),spoj(RT,[RH],R).
reverse_GEN([H|T],R):-reverse_GEN(T,R1),spoj(R1,[H],R),!.

% Vytvorte funkci pro vlozeni prvku na n-tou pozici linearniho seznamu
% insert_pos(LIST,POSITION,ITEM,OUTPUT)
insert_pos(L,1,I,[I|L]).
insert_pos(L,P,I,NL):-delka(L,S),P > S, X is S+1, insert_pos(L,X,I,NL),!. 
insert_pos([H|T],P,I,[H|NT]):- P1 is P-1, insert_pos(T,P1,I,NT),!. 

% Vytvorte funkci pro inkrementaci kazdeho prvku obecneho seznamu o hodnotu hloubky zanoreni prvku
% increment_general(INPUT,OUTPUT)
% input [0,0,[0]] -> output [1,1,[2]]
increment_general(L,NL):-pom_increment(L,1,NL).
pom_increment([],_,[]).
pom_increment([H|T],D,[NH|NT]):-is_list(H),DD is D+1,pom_increment(H,DD,NH),pom_increment(T,D,NT).
pom_increment([H|T],D,[NH|NT]):-NH is H+D, pom_increment(T,D,NT),!.
