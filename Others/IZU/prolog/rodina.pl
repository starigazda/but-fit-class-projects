muz(iluvatar).
muz(elros).
muz(elrond).
muz(john).
muz(paul). 
muz(george).
muz(ringo).

zena(clare).
zena(lucy).
zena(mary). 
zena(jane). 
zena(linda).

otec(iluvatar,elrond).
otec(iluvatar,elros).
otec(elros,clare).
otec(elros,lucy).
otec(elrond,paul).
otec(elrond,george).
otec(paul,john). 
otec(george,mary).
otec(john,ringo).
otec(john,jane).

matka(clare,john). 
matka(lucy,mary).
matka(linda,ringo).
matka(mary,jane).


rodic(X,Y) :- otec(X,Y).
rodic(X,Y) :- matka(X,Y).

je_matka(X) :- matka(X,_).

%tyto pravidla doplnte
sourozenec(X,Y) :- rodic(Z,X), rodic(Z,Y), X\==Y.
teta(X,Y) :- sestra(X,Z), rodic(Z,Y). 
sestra(X,Y) :- sourozenec(X,Y), zena(X).  
deda(X,Y) :- muz(X), rodic(X,Z), rodic(Z,Y).  

% (Prastryc, Pravnuk);
bratr(X,Y) :- sourozenec(X,Y), muz(X).
stryc(X,Y) :- bratr(X,Z), rodic(Z,Y).
babca(X,Y) :- zena(X), rodic(X,Z), rodic(Z,Y).
prarodic(X,Y) :- deda(X,Y) ; babca(X,Y).

prastryc(X,Y) :- bratr(X,Z), prarodic(Z,Y).
prastryc(X,Y) :- stryc(X,Z), rodic(Z,Y).
