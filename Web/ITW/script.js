// JavaScript Document
/**
 * Funkcia nastavi class prvkov sekcie "Aktuality" na closed,
 * co sa vdaka css stylesheetu prajvi nezobrazenim aktuality,
 * iba jej nazvu (display: none). V pripade zeby prehliadac 
 * nepodporoval javascript, tak budu vsetky aktuality zobrazene.
 * @param ziadny Funkcia nema ziadny parameter, pretoze v nasom
 * pripade pracujeme "natvrdo" s dvomi Aktualitami, kvoli ukazke.
 */      
function setAktualityClosed()
{ 
  var aktuality = new Array('prva', 'druha');
  
  for (i in aktuality)
  {// vyberie element podla zadaneho id
	  var element = document.getElementById(aktuality[i]);
	  // skusi ziskat jeho nastavenu hodnot z cookie, pokial
	  // nie je nastavena vracia 0
   	var cookie = getCookie(aktuality[i]);

    // pokial je v cooie closed, nastavy aktualitu na closed
	  if (cookie == 'closed')
		  element.className = 'closed';
		// analogicky pre shown
	  else if (cookie == 'shown')
  	   			element.className = 'shown';
         else
			   {// nie je zaznam v cookies
			     element.className = 'closed';
         }
  }
}

 //////////////////////////////////////////////////////////////////////////// 
 
/**
 * Funkcia podla zadaneho id (nie len aktuality) urci aktualnu triedu,
 * pokial sa nezhoduje s 'closed' tak ju zavrie a naopak, tuto zmenu 
 * zapise do cookies
 */     
function change(id)
{	// podla id zika element			
	var element = document.getElementById(id);
  // zmeni nastavenie triedy po kliknuti
	if (element.className == 'closed')
		element.className = 'shown';
  else
		element.className = 'closed';
  // zapise zmenu
	document.cookie = id + "=" + element.className;
}

//////////////////////////////////////////////////////////////////////////////
/**
 * Funkcia ziska cookies zo zonamu, pokial existuje relevantny
 * zaznam pre dany prvok (o jeho poslednom nastaveni vid. change())
 * tak vrati ziskanu hodnotu, s ktoru pracuje funkcia pra pociatocne
 * nastavenie triedy aktualit na closed (setAktualityClosed())
 */     
function getCookie(id)
{
	var value = 0;
  
  // nacita do pola cookies
	cookieArr = document.cookie.split("; ");

  // pre kazde cookie v poli
	for (i in cookieArr)
	{
	  // vybera id daneho cookie 
		first = cookieArr[i].split("=");
		// kontroluje zhodu s nasim zadanym
		if (first[0] == id)
		// v pripade zhody ulozi nastavenie (shown/closed)
			value = first[1];
	}
  // vracia ziskanu hodnotu, alebo 0
	return value;
}