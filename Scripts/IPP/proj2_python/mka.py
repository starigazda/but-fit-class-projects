#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#MKA:xstari01

#
# Project:      MKA: Minimalizace konečného automatu
# File:         mka.py
# Author:       Michal Starigazda, xstari01@stud.fit.vutbr.cz
# Date:         23.4.2012
# Description:  Projekt do predmetu IPP 2011/2012.
#               Skript pre zpracovanie a pripadnu minimalizaciu KA (DSKA).
#

# Useful libs:
import sys      # sys.exit(), sys.argv
import getopt   # spracovanie parametrov
import re       # regularne vyrazy
import io       # I/O, praca so subormi

# Makra
TRUE=1
FALSE=0
PARERR=2
FSM=5           # konecny automat je definovany ako 5-tica

###
# 
# Funkcia na tlac napovedy
#
###
def printHelp():
  'Funkcia tlaci napovedu'
  print(" Napoveda: vid. zadanie projektu ")
  sys.exit(0)

###
# 
# Funkcia ukonci skript s kodom 1 - chybne parametere, a vypise info na stderr
#
###  
def die():
  sys.stderr.write("Chybne parametry prikazoveho riadku.\nPouzite --help pre napovedu.\n")
  sys.exit(1)
                   
###
# 
# Pomocna debugovacia funkcia. Tlaci hodnoty zvolenych parametrov prikazoveho riadku
#
###              
def debugPrintParams(params):
  if params['input'] != FALSE: print ('Input file is :', params['input'])
  if params['output'] != FALSE: print ('Output file is :', params['output'])
  if params['f'] != FALSE: print ('Find non finishing : ACTIVE')
  if params['m'] != FALSE: print ('Minimize : ACTIVE')
  if params['i'] != FALSE: print ('Case insensitive : ACTIVE')

###
#
# Funkcia validuje parametry prikazoveho riadku. Pri chybe ukonci skript.
# param: asociativne pole <nazov_parametru:hodnota>
#
###  
def validateParams(params):
  if (params['i'] >= PARERR or 
      params['f'] >= PARERR or 
      params['m'] >= PARERR or 
     (params['m'] == params['f'] and params['m'] == TRUE)):
    die()
    
###
# 
# Funkcia spracovava (syntakticky/semanticky analyzuje) parametre prikazoveho riadku.
# param: pole uzivatelom zadanych parametrov
#
###
def getParams(argv):
  params = {'input':FALSE,'output':FALSE,'f':FALSE,'m':FALSE,'i':FALSE}
  try:
    opts, args = getopt.getopt(argv,"fim",["help","input=","output=","find-non-finishing","minimize","case-insensitive"])
  except getopt.GetoptError:
    die()
  for opt, arg in opts:
    if opt in ('-i',"--case-insensitive"):
      params['i']+=1
    elif opt in ('-f',"--find-non-finishing"):
      params['f']+=1
    elif opt in ('-m',"--minimize"):
      params['m']+=1
    elif opt in ("--input"):
      if params['input'] == FALSE: params['input'] = arg
      else: die()
    elif opt in ("--output"):
      if params['output'] == FALSE: params['output'] = arg
      else: die()
    elif opt in ("--help"):
      if len(argv)>1: 
        die()
      else:
        printHelp()
  
  validateParams(params)
  #debugPrintParams(params)
  return params;

###
#
# Funkcia nacita obsah suboru/stdin do retazca typu string.
# param: nazov suboru, pokial je to prazdny retazec nacitava sa zo stand. vstupu
#
###
def getInput(fileName):
  text = ""
  if fileName == FALSE:
    text = sys.stdin.read()
  else:
    try:
      fd = open(fileName, mode='r', encoding='utf8')
      try:
        text = fd.read()
      finally:
        fd.close()
    except IOError:
      sys.stderr.write("Neexistujuci zadany vstupny subor alebo chyba otvarania zadaneho vstupneho subor pre citanie\n")
      sys.exit(2);
  return text
  
###
#
# Funkcia pre tlac do suboru/stdout.
# param string:   retazec urceny pre tlac (vypis)
# param fileName: nazov suboru, pokial je to prazdny retazec tlaci sa na stdout
#
###
def printToFile(string, fileName):
  if fileName == FALSE:
    if string != '': sys.stdout.write(string)
  else:
    try:
      fd = open(fileName, mode='w', newline='\n', encoding='utf8')
      try:
        fd.write(string)
      finally:
        fd.close()
    except IOError:
      sys.stderr.write("Chyba pri otvarani zadaneho vystupneho suboru pre zapis\n")
      sys.exit(3);
   

################################################################################
##              TRIEDA REPREZENTUJUCA KONECNY AUTOMAT                          #
################################################################################
class FSM:
  'Trieda reprezentujuca konecny automat'
  
  def __init__(self, states, abcd, rules, initState, endStates):
    'Konstruktor triedy FSM, vyzaduje komponenty automatu'
    self.states = states      # mnozina stavov
    self.abcd = abcd          # vstupna abeceda
    self.rules = rules        # mnozina pravidiel
    self.init = initState     # startovaci stav
    self.ends = endStates     # mnozina koncovych stavov
    
  ##
  # Pomocna metoda, pre vypis automatu na stand. vystup
  ##
  def displayFSM(self):
    'Metoda vypise aktualny KA (jeho komponenty) na stdout'
    print("Mnozina stavov:    ", self.states )
    print("Vstupna abeceda:   ", self.abcd )
    print("Slovnik pravidiel: ", self.rules )
    print("Startujuci stav:   ", self.init )
    print("Koncove stavy:     ", self.ends )
  
  ##
  # Metoda hlada nedostupne stavy. Vyuziva variaciu na DFS algoritmus s vyuzitim CLOSE.
  # Zo startovacieho stavu sa generuju stavy (do OPEN) kam sa da dostat z tohoto stavu.
  # Pokial sa dlzka mnoziny CLOSE lisi od mnoziny stavov automatu, existuje nedostupny stav.
  # return: Funkcia vratia True, pokial sa nenajde nedostupny stav.
  ##
  def noneUnreachable(self):
    'Vracia True pokial sa nenajde nedostupny stav'
    open = [self.init,]
    closed = set()
    
    while open:
      state = open.pop()
      for i in self.rules[state].values():
        if i not in closed: 
          open.append(i)
      closed.add(state)
    
    if len(closed) == len(self.states):
      return True # nenasiel sa nedostupny stav
      
  ##
  # Metoda hlada neukoncujuce stavy. Opat vyuziva variaciu na algoritmus prehladavania 
  # stavoveho priestoru. Pre kazdy stav z mnoziny stavov, sa pomocou DFS zistuje,
  # ci sa z tohoto stavu da dostat do koncoveho stavu, tieto stavy sa pocitaju.
  # Pokial existuje prave jeden nedostupny stav tak sa ulozi (pre prepinac -f) 
  # return: vracia sa pocet neukoncujucich stavov (rozdiel velkosti mnoziny stavov a pocitadla)
  ##
  def numOfNonFinStates(self):
    'Funkcia vracia pocet neukoncujucich stavov, pokial je iba 1 tak ho ulozi'
    cnt = 0
    tmpset = set()
    tmpstate = ''
    # prechod cez stavy mnoziny stavov KA
    for i in self.states:
      tmpstate = i
      open = [i]
      closed = set()
      while open:
        state = open.pop()
        if state in self.ends:
          # podarilo sa dostat do koncoveho stavu, zo stavu i
          cnt += 1
          tmpset.add(tmpstate)
          break
        # doplnenie OPEN podla pravidiel
        for i in self.rules[state].values():
          if i not in closed: 
            open.append(i)
        closed.add(state)
    
    # pocet neukoncujucich stavov
    nonFinCnt = len(self.states) - cnt
    if nonFinCnt == 1:
      self.nonFinState = list(self.states.difference(tmpset)).pop()
    elif nonFinCnt == 0:
      self.nonFinState = ''

    return nonFinCnt
        
  ##
  # Funkcia overuje dobru specifikovanost automatu.
  # return: True pokial je automat dobre specifikovany
  ##    
  def isDSKA(self):
    'Vracia True pokial je automat dobre specifikovany'
    unreachable = 0
    nonfinishing = 0
    # kontrola uplnosti
    kstates = self.rules.keys()           # pociatocne stavy pravidiel
    if set(kstates) == self.states:
      for key in self.rules:
        # kontrola ci ma stav prechod pre kazdy symbol
        if set(self.rules[key].keys()) != self.abcd:
          return None
          
      # automat je uplny
      # kontrola nedostupnych stavov
      if self.noneUnreachable():
        # kontrola max. jedneho neukoncujuceho stavu
        if self.numOfNonFinStates() > 1: return None
        else: return True
      else:
        return None
    
    else:
      return None
    
    return True
  
  ##
  # Metoda vracia identifikator (retazec) neukoncujuceho stavu.
  # Volaniu tejto metody musi predchadzat kontrola dobrej specifikacie automtu.
  # return: Pokial neukoncujuci stav neexistuje vracia prazdny retazec
  ##
  def getNonFinishing(self):
    'vracia ulozeny neukoncujuci stav (alebo prazdny retazec)'
    return self.nonFinState
  
  ##
  # Metoda transformuje aktualny KA na minimalny KA - MKA.
  # V nekonecnom cykle sa iteruje mnozinou Qm (vid. prednasku IFJ)
  # Pokial behom iteracie dojde k stiepeniu, iteracia sa prerusi, aktualizuje sa
  # Qm, a dalej sa pokracuje v nekonecnom cykle. Ak prebehne iteracia bez toho
  # aby dosl k stiepeniu, tak sa nekonecny cyklus prerusi a aktualizuju sa komponenty
  # reprezentujuce konecny automat((Q,E,R,s,F) => (Qm,E,Rm,sm,Fm)).
  # Automat sa transformuje interne, v ramci objektu (instancie triedy FSM)
  ##  
  def minimize(self):
    'Minimalizuje konecny automat'    
    Qm = set()
    Qm.add(frozenset(self.ends))
    Qm.add(frozenset(self.states.difference(self.ends)))
    
    # nekonecny cyklus sa vykonava, pokial dochaza k stiepeniu
    while True:
      dividing = None # priznak stiepenia
      
      # iteracia mnozinou mnozin Qm
      for X in Qm:
        # pokial doslo k stiepeniu prerus iteraciu
        if dividing: break
        dividing = None
        
        # iteracia abecedou
        for c in self.abcd:
          outSet = {}   # <cielovy_stav:pociatocny_stav>
          # iteracia mnozinou X
          for state in X:
            if not self.rules[state][c] in outSet:
              outSet[self.rules[state][c]] = state
            else:
              tmplist = []
              if isinstance(outSet[self.rules[state][c]], list):
                for tmp in outSet[self.rules[state][c]]:
                  tmplist.append(tmp)
                tmplist.append(state)
                outSet[self.rules[state][c]]=tmplist
              else:
                outSet[self.rules[state][c]]=[outSet[self.rules[state][c]],state]
          isSub = None
          # kontrola ci koncove stavy pre dany znak, a pociatocne stavy z mnoziny
          # X su podmnozinou nejakej mnoziny Qm
          for tstates in Qm:
            if set(outSet.keys()).issubset(tstates):
              isSub = True
          # pokial su podmnozinou tak pokracuj v iteracii, inak nastav priznak
          # stiepenia na true a ukonci iteraciu abecedou
          if not isSub:
            dividing = True
            break;
      
      #pokial doslo k stiepeniu aktualizuj mnozinu Qm
      if dividing:
        Qm = self.divideSet(outSet, Qm)
      else: 
        break # break z whilu
        
    # Aktualizacia komponent autoamtu (transformacia na MKA)
    self.recalcComponents(Qm)
  
  ##
  # Aktualizacia komponent automatu po minimalizacii.
  # param:  setOfSets - mnozina Qm, premenovana iba konfliktu, vo funkcii sa pracuje
  #         s identifiaktorm Qm, ktory ale uz nepredstavuje mnozinu mnozin, ale
  #         iba mnozinu stavov po minimalizacii (zlucenych)
  ##
  def recalcComponents(self, setOfSets):
    'Nahradenie komponent povodneho DSKA = (Q,E,R,s,F), komponentami po minimalizacii M = (Qm,E,Rm,sm,Fm)'
    Qm = set()
    Rm = {}
    sm = ''
    Fm = set()

    # nova mnozina stavov (zlucovanie stavov)
    for X in setOfSets:
      Qm.add(self.myUnite(X))
    # aktualizacia komponenty stavov konecneho automatu
    self.states = Qm
    
    # vytvorenie novych pravidiel Rm
    for X in setOfSets:
      for c in self.abcd:
        finStates = set()
        for state in X:
          finStates.add(self.rules[state][c])
        if self.myUnite(finStates) not in Qm:
          for tmp in setOfSets:
            if finStates.issubset(tmp):
              finStates = tmp;
        if self.myUnite(X) not in Rm:
          Rm[self.myUnite(X)] = {c:self.myUnite(finStates)}
        else:
          Rm[self.myUnite(X)][c] = self.myUnite(finStates)
    
    # aktualizacia komponenty pravidiel
    self.rules = Rm
    
    # novy startovaci stav
    for X in setOfSets:
      if self.init in X:
        sm = self.myUnite(X)
        break;
    # aktualizacia komponenty startovacieho stavu
    self.init = sm    
    
    # nove koncove stavy
    for X in setOfSets:
      if X.intersection(self.ends):
        Fm.add(self.myUnite(X))
    # aktualizacia komponenty koncovych stavov
    self.ends = Fm
    
  ##
  # Metoda zlucuje stavy v mnozine do jedneho.
  # param:  mnozina stavov k spojeniu
  # return: retazec reprezentujuci novy stav (lex. zoradene stavy mnoziny)
  ##
  def myUnite(self, setOfStates):
    'Funkcia spoji stavy v mnozine do jedneho stavu, ktory reprezentuje ako string'
    return '_'.join(sorted(list(setOfStates)))
    
  ##
  # Metoda stiepenia mnoziny. Pomocna metoda minimalizacie.
  # param outSet: asociativne pole <cielovy_stav:pociatocny_stav> (vid. metodu minimize)
  # param Qm:     mnozina mnozin
  # return:       Vracia aktualizovanu mnozinu Qm  
  def divideSet(self, outSet, Qm):
    'Funkcia stiepy jednu z mnozin Qm'
    # iteracia mnozinami Qm
    for X in Qm:
      X1 = {}
      X2 = {}
      # iteracia sa ukonci pokial sa najde take X pre ktore plati: vid. prednaska IFJ
      for outstate in set(outSet.keys()):
        if outstate in X: X1[outstate]=outSet[outstate]
        else: X2[outstate]=outSet[outstate]
      if len(set(X1.keys())) == 0:
        pass
      else:
        if len(X.intersection(set(X2.keys()))) == 0 and len(set(X2.keys())) > 0:
          # odstranenie mnoziny ktora viedla k stiepeniu
          Qm.remove(frozenset(self.getSetFromValues(outSet.values())))
          # jej nahradenie mnozinami (pociatocnymi stavmi) X1 a X2
          Qm.add(frozenset(self.getSetFromValues(X1.values())))
          Qm.add(frozenset(self.getSetFromValues(X2.values())))
          #print("Qm po stiepeni: ", Qm)  
          return Qm
  
  ##
  # Metoda vracia mnozinu, ktoru vytvara z atomv obecneho zonamu
  # param: obecny zoznam
  ##
  def getSetFromValues(self, values):
    'vytvori mnozinu z atomov obecneho zoznamu'
    myset = set()
    for item in values:
      if isinstance(item, list):
        for i in item: myset.add(i)
      else: myset.add(item)
    return myset
  
  ##
  # Metoda transformuje komponenty na retazec, ktory odpoveda normalnej forme vystupu
  # definovanej v zadani projektu.
  # reutrn: retazec reprezentujuci automat a odpovedajuci normalnej forme vystupu
  ##        
  def getNormalizedFormat(self):
    'Vracia string reprezentujuci KA normalizovany podla zadania'
    format = "(\n{"
    
    # pripoji normalne naformatovanu komponentu stavov
    tmplist = sorted(list(self.states))
    for i in range(len(tmplist)):
      format += tmplist[i]
      if i != len(tmplist)-1:
        format += ", "
    format += "},\n{"
    
    # pripoji normalne naformatovanu komponentu vstupnej abecedy
    tmplist = sorted(list(self.abcd))
    for i in range(len(tmplist)):
      format += "\'"+tmplist[i]+"\'"
      if i != len(tmplist)-1:
        format += ", "
    format += "},\n{\n"
    
    # pripoji spravne naformatovanu komponentu pravidiel
    sortedStarts = sorted(self.rules.keys())
    for i in range(len(sortedStarts)):
      sortedSymbols = sorted(self.rules[sortedStarts[i]].keys())
      for j in range(len(sortedSymbols)):
        format += sortedStarts[i] + " \'"+sortedSymbols[j]+"\'" + " -> "
        format += self.rules[sortedStarts[i]][sortedSymbols[j]]
        if i != len(sortedStarts)-1 or j != len(sortedSymbols)-1:
          format += ",\n"
        else:
          format += "\n"
    format += "},\n"
    
    # pripoji spravne naformatovanu komponentu startovacieho stavu
    format += self.init+",\n{"
    
    # pripoji spravne naformatovanu komponentu koncovych stavov
    tmplist = sorted(list(self.ends))
    for i in range(len(tmplist)):
      format += tmplist[i]
      if i != len(tmplist)-1:
        format += ", "
    format += "}\n"
    
    format += ")" 
    
    return format
    
################################################################################
##          KONIEC TRIEDY REPREZENTUJUCEJ KONECNY AUTOMAT                     ##
################################################################################

###
# 
# Funkcia validuje identfikator stavu (podla zadania - odpoveda identifikatoru
# jazyka C, nezacina/nekonci '_'). Pri chybe ukonci skript s lexikalnou chybou.
# param: identifikator
#
###
def validateState(state):
  if state[0] != '_' and state[-1] != '_':
    for char in state:
      if char.isalnum() or char == '_' : continue
      else: 
        sys.stderr.write("LEX: chybny identifikator stavu\n"); sys.exit(60)
  else:     
    sys.stderr.write("LEX: chybny identifikator stavu\n"); sys.exit(60)  

###
#
# Funkcia s vyuzitim konecneho automatu spracovava a syntakticky/lex. analyzuje
# retazec predstavujuci obsah kompnenty KA, konkretne vstupnej abecedy.
# param:  obsah komponenty - retazec
# return: vstupnu abecedu - zoznam znakov vstupnej abecedy
#
###
def getElements(string):
  list = []
  state = 'init'
  for c in string:
    if state == 'init':
      if c.isspace(): continue;
      elif c == '\'': state = 'apos1'
      else: list.append(c); state = 'comma'
    elif state == 'comma':
      if c.isspace(): continue
      elif c == ',': state = 'init'
      else: 
        sys.stderr.write("Lex./Syntak. chybna kompnenta: vstupna abeceda\n")
        sys.exit(60)
    elif state == 'apos1':
      if c != '\'': list.append(c); state = 'apos3'
      elif c == '\'': state = 'apos2'
    elif state == 'apos2':
      if c == '\'': list.append('\''); state = 'apos3' 
      else: 
        sys.stderr.write("Lex./Syntak. chybna kompnenta: vstupna abeceda\n")
        sys.exit(60)
    elif state == 'apos3':
      if c == '\'': state = 'comma'
      else: 
        sys.stderr.write("Lex./Syntak. chybna kompnenta: vstupna abeceda\n")
        sys.exit(60)  
  if state != 'comma':
    sys.stderr.write("Lex./Syntak. chybna kompnenta: vstupna abeceda\n")
    sys.exit(60)
  return set(list)

###
#
# Funkcia s vyuzitim konecneho autoamtu spracovava a syntakticky/lexikalne analyzuje
# spravnost retazca, ktory reprezentuje obsah komponenty reprezentujucej pravidla 
# konecneho automatu, zaroven automaticky kontroluj dobru specifikovanost automatu
# (determinizmus). Pri chybe sa skript ukonci s navratovym kodom odpovedajucim
# odhalenej chybe.
# param:  obsah komponenty (pravidla) - retazec
# return: funkcia vracia asociativne pole <pociatocny_stav:ARC>, kde ARC je dalsie
#         asociativne pole <symbol_vstupnej_abecedy:cielovy_stav>
#
###
def getRules(string):
  rulesDict = {}
  state = 'init'

  # pre prazdny retazec vracia prazdnu mnozinu pravidiel (prazdne asoc. pole)
  if string == '': return rulesDict
  
  for c in string:
    if state == 'init':
      start = ''      
      symbol = ''
      end = ''
      tmpDict = {}
      if c.isspace(): continue
      else: start += c; state = 'id'
    elif state == 'id':
      if c.isspace(): state = 'presymbol'
      elif c == '\'': state = 'strsymbol'
      elif c == '-': state = 'arc'
      else: start += c
    elif state == 'presymbol':
      if c.isspace(): continue
      elif c == '\'': state = 'strsymbol'
      elif c == '-': state = 'arc'
      else: symbol = c; state = 'prearc'
    elif state == 'strsymbol':
      symbol = c; state = 'apos'
    elif state == 'apos':
      if c == '\'': state = 'prearc'
      else: 
        sys.stderr.write("Lex./Syntak. chybna kompnenta: pravidla, chybajuci apostrof\n")
        sys.exit(60)
    elif state == 'prearc':
      if c.isspace(): continue
      elif c == '-': state = 'arc'
      else:
        sys.stderr.write("Lex./Syntak. chybna kompnenta: pravidla, ocakavala sa '->'\n")
        sys.exit(60)
    elif state == 'arc':
      if c == '>': state = 'endinit'
      else: 
        sys.stderr.write("Lex./Syntak. chybna kompnenta: pravidla, chybna '->'\n")
        sys.exit(60)
    elif state == 'endinit':
      if c.isspace(): continue
      else: end += c; state = 'end'
    elif state == 'end':
      if c.isspace(): 
        state = 'comma'
        if not start in rulesDict:
          tmpDict[symbol] = end
          rulesDict[start] = tmpDict
        else:
          if (symbol in rulesDict[start]):
            if rulesDict[start][symbol] != end:
              sys.stderr.write("Nedeterminizmus v pravidlach - chybny DSKA\n")
              sys.exit(62)
          else:
            rulesDict[start][symbol] = end
      elif c == ',': 
        state = 'init'
        if not start in rulesDict:
          tmpDict[symbol] = end 
          rulesDict[start] = tmpDict
        else:
          if (symbol in rulesDict[start]):
            if rulesDict[start][symbol] != end:
              sys.stderr.write("Nedeterminizmus v pravidlach - chybny DSKA\n")
              sys.exit(62)
          else:
            rulesDict[start][symbol] = end
      else: end += c
    elif state == 'comma':
      if c.isspace(): continue
      elif c == ',': state = 'init'
  
  # po ukonceni iteracie cez retazec - dokoncenie analyzy
  if state != 'comma' and state != 'end':
        sys.stderr.write("Lex./Syntak. chybna kompnenta: pravidla\n")
        sys.exit(60)
  if state == 'end':
    if not start in rulesDict:
      tmpDict[symbol] = end 
      rulesDict[start] = tmpDict
    else:
      if (symbol in rulesDict[start]):
        if rulesDict[start][symbol] != end:
           sys.stderr.write("Nedeterminizmus v pravidlach - chybny DSKA\n")
           sys.exit(62)
      else:
        rulesDict[start][symbol] = end  

  return rulesDict     

###
#
# Semanticka kontrola pravidiel, a zaroven aj kontrola dobrej specifikovanosti automatu,
# konkretne nepritomnost epsilon pravidiel. Pri chybe sa skript ukonci s odpovedajucim
# chybovym kodom
# param rules:  Mnozina pravidiel (asociativne pole reprezentujuce pravidla)
# param states: Mnozina stavov automatu
# param abcd:   Vstupn abeceda automatu
#
###
def validateRules(rules, states, abcd):
  "Semanticka kontrola pravidiel, parametrom je slovnik pravidiel"
  for key in rules:
    if key not in states:
      sys.stderr.write("Semanticka chyba, poc. stav v pravidle nie je stavom\n")
      sys.exit(61)     
    for kkey in rules[key]:
      if kkey not in abcd and kkey != '':
        sys.stderr.write("Semanticka chyba, cteny vstupny symbol, nepatri do vstupnej abecedy\n")
        sys.exit(61) 
      elif kkey == '':
        sys.stderr.write("Epsilon prechod, chybny DSKA.\n")
        sys.exit(62) 
      if rules[key][kkey] not in states:
        sys.stderr.write("Semanticka chyba, cil. stav v pravidle nie je stavom\n")
        sys.exit(61)

###
# Funkcia spracovava a analyzuje komponenty automatu ziskane zo vstupu.
# Sucastou su aj semanticke kontroly.
# param: asociativne pole <komponenta:retazec_reprezentujuci_obsah_komponenty>
# return: vracia objekt reprezentujuci konecny autmoat (instancia triedy FSM)
###
def getFSMstruct(fsmDict):
  # parsovanie a validacia stavov
  states = re.sub(r'(\A\{)|(\}\Z)', "", fsmDict.group('stavy'))
  if states.strip() == '':
    # prazdna mnozina stavov vedie na seman. chybu
    sys.stderr.write("Prazdna mnozina stavov\n"); sys.exit(61)
  statesList = states.split(",")
  for i in range(len(statesList)): 
    statesList[i] = statesList[i].strip(); 
    validateState(statesList[i])
  # Mnozina stavov
  states = set(statesList)
  
  # parsovanie symbolov abecedy
  abcd = re.sub(r'(\A\{)|(\}\Z)', "", fsmDict.group('abeceda'))
  if abcd.strip() == '':
    # prazdna vstupna abeceda vedie na seman. chybu
    sys.stderr.write("Prazdna vstupna abeceda\n"); sys.exit(61)
  # Mnozina vstupnych symbolov
  abcd = getElements(abcd)
  
  # overenie nedisjunktnosti prvej a druhej komponenty
  if not states.isdisjoint(abcd):
    sys.stderr.write("Nedisjunktne komponenty (prva a druha)\n"); 
    sys.exit(61)

  # startovaci stav
  initState = fsmDict.group('start')
  if initState not in states:
    sys.stderr.write("Startovaci stav neni v mnozine stavov\n");
    sys.exit(61)

  # parsovanie pravidiel
  rulesStr = re.sub(r'(\A\{)|(\}\Z)', "", fsmDict.group('pravidla'))
  rulesStr = rulesStr.strip()
  rules = getRules(rulesStr)
  # validacia pravidiel (semanticka kontrola)
  validateRules(rules, states, abcd)

  # parsovanie a validacia koncovych stavov
  endStates = re.sub(r'(\A\{)|(\}\Z)', "", fsmDict.group('final'))
  if not endStates.strip() == '':
    endStatesList = endStates.split(",")
    for i in range(len(endStatesList)): 
      endStatesList[i] = endStatesList[i].strip(); 
      validateState(endStatesList[i])
      endStates = set(endStatesList)
  else: endStates = set(endStates)
  # overenie ci je mnozina koncovych stavov podmnozinou stavou
  if not endStates.issubset(states):
    sys.stderr.write("Koncovy stav neni prvkom prvej komponenty (stavy).\n"); 
    sys.exit(61)
  
  # Vracia obejkt reprezentujuci KA
  return FSM(states, abcd, rules, initState, endStates)
  
###
#
# Hlavna funkcia syntaktickej analyzy vstupneho suboru.
# param:  retazec reprezentujuci vstupny subor
# return: Objekt triedy FSM reprezentujuci KA
#         Pri chybnom formate vsupneho automatu konci skript a odpovedajucou chybou
###
def parseFSM(fsmString):
  # odstranenie komentarov
  fsmString = re.sub(r'\'#\'', "&TMP", fsmString)
  fsmString = re.sub(r'#.*\n', "", fsmString)
  fsmString = re.sub(r'#.*$', "", fsmString)
  fsmString = re.sub(r'&TMP', "\'#\'", fsmString)
  # rozdelenie mnozin (komponent)
  matchObj = re.search(r'\A\s*\(\s*(?P<stavy>\{.*\})\s*,\s*(?P<abeceda>\{.*\})\s*,\s*(?P<pravidla>\{.*\})\s*,\s*(?P<start>[a-zA-Z]|[a-zA-Z]\w*[a-zA-Z\d])\s*,\s*(?P<final>\{.*\})\s*\)\s*\Z', fsmString, re.S)
  if matchObj:
    fsm = getFSMstruct(matchObj)
  else:
    # nesplnuje lexikalne pravidla ({},{},{},id,{})
    sys.stderr.write("Chybny format vstupneho automatu\n"); sys.exit(60)
  
  return fsm;

###
#
# Hlavna funkcia skriptu - main()
# param: zoznam parametrov prikazoveho riadku
#
###
def main(argv):
  params = getParams(argv)                # nacitaj parametre
  inputFSM = getInput(params['input'])    # nacitaj vstupne data
  
  if params['i'] == TRUE: 
    inputFSM = inputFSM.lower()           # to lower pre case-insensitive mod

  fsm = parseFSM(inputFSM)                # parsing zadaneho konecneho automatu
  #fsm.displayFSM()                        # debug
  
  # validacia nacitanheo dobre specifikovaneho automatu
  if fsm.isDSKA():
    if params['f'] == TRUE:
        # vutlac neukoncujuci stav
        printToFile(fsm.getNonFinishing(), params['output'])
        sys.exit(0)
    elif params['m'] == TRUE:
        # minimalizuj konecny automat
        fsm.minimize()
        #fsm.displayFSM()
    # tlac automatu (normalna forma vystupu)
    printToFile(fsm.getNormalizedFormat(), params['output'])
    #fsm.displayFSM()
  else:
    sys.stderr.write("Nacitany automat nie je dobre specifikovany\n")
    sys.exit(62)
  
################################################################################
######################       Start skriptu     #################################
################################################################################ 
 
if __name__ == "__main__":
   main(sys.argv[1:])

################################################################################
#####################        Koniec skriptu    #################################
################################################################################