#!/usr/bin/perl

#JSN:xstari01
#
# Project:      JSN: JSON2XML
# File:         proj1.pl
# Author:       Michal Starigazda, xstari01@stud.fit.vutbr.cz
# Date:         24.3.2012
# Description:  Projekt do predmetu IPP 2011/2012.
#               Skript pro konverzi JSON formatu (viz RFC 4627) do XML.
#

# Useful libs:
use JSON::XS;
use XML::Writer;
use IO::File;
use Data::Dumper;
use Getopt::Long;

use strict;

# Komfiguracia kniznic:
Getopt::Long::Configure ('posix_default','no_auto_abbrev','bundling','no_ignore_case',);

# Chybove kody
use constant {
  PARAMS_ERR            => 1,
  OPEN_IN_ERR           => 2,
  OPEN_OUT_ERR          => 3,
  FORMAT_ERR            => 4,
  EXP_INVALID_EL_NAME   => 50,
  IMP_INVALID_EL_NAME   => 51,
  CLOSE_ERR             => 100,
  INTERNAL_ERROR        => 101
};

# Ukoncenie programu pri chybne zadanych parametroch
sub mydie() {
  $! = PARAMS_ERR;
  die ("Chybne parametry prikazoveho riadku.\n",
       "Pouzite --help pre napovedu.\n");
}

# Tlac napovedy
sub printHelp(){
  print "Napoveda: vid. zadanie projektu\n";
  exit 0;
}

# Validacia zadanych parametrov, kontrola kompatibility, duplicity...
sub validateParams{
  my ($hash, $argv) = @_;
  my %params = %$hash;
 
  if ($params{input}      &&  @{$params{input}}>1)      { mydie; }
  if ($params{output}     &&  @{$params{output}}>1)     { mydie; }
  if ($params{n}          &&  $params{n}>1)             { mydie; }
  if ($params{r}          &&  @{$params{r}}>1)          { mydie; }
  elsif ($params{r}){ @{$params{r}}[0] =~ s/^=(.*)$/$1/;
         utf8::decode(@{$params{r}}[0]);}
  if ($params{array_name} &&  @{$params{array_name}}>1) { mydie; }
  elsif ($params{array_name}){utf8::decode(@{$params{array_name}}[0]);}
  if ($params{item_name}  &&  @{$params{item_name}}>1)  { mydie; }
  elsif ($params{item_name}){utf8::decode(@{$params{item_name}}[0]);}
  if ($params{s}          &&  $params{s}>1)             { mydie; }
  if ($params{i}          &&  $params{i}>1)             { mydie; }
  if ($params{l}          &&  $params{l}>1)             { mydie; }
  if ($params{c}          &&  $params{c}>1)             { mydie; }
  if ($params{array_size} &&  $params{array_size}>1)    { mydie; }
  if ($params{index_items}&&  $params{index_times}>1)   { mydie; }
  if ($params{start}      &&  @{$params{start}}>1)      { mydie; }
  if ($params{start}      &&  !$params{index_items})    { mydie; }    
  if ($params{padding}    &&  !$params{padding})        { mydie; }   
  # Handling Help
  if ($params{help} &&  ($params{help}>1 || $argv != 1)){ mydie; }
  if ($params{help} &&  $argv == 1){ printHelp; }
}

# Spracovanie parametrov prikazoveho riadku
sub getParams(){
  my %params;
  my $argv = scalar @ARGV;
    GetOptions( 'input=s@'        => \$params{input},
                'output=s@'       => \$params{output},
                'n+'              => \$params{n},
                'r=s@'            => \$params{r},
                'array-name=s@'   => \$params{array_name},
                'item-name=s@'    => \$params{item_name},
                's+'              => \$params{s},
                'i+'              => \$params{i},
                'l+'              => \$params{l},
                'c+'              => \$params{c},
                'a|array-size+'   => \$params{array_size},
                't|index-items+'  => \$params{index_items},
                'start=i@'        => \$params{start},
                'help+'           => \$params{help},
                'padding+'        => \$params{padding}) or mydie;
  # validacia parametrov
  validateParams(\%params, $argv); 
  return %params;
} 

###############################################################################
#
## Hlavne telo skriptu.
#
###############################################################################

# Spracovanie parametrov - ulozne v hashi.
my %params = getParams;

# Validacia pripadnych uzivatelom zadanych XML znaciek.
valUsrStr(\%params);

# Inicializacia premennych na defaultne hodnoty. Mozu byt pozmenene podla 
# uzivatelom nadefinovanych parametrov.
my $input = $params{input}[0];
my $output = $params{output}[0];
my $array_el_name = ($params{array_name}) ? @{$params{array_name}}[0] : "array";
my $item_el_name = ($params{item_name}) ? @{$params{item_name}}[0] : "item";
my $init_index = ($params{start}) ? @{$params{start}}[0] : 1;
my $c = $params{c};   # priznak aktivity prepinaca -c

#pomocna premenna rozirenia JPD
my $size;
#my $index = $init_index - 1;

# Premenne pre nacitanie JSON formatu zo zdrojoveho suboru.
my $json_text;
my @text;

# Procedura oznacenia literalov true, false, null v texte JSONu, bez narusenia
# validity formatu.
sub encodeLiterals{
  $json_text =~ s/((\"\s*:|[el\]0-9\"]\s*,|\[)\s*)(true)(\s*([}\]]\s*,?|,\s*[\[\"{0-9]?))/$1\"!_,--!!--,_!$3!_,--!!--,_!\"$4/g;
  $json_text =~ s/((\"\s*:|[el\]0-9\"]\s*,|\[)\s*)(false)(\s*([}\]]\s*,?|,\s*[\[\"{0-9]?))/$1\"!_,--!!--,_!$3!_,--!!--,_!\"$4/g;
  $json_text =~ s/((\"\s*:|[el\]0-9\"]\s*,|\[)\s*)(null)(\s*([}\]]\s*,?|,\s*[\[\"{0-9]?))/$1\"!_,--!!--,_!$3!_,--!!--,_!\"$4/g;
}

# Otvorenie zdrojoveho suboru, pokial nie je zadany, vstup zo STDIN
if ($input) { 
  open(input, "<$input") 
    or { $! = OPEN_IN_ERR and die "Nepodarilo sa otvorit zdrojovy subor!\n" };
  # nacitanie JSON formatu
  while (<input>) {  push(@text,$_); }
  $json_text = join('',@text);
}
else {
  while (<STDIN>) {  push(@text,$_); }
  $json_text = join('',@text);
}

# Oznacenie literalov v jsone-texte.
encodeLiterals;

#Parsovanie/decodovanie JSON formatu
my $json;
eval { $json = JSON::XS->new->utf8->decode ($json_text); };
if ($@) { $! = FORMAT_ERR and die "[[JSON ERROR]]:\n\t $@\n"; }

#print ref($json)," => [", Dumper($json)," ]\n"; # DEBUG line

# Vytvorenie vystupneho XML suboru a XML writeru
my $flag = ($params{r} && $c) ? 0 : 1;      # priznak pouzitia korenoveho elementu
my $doc;
my $writer;
if ($output) {
  $doc = new IO::File(">$output") or {$!= OPEN_OUT_ERR and die "Cannot open file for output\n"};
  # Konfiguracia popisovaca XML
  $writer = new XML::Writer(OUTPUT => $doc, DATA_INDENT => 2, DATA_MODE => 1,
                            UNSAFE => $flag, ENCODING => 'utf-8');
}
else {
  # Pokial nebola zadany output tak vystup smeruje na STDOUT
  $writer = new XML::Writer(DATA_INDENT => 2, DATA_MODE => 1,UNSAFE => $flag,
                            ENCODING => 'utf-8');
}

# XML hlavicka, negeneruj pokial je zadany parameter '-n'
if (!$params{n}){ $writer->xmlDecl("UTF-8"); }

# korenovy element, neobaluj nim XML vystup pokial nie je zadany parameter 'r'
if ($params{r}){ $writer->startTag( "@{$params{r}}[0]"); }

# transformacia (rekurzivna) JSON2XML
createXML($json, $writer);

# korenovy element - koniec
if ($params{r}){
  $writer->endTag();  
  eval {$writer->end();}; if ($@){$! = INTERNAL_ERROR and die "Neocakvana chyba\n"};   
}

# Zatvorenie vstupnehoo suboru (pokial bol otvoreny)
if ($input) {
 close( input ) 
  or { $! = CLOSE_ERR and die "Cannot close $input\n" };
}
# Zatvorenie vystupneho suboru
if ($output) { 
  $doc->close() or { $! = CLOSE_ERR and die "Cannot close $input\n" };
}

exit 0;

###############################################################################
#
## Pomocne funkcie pre spracovanie chyb a validaciu.
#
###############################################################################

# Uzatvorenie suborov pri vyskyte chyby.
sub closeFiles{
  # Zatvorenie vstupnehoo suboru (pokial bol otvoreny)
  if ($input) { close( input ) or die "Cannot close $input\n"; }
  # Zatvorenie vystupneho suboru
  if ($output) { $doc->close() or die "Cannot close $input\n"; }
}

# Validacia uzivatelom definovanych naz#vov elementov (pokial nejake su), 
# parametrom je zadany nazov elementu.
sub valUsrStr{
  my ($hash) = @_;
  my %params = %$hash;
  
  if ($params{r}){ validate(@{$params{r}}[0], EXP_INVALID_EL_NAME); }
  if ($params{array_name}){ validate(@{$params{array_name}}[0], EXP_INVALID_EL_NAME); }
  if ($params{item_name}){ validate(@{$params{item_name}}[0], EXP_INVALID_EL_NAME); }
}

# Samotna validacia nazvu elementu, parametrom je nazov elementu a navratovy
# chybovy kod, ktorym bude skript ukonceny pri chybnom nazve.
sub validate{
  my @name = split('',$_[0]); 
  my $ecode = $_[1];
  #check start char
  if ( $name[0] !~ m/([:_a-zA-Z])/ && ord("$name[0]") < 128 ) { 
    if ($ecode == IMP_INVALID_EL_NAME) { closeFiles; }
    $! = $ecode;  
    die "Nazev elementu veduci na nevalidnu XML znacku\n"; 
  }
  # zvysok nazvu elementu
  foreach (@name){
    if ( $_ !~ m/[:_a-zA-Z0-9.-]/ && ord("$_") < 128 ) { 
      if ($ecode == IMP_INVALID_EL_NAME) { closeFiles; }
      $! = $ecode; 
      die "Nazev elementu veduci na nevalidnu XML znacku\n"; 
    }
  }
}

# Nahradenie invalidnych znakov v nazvoch elemtoch (z JSONu) pomlckami.
# Parametrom je opat nazov, funkcia vracia modifikovany nazov.
sub substitut{
  my @name = split('',$_[0]);
  my @new;
  # prechod retazcom (polom)
  foreach (@name){
    if (ord("$_") < 128 ) {
      $_ =~ s/[^:_a-zA-Z0-9.-]/-/;
    } 
    push(@new, $_);
  }
  return join('',@new);
}

###############################################################################
#
## Funkcie pre samotnu transformaiu (nepriama rekurzia) a pomocne funkcie.
#
###############################################################################

# Inicializacna funkcia vytvarania XML formatu, podla typu dat v strukture
# (na aktualnej urovni) vola handler daneho typu
sub createXML{
  if (ref($_[0]) eq "HASH") { handleHash(@_);}
  elsif (ref($_[0]) eq "ARRAY") {handleArray(@_);}
  elsif (!ref($_[0])) {handleScalar(@_);}
}

# Pomocna funkcia, vracia dekodovany literal true, false, alebo null
sub literal{
  my $str = $_[0];
  $str =~ s/!_,--!!--,_!((true|false|null))!_,--!!--,_!/$1/;
  return $str;
}

# Funkcia vraciu true pokial je zadany parameter zakodovanym literalom.
sub isLiteral{
  if ( $_[0] =~ /!_,--!!--,_!true!_,--!!--,_!/  ||
       $_[0] =~ /!_,--!!--,_!false!_,--!!--,_!/ ||
       $_[0] =~ /!_,--!!--,_!null!_,--!!--,_!/ )
  { return 1; }
  else { return 0;}
}

# Funkcia ouvodzovkuje zadanu hodnotu (transformacia na retazec), vracia upravenu
# hodnotu.
sub quoteValue{
  my $value = $_[0];
  my $tmp = value($value); 
  my @arr = split('',"$tmp");
  unshift(@arr, '"');
  push(@arr, '"');
  return join('',@arr);
}

# Handlar formatovania vystupu hashu => JSON objektov
sub handleHash{
  my %hash = %{$_[0]};
  my $writer = $_[1];
  while (my($key_b, $value) = each %hash)
  {
    # uprava nevalidnych znakov v nazve elementu na pomlcky
    my $key = substitut($key_b);
    # validacie noveho nazvu
    validate($key, IMP_INVALID_EL_NAME);
    
    if (!ref($value)){
      # hodnota je scalar, spracavonie podla zadanych parametrov
      if ($params{s} && !isNumber($value) && !isLiteral($value)){
        goto LabHasHand;
      }    
      elsif ($params{i} && isNumber($value)){
        goto LabHasHand;;
      }
      elsif ($params{l} && isLiteral($value)){
        goto LabHasHand;
      }
      else { 
        if ($c) { $writer->emptyTag( "$key", value => value($value)); }
        else {
          $value = quoteValue($value); 
          $writer->raw("<$key value=$value/>"); 
        }
      }
      next;
    }

LabHasHand:
    # Hodnoa nebola skalar, alebo je sklar urceny k spracovaniu mi mo atribut
    # elementu
    $writer->startTag( "$key");
    createXML($value, $writer); #rekurzia
    $writer->endTag( "$key");
    
  }
}

# Funkcia updatuje index pri indexovani prvkov pola, podpora rozsirenia JPD 
# (doplnenie najmensie mozneho poctu 0 zlava do indexu)
sub updateIndex{
  my $index = $_[0];
  $index++;
  if ($params{padding}){
    # max. dlzka indexu
    my $tmp = $size + $init_index - 1; "$tmp";
    my @max_len = (split('',"$tmp"));
    # aktualna dlzka indexu
    $tmp = $index;
    my @act_len = (split('',"$tmp"));
    # doplnenie nul
    @act_len = reverse(@act_len);
    while (scalar @act_len < scalar @max_len){ push(@act_len, 0);}
    @act_len = reverse(@act_len);
    # update indexu
    $index = join('',@act_len);
  }
  return $index;
}

# Handler formatovania vystupu array => pole
sub handleArray{
  my @array = @{$_[0]};
  my $writer = $_[1];
  # Incializacia hodnot (velkost pola, pociatocny index)
  $size = scalar @array;
  my $index = $init_index - 1;
  $index = updateIndex($index);
  
  # Element array bude obsahovat atribut s velkostou pola
  if ($params{array_size})
  { $writer->startTag( "$array_el_name", size => "$size"); }
  else
  { $writer->startTag( "$array_el_name"); }
  
  # Formatovanie jednotlivych prvkov pola
  foreach my $item (@array){
    # 0 - neindexuj prvky, 1 - indexuj prvky
    if (!$params{index_items}){printItem($item, 0, 0);}
    else {printItem($item, 1, $index);
    $index = updateIndex($index);;}
  }
  $writer->endTag( "$array_el_name");
}

# Pomocna funkcia ocakava hodnotu (string, number, zakodovany literal).
# Rozhdno o jaky typ sa jedna a vrati odpovedajucu hodnotu (number - uz zaokruhleny,
# string bez zmeny, literal - dekodovany)
sub value {
  my $value = $_[0];
  if (isNumber($value)){ return myRound($value); }
  else{ 
  return (isLiteral($value))?literal($value):"$value"; }
}

# Funkcia formatuje na vystup jednotlive prvky elementu typu pole.
# Vstupnym parametrom je konkretny prvok, priznak indexacie prvkov a aktualny
# index urceny prvku v ramci pola.
sub printItem{
  my $item = $_[0];
  my $index_flag = $_[1];
  my $index = $_[2];
  
  # Kontrola zda je prvok skalar
  if (!ref($item)){
        if (isLiteral($item) && $params{l}){ goto LabPrIt;}
        if (!$index_flag){
          # indexacia off 
          if ($c) { $writer->emptyTag( "$item_el_name", value => value($item)); }
          else {
            $item = quoteValue($item); 
            $writer->raw("<$item_el_name value=$item/>"); 
          }
        }
        else { 
          # indexacia on
          if ($c) 
            { $writer->emptyTag( "$item_el_name",index => "$index", value => value($item));}
          else {
            $item = quoteValue($item);
            my $tmp = quoteValue($index); 
            $writer->raw("<$item_el_name index=$tmp value=$item/>"); 
          }
        }
        return;   
    }

LabPrIt:
    # Prvok neni skalar
    if (!$index_flag) {$writer->startTag( "$item_el_name");}
    else {
       $writer->startTag( "$item_el_name", index => "$index");
       updateIndex;
    }
    createXML($item, $writer);    # rekurzia
    $writer->endTag( "$item_el_name");  
}

# Handler formatovania typu scalar => string, number, literal (true, false, null).
# Neformatuje hodnoty ako atributy elementov ale ako textove polia.
sub handleScalar{
  my $value = $_[0];
  my $writer = $_[1];
  if (isNumber($_[0])) {$writer->characters(myRound($_[0]));}
  elsif (isLiteral($_[0])) {$writer->emptyTag(literal($value));}
  else {
    if ($c) {$writer->characters($value);}
    else {$writer->raw("$value");}
  } 
}

# Funkcia zaokruhluje desatinne cisla na cele.
sub myRound{
  my $value = $_[0];
  my $tmp = int(abs($value)+0.5);  
  return ($value >= 0) ? (0 + $tmp) : (0 - $tmp);
}

# Funkcia ako parameter dostava scalar, a vracia true pokial je to typ number.
sub isNumber{
  my $tmp = encode_json [$_[0]];
  return ($tmp !~ m/\[\".*/);
}