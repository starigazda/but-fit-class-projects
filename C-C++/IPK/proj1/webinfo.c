/**
 * @file: webinfo
 * @date: 20.2.2012
 * @author: Michal Starigazda, xstari01@stud.fit.vutbr.cz
 */
 
 #include <stdio.h>
 #include <stdlib.h>
 #include <stdbool.h>
 #include <string.h>
 #include <strings.h>
 #include <sys/types.h>
 #include <sys/socket.h>
 #include <netinet/in.h>
 #include <netinet/tcp.h>
 #include <netdb.h>
 #include <unistd.h>
 #include <ctype.h>
 #include <locale.h>
 #include <errno.h>
 
 // ecodes
 #define MALL_ERR		-1	// allocation error
 #define EOK			0	// OK
 #define PROTOCOL_ERR	1	// unsuported protocol
 #define CR_SOCKET_ERR	2	// could not create socket
 #define URL_ERR 		3	// malformed URL
 #define SC4XX_ERR 		4	// status code 4xx in response
 #define SC5XX_ERR 		5	// status code 5xx in response
 #define GETHOST_ERR 	6	// could not resolve host
 #define PARAMS_ERR		7	// wrong options see -h for help
 #define CONN_ERR		8	// connection problems
 #define COM_ERR		9	// communication problems
 #define REDIR_ERR		10	// redirect counter overflow
 #define UNKNOWN_ERR	11	// unexpected err occur
 
 // default values
 #define BUFFER_SIZE 	512
 #define MSG_CONST		3
 #define PORT 			80
 #define OPTS			4	// -l -t -s -m
 #define REDIR_CNT		5	// redirect counter
 
 //debug
 //#define DEBUG
 
 /** Structure for CL's options. */
  typedef struct params{
	bool l;			// lenght					-l
	bool s;			// server identification 	-s
	bool m;			// modifications			-m
	bool t;			// type						-t
	char optOrder[OPTS]; 
 }TParams;
 
 /** Structure representing URL */
 typedef struct url{
	 char host[BUFFER_SIZE];
	 char path[BUFFER_SIZE];
	 int port;
}TUrl;

 /** Buffers */
 typedef struct buffs{
	 char *length;
	 char *server;
	 char *modif;
	 char *type;
}TBuffs;

 /** Print help. */
  void printHelp()
 {
	 printf("Synopsis: webinfo [-l] [-s] [-m] [-t] URL\n"
			"------------------------------------------\n"
			"Description of exit codes:\n"
			"\t -1 \t Allocation problem.\n"
			"\t 1 \t Unsuported protocol.\n"
			"\t 2 \t Couldn't create socket.\n"
			"\t 3 \t Malformed URL\n"
			"\t 4 \t status code 4xx in response\n"
			"\t 5 \t status code 5xx in response\n"
			"\t 6 \t Couldn't resolve hsot\n"
			"\t 7 \t Wrong CL's options.\n"
			"\t 8 \t Connection problems.\n"
			"\t 9 \t Communication problems.\n" 
			"\t 10 \t Redirect problem.\n" 
			"\t 11 \t Unenxpcted/unknown error.\n" );
 }
  
 /**
  * Print string to stdout without CR (LF only).
  * @param msg String.
  */
 void myprint(char *msg){
	for (int i = 0; i < strlen(msg); i++){
		if (msg[i] != '\r') fputc(msg[i], stdout);
	}
 }
 
 /**
  * Clear structure TUrl.
  */ 
 void clearUrl(TUrl *url){
	bzero(&(url->host), BUFFER_SIZE);
	bzero(&(url->path), BUFFER_SIZE);
	url->port = 0;
 }
 
 /**
  * URL parser.
  * Parsing protocol, host, optional port, path.
  * @param s String containing URL.
  * @param url Structure for url's elements.
  * @return Ecode.
  */
 int getUrl(char *s, TUrl *url){
	 int ecode = EOK;
	 int i = 0;
	 char tmp[BUFFER_SIZE];
	 bzero(&tmp, sizeof(tmp));
	
	 // expected minimal url's length
	 if (strlen(s) > 10){
		 //first 7 chars - expected"http://0"
		 for (; i<7; i++) tmp[i]=s[i];
		 if (strcmp(tmp,"http://")==0){
			 
			//initializing structure 
			bzero(&(url->host), BUFFER_SIZE);
			bzero(&(url->path), BUFFER_SIZE);
			url->port = PORT;
			
			bzero(&tmp, BUFFER_SIZE);
			
			int j = 0;
			for(; i<=strlen(s);i++){
				// get host
				if (s[i] != '?' && s[i] != '#' && s[i] != '/' && s[i] != ':'){
					url->host[j] = s[i];
					j++;
				}
				else{
					#ifdef DEBUG
						printf("getUrl: host: %s\n", url->host);
					#endif
					
					bzero(&tmp, BUFFER_SIZE);
					j = 0;
					
					if (s[i] == ':'){
						// get port
						i++;
						url->port = 0;
						while (i <= strlen(s) && isdigit(s[i])){
							url->port = url->port * 10 + (s[i] - '0');
							if (url->port > 65535){
								fprintf(stderr,"URL: invalid port number.\n");
								return URL_ERR;
							}
							if (s[i] != '/') url->port = 0;
							i++;
						}
						if (url->port == 0){
							fprintf(stderr,"URL: invalid port number.\n");
							return URL_ERR;
						}
							
						#ifdef DEBUG
							printf("getUrl: usr port: %d\n", url->port);
						#endif
					}
					
					//get path
					if (s[i] != '/') { tmp[j] = '/'; j++;}
					for(; i<=strlen(s);i++){ tmp[j] = s[i]; j++;}
					
					strncpy(url->path,tmp,j);
					
					#ifdef DEBUG
						printf("getUrl: path: %s\n", url->path);
					#endif
				}
			}
			if (url->path[0] != '/')url->path[0] = '/';
		 }
		 else{
			fprintf(stderr,"URL: unsupported protocol\n");
			ecode = PROTOCOL_ERR;
		 }
	 }
	 else{
		fprintf(stderr,"URL: malformed URL\n");
		ecode = URL_ERR;
	 }
	 
	 #ifdef DEBUG
		printf("getUrl: FINISHED \n\n");
	#endif
	
	 return ecode;
 }
 
 /**
  * Options parser.
  * Parsing options by getopt().
  * @param params Structure for options and order of options.
  * @param argc Command line params.
  * @param argv Array of CL params.
  * @return Ecode.
  */
 int getParams(TParams *params, int argc, char **argv){	
	int opt;
	int i = 0;
	int ecode = EOK;
	while ((opt = getopt(argc,argv,":lsmt")) != -1){
		switch (opt){
			case 'l':
				if (!params->l){
					params->l = true;
					params->optOrder[i]='l'; i++;
				}
				break;
			case 's':
				if (!params->s){
					params->s = true;
					params->optOrder[i]='s'; i++;
				}
				break;
			case 'm':
				if (!params->m){
					params->m = true;
					params->optOrder[i]='m'; i++;
				}
				break;
			case 't':
				if (!params->t){
					params->t = true;
					params->optOrder[i]='t'; i++;
				}
				break;
			default:
				fprintf(stderr,"webinfo: invalid options, see -h.\n");
				printHelp();
				ecode = PARAMS_ERR;
			}
		}
					
	#ifdef DEBUG
		printf("getParams: %s\ngetParams: FINISHED\n\n", params->optOrder);
	#endif
	
	return ecode;
 }
 
 /**
  * Free tmp buffers.
  * @param buffer Structure contains pointers to buffers.
  */
 void freeOutBuffs(TBuffs *buffers){
	if (buffers->length != NULL) free(buffers->length);
	if (buffers->server != NULL) free(buffers->server);
	if (buffers->modif != NULL) free(buffers->modif);
	if (buffers->type != NULL) free(buffers->type);
 }
 
 /**
  * Initializing of tmp buffers.
  * @param buffers Structure containing buffers.
  * @param params Structure containing parsed CL options.
  * @return ecode.
  */
 int initOutBuffs(TBuffs *buffers, TParams *params){
	int ecode = EOK;
	for (int i = 0; i < OPTS; i++){
		switch (params->optOrder[i]){
			case 'l':
				if ((buffers->length = (char*)malloc(BUFFER_SIZE)) == NULL){
					freeOutBuffs(buffers);
					perror("Cannot allocate memory");
					return MALL_ERR;
				}bzero(buffers->length, BUFFER_SIZE);
				break;
			case 's':
				if ((buffers->server = (char*)malloc(BUFFER_SIZE)) == NULL){
					freeOutBuffs(buffers);
					perror("Cannot allocate memory");
					return MALL_ERR;
				}bzero(buffers->server, BUFFER_SIZE);
				break;
			case 'm':
				if ((buffers->modif = (char*)malloc(BUFFER_SIZE)) == NULL){
					freeOutBuffs(buffers);
					perror("Cannot allocate memory");
					return MALL_ERR;
				}bzero(buffers->modif, BUFFER_SIZE);
				break;
			case 't':
				if ((buffers->type = (char*)malloc(BUFFER_SIZE)) == NULL){
					freeOutBuffs(buffers);
					perror("Cannot allocate memory");
					return MALL_ERR;
				}bzero(buffers->type, BUFFER_SIZE);
				break;
			default:
				break;
		}
	}
	return ecode;
 }
 
 /**
  * Executing request.
  * Creates socket, resolve host, connects, send request, receive answer.
  * @param msg Body of request.
  * @param port Port number.
  * @param host String containing remote host name.
  * @return ecode.
  */
 int execRequest(char *msg, TUrl *url){
	#ifdef DEBUG
		printf("execRequest: starting request xecuting\n");
	#endif
	
	int ecode = EOK;
	int sock;
	struct sockaddr_in remote_addr;
	struct hostent *remote;
	
	// creating socket	
	if ((sock = socket(PF_INET, SOCK_STREAM,0)) < 0) {
		perror("Cannot create socket");
		return CR_SOCKET_ERR;
	}

	// resolving host
	if ((remote = gethostbyname(url->host)) == NULL){
		perror("Host does not exist");
		return GETHOST_ERR;
	}
		
	bzero(&(remote_addr), sizeof(remote_addr));
	remote_addr.sin_family = PF_INET;
	remote_addr.sin_port = htons(url->port);
	bcopy(remote->h_addr_list[0], &(remote_addr.sin_addr.s_addr), remote->h_length);
		
	// connecting...	
	if (connect(sock, (struct sockaddr*)&remote_addr, sizeof(remote_addr))<0){
		perror("Cannot connect to remote host");
		return CONN_ERR;
	}

	// create request 
	 char request[BUFFER_SIZE];
	 bzero(&request, BUFFER_SIZE);
	 sprintf(request, "HEAD %s HTTP/1.1\r\nHost: %s\r\nConnection: close\r\n\r\n",url->path, url->host);
	 
	#ifdef DEBUG
		printf("execRequest: sending request...:\n-------------------------\n");
		myprint(request);
		printf("-------------------------\n");
	#endif
	 
	// sending request
	int tmp;
	tmp = write(sock,request,strlen(request));
	if (tmp<0) {
		perror("Error, sending request failure");
		return COM_ERR;
	}
		
	#ifdef DEBUG
		printf("execRequest: ...complete\nexecRequest: receiving...\n");
	#endif
	
	// receiving response
	bzero(msg, sizeof(msg));
	tmp = read(sock,msg,BUFFER_SIZE*MSG_CONST);
	if (tmp<0) {
		perror("Error, receiving failure");
		return COM_ERR;
	}				

	#ifdef DEBUG
		printf("execRequest: response:\n------------------\n");
		myprint(msg);
		printf("------------------\n");
		printf("execRequest: ...complete\n");
		printf("execRequest: FINISHED\n\n");
	#endif
	
	// close socket
	close(sock);
	return ecode;
 }
 
 /**
  * Get status code from server response.
  * @param msg Server response.
  * @return Status code.
  */
 int getCode(char *msg){
	#ifdef DEBUG
		printf("getCode: proccessing status code from response...\n");
	#endif
	
	int i =0;
	int code = 0;
	while (i < strlen(msg) && !isspace(msg[i])){i++;}
	if(msg[i] == '\n') return code;
	i++;
	while (i < strlen(msg) && isdigit(msg[i])){
		code = code * 10 + (msg[i] - '0');
		i++;
	}
	
	#ifdef DEBUG
		printf("getCode: status Code: %d\ngetCode: FINISHED\n\n",code);
	#endif
	
	return code;
 }
 
 /**
  * Printing information from response according to CL options.
  * @param buffers Buffers for each particular piece of info.
  * @param msg Server response.
  * @param order String containing order of options (displaying info).
  */
 void printResp(TBuffs *buffers, char *msg, char order[OPTS]){
	#ifdef DEBUG
		printf("printResp: printing by user's options...\n-------------------------------\n");
	#endif
	
	if (order[0] == 0){
		#ifdef DEBUG
			printf("printResp: print: whole msg, no options:\n\n");
		#endif
		msg[strlen(msg)-1] = '\0';
		 myprint(msg);
		 return;
	}
	
	char *tmp;
	for (int i = 0; i < OPTS; i++){
		switch (order[i]){
			// fill & print buffers
			case 'l':
				if ((tmp = strstr(msg,"Content-Length:")) == NULL)
					printf("Content-Length: N/A\n");
				else{
					for (int j =0;; j++){
						buffers->length[j] = *(tmp +j);
						if (buffers->length[j] == '\n'){
							buffers->length[j+1] = '\0'; break;
						}
					}
					myprint(buffers->length);
				}
				break;
			case 's':
				if ((tmp = strstr(msg,"Server:")) == NULL)
					printf("Server: N/A\n");
				else{
					for (int j =0;; j++){
						buffers->server[j] = *(tmp +j);
						if (buffers->server[j] == '\n'){
							buffers->server[j+1] = '\0'; break;
						}
					}
					myprint(buffers->server);
				}
				break;
			case 'm':
				if ((tmp = strstr(msg,"Last-Modified:")) == NULL)
					printf("Last-Modified: N/A\n");
				else{
					for (int j =0;; j++){
						buffers->modif[j] = *(tmp +j);
						if (buffers->modif[j] == '\n'){
							buffers->modif[j+1] = '\0'; break;
						}
					}
					myprint(buffers->modif);
				}
				break;
			case 't':
				if ((tmp = strstr(msg,"Content-Type:")) == NULL)
					printf("Content-Type: N/A\n");
				else{
					for (int j =0;; j++){
						buffers->type[j] = *(tmp +j);
						if (buffers->type[j] == '\n'){
							buffers->type[j+1] = '\0'; break;
						}
					}
					myprint(buffers->type);
				}
				break;
			default:
				
				#ifdef DEBUG	
					printf("-----------------------------\nprintResp: FINISHED\n\n");
				#endif
				
				return;
		}
	}
	#ifdef DEBUG
		printf("-----------------------------\nprintResp: FINISHED\n\n");
	#endif
}
 
  /**
   * Print errors based on response status code to stderr.
   * @param msg Server response's body.
   */
 void printErr(char *msg){
	#ifdef DEBUG
		printf("printErr: printing error (status code 4xx, 5xx)\n");
	#endif
	
	fprintf(stderr, "Chyba:");
	
	int i = 0; int c; bool tmp = false;
	while((c=msg[i]) != '\n'){
		if (isspace(c)) tmp = true;
		if (tmp && c!='\r') fputc(c,stderr);
		i++;
	}
	fputc(c,stderr);

	#ifdef DEBUG
		printf("printErr: FINISHED\n\n");
	#endif
 }
 
 /**
  * Execute redirection according to server's answer.
  * Max. 5 rediretions.
  * @param msg Server response's body.
  * @param buffers Structure with tmp buffers.
  * @param order String containing CL options order.
  * @param url Structure for URL (parsed from response 'Location:').
  * @return ecode.
  */
 int execRedirect(char *msg, TBuffs *buffers, char order[OPTS], TUrl *url){
	#ifdef DEBUG
		printf("execRedirect: REDIRECTING TO ...\n");
	#endif
	
	int ecode = EOK;
	int code = 0;
	char *tmp = NULL;
	for (int i = 0; i<REDIR_CNT; i++){
		if ((tmp = strstr(msg,"Location:")) == NULL){
			fprintf(stderr,"Redirect: Cannot find location for next redirect\n");
			return COM_ERR;
		}
		else{
			char *new = malloc(BUFFER_SIZE);
			if (!new) return MALL_ERR;
			
			bzero(new, BUFFER_SIZE);
			
			// parsing URL from response location's line
			int j = 0;
			while (!isspace(*(tmp+j))) j++;
			j++;
			int k = 0;
			for (;; j++){
				if (*(tmp+j) == '\r')break;
				new[k] = *(tmp +j);
				k++;
			}
			
			#ifdef DEBUG
				printf("execRedirect: location: %s\n", new);
			#endif
			
			// parsing new url
			clearUrl(url);
			int ecode = getUrl(new, url);
			if (ecode != EOK){
				free(new);
				return ecode;
			}
			
			// execute new request
			if ((ecode = execRequest(msg, url)) != EOK){
				free(new);
				return ecode;
			}
			
			free(new);
			
			// proccess new response according to server response
			code = getCode(msg);
			
			#ifdef DEBUG
				printf("execRedirect: proccessing status code from newresponse: %d\n", code);
			#endif
		
			if (code == 200) {
				printResp(buffers, msg, order); return EOK;}
			else
				if (code >= 400){
					
					#ifdef DEBUG
						printf("execRedirect: FINISHED\n");
					#endif
	
					printErr(msg); return EOK;}
			else
				if (code == 0){
					fprintf(stderr,"Redirect: status code: value out of expected range occur, possible Bad request\n");
					
					#ifdef DEBUG
						printf("execRedirect: FINISHED\n");
					#endif
					
					return UNKNOWN_ERR;
	}
		}
	}
	fprintf(stderr,"Redirect: Too much redirects -> loop.\n");
	ecode = REDIR_ERR;
	
		
	#ifdef DEBUG
		printf("execRedirect: FINISHED\n");
	#endif
	
	return ecode;
}
 
 /**
  * Main function.
  * @return See -h (printHelp()) for exit codes list.
  */ 
 int main(int argc, char **argv){
	if (argc == 1){
	  printHelp();
	  return PARAMS_ERR;
	}
	
	#ifdef DEBUG
		printf("main: here I go...\n\n");
	#endif
	 
	 int ecode = EOK;

	 // chceking for -h option
	 if (argc < 3 && argv[1][0] == '-'){
		printHelp();
		if ((strcmp(argv[1],"-h")==0))
			return 0;
		else 
			return PARAMS_ERR;
	 }
	 
	 TUrl url;
	 clearUrl(&url); 
	 // URL validating (url is last param)
	 if ((ecode = getUrl(argv[argc-1], &url)) != EOK){
		return ecode;
	 }
	 
	 TParams params = {false,false,false,false,{0}};
	 // Usr options validating
	 if ((ecode = getParams(&params, argc, argv)) != EOK){
		return ecode;
	 }
	 
	 TBuffs buffers = {NULL, NULL, NULL, NULL};
	 // tmp buffers initializing
	 if ((ecode = initOutBuffs(&buffers, &params)) != EOK){
		return ecode;
	 }
	 
	 char *msg = malloc(BUFFER_SIZE*MSG_CONST);	 
	 bzero(msg, sizeof(msg));
	 
	 // execute request
	 if ((ecode = execRequest(msg, &url)) != EOK){
		return ecode;
	 }
	 
	 // proccessing response according to status code
	 int code = getCode(msg);
	 
	 if (code == 200) // OK
		printResp(&buffers, msg, params.optOrder);
	 else
	 if (code == 301 || code == 302){ // Redirect
		if ((ecode = execRedirect(msg, &buffers, params.optOrder, &url)) != EOK){
			free(msg);
			return ecode;
		}
	 }
	 else
	 if (code >= 400 && code < 600){ // 4xx, 5xx errors
		if (code >= 500) ecode = SC5XX_ERR;
		else ecode = SC4XX_ERR;
		printErr(msg);}
	 else{
		free(msg);
		fprintf(stderr,"Status code: value out of expected range occur, possible Bad request\n");
		return UNKNOWN_ERR;
	}
	
	//cleaningS
	 free(msg);
	 freeOutBuffs(&buffers);
	 
	#ifdef DEBUG
		printf("main: FINISHED\n");
	#endif
	
	 return ecode;
 }
