/**
 * @file: error.h
 * @date: 7.3.2012 
 * @author: Michal Starigazda, xstari01@stud.fit.vutbr.cz 
 */

#define EOK           0
#define PARAMS_ERR    1
#define REGEX_ERR     2
#define COM_ERR       3
#define CR_SOCKET_ERR 4
#define BIND_ERR      5
#define LISTEN_ERR    6
#define FORK_ERR      7
#define GETHOST_ERR   8
#define CONN_ERR      9

const char *ecodes[] = {
  "OK",
  "Chybne parametre",
  "Chyba pri kompilacii RE",
  "Chyba behom zapisu/citania zo socketu",
  "Chyba pri vytvarani socketu",
  "Chyba pri viazani socketu",
  "Chyba pri inicializacii nacuvania",
  "Chyba pri vytvarani podprocesu",
  "Chyba pri ziskavani adresy serveru",
  "Chyba pri vytvarani spojenia so serverom"
};
