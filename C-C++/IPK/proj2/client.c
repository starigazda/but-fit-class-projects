/** 
 * @file: client.c
 * @date: 7.4.2012
 * @author: Michal Starigazda, xstari01@stud.fit.vutbr.cz
 */
 
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <netdb.h>
#include <locale.h>
#include <regex.h>
#include <errno.h>
#include <ctype.h>
#include "error.h"

#define BUFFER_LEN 256

/**
 * Struktura obsahujuca parametre.
 */ 
typedef struct params{
  char host[BUFFER_LEN];    // nazov serveru
  int port;                 // cislo portu
  char order[3];            // ziskavana vwrzia IP adresy (poradie vypisu)
  char *domain_name;        // meno domeny urcene k prekladu
} TParams;

/**
 * Funkcia validuje a parsuje retazec obsahujuci nazov serveru a cislo portu.
 * @param params Struktura k ulozeniu parametrov.
 * @return Chybovy kod.
 */  
int getHostPort(char *argv, TParams *params){
  int ecode = EOK;
  
  // parsing pomocou reg. vyrazov
  regmatch_t pmatch[3];
  char *pattern = "^(.*):([0-9]*)$"; 
  regex_t re;
  int status;
  char buf[BUFFER_LEN] = {0};
  char string[BUFFER_LEN] = {0};
  strcpy(string,argv);
  
  if(regcomp( &re, pattern, REG_EXTENDED)!= 0) {
    perror("client: regcomp(): Error while compiling regexp !");
    return REGEX_ERR;
  }

  status = regexec( &re, string, 3, pmatch, 0);
  if(status == 0){
    strncpy(buf,string+pmatch[1].rm_so,pmatch[1].rm_eo-pmatch[1].rm_so);
    strncpy(params->host, buf, sizeof(buf));  // nazov serveru
    bzero(&buf, BUFFER_LEN);
    strncpy(buf,string+pmatch[2].rm_so,pmatch[2].rm_eo-pmatch[2].rm_so);
    params->port = atoi(buf); // cislo portu
  } else {
    regerror(status, &re, buf, BUFFER_LEN);
    fprintf(stderr, "client: regexec(): error: %s\n", buf);
    ecode = PARAMS_ERR;
  }
  regfree( &re);
  
  return ecode;
}

/**
 * Spracovanie parametrov.
 * @param params Struktura k ulozeniu parametrov/nastaveni.
 * @return Chybovy kod.
 */ 
int getParams(char **argv, int argc, TParams *params){
  int ecode = EOK;
  
  if (argc == 4 || argc == 5){
    // spracovanie mena serveru a portu
    if ((ecode = getHostPort(argv[1], params)) == EOK){
      if (argc == 4){
        if (strcmp(argv[2], "-4") == 0){ 
          params->order[0] = '4';
        }
        else
        if (strcmp(argv[2], "-6") == 0){ 
          params->order[0] = '6';
        }
        else if (strcmp(argv[2], "-46") == 0){
          params->order[0] = '4';
          params->order[1] = '6';
        }
        else if (strcmp(argv[2], "-64") == 0){
          params->order[0] = '6';
          params->order[1] = '4';
        }
        else 
          return PARAMS_ERR;
      }
      else if (argc == 5){
        if (strcmp(argv[2], "-4") == 0 && strcmp(argv[3], "-6") == 0){
          params->order[0] = '4';
          params->order[1] = '6';
        }
        else if (strcmp(argv[2], "-6") == 0 && strcmp(argv[3], "-4") == 0){
          params->order[0] = '6';
          params->order[1] = '4';
        }
        else
          return PARAMS_ERR;
      }
      params->domain_name = argv[argc-1];
    }  
  }
  else{
    ecode = PARAMS_ERR;
  }

  return ecode;
}

/**
 * Funkcia tlaci napovedu na standardny vystup.
 */ 
void printHelp(){
  printf("Help:  \n"
         "Synopsis: client HOST:PORT [-4] [-6] DOMENJMENO\n");
  printf("-------------------------------------------------------------\n"
         "Popis chybovych kodov:\n");
  for (int i = 0; i<10; i++){
    printf("%d\t %s\n", i, ecodes[i]);
  }
}

/**
 * Funkcia parsuje a tlaci IP adresu zo stringu.
 * @param Pointer na zaciataok riadku obsahujuceho IP adresu.
 */  
void myPrint(char *tmp_p, const char *prefix){
  // nacitanie adresy
  char addr[BUFFER_LEN] = {0};
  int j = 0;
	while (!isspace(*(tmp_p+j))) j++;
	j++;
	int k = 0;
	for (;; j++){
		if (*(tmp_p+j) == '\n')
      break;
		addr[k] = *(tmp_p + j);
		k++;
	}
  // kontrola uspesnosti resolveru
  if (strcmp(addr,"N/A") == 0){
  // nenalezena
  fprintf(stderr,"%s Nenalezeno.\n", prefix);
  }
  else
    printf("%s\n", addr);
}

/**
 * Funkcia tlaci pozadovane informacie (podla parametrov) z odpovede serveru.
 * @param msg Odpoved serveru na poziadavok.
 * @param params Struktura s parametrami (uzivatelom definovane)
 */  
void printAddrs(const char *msg, TParams *params){
  char *p_ipv4 = strstr(msg, "IPv4:");
  char *p_ipv6 = strstr(msg, "IPv6:");
  
  for (int i = 0; i<strlen(params->order); i++){
    if (params->order[i] == '4'){
      if (p_ipv4) myPrint(p_ipv4, "Err4:");
      else fprintf(stderr, "Err4: Nenalezeno.\n");
    }
    else if (params->order[i] == '6'){
      if (p_ipv6) myPrint(p_ipv6, "Err6:"); 
      else fprintf(stderr, "Err6: Nenalezeno.\n");
    }
  }
}

/**
 * Funkcia vykonava sluzbu poskytovanu klientom - resolver.
 * @param sock Socket pre komunikaciu so serverom.
 * @param params Struktura obsahujuca domenove meno urcene k prekladu.
 * @return Chybovy kod.
 */    
int service(int sock, TParams *params){
  int ecode = EOK;
  int tmp;

  // Zaslanie poziadavku (requestu)
  char msg[BUFFER_LEN] = {0};
  sprintf(msg, "SGAP 1.0 REQUEST\nDomain-name: %s\n",params->domain_name);
  if ((tmp = write(sock,msg,strlen(msg))) < 0){
    perror("client: write(): Error, sending request failure");
    return COM_ERR;
  }
  
  // Obdrzanie odpovede
  bzero(&msg,BUFFER_LEN);
  if ((tmp = read(sock,msg,BUFFER_LEN)) < 0){
    perror("client: read(): Error, receiving failure");
    return COM_ERR;
  }
  
  // Tlac na STDOUT (STDERR) podla parametrov
  printAddrs(msg, params);
  
  return ecode;
}

/*****************************************************************************
 ** Funkcia MAIN
 *****************************************************************************/ 
int main(int argc, char **argv){
  int ecode = EOK;
  
  // Spracovanie parametrov
  TParams params = {{0}, 0, {0}, NULL};
  if ((ecode = getParams(argv, argc, &params)) != EOK){
    fprintf(stderr,"Chybne parametry prikazoveho riadku.\n");
    printHelp();
    return ecode;
  }
  
  //////////////////////// CONNECTION /////////////////////////////////
  int sock;
	struct sockaddr_in remote_addr;
	struct hostent *remote;
	
	// Vytvorenie socketu	
	if ((sock = socket(PF_INET, SOCK_STREAM,0)) < 0) {
		perror("client: sock(): Cannot create socket");
		return CR_SOCKET_ERR;
	}

	// resolving host
	if ((remote = gethostbyname(params.host)) == NULL){
		perror("clientt: gethostbyname(): Host does not exist");
		return GETHOST_ERR;
	}
		
	bzero(&(remote_addr), sizeof(remote_addr));
	remote_addr.sin_family = PF_INET;
	remote_addr.sin_port = htons(params.port);
	bcopy(remote->h_addr_list[0], &(remote_addr.sin_addr.s_addr), remote->h_length);
		
	// Vytvorenie spojenia
	if (connect(sock, (struct sockaddr*)&remote_addr, sizeof(remote_addr))<0){
		perror("client: connect(): Cannot connect to remote host");
		return CONN_ERR;
	}

  // Vykonanie sluzby klienta (DNS resolver)
  ecode = service(sock, &params);
  
	// close socket
	close(sock);
  
  return ecode;
}
/** End of file client.c **/
