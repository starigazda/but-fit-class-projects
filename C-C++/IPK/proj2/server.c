/**
 * @file: server.c
 * @date: 7.3.2012 
 * @author: Michal Starigazda, xstari01@stud.fit.vutbr.cz 
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h> 
#include <signal.h>
#include <ctype.h>
#include "error.h"

#define QUEUE 5         // standard value
#define BUFFER_LEN 256

/**
 * SIGCHLD handler.
 */ 
void SigCatcher(int n){
	wait3(NULL,WNOHANG,NULL);
}

/**
 * Funkcia poskytuje sluzbu klientovy. Poskytuje iba jednu sluzbu - vracia IP
 * adresy pre domenu danu poziadavkom klienta. IP adresy (v4,v6) ziskava
 * lokalny resolverom (getaddrinfo).
 * @param sock Socket cez ktory sa komunikuje s klientom.
 * @return Chybovy kod.
 */     
int service (int sock){
  int ecode = EOK;
  
  // Nacitanie poziadavky (requestu) zo socketu
  char msg[BUFFER_LEN]={0};
  int tmp;
  if ((tmp = read(sock,msg,BUFFER_LEN)) < 0){
    perror("server: ERROR reading from socket");
    return COM_ERR;
  }

  char *tmp_p = strstr(msg,"Domain-name:");
  // Domain-name je vzdy polozka requestu, takze pokial prebehol read() 
  // v poriadk uurcite sa "Domain-name:" nachadzav msg, preto bez testu na NULL
  char domain_name[BUFFER_LEN] = {0};
	
	// ----------------- get domain name (from request) ------------
  int j = 0;
	while (!isspace(*(tmp_p+j))) j++;
	j++;
	int k = 0;
	for (;; j++){
		if (*(tmp_p+j) == '\n')
      break;
		domain_name[k] = *(tmp_p + j);
		k++;
	}
  
  //------------------- get IP addresses ----------------------
  struct addrinfo hints, *res, *i;
  char ipv4[INET_ADDRSTRLEN] = {0};
  char ipv6[INET6_ADDRSTRLEN] = {0};
  
  bzero(&hints, sizeof(hints));
  hints.ai_family = PF_UNSPEC;    // akceptuje IPv4 aj IPv6
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_flags |= AI_CANONNAME;

  // incializacia
  strcpy(ipv4,"N/A");
  strcpy(ipv6,"N/A");
    
  int error = getaddrinfo(domain_name, NULL, &hints, &res);
  if (error == 0)
  { // getaddrinfo bolo uspesne
    bool ipv4_flag = false;
    bool ipv6_flag = false;
    for(i = res;i != NULL; i = i->ai_next){
        // uloz iba prvy zaznam IPv4 adresy (moze ich byt aj viac)
        if (i->ai_family == AF_INET && !ipv4_flag) {
            inet_ntop(AF_INET, &(((struct sockaddr_in *)i->ai_addr)->sin_addr), ipv4, INET_ADDRSTRLEN);
            ipv4_flag = true;
        }
        // opat uloz prvu IPv6 adresu ktora sa vyskytne 
        else if (i->ai_family == AF_INET6 && !ipv6_flag){
            inet_ntop(AF_INET6, &(((struct sockaddr_in6 *)i->ai_addr)->sin6_addr), ipv6, INET6_ADDRSTRLEN);
            ipv6_flag = true;
        }
    } 
    // uvolnenie struktury addrinfo
    freeaddrinfo(res);
  }

  // ------------------- fill response ----------------------------
  bzero(&msg,BUFFER_LEN);
  sprintf(msg, "SGAP 1.0 RESPONSE\nIPv4: %s\nIPv6: %s\n",ipv4,ipv6);
  
  if ((tmp = write(sock,msg,strlen(msg))) < 0){
    perror("server: ERROR writing to socket");
    return COM_ERR;
  }

  return ecode;
}

/**
 * Spracovanie parametrov.
 * @return Chybovy kod.
 */  
int getParams(int argc, char **argv, int *port){
  int ecode = EOK;
  
  // validne parametre: -p PORT
  if (argc == 3 && strcmp(argv[1],"-p")==0){
    for (int i = 0; i < strlen(argv[2]); i++){
      if (!isdigit(argv[2][i]))
        return PARAMS_ERR;
    }
    *port = atoi(argv[2]);
  }
  else 
    ecode = PARAMS_ERR;
  
  return ecode;
}

/**
 * Funkcia tlaci napovedu na standardny vystup.
 */ 
void printHelp(){
  printf("Help:  \n"
         "Synopsis: server -p PORTNUMBER\n");
  printf("-------------------------------------------------------------\n"
         "Popis chybovych kodov:\n");
  for (int i = 0; i<10; i++){
    printf("%d\t %s\n", i, ecodes[i]);
  }
}

/****************************************************************************
 *  Funkcia MAIN
 ****************************************************************************/  
int main (int argc, char **argv) {
  int ecode = EOK;
  int port = 0;
  
  // Spracovanie parametrov
  if ((ecode = getParams(argc, argv, &port)) != EOK){
    printHelp();
    return ecode;
  }
  
	int listen_socket;
	struct sockaddr_in sa;
	struct sockaddr_in sa_client;
	
	socklen_t sa_client_len=sizeof(sa_client);
	
  // vytvorenie soketu
	if ((listen_socket = socket(PF_INET, SOCK_STREAM, 0)) < 0)
	{
		perror("server: socket(): can not create socket");
		return CR_SOCKET_ERR;
	}
	
	bzero(&sa,sizeof(sa));
	sa.sin_family = AF_INET;
	sa.sin_addr.s_addr = INADDR_ANY;
	sa.sin_port = htons(atoi(argv[2]));	
	
  // bindovanie socketu
  if (bind(listen_socket, (struct sockaddr*)&sa, sizeof(sa)) < 0)
	{
		perror("server: bind(): can not bind socket");
		return BIND_ERR;
	}
	
  // inicializacia nasluchania
  if (listen(listen_socket, QUEUE) < 0)
	{
		perror("server: listen(): failed");
		return LISTEN_ERR;				
	}
	
	// spracovanie potomkov, nech nam tu nevisia zombici
	signal(SIGCHLD,SigCatcher);
	
	int new_socket;
	while(1)
	{
		if ((new_socket = accept(listen_socket, (struct sockaddr*)&sa_client, &sa_client_len)) <= 0)
		{
		  // mozno nejaky print do logu, zotavujeme sa z toho
			continue;
    }
    
    // vytvorenie procesu, pre spracovanie klienta
		int pid = fork();
		if (pid < 0) 
		{ // zlyhal fork
			perror("server: fork(): failure");
			return FORK_ERR;				
		}
		
		if (pid == 0)
		{ // procces spracovavajuci klienta     	
			close(listen_socket);	// uzavretie zdedeneho socketu, nepovinny close
			service(new_socket);  // poskytnutie sluzby
			close(new_socket);		// uzavretie socketu
			exit(0);
		}
		else
		  // rodic
			close(new_socket);
	}	
	close(listen_socket);
	return ecode;
}
/*** End of file server.c **/
