/**
 * Project:	BMS 2012/2013, Project 1. - Error Correction Code
 * Author:	Michal Starigazda
 * Date:	10.11.2013
 * File:	bms.h - (Reed-Solomon) coder/decoder header file
 * Note:	Used libraries: RScode <http://rscode.sourceforge.net/>
 */ 

#define WORD_LEN		139					// input word length

/* For NPAR see rscode library (file ecc.h). */
const int CDW_LEN 	= 	WORD_LEN + NPAR;	// codeword length
const int ERR_LIM 	= 	NPAR>>1;			// max err. corrections	

unsigned char codeword[256];				// max size by RScode lib.
