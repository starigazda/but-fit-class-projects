/**
 * Project:	BMS 2012/2013, Project 1. - Error Correction Code
 * Author:	Michal Starigazda
 * Date:	10.11.2013
 * File:	bms1B.c - (Reed-Solomon) decoder
 * Note:	Used libraries: RScode <http://rscode.sourceforge.net/>
 */ 

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ecc.h"
#include "bms.h"

int offset = 0;		/* Number of whole codewords in encrypted file. */
int offsize = 0;	/* Size of "last" uncomplete codeword if any. */

/** Get file size. */
int f_size(FILE *fp) {
	fseek(fp, 0L, SEEK_END);
	int size = ftell(fp);
	fseek(fp, 0L, SEEK_SET);	
	return size;
}

/** If exists uncomplete codeword, deinterleaving gets complicated.
 * One (last) byte of first "offsize" interleaved blocks are part of 
 * last codeword. Example:
 * codewords:  | abcde | 12345 | FGH |-> last is uncomplete
 * interleaved:| a|1|F | b|2|G | c|3|H | d|4 |e|5 |
 * Function returns appropriate index of byte depending on processed 
 * block (size of block is offset) and occurence flag of last codeword 
 * byte in block.
 * @param tmp Occurence flag (0 for false, else true).
 * @param j	Sequence number of processed block.
 * @return Index of appropriate byte.
 */
int getoffset(int tmp, int j) {
	if (tmp != 0) {
		return (offset + 1)*j;
	} else {
		return ((offset+1)*offsize) + offset*(j-offsize);
	}
}

/** Deinterleaving, decoding, error corrections and writing to output 
 * file.
 * @param srcmem Memory containig encrypted codewords.
 * @param fout File descriptor of output file.
 */
void decode (unsigned char *srcmem, FILE *fout){
        /* Initialization of ECC - rscode lib. */
        initialize_ecc ();

		/* Read codeword from srcmem, decode and write. */
        for (int i = 0; i < offset; i++){
                int offcnt = offsize;
                
                memset(&codeword, 0, CDW_LEN);
                
                // get codeword by deinterleaving, each byte from 
                // another offset block
                for (int j = 0; j < CDW_LEN; j++){
                        int tmp = (offcnt-- > 0) ? 1 : 0;
                        codeword[j] = srcmem[getoffset(tmp, j) + i];
                }

                decode_data(codeword, CDW_LEN);

                /* check if syndrome is all zeros */
                if (check_syndrome () != 0) {
                        correct_errors_erasures (codeword, CDW_LEN, 0, NULL);
                }

				fwrite(codeword, sizeof(unsigned char), WORD_LEN, fout);
        }

        /* Handle last uncomplete codeword. */
        memset(&codeword, 0, CDW_LEN);
        
        // deinterleave
        for (int j = 0; j < offsize; j++){
                codeword[j] = srcmem[(j+1)*(offset+1) - 1];
        }
        
        decode_data(codeword, CDW_LEN);
                
        /* check if syndrome is all zeros */
        if (check_syndrome () != 0) {
                correct_errors_erasures (codeword, CDW_LEN, 0, NULL);
        }

        fwrite(codeword, sizeof(unsigned char), offsize - NPAR, fout);
}

/** Finalization and exiting program. */
void _exit (FILE *fp, FILE *fout, unsigned char *mem1, int retcode) {
	if (fp != NULL){ fclose(fp); }
	if (fp != NULL){ fclose(fout); }
	if (mem1 != NULL){ free(mem1); }
	exit(retcode);
}

int main(int argc, char** argv) {
	
	/* Check arguments. */
	if (argc != 2) { 
		fprintf(stderr,"Missing input file\n");
		exit(-1);
	}

	/* Input file. */
	FILE *fp = fopen(argv[1], "r");
	if (fp == NULL){
		perror("File could not be opened!");
		exit(-1);
	}
	
	/* Ouput file. */
	FILE *fout = fopen(strcat(argv[1],".ok"), "w");
	if (fout == NULL){
		perror("File could not be opened!");
		_exit(fp, NULL, NULL, -1);
	}
	
	int fsize = f_size(fp);				// get size of input file
	int ncword = fsize/CDW_LEN;			// number of whole codewords
	
	offset = ncword;					// 
	offsize = fsize%CDW_LEN;			// size of last codeword	
		
	unsigned char *encmemp = (unsigned char *)malloc(sizeof(unsigned char)*fsize);
	if (encmemp == NULL){
		perror("Memory allocation failure!");
		_exit(fp, fout, NULL, -1);		
	}
	
	//read file to memory
	fread(encmemp, sizeof(unsigned char), fsize, fp);
	
	/* Main function of decoding, includes deinterleaving, error 
	 * correction and writing to output file. */
	decode(encmemp, fout);
	  
	/* Finalization */
	_exit(fp, fout, encmemp, 0);
	
	exit(0);
}

