#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int f_size(FILE *fp) {
	fseek(fp, 0L, SEEK_END);
	int size = ftell(fp);
	fseek(fp, 0L, SEEK_SET);	
	return size;
}

int main(int argc, char** argv) {
	
	if (argc != 3) { 
		fprintf(stderr,"Missing argument\n");
		exit(-1);
	}

	int nerr_p = atoi(argv[2]);

	FILE *fp = fopen(argv[1], "r");
	if (fp == NULL){
		perror("File could not be opened!");
		exit(-1);
	}
	
	FILE *fout = fopen(strcat(argv[1],".err"), "w");
	if (fout == NULL){
		perror("File could not be opened!");
		fclose(fp); 
		exit(-1);
	}
	
	int fsize = f_size(fp);
	int nerr = (int)(((double)fsize/(double)100)*(double)nerr_p);
	
	if (nerr > fsize){
		printf("Too much errors for short file\n");
		fclose(fp);
		fclose(fout);
		exit(-1);
	}

	printf("File size: %d, Errors: %d (%d %%)\n", fsize, nerr, nerr_p);

	unsigned char *memp = (unsigned char *)malloc(fsize);
	if (memp == NULL){
		perror("Memory allocation failure!");
		fclose(fp);
		fclose(fout);
		exit(-1);		
	}
	
	fread(memp, fsize, 1, fp);
	memset(memp, 0, nerr);
	fwrite(memp, fsize, 1, fout);
	
	free(memp);
	fclose(fp);
	fclose(fout);

	exit(0);
}
