/**
 * Project:	BMS 2012/2013, Project 1. - Error Correction Code
 * Author:	Michal Starigazda
 * Date:	10.11.2013
 * File:	bms1A.c - (Reed-Solomon) coder
 * Note:	Used libraries: RScode <http://rscode.sourceforge.net/>
 */ 

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ecc.h"
#include "bms.h"

/** Number of whole words in file. */ 
int offset = 0;

/** Get file size. */
int f_size(FILE *fp) {
	fseek(fp, 0L, SEEK_END);
	int size = ftell(fp);
	fseek(fp, 0L, SEEK_SET);	
	return size;
}

/** Get necessary memory size for encoding file (substracted by offset).
 * @param nword Number of whole words in file (WORD_LEN).
 * @return Size of memory, added size of parity bytes. 
 */ 
int get_memsize_enc(int nword) {
	return sizeof(unsigned char)*(nword*(WORD_LEN + NPAR));
}


/** Interleave new codeword.
 * Example of interleaved codewords | ABCDXY | 123456 | abcdxy |:
 * |A|1|a|B|2|b|C|3|c|D|4|d|X|5|x|Y|6|y|
 * @param codeword Pointer to encoded word.
 * @param mem Pointer to dest. memory.
 * @param cnt Sequence number of new codeword.
 */
void interleave (unsigned char *codeword, unsigned char *mem, int cnt){
	for (int i = 0; i < CDW_LEN; i++){
		mem[offset*i + (cnt-1)] = codeword[i];
	}
}

/** Write memory content to file. Memory contains interleaved codewords,
 * but last codeword (size is (offset!=0) ? offset + NPAR : 0) which
 * requires interleaving.
 * @param fout Output file descriptor.
 * @param mem Src memory.
 * @param codeword "Last" encrypted word.
 * @param nread Size of "last" word (unencrypted).
 */
void f_write (FILE *fout, unsigned char *mem, unsigned char *codeword, int nread) {
	for (int i = 0; i < CDW_LEN; i++){
		fwrite(mem + (offset*i), offset, 1, fout);
		if (i < (nread+NPAR) && nread > 0){
			fwrite(codeword+i, 1, 1, fout);
		}
	}
}

int main(int argc, char** argv) {
	
	/* Check arguments.*/
	if (argc != 2) { 
		fprintf(stderr,"Missing input file\n");
		exit(-1);
	}

	/* Input file.*/
	FILE *fp = fopen(argv[1], "r");
	if (fp == NULL){
		perror("File could not be opened!");
		exit(-1);
	}
	
	/* Output file.*/
	FILE *fout = fopen(strcat(argv[1],".out"), "w");
	if (fout == NULL){
		perror("File could not be opened!");
		fclose(fp);
		exit(-1);
	}
	
	int fsize = f_size(fp);					// get size of input file
	int nword = fsize/WORD_LEN;				// number of words in file
	int memsize = get_memsize_enc(nword);
	
	offset = nword;
	
	unsigned char *memp = (unsigned char *)malloc(memsize);
	if (memp == NULL){
		perror("Memory allocation failure!");
		fclose(fp);
		exit(-1);		
	}
	
	/* init. memory */
	memset(memp, 0, memsize);
	
	/*  Initialization the ECC library */			
	initialize_ecc ();

	int cnt = 0;	// counter of processed words	
	int nread = 0;	// counter of read bytes by fread()
	unsigned char buffer[WORD_LEN];							
	
	/* Read and encode file.*/
	while ( !feof(fp) ){	
		memset(&buffer, 0, WORD_LEN);
		
		nread = fread(buffer, sizeof(unsigned char), WORD_LEN, fp);
		if ( ferror(fp) != 0 ){
			fprintf(stderr,"Read file error!\n");
			fclose(fp);
			exit(1);
		}
		
		/* Encode new word. */
		memset(&codeword, 0, CDW_LEN);
		encode_data(buffer, nread, codeword);
		
		cnt++;
						
		/* Interleave if not last word. */
		if (nread == WORD_LEN){
			interleave(codeword, memp, cnt);
		}
	}
	
	/* Write memory to file, interleave last encoded word if needed. */
	f_write(fout, memp, codeword, nread);

	/* Finalization.*/
	free(memp);
	fclose(fp);
	fclose(fout);
	
	//fprintf(stderr,"Original file size: %d, encrypted file size: %d, RATIO: %.2f%%\n", 
	//		fsize, memsize+offset+NPAR, ((double)((memsize+offset+NPAR)-fsize)/(double)fsize)*100);
	
	exit(0);
}
