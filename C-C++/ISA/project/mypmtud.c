/***
 * 
 * @file    mypmtud.c
 * @author  Michal Starigazda, xstari01@stud.fit.vutbr.cz
 * @date    17.11.2012
 * 
 * Path MTU Discovery for course ISA, FIT VUT Brno
 * 
 */


#include "mypmtud.h"

#define LOW4(size) (size < MIN4_OFFIC_MTU) ? MIN4_MTU : MIN4_OFFIC_MTU
#define LOW6(size) (size < MIN6_OFFIC_MTU) ? MIN6_MTU : MIN6_OFFIC_MTU

// verbose mode flag
int VERBOSE = OFF;

/**
 * Turn on verbose mode for all important functions (by setting global flag.)
 */
void setVerbose(){
  VERBOSE = ON;
}

/**
 * Print head of processing table to stdout.
 * Verbose mode in active state (VERBOSE == ON) required.
 */
void setDebugHead(){
    /* create head */
    printf( "Description:\n"
            "\tseq. - echo request sequence number, sl - search_low, sh - search_high, \n"
            "\ttpmtu - size of tested MTU, FN - fragmentation needed (next-hop MTU),\n"
            "\tPTB -Paket Too Big (next-hop MTU)\n"
            "--------------------------------------------------------\n");
    printf("Seq.\tid\tsl\tsh\ttpmtu\tstatus\n");
}

/**
 * Print values to stdout in summary fomat.
 * Verbose mode in active state (VERBOSE == ON) required.
 */
void printStats(int timeoutcnt, int total_size, int resume){
    printf( "--------------------------------------------------------\n"
            "Stats:\n"
            "  %d timeouts, %d bytes sent, PMTU discovered: %d\n"
            "--------------------------------------------------------\n",
            timeoutcnt, total_size, resume);
}

/**
 * Checksum function based on RFC 1071 (Computing the Internet Checksum
 * by R. BradenD. Borman, and C. Partridge, 1988).
 *
 * The following "C" code algorithm computes the checksum with an inner
 * loop that sums 16-bits at a time in a 32-bit accumulator.
 */
unsigned short int cksum(unsigned short *addr, int count)
{
    /*
     * Compute Internet Checksum for "count" bytes
     * beginning at location "addr".
     */

    int sum = 0;

    while( count > 1 )  {
    /*  This is the inner loop */
        sum += *addr++;
        count -= 2;
    }

    /*  Add left-over byte, if any */
    if( count > 0 )
        sum += * (unsigned char *) addr;

    /*  Fold 32-bit sum to 16 bits */
    while (sum>>16)
        sum = (sum & 0xffff) + (sum >> 16);

    return ~sum;
}



/**
 * 'Wrapper' function for dicovering max path MTU.
 * @param result Pointer to store a discovered value.
 * @param straddr String containing address (unvalidated format).
 * @param max_limit Limit for maximal size of tested MTU
 * @return True for successful discovery, else false.
 */
bool discoverMTU(int *result, char *straddr, int max_limit){

    bool retval = false;
    
    /* Resolve address. */
    struct addrinfo hints, *res, *i;

    /* Strings used for verbose mode. */
    char ipv4[INET_ADDRSTRLEN];
    char ipv6[INET6_ADDRSTRLEN];

    // init addrinfo structure, default values of flags
    memset(&hints, 0, sizeof(struct addrinfo));
    hints.ai_family = AF_UNSPEC;    // allow IPv4 or IPv6
    hints.ai_socktype = SOCK_RAW;
    hints.ai_flags = 0;
    hints.ai_protocol = 0;

    // resolving 
    int error = getaddrinfo(straddr, NULL, &hints, &res);
    if (error != 0)
    {// getaddrinfo failed
        fprintf(stderr, "mypmtud: getaddrinfo() failure, cannot resolve address\n");
        retval = false;
    }
    else
    {// getaddrinfo success

        int sock;               // socket descriptor
        int domain;             // specify protocol family for socket
        int protocol;           // specify protocol for socket, IPPROTO_[ICMP|ICMPV6]

        struct sockaddr_in remoteAddr;
        struct sockaddr_in6 remoteAddr6;

        /* Loop over address structures in the list.
         * Function getaddrinfo() implicitly sorts adrress structures.
         * Try each address until we successfully connect (IPv6 addresses
         * are preferred by getaddrinfo() sorting function).
         */
        for (i = res; i != NULL; i = i->ai_next){
            /* identify IP protocol version */
            
            if (i->ai_family == AF_INET6){

                // init socket params
                domain = AF_INET6;  
                protocol = IPPROTO_ICMPV6;

                if (VERBOSE){
                    inet_ntop(AF_INET6, &(((struct sockaddr_in6 *)i->ai_addr)->sin6_addr), ipv6, INET6_ADDRSTRLEN);
                    printf( "IPv6 address resolved: \t %s \ntrying Path MTU Discovery...\n"
                            "--------------------------------------------------------\n", ipv6);
                }

            }
            else {

                // init socket params
                domain = AF_INET;   
                protocol = IPPROTO_ICMP;
                
                if (VERBOSE){
                    inet_ntop(AF_INET, &(((struct sockaddr_in *)i->ai_addr)->sin_addr), ipv4, INET_ADDRSTRLEN);
                    printf( "IPv4 address resolved: %s \ntrying Path MTU Discovery...\n"
                            "--------------------------------------------------------\n", ipv4);
                }
            }

            /* Create raw socket with ICMP[v6] protocol*/
            if ((sock = socket(domain, SOCK_RAW, protocol)) == -1){
                // try next address in list
                    continue;
            }

            /* call path MTU discovery function for IPv4 or IPv6 */
            switch(i->ai_family){
                // IPv4
                case AF_INET:
                        memcpy(&remoteAddr, i->ai_addr, i->ai_addrlen);

                        if ((*result = pmtud(sock, &remoteAddr, max_limit)) > 0){
                            // detection succeeded
                            retval = true;
                        }
                        // else try next address, retval is false
                        close(sock); 
                    break;

                // IPv6
                case AF_INET6:
                        memcpy(&remoteAddr6, i->ai_addr, i->ai_addrlen);
                
                        if ((*result = pmtud6(sock, &remoteAddr6, max_limit)) > 0){
                            // detection succeeded
                            retval = true;
                        }
                        // else try next address, retval is false
                        close(sock); 
                    break;

                default:
                    break;
            }// switch      

            if (retval) {
                // detection complete, dont try next addr
                break;  
            }
            
        }// for (i = res; i != NULL; i = i->ai_next)

        if (!i){ // no adress suceeded
            fprintf(stderr,"mypmtud: Cannot detect PMTU (resume = 0).\n");
            retval = false;
        }

        // struct addrinfo no longer needed
        freeaddrinfo(res);
    } 

    return retval;
}

/**
 * Set socket options for AF_INET socket required by this PMTUD implementation.
 * For more detailed description see particular setsockopt() callings in function.
 * @param sock Socket descriptor.
 * @return On success, zero is returned. On error, -1.
 */
int mysetsock(int sock){

    int optval = 1;                 // optval for setsockopt
    int retval = 0;
    
    /* Set IP_HDRINCL option for socket to enable user specified IP headers. */
    if (setsockopt(sock, IPPROTO_IP, IP_HDRINCL, (int *)&optval, sizeof(optval)) == -1){
        fprintf(stderr,"mypmtud: IP_HDRINCL setting failure (IPv4)\n");
        return -1;
    }

    /* Disable system Path MTU discovery restriction on this socket. */
    optval = IP_PMTUDISC_PROBE; // enable outgoing packets with size more then detected path MTU
    if (setsockopt(sock, IPPROTO_IP, IP_MTU_DISCOVER, (int *)&optval, sizeof(optval)) == -1){
        fprintf(stderr,"mypmtud: IP_MTU_DISCOVER (disable) setting failure (IPv4)\n");
        return -1;
    }

    return retval;
}

/**
 * Set socket options for AF_INET6 socket required by this PMTUD (for IPv6) implementation.
 * For see particular setsockopt() callings in function.
 * @param sock Socket descriptor.
 * @return On success, zero is returned. On error, -1.
 */
int mysetsock6(int sock){

    int optval = 0;
    int retval = 0;
    
    // socket traffic filter
    struct icmp6_filter  myfilt;

    ICMP6_FILTER_SETBLOCKALL(&myfilt);
    ICMP6_FILTER_SETPASS(ICMP6_PACKET_TOO_BIG, &myfilt); 
    ICMP6_FILTER_SETPASS(ICMP6_ECHO_REPLY, &myfilt);
    ICMP6_FILTER_SETPASS(ICMP6_ECHO_REQUEST, &myfilt);
    retval = setsockopt(sock, IPPROTO_ICMPV6, ICMP6_FILTER, &myfilt, sizeof(myfilt));
    
    // set time to live, for outgoing packets (IPv6 header field)
    optval = TTL;
    retval = setsockopt(sock, IPPROTO_IPV6, IPV6_UNICAST_HOPS, (int *)&optval, sizeof(optval));

    // ignore system Path MTU discovery
    optval = IPV6_PMTUDISC_PROBE; 
    retval = setsockopt(sock, IPPROTO_IPV6, IPV6_MTU_DISCOVER, (int *)&optval, sizeof(optval));

    return retval;
}

/**
 * Function for validating incoming packet as ICMP Unreachable message (type 3)
 * with code Fragmentation needed (code 4) and checking its relation to last
 * ICMP Echo Request message sent by mypmtud.
 * For more details see RFC 792 ( INTERNET CONTROL MESSAGE PROTOCOL).
 * @param recvICMP Pointer to payload of received packet.
 * @param size Size of payload.
 * @param pid Process id of mypmtud which is also id for all sent Echo Requests.
 * @param seq_num Sequnce number of last sent Echo Request ICMP message.
 * @return Returns -1 for: invalid ICMP type 3, code 4 message,
 *   invalid size of received ICMP message (probably not at all ICMP message),
 *   unexpected id or sequence in echo request (in data field).
 *   On success return 0 or next-hop MTU if specified (in unused field).
 */
int ck_frag_needed(struct icmphdr *recvICMP, unsigned short size, unsigned short pid, short seq_num){

    int retval = -1;
    
    /* unreachable + fragmented needed:
     *  |--8B ICMP header--|--IP header + 64 bits of original data--|
     * ...so expected:
     *  8 bytes of received ICMP msg + IP header of our request + 8 bytes of our echo request ICMP msg
     */

    // set pointers
    struct iphdr *sendIP = (struct iphdr*)((char *)recvICMP + sizeof(struct icmphdr));
    struct icmphdr *sendICMP = (struct icmphdr*)((char *)sendIP + (sendIP->ihl << 2));

    // check for expected size (see ICMP spec.)
    if (size != (sizeof(struct icmphdr)<<1) + (sendIP->ihl << 2)) return -1;


    // check values in ICMP headers
    if (recvICMP->type == ICMP_DEST_UNREACH &&
        recvICMP->code == ICMP_FRAG_NEEDED &&
        sendICMP->type == ICMP_ECHO &&
        sendICMP->code == 0 &&
        sendICMP->un.echo.id == pid &&
        sendICMP->un.echo.sequence == seq_num)
    {
        // valid unreachable + frag. needed ICMP message detected
        // check Next-Hop MTU field
        retval = (ntohs(recvICMP->un.frag.mtu) > 0) ? ntohs(recvICMP->un.frag.mtu) : 0;
    }

    return retval;
}

/**
 * Function for validating incoming packet as expected ICMPv6 Packet Too Big error message.
 * @param recvICMP6 Pointer to payload of incoming packet (IPv6).
 * @param size Size of payload.
 * @param pid Process id of mypmtud which is also id for all sent Echo Requests.
 * @param seq_num Sequnce number of last sent Echo Request ICMP message.
 * @return On success, returns next-hop MTU from corresponding field in Packet Too Big message.
 *  On error, returns -1.
 */
int ck_packet_too_big(struct icmp6_hdr *recvICMP6, unsigned short size, unsigned short pid, short seq_num){

    int retval = -1;

    // set pointers
    struct ip6_hdr *sendIP6= (struct ip6_hdr*)((char *)recvICMP6 + sizeof(struct icmp6_hdr));
    struct icmp6_hdr *sendICMP6 = (struct icmp6_hdr*)((char *)sendIP6 + MIN6_HDR);
    
    // check values in ICMPv6 headers
    if (recvICMP6->icmp6_type == ICMP6_PACKET_TOO_BIG &&
        //recvICMP6->icmp6_code == 0 // ignored by receiver, rfc 4443
        sendICMP6->icmp6_type == ICMP6_ECHO_REQUEST &&
        sendICMP6->icmp6_code == 0 &&
        sendICMP6->icmp6_id == pid &&
        sendICMP6->icmp6_seq == seq_num)
    {
        // valid Packet Too Big message as reaction to my echo request detected
        retval = (int)ntohl(recvICMP6->icmp6_mtu);
    }

    return retval;
}

/**
 * Main function for (my)Path MTU Discovery for IPv4.
 * @param sock Socket descriptor.
 * @param destAddr Remote host address (end of tested path).
 * @param usrLimit Max. value for tested MTU size.
 * @return Discovered MTU.
 */
int pmtud(int sock, struct sockaddr_in *destAddr, int usrLimit){
    
    errno = 0;
    
    int recvDataSize = 0;           // bytes counter for recvfrom()
    char recvBuf[IP_MAXPACKET];     // buffer for incoming messages

    int recvICMPsize = 0;           // size of received ICMP msg
    
    short int seq_num = 1;          // init sequence number for ICMP message
    unsigned short pid = getpid();  // id for ICMP message (echo)
    int fragNeededTmp=0;            // for ck_frag_needed()'s return value

    // counters for stats
    int timeoutcnt = 0;             // timeout counter
    int total_size = 0;             // counter of sent data
    
    bool waiting = true;            // waiting flag for timer
    
    /* Variables for select()*/
    fd_set rfds;
    struct timeval tv;
    int select_retval;

    /* Select initial values for searching (based on RFC 4821 section 7.). */
    unsigned short search_low = LOW4(usrLimit); // see header file
    unsigned short search_high = usrLimit;      // user defined max. tpmtu
    unsigned short tpmtu = 0;                   // tested path MTU
    unsigned short eff_mtu = 0;                 // last effective MTU

    /* Pointers for packet parsing. */
    struct iphdr *sendIP, *recvIP;
    struct icmphdr  *sendICMP, *recvICMP;

    /* Set additional options for socket. */
    int err = mysetsock(sock);
    if (err < 0){
        return -1;
    }

    if (VERBOSE) setDebugHead();
    
    // main loop for detection
    while(search_low <= search_high){

        errno = 0;
        
        // set tested MTU size, use value from Frag. needed flag if there is any
        tpmtu = (fragNeededTmp > 0 ) ? fragNeededTmp : (search_low + search_high) >> 1;
            
        /* Allocate IPv4 datagram. And set pointers:
         *
         *                      datagram (MTU)
         *   |------------|--------------------------------------|
         *   ^            ^
         *   *sendIP points to start of IP header
         *                ^
         *                *sendICMP points to start of ICMP header 
         */
        if ((sendIP = (struct iphdr*)malloc(tpmtu)) == NULL){
            fprintf(stderr,"mypmtud: pmtud(): malloc() failure occured\n");
            return -1;
        }
        
        sendICMP = (struct icmphdr*)((char*)sendIP + sizeof(struct iphdr));
        memset(sendIP,0,tpmtu);
    
        /* Fill IP header, some fields will fill in kernel */
        sendIP->version = 4;            // IPv4
        sendIP->ihl = 5;                // minimum IP header size: 5 * 32bit (4 bytes) = 160bits = 20 bytes
        sendIP->tos = 0;                // type of service (0x00 is normal priority)
        sendIP->tot_len = htons(tpmtu); // total length, with network byte order correction
        sendIP->id = 0;                 // dont care
        sendIP->frag_off = ntohs(IP_DF);// set DF flag
        sendIP->ttl = TTL;              // time to live
        sendIP->protocol = IPPROTO_ICMP;// ICMP protocol
        sendIP->check = 0;              // go ahead kernel...
        sendIP->saddr = 0;              // I choose you... KERNEL
        sendIP->daddr = destAddr->sin_addr.s_addr; 
        

        /* Fill ICMP header */
        sendICMP->type = ICMP_ECHO;             // type
        sendICMP->code = 0;                     // code
        sendICMP->checksum = 0;                 // for computing checksum in the end this field should be zero
        sendICMP->un.echo.id = pid;             // id
        sendICMP->un.echo.sequence = seq_num++; // sequence number

        // copmplete icmp header with checksum from whole ICMP message
        sendICMP->checksum = cksum((unsigned short*)sendICMP, tpmtu - sizeof(struct iphdr));

        /* Send ICMP echo request */

        if (VERBOSE){
            fprintf(stdout,"%d\t%d\t%d\t%d\t%d\t", seq_num-1, pid, search_low, search_high, tpmtu);
            fflush(stdout);
        }
            
        if (sendto(sock, sendIP, tpmtu, 0, (struct sockaddr *)destAddr, sizeof(*destAddr))<0){
            /* Error occured */
            if (errno == EMSGSIZE){
                /* Cannot send message due to size, check MTU on your network interface */
                search_high = tpmtu -1; // search values correction
                fragNeededTmp = 0;
                free(sendIP);           // we will allocate new IP packet with different size

                if (VERBOSE) fprintf(stdout,"EMSGSIZE sendto()\n");

                if (search_low > search_high)
                    // MTU detected
                    break;
                    
                // try send next ICMP message with new size (tpmtu)
                continue;
            }
            else{
                fprintf(stderr,"error: sendto() failure occured (%d)\n", errno);
                return -1;
            }
        }

        total_size += tpmtu;
    
        /* Set timeout using select. Code based on man pages, see man select (example). */
        waiting = true;

        FD_ZERO(&rfds);
        FD_SET(sock, &rfds);

        tv.tv_sec = TIMEOUT;        // default timeout are 3 seconds
        tv.tv_usec = 0;
            
        while(waiting){

            select_retval = select(sock+1, &rfds, NULL, NULL, &tv);
            if (select_retval == -1){
                // select failure
                perror("mypmtud: pmtud: select()");
                return -1; 
            }
            else if (select_retval && FD_ISSET(sock, &rfds)){
                // read data from socket
                recvDataSize = recvfrom(sock, recvBuf, IP_MAXPACKET, 0, NULL, NULL);
                if (recvDataSize<0){
                    // recvfrom failure
                    fprintf(stderr,"error: recvfrom() failure occured\n");
                    return -1;
                }

                // set pointers
                recvIP = (struct iphdr*) recvBuf;
                recvICMP = (struct icmphdr *)((char *)recvIP + (recvIP->ihl << 2));

                // check for ECHO reply
                if (recvICMP->type == ICMP_ECHOREPLY &&
                    recvICMP->un.echo.id == pid &&      
                    recvICMP->un.echo.sequence == seq_num-1){

                    // valid echo reply detected
                    search_low = tpmtu + 1;
                        
                    fragNeededTmp = 0;          // reset value
                    eff_mtu = tpmtu;            // set effective MTU
                    
                    if (VERBOSE)
                        fprintf(stdout,"OK (reply received)\n");

                    waiting = false;
                }
                else{
                    /* check UNREACHABLE type 3, code 4 */

                    // check length of received datagram, expected ICMP unreachable message:
                    // 8 bytes header + IP header + 64 bit original data

                    recvICMPsize = (recvDataSize - (recvIP->ihl << 2));
                    
                    if (recvICMPsize >= (sizeof(struct icmphdr)<<1) + sizeof(struct iphdr))
                    {
                        fragNeededTmp = 0;
                        
                        // check unreachable msg + frag. needed code
                        fragNeededTmp = ck_frag_needed(recvICMP, (unsigned short)recvICMPsize, pid, seq_num-1);

                        if (fragNeededTmp >= 0){
                            /* 'Fragmentation needed' detected. */

                            // next-hop MTU can't be more then search_high
                            if (fragNeededTmp > search_high)
                                fragNeededTmp = 0;
                            
                            // search values correction
                            search_high = (fragNeededTmp > 0) ? (unsigned short)fragNeededTmp : tpmtu-1;

                            if (VERBOSE) fprintf(stdout,"FN (%d)\n", fragNeededTmp);

                            waiting = false;
                        }                   
                    }
                    // else 
                    //  received something unexpected, neither expecting UNREACHABLE or ECHOREPLY
                    //  keep timer alive
                    continue;
                }
            }
            else{
                // timeout, no valid ICMP response within timer time
                timeoutcnt++;
                fragNeededTmp = 0;

                if (VERBOSE) fprintf(stdout,"TIMEOUT\n");

                // BC, congestion detection not implemented

                search_high = tpmtu - 1;    // search values correction

                waiting = false;
            }
        }// waiting loop end

        // free IP packet
        free(sendIP);

    }// main loop end

    if (VERBOSE) printStats(timeoutcnt, total_size, eff_mtu);

    return eff_mtu;
}

/**
 * Main function for (my)Path MTU Discovery for IPv6.
 * @param sock Socket descriptor.
 * @param destAddr Remote host address (end of tested path).
 * @param usrLimit Max. value for tested MTU size.
 * @return Discovered MTU.
 */
int pmtud6(int sock, struct sockaddr_in6 *destAddr, int usrLimit){

    errno = 0;
    
    int recvICMP6size = 0;          // bytes counter for recvfrom()
    char recvBuf[IP_MAXPACKET];     // buffer for incoming messages

    short int seq_num = 1;          // sequence number for ICMP message
    unsigned short pid = getpid();  // id for ICMP message (echo)

    // counters for stats
    int timeoutcnt = 0;             // timeout counter
    int total_size = 0;             // counter of sent data
    
    bool waiting = true;            // timer flag

    int PacketTooBigTmp = 0;        // for ck_packet_too_big()'s return value

    /* Select initial values for searching (RFC 4821 section 7.) */
    unsigned short search_low = LOW6(usrLimit);     // see header file
    unsigned short search_high = usrLimit;          // user limit for max.tested size
    unsigned short tpmtu = 0;                       // size of tested MTU
    unsigned short eff_mtu = 0;                     // last effective MTU

    /* Set additional options for socket. */
    int err = mysetsock6(sock);
    if (err < 0){
        fprintf(stderr,"mypmtud: setsockopt(faiure) for AF_INET6 socket\n");
        return -1;
    }
    
    /* Variables for select()*/
    fd_set rfds;
    struct timeval tv;
    int select_retval;

    /* Pointers for packet parsing. */
    struct icmp6_hdr *sendICMP6, *recvICMP6;

    if (VERBOSE) setDebugHead();
    
    // main loop for detection
    while(search_low <= search_high){

        errno = 0;
        
        // set MTU size 
        tpmtu = (PacketTooBigTmp > 0 ) ? PacketTooBigTmp : (search_low + search_high) >> 1;

        if (!(sendICMP6 = (struct icmp6_hdr*)malloc(tpmtu-MIN6_HDR)))   {
            fprintf(stderr,"mypmtud:pmtud6: malloc error\n");
            return -1;
        }
        memset(sendICMP6,0,tpmtu-MIN6_HDR);
        
        /* Fill ICMP header */
        sendICMP6->icmp6_type = ICMP6_ECHO_REQUEST; 
        sendICMP6->icmp6_code = 0;                  
        sendICMP6->icmp6_id = pid;                  
        sendICMP6->icmp6_seq = seq_num++;
        sendICMP6->icmp6_cksum = cksum((unsigned short *)sendICMP6, tpmtu-MIN6_HDR);

        if (VERBOSE){
            fprintf(stdout,"%d\t%d\t%d\t%d\t%d\t", seq_num-1, pid, search_low, search_high, tpmtu);
            fflush(stdout);
        }

        /* Send ICMP6 echo request */
        if (sendto(sock, sendICMP6, tpmtu-MIN6_HDR, 0, (struct sockaddr *)destAddr, sizeof(*destAddr))<0){
            /* Error occured */
            if (errno == EMSGSIZE){
                /* Cannot send message due to size, check MTU on your network interface */
                search_high = tpmtu -1; // search values correction
                PacketTooBigTmp = 0;
                free(sendICMP6);        // we will allocate new IP packet with different size

                if (VERBOSE) fprintf(stdout,"EMSGSIZE sendto()\n");
                
                if (search_low > search_high)
                    // MTU detected
                    break;
                    
                // try send next ICMP6 message with new size (tpmtu)
                continue;
            }
            else{
                fprintf(stderr,"error: sendto() failure occured (%d)\n", errno);
                return -1;
            }
        }

        total_size += tpmtu;
        
        /* Set timeout using select. Code based on man pages, see man select (example). */
        waiting = true;

        FD_ZERO(&rfds);
        FD_SET(sock, &rfds);

        tv.tv_sec = TIMEOUT;        // default timeout are 3 seconds
        tv.tv_usec = 0;
                    
        while(waiting){

            select_retval = select(sock+1, &rfds, NULL, NULL, &tv);
            if (select_retval == -1){
                // select failure
                perror("mypmtud: pmtud: select()");
                return -1; 
            }
            else if (select_retval && FD_ISSET(sock, &rfds)){
                // read data from socket, received data do nt icnlude IPv6 header
                recvICMP6size = recvfrom(sock, recvBuf, IP_MAXPACKET, 0, NULL, NULL);
                if (recvICMP6size<0){
                    // recvfrom failure
                    fprintf(stderr,"error: recvfrom() failure occured\n");
                    return -1;
                }

                // set pointer
                recvICMP6 = (struct icmp6_hdr *)recvBuf;

                // check for ECHO reply
                if (recvICMP6->icmp6_type == ICMP6_ECHO_REPLY &&
                    recvICMP6->icmp6_id == pid &&       
                    recvICMP6->icmp6_seq == seq_num-1){

                    // valid echo reply detected
                    search_low = tpmtu + 1;
                    
                    PacketTooBigTmp = 0;        // reset value
                    eff_mtu = tpmtu;            // set effective MTU
                    
                    if (VERBOSE) fprintf(stdout,"OK (reply received)\n");

                    waiting = false;
                }
                else{
                    /* Expecting Packet Too Big message. */

                    // check length of received datagram, expected ICMP unreachable message:
                    // 8 bytes header + as much of invoking packet as possible without exceeding minimum IPv6 MTU (1280B, rfc 2460)
                    if (recvICMP6size <= MIN6_OFFIC_MTU)
                    {
                        PacketTooBigTmp = 0;
                        
                        // check unreachable msg + frag. needed code
                        PacketTooBigTmp = ck_packet_too_big(recvICMP6, (unsigned short)recvICMP6size, pid, seq_num-1);

                        if (PacketTooBigTmp >= 0){
                            /* 'Packet Too Big' detected. */
                                
                            // search values correction
                            search_high = PacketTooBigTmp;

                            if (VERBOSE) fprintf(stdout,"PTB (%d)\n", PacketTooBigTmp);

                            waiting = false;
                        }                   
                    }
                    // else 
                    //  received something unexpected, neither expecting UNREACHABLE or ECHOREPLY
                    //  keep waiting
                    continue;
                }
            }
            else{
                // timeout, no valid ICMPv6 response within timer time
                timeoutcnt++;
                PacketTooBigTmp = 0;
                
                if (VERBOSE) fprintf(stdout,"TIMEOUT\n");
                
                search_high = tpmtu - 1;    // search values correction

                waiting = false;
            }
        }// waiting loop end

        // free IP packet
        free(sendICMP6);

    }// main loop end
    
    if (VERBOSE) printStats(timeoutcnt, total_size, eff_mtu);
    
    return eff_mtu;
}

/*** End of file mypmtud.c */
