/***
 * 
 * @file    main.c
 * @author  Michal Starigazda, xstari01@stud.fit.vutbr.cz
 * @date    12.10.2012
 * 
 * Path MTU Discovery for course ISA, FIT VUT Brno
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <limits.h>
#include <errno.h>
#include "mypmtud.h"

#define ROOT_UID 0

/**
 * Structure for CLI arguments
 */
typedef struct params {
    bool help;  // usage flag
    int max;    // max size (bytes) for testing
    char *addr; // address
    int v;      // verbose flag
} TParams;

/**
 * Convert and validate a string to number (value for max MTU).
 * @param str Pointer to string 'number'.
 * @param val Pointer to storage for number.
 * @return True for success, else false.
 */
bool validMAX(char *str, int *val){
    int base = 10;
    long value = 0;
    char *end;

    errno = 0;
    value = strtol(str, &end, base);

    // Check errors
    if ((errno == ERANGE && (value == LONG_MAX || value == LONG_MIN))
        || (errno != 0 && value == 0))
    {
        perror("Invalid 'max' value");
        return false;
    }
    // Check range
    if (value < 0)
    {
        fprintf(stderr, "Invalid value for max. tested MTU!\n");
        return false;
    }

    if (value > IP_MAXPACKET){
        printf("Maximal tested value set to max. IP packet size: %d\n", IP_MAXPACKET);
        value = IP_MAXPACKET;
    }
    
    // Check there is not any trash after digit in string
    if (*end != '\0')
    {
        fprintf(stderr, "Invalid max number, try only digits.\n");
        return false;
    }

    *val = (int)value;
    
    return true;
}

/**
 * Processing of CL's arguments.
 * @param params Structure for arguments.
 * @param argc Number of arguments.
 * @param argv Array of arguments.
 * @return False for wrong arguments, esle true.
 */
bool getParams(TParams *params, int argc, char **argv){
    bool retval = true;

    switch(argc){
        case 2:
            // Expecting ./mypmtud { { -h | --help } | address }
            if (strcmp(argv[1], "-h") == 0 || strcmp(argv[1], "--help") == 0)
            {// -h, --help
                params->help = true;
            }
            else
            {// address
                params->addr = argv[1];
            }
            break;
        case 3:
            // Expecting ./mypmtud address  { -v | --verbose }
            if (strcmp(argv[2], "-v") == 0 || strcmp(argv[2], "--verbose") == 0)
            {// -v, --verbose
                setVerbose();
                params->v = ON;
            }
            else retval = false;
            
            // address
            params->addr = argv[1];
            break;
        case 5:
            // Expecting ./mypmtud -m max address { -v | --verbose }
            if (strcmp(argv[4], "-v") == 0 || strcmp(argv[4], "--verbose") == 0)
            {// -v, --verbose
                setVerbose();
                params->v = ON;
            }
            else{
                retval = false;
                break;
            }
        case 4:
            // Expecting ./mypmtud -m max address
            if (strcmp(argv[1], "-m") == 0 && validMAX(argv[2], &params->max))
            {// option '-m' and max value are alright
                params->addr = argv[3];
                break;
            }
        default:
            retval = false;
    }// switch
    
    return retval;
}

/**
 * Print usage.
 */
void printHelp(){
    printf( "--------------------------------------------------------\n"
            "Usage:\tmypmtud [-m max] address [ -v | --verbose ]\n"
            "--------------------------------------------------------\n"
            "Description:\n"
            "\t-m max\n"
            "\t\tset maximal size (bytes) of tested MTU\n"
            "\taddress\n"
            "\t\tIPv4, IPv6, DNS format of address\n"
            "\t-v, --verbose\n"
            "\t\tactivate verbose mode\n"
            "--------------------------------------------------------\n");
}

/** MAIN */
int main(int argc, char **argv){

    /* Check for root privileges - needed for raw socket. */
    if (getuid() != ROOT_UID){
        fprintf(stderr,"mypmtud: Access denied, try again with root privileges.\n");
        return EXIT_FAILURE;
    } 
    
    
    TParams params = {false, DEFAULT_MAX, NULL};
    
    /* Parameters check. */
    if (getParams(&params, argc, argv)){
        if (params.help)
        {// Print usage of program and exit
            printHelp();
            return EXIT_SUCCESS;
        }
        else
        {// lets my Path MTU Discovery ROCKS

            if (params.v){
                printf( "--------------------------------------------------------\n"
                        "Max. tested MTU size: %d \tAddr: %s\n"
                        "--------------------------------------------------------\n",
                        params.max, params.addr);
            }
    
            int mtu = 0;
            
            /* Run my path MTU discovery. */
            if (discoverMTU(&mtu, params.addr, params.max))
            {// print resume
                printf("resume: %d bytes\n", mtu);
            }
            else
            {// something bad happened and MTU discovery failed
                return EXIT_FAILURE;
            }
        }
    }
    else
    {// Wrong command line arguments
        fprintf(stderr,"mypmtud: Invalid CLI option, see -h or --help for more. \n");
        return EXIT_FAILURE;
    }
    
    return EXIT_SUCCESS;
}
/*** END OF FILE main.c **/
