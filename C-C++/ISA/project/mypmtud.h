/***
 * 
 * @file    mypmtud.h
 * @author  Michal Starigazda, xstari01@stud.fit.vutbr.cz
 * @date    17.11.2012
 * 
 * Path MTU Discovery for course ISA, FIT VUT Brno
 * 
 */

#ifndef MYPMTUD_H
  #define MYPMTUD_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <string.h>
#include <netdb.h>
#include <unistd.h>
#include <netinet/ip.h>
#include <netinet/ip_icmp.h>
#include <netinet/ip6.h>
#include <netinet/icmp6.h>
#include <errno.h>
#include <arpa/inet.h>

/* Default values */
#define DEFAULT_MAX     1500  // default max MTU for this program
#define TTL             30    // hops
#define TIMEOUT         3     // timeout 3s

/* Known values */
#define MIN4_MTU        28   // min. IPV4 header (20B) + ICMPv4 header (8B)
#define MIN6_HDR        40   // min. IPv6 header size
#define MIN6_MTU        48   // min. IPv6 header (40B) + ICMPv6 header (8B)

/* Minimla official MTU () */
#define MIN4_OFFIC_MTU  68    // max. IPv4 header (60B) + min. fragment size
#define MIN6_OFFIC_MTU  1280  // RFC 2460

#define ON  1
#define OFF 0

/**
 * Turn on verbose mode for all important functions (by setting global flag.)
 */
void setVerbose();

/**
 * Function for dicovering max path MTU.
 * @param result Pointer to store a discovered value.
 * @param straddr String containing address (unvalidated format).
 * @param max_limit Limit for maximal size of tested MTU
 * @return True for successful discovery, else false.
 */
bool discoverMTU(int *result, char *straddr, int max_limit);

/**
 * Main function for (my)Path MTU Discovery for IPv4.
 * @param sock Socket descriptor.
 * @param destAddr Remote host address (end of tested path).
 * @param usrLimit Max. value for tested MTU size.
 * @return Discovered MTU.
 */
int pmtud(int sock,  struct sockaddr_in *destAddr, int usrLimit);

/**
 * Main function for (my)Path MTU Discovery for IPv6.
 * @param sock Socket descriptor.
 * @param destAddr Remote host address (end of tested path).
 * @param usrLimit Max. value for tested MTU size.
 * @return Discovered MTU.
 */
int pmtud6(int sock, struct sockaddr_in6 *destAddr, int usrLimit);
 
#endif
