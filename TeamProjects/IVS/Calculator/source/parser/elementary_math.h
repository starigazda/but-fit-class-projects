/**
 * @file elementary_math.h
 * 
 * @brief The header file with mathematical functions
 * @author Stanislav Laznicka
 */
 
namespace CubeSoft {
	
	namespace Calculator {
		
		double OP_PLUS(double, double);
		double OP_MINUS(double, double);
		double OP_MULTIPLY(double, double);
		double OP_DIVIDE(double, double);
		unsigned long int OP_FACTORIAL(unsigned long int);
		double OP_POWER(double, unsigned long);
		double myAbs(double, double);
		double OP_LOGARITHM(double, double, double);
	}
}
/*** End of file elementary_math.h ***/
