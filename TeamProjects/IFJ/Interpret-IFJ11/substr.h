//Ján Porhinčák (xporhi00)

#ifndef SUBSTR_H
	#define SUBSTR_H

	#ifndef STR_H
  		#include "str.h"
  		#define STR_H
	#endif

string* substr(string*, double, double, int*);
int borderTransf(int, double);
string* concat(string*, string*, int*);
#endif
