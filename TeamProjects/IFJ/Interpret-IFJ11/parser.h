/**
 *	Project:	Interpret of the Lua programming language derivate IFJ11.
 *	File:		parser.h
 *	Date:		Fri Dec  2 12:13:21 CET 2011
 *	Author:		Michal Srubar,	xsruba03@stud.fit.vutbr.cz	
 *
 */

#ifndef PARSER_H
	#define PARSER_H

	#ifndef STR_H
		#include "str.h"
	#endif
	#ifndef IAL_H
		#include "ial.h"
	#endif
	#define FLUSH		1
	#define NEXT		1
	#define ZERO		0
	#define UNDECLARED 	-1

	#define VAR			0
	#define FUNC		1
	#define FUNC_VAR	2
	#define VAR_ID		3
	#define VAR_ARG		1

	// analysers
	#define PARSER 				1
	#define PRECEDENCE_PARSER	2

/**
 * Vycet typov tokenov. Poradie musi zostat zachovane, vyuziva sa pri urcovani
 * ulozitelnych terminalov do zasobniku precedencnej analyzy.
 */
typedef enum tokenType {				
	ID,					// identifikator
	
  	// vycet klucovych slov
  	KW_DO,				// do	
  	
  	KW_NIL,				// nil, zaroven konstanta
  	//KW_TRUE,			// true, zaroven konstanta
   	//KW_FALSE,			// false, zaroven konstanta 	
  	KW_THEN,			// then
  	KW_FUNCTION,		// function
 	KW_MAIN,			// main
  	KW_ELSE,			// else
  	KW_END,				// end						// 6
  	KW_IF,				// if			7
	KW_LOCAL,			// local
  	KW_READ,			// read
  	KW_RETURN,			// return
  	KW_WHILE,			// while
  	KW_WRITE,			// write		12
  	
  	// vnutorne funkcie
  	KW_TYPE,			// type
  	KW_FIND,			// find
  	KW_SORT,			// sort
  	KW_SUBSTR,			// substr		16
  	
  	//formaty
  	FORMAT_L,			// *l
  	FORMAT_N,			// *n
  	FORMAT_A,			// *a
  	
  	// rezervovane slovo
	RESERVED_WORD, 					//	17
	
	// vycet rezervovanych slov
  	RW_BREAK,			// break
  	RW_ELSEIF,			// elseif
  	RW_FOR,				// for
  	RW_IN,				// in
  	RW_REPEAT,			// repeat
  	RW_UNTIL, 			// until		//23

    // vycet typov konstant
  	NUMBER,
  	BOOL,
  	STRING,							// 26
  	
  	// vycet operatorov
  	KONKAT,				// ..
  	LESS,				// <
  	GREATER,			// >	
  	LESS_EQ,			// <=
  	GREATER_EQ,			// >=
  	NOT_EQ,				// ~=
  	EQ,					// ==
  	ASSIG,				// =	
  	ADD,				// +
  	SUB,				// -						// 36
  	MULTIP,				// *
  	DIV,				// /
  	POWER,				// ^			40
  	
  	// operatory rozsirenia LOGOP
	AND,				// and
  	OR,					// or
  	NOT,				// not			43
  	
  	// others
	SEMICOLON,			// ;
	COMMA,				// ,
	R_PAR,				// )
	L_PAR,				// (						// 47
	
	END_OF_FILE,		// EOF	
	
	// pomocne typy exprParser
	E,					// neterminal preced. analyzy
	L,					// neterminal preced. analyzy
	FUNCT,
	POM,
	EMPTY				// 53
} tTokenType;


/**
 * Structure token defines current working token.
 */
typedef struct token {
	// a datovy typ jako nil, number, string je kde ???
	
	tTokenType type;	// e.g. ID = indentifier, KD_DO = key word do, ...
	string key;			// key for searching and saving in hash table 
	TValue data;			// data of token (e.g. "hello" for a = "hello" or name
						// of the function ...)
} tToken;

	#ifndef ILIST_H
		#include "ilist.h"
	#endif


	tListOfInstr instrList;	// list of instructions

	// pointer to linear list of function, every item contains pointer into 
	// table of symbols for the function
	TFunctionList *p_funcList;	

	/**
	 * Interpret status structure. This structure can be changed only if you 
	 * put it as parameter to a function from main.c.
	 */
	typedef struct progStat {
		FILE *file;			// pointer to source code interpreted file
		char *sourceName;	// name of the processing source file 
		char *intprName;	// name of the binary interpret
		int ecode;			// finall error code 

			
	} TProgStat;

	/**
	 * Parser and precedence analyser status structure.
	 */
	typedef struct parserStat {
		bool inFuncBody;	// signalize if I'm in function
		bool write;			// signalize if I'm in inter function write
		bool main;			// signalize if there is the main function
		bool local;			// signalize if we define a variable
		bool white;

		char *sourceFile;	// name of the processing source file 

		int line;			// working line of the interpret
		int ecode;			// finall error code 
		int who;			// who currently uses this strucutre? (PARSER or PRECE...)

		void *addrOfFunLabel;
		
		tData *p_actData;  // pointer to last data heading to intstruction

		tToken *p_token;	// working token which process the interpret

		// pointer to current working item of function list
		TFunctionItem *p_workFuncItem;

		// working table of the symbols
		tHTable *p_workTable;	

	} TParserStat;

	void generateInstruction(int instType, void *addr1, void *addr2, void *addr3);
	//bool parser(TProgStat *p_prgStat);
	void parser(TProgStat *p_prgStat);

#endif
