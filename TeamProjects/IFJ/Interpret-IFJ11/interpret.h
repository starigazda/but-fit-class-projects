/**
 * @file - interpret.h
 * @author - Miroslav Slivka, xslivk02
 * @date - 17.11.2011
 */
#ifndef INTERPRET_H
  #define INTERPRET_H

/* Makra */

/**
 * definicie pre instrukciu read
 * N pre *n
 * L pre *l
 * A pre *a
 */
#define N -10  
#define L -11
#define A -12
#define READTYPE_ERROR -13

#define ARG_COUNT 3   //offset poctu argumentov fcie na stacku
#define F_DEST 2      //offset adresy kde ulozit navratovu hodnotu fcie na stacku

#define S_SIZE 50    /* velkost zasobniku na zaciatku interpretacie */

/* Hlavicky funkcii pre pracu so zasobnikom. */

typedef struct {
	tData *data;                             /* data na zasobniku */
	int top;                                /* index prvku na vrcholu zásobníku */
	int base;						/* index zaciatku */
	int size;
	int highest;		/* ako najvyssie bol vrchol */
} tStack;

                                  
void istackInit ( tStack* s );
int istackEmpty ( const tStack* s );
int istackFull ( const tStack* s );
void istackTop ( const tStack* s, void** data);
void istackPop ( tStack*  s);
void istackPush ( tStack* s, tData data);
void istackClear(tStack *s);
void istackRealloc(tStack* s);

/* Hlavičky funkcii pre pracu so zoznamom offsetovna premenne na zasobniku */

typedef struct inData {
	 char *name;
	 int index;
	 struct inData *ptr;
 } *pTInData;
 
typedef struct tInList{ 
   int length;
   pTInData Act;
   pTInData First;
} tInList;
 
void inListInit(tInList *  );
pTInData inListInsert(tInList * , char *name,int index);
void inListSearch(tInList * ,char *name,int *index);
void inListClear(tInList * );
void inListDeleteFirst(tInList * );

/* Hlavicky funkcii pre interpret */
  
int readGetType(tData data);
void destControl(void *src, void **dest, tStack *stack, tInList *inList);
void opControl(void *src, void **dest, tStack *stack, tInList *inList);
int inter(tListOfInstr *instrList);
void generateInstruction(int instType, void *addr1, void *addr2, void *addr3);



#endif
