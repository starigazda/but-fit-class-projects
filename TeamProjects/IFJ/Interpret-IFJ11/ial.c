/** 
 *  @author hash table - Mirosslav Slivka, xslivk02
 *  @author internal functions - Jan Porhincak, xporhi00
 *  @lastChange - 20.11.2011
 *  @file - ial.c
 * ---------------------------
 */

#ifndef IAL
  #include "ial.h"
#endif


//---------------------------------------------------------------------------

/**
 * Funkcia sort vytvory kopiu vstupneho retazca a nasledne zavola funkciu shellsort(), ktora obstara samotne radenie
 * @param: strIn vstupny string
 * @param: Error v pripade chyby je prislusna chybova hodnota navratena pomocou tohoto ukazatela
 * @return: funkcia vracia uz zoradenu kopiu vstupneho stringu
**/
string* sort(string* strIn, int* Error)
{
	//deklaracia
	string* strOut;
	
	//osetrenie uvodnych chybovych vstupov
	if ((strIn == NULL) || (strIn->str == NULL))
		return NULL;
	
	//inicializacia a vytvorenie kopie vstupneho stringu
	strOut = malloc(sizeof(string));
	if (strOut == NULL) {
		*Error = 5;
		return NULL;
		}
	strOut->str = malloc(strIn->allocSize);
	if (strOut->str == NULL) {
		free(strOut);
		*Error = 5;
		return NULL;
		}
	strcpy(strOut->str, strIn->str);
	strOut->length = strIn->length;
	strOut->allocSize = strIn->allocSize;
	
	shellsort(strOut);
	return strOut;
}

/**
 * Funkcia zoradi znaky priamo vo vstupnom retazci
 * @param: strIn vstupny retazec, ktory bude radeny
**/
void shellsort(string* strIn)
{
	//deklarace
	int step, i, j;
	char xchg;
	//inicializacia
	step = strIn->length / 2;

	//samotne radenie metodou shell-sort
	while (step > 0) {
		for (i=step; i < strIn->length; i++) {
			j = i - step;
			while ((j >= 0) && (strIn->str[j] > strIn->str[j + step])) {
				xchg = strIn->str[j];
				strIn->str[j] = strIn->str[j + step];
				strIn->str[j + step] = xchg;
				j = j - step;
				}
			}
		step = step / 2;
		}
}

/**
 * Funkcia pre navrat indexu zaciatku hladaneho podretazca v retazci.
 * @param: P string vzoru, ktory sa bude vyhladavat
 * @param: T string textu, ktory bude prehladavany
 * @param: Error priznak chyby, pri bezchybnom stave obsahuje hodnotu 0
 * @return: index prveho znaku hladaneho retazca
**/
int KMPfind(string* P, string* T)
{
	//osetrenie vstupnych stavov
	if ((P == NULL) || (P->str == NULL) || (T == NULL) || (T->str == NULL))
		return -1;
	//navratova vynimka pri hladani prazdneho retazca
	if (!strcmp(P->str, "")) {
		return 0;
		}
	
	//deklaracia
	int fail[P->length], TInd, PInd;
	
	//inicializacia
	KMPVector(P, fail);
	TInd = PInd = 0;
	
	//cyklus prehladavania retazca Knuth-Moris-Pratt algoritmom
	while ((TInd < T->length) && (PInd < P->length)) {
		if ((PInd == -1) || (T->str[TInd] == P->str[PInd])) {
			TInd++;
			PInd++;
			}
		else
			PInd = fail[PInd];
		}
	if (PInd >= P->length)
		return TInd - P->length +1;
	else
		return -1;

}

/**
 * Funkcia naplna vektor fail, hodnotamy.
 * @param: P string vzoru, ktory sa bude vyhladavat
 * @param: T string textu, ktory bude prehladavany
 * @param: fail[] ukazatel na vektor, ktory sa bude naplnat
**/
void KMPVector(string* P, int* fail)
{
	//deklaracia //inicializacia
	int k=1, r;
	fail[0] = -1;
	
	for (; k < P->length; k++) {
		r = fail[k-1];
		while ((r > 0) && (P->str[r] != P->str[k-1])) {
			r = fail[r];
			}
		fail[k] = r+1;
		}
}

//---------------------------------------------------------------------------
int HTSIZE = MAX_HTSIZE;

/*          -------
** Rozptylovac� funkce - jej�m �kolem je zpracovat zadan� kl�� a p�id�lit
** mu index v rozmez� 0..HTSize-1.  V ide�ln�m p��pad� by m�lo doj�t
** k rovnom�rn�mu rozpt�len� t�chto kl��� po cel� tabulce.  V r�mci
** pokus� se m��ete zamyslet nad kvalitou t�to funkce.  (Funkce nebyla
** volena s ohledem na maxim�ln� kvalitu v�sledku). }
*/

int hashCode ( tKey key ) {
	int retval = 1;
	int keylen = strGetLength(&key);
	for ( int i=0; i<keylen; i++ )
		retval += key.str[i];
	return ( retval % HTSIZE );
}

/*
** Inicializace tabulky s explicitn� z�et�zen�mi synonymy.  Tato procedura
** se vol� pouze p�ed prvn�m pou�it�m tabulky.
*/

void htInit ( tHTable* ptrht ) {
     
    if(ptrht != NULL) { 
	    for(int i=0; i<HTSIZE; i++) 
	        (*ptrht)[i] = NULL;
	}

}

/* TRP s explicitn� z�et�zen�mi synonymy.
** Vyhled�n� prvku v TRP ptrht podle zadan�ho kl��e key.  Pokud je
** dan� prvek nalezen, vrac� se ukazatel na dan� prvek 
** Pokud prvek nalezen nen�, vrac� se hodnota NULL.
**
*/

tHTItem* htSearch ( tHTable* ptrht, tKey key ) {

    tHTItem *temp = NULL;

    if(ptrht != NULL) { 
        int h = hashCode(key);
        temp = (*ptrht)[h];
        while(temp != NULL) {
	    	if(strCmpString(&(temp->key),&key) == 0)
	    	    break;
	    	else {
	    		temp = temp->ptrnext;
	    	}
    	}
	}
	
    return temp;
}

/* 
** TRP s explicitn� z�et�zen�mi synonymy.
** Tato procedura vkl�d� do tabulky ptrht polo�ku s kl��em key a s daty
** data.  Proto�e jde o vyhled�vac� tabulku, nem��e b�t prvek se stejn�m
** kl��em ulo�en v tabulce v�ce ne� jedenkr�t.  Pokud se vkl�d� prvek,
** jeho� kl�� se ji� v tabulce nach�z�, aktualizujte jeho datovou ��st.
**
** Vyu�ijte d��ve vytvo�enou funkci htSearch.  P�i vkl�d�n� nov�ho
** prvku do seznamu synonym pou�ijte co nejefektivn�j�� zp�sob,
** tedy prove�te.vlo�en� prvku na za��tek seznamu.
**/

void htInsert ( tHTable* ptrht, tKey key, tData data) {

    tHTItem *temp = NULL;
   
    if(ptrht != NULL) { 
        if((temp = htSearch(ptrht,key)) == NULL) {
		    int h = hashCode(key);
	    	temp =(tHTItem *) malloc(sizeof(tHTItem));
	    	strInit(&(temp->key));
    		strCopyString(&(temp->key),&key);
    	    if(data.type == _STRING) {
				temp->data.type = data.type;
				//temp->data.declared = data.declared;
				strInit(&(temp->data.value.stringValue));
    	        strCopyString(&(temp->data.value.stringValue),&(data.value.stringValue));
			}
    	    else
    	        temp->data = data;
    	    
    	    temp->data.id = strGetStr(&(temp->key));
    		temp->ptrnext = (*ptrht)[h];
    		(*ptrht)[h] = temp;
    	}
    	else { //ak je polozka s rovnakym klucom tak aktualizujeme data
    		if(data.type == _STRING) {
				temp->data.type = data.type;
				//temp->data.declared = data.declared;
				strInit(&(temp->data.value.stringValue));
    	        strCopyString(&(temp->data.value.stringValue),&(data.value.stringValue));
			}
    	    else
    	        temp->data = data;
    	        
    	    temp->data.id = strGetStr(&(temp->key));
    	}
	}
}

/*
** TRP s explicitn� z�et�zen�mi synonymy.
** Tato funkce zji��uje hodnotu datov� ��sti polo�ky zadan� kl��em.
** Pokud je polo�ka nalezena, vrac� funkce ukazatel na polo�ku
** Pokud polo�ka nalezena nebyla, vrac� se funk�n� hodnota NULL
**
** Vyu�ijte d��ve vytvo�enou funkci HTSearch.
*/

tData* htRead ( tHTable* ptrht, tKey key ) {

    tHTItem *temp = NULL;
    
    if(ptrht != NULL) { 
        if((temp = htSearch(ptrht,key)) != NULL)
            return &(temp->data);
	}
	
	return NULL;
}

/*
** TRP s explicitn� z�et�zen�mi synonymy.
** Tato procedura vyjme polo�ku s kl��em key z tabulky
** ptrht.  Uvoln�nou polo�ku korektn� zru�te.  Pokud polo�ka s uveden�m
** kl��em neexistuje, d�lejte, jako kdyby se nic nestalo (tj. ned�lejte
** nic).
**
** V tomto p��pad� NEVYU��VEJTE d��ve vytvo�enou funkci HTSearch.
*/

void htDelete ( tHTable* ptrht, tKey key ) {

    tHTItem *temp = NULL, *temp1 = NULL;

    if(ptrht != NULL) { 
        int h = hashCode(key);
        temp = (*ptrht)[h];
        if(temp != NULL) {
            if(strCmpString(&(temp->key),&key) == 0) { //prvy prvok v zretazenom zozname
	        	(*ptrht)[h] = (*ptrht)[h]->ptrnext;
	        	if(temp->data.type == _STRING)
                        strFree(&(temp->data.value.stringValue));
                    strFree(&(temp->key));
	        	free(temp);
	        }
	        else {
                while(temp->ptrnext != NULL) {
	    	        if(strCmpString(&(temp->ptrnext->key),&key) == 0) { //prvok niekde v strede zoznamu
			        	temp1 = temp->ptrnext;
			        	temp->ptrnext = temp1->ptrnext;
			        	if(temp1->data.type == _STRING)
                            strFree(&(temp1->data.value.stringValue));
                        strFree(&(temp1->key));
			        	free(temp1);
			        	break;
			        }
	            	else {
	            		temp = temp->ptrnext;
	            	}
	            }
	    	}
		}
	}
}

/* TRP s explicitn� z�et�zen�mi synonymy.
** Tato procedura zru�� v�echny polo�ky tabulky, korektn� uvoln� prostor,
** kter� tyto polo�ky zab�raly, a uvede tabulku do po��te�n�ho stavu.
*/

void htClearAll ( tHTable* ptrht ) {

    tHTItem *temp;
    if(ptrht != NULL) { 
        for(int i=0;i<HTSIZE;i++)
            if((*ptrht)[i] != NULL) {  //nasli sme nejaku polozku
		    	while((*ptrht)[i] != NULL) {
		    		temp = (*ptrht)[i];
		    		(*ptrht)[i] = (*ptrht)[i]->ptrnext;
		    		if(temp->data.type == _STRING)
                        strFree(&(temp->data.value.stringValue));
                    strFree(&(temp->key));
		    	    free(temp);
		    	}
		    }
		}
}
