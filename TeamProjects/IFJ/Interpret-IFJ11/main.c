/**
 *	Project:	Interpret of the Lua programming language derivate IFJ11.
 *	File:		main.c
 *	Date:		Fri Dec  2 12:13:21 CET 2011
 *	Author:		Michal Srubar,	xsruba03@stud.fit.vutbr.cz	
 *
 *	Description:
 *	Main control file initialize structure progStat and it controls the entire
 *	process of the interpretation. It runs two main modules: parser and
 *	interpret.
 */

#ifndef STDLIB_H
	#include <stdlib.h>
#endif
#ifndef STDIO_H
	#include <stdio.h>
	#define STDIO_H
#endif
#ifndef BOOL_H
	#include <stdbool.h>
	#define BOOL_H
#endif
#ifndef ERRNO_H
	#include <errno.h>
	#define ERRNO_H
#endif

#ifndef PARSER_H
	#include "parser.h"
#endif 
#ifndef ERROR_H
	#include "error.h"
#endif 

#ifndef ILIST_H
	#include "ilist.h" 
#endif
#ifndef SCANNER_H
	#include "scanner.h" 
#endif
#ifndef INTERPRET_H
	#include "interpret.h" 
#endif

const char HELPMSG[] = 
"This is compiler for language IFJ11,\n"
"it is derivate of scripting language LUA and its authors\n"
"are profi-coders and never sleeping students of\n"
"Brno University of Technology - Faculty of Information Technology.\n\n"
"King of the lines Michal Starigazda\n"
"Close second king of the lines Michal hitxh Srubar\n"
"Just a regular coder Miroslav Slivka\n"
"And team vampire Jan KeNaCo Porhincak\n"
"You can find language description on website of course Formal\n"
"Languages and Compilers\n"
"Our compiler expects on the input only one parameter:\n\n"
"		./ifj11 -h\n"
"				[print this message]\n\n"
"		./ifj11 <your_source_file>\n"
"				[interprets content of <your_source_file>]\n";

			

int main(int argc, char *argv[]) {

	// programme status structure
	TProgStat stat;				

	// init programme status strucuture
	stat.file = NULL;
	stat.sourceName = argv[1];
	stat.intprName = argv[0];
	stat.ecode = EOK;

	// init function list
	TFunctionList funcList;
	p_funcList = &funcList;
	initFunctionList(p_funcList);

	// init list of instructions
	listInit(&instrList);

	// FIXME:
	// add some help stufff....

	if (argc != 2) {
		printf("%s: no input file\nTry '%s -h' for more information.\n", 
				stat.intprName, stat.intprName);
		return INTER_ERROR;
	}
	
	else if (strcmp(argv[1],"-h") == 0) {
		fprintf(stdout,"%s",HELPMSG);
		return 0;
	}

	// open source code file
	else if ((stat.file = fopen(argv[1], "r")) == NULL) {
		fprintf(stderr, "%s: %s: ", stat.intprName, argv[1]);	
		perror("");	
		return INTER_ERROR;
	}

	// init scanner 
	setSourceFile(stat.file);

	// run parser
	parser(&stat);

	if (stat.ecode == EOK) {

		// print list of instructions just for debugging
		//printList(&instrList);

		stat.ecode = inter(&instrList);
	}

	// dealocate the LIST OF INSTRUCTION
	listFree(&instrList);

	#ifdef FREE
	printf(">> freeFunctionList\n");
	#endif
	freeFunctionList(p_funcList);

	// close source code file
	if (fclose(stat.file) == EOF) {
		fprintf(stderr, "%s: %s: ", stat.intprName, argv[1]);	
		perror("");	
	}

	stat.file = NULL;

	return stat.ecode;
}




