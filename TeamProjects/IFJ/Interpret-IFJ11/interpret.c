/**
 * @file - interpret.c
 * @author - Miroslav Slivka, xslivk02
 * @date - 17.11.2011
 */

#ifndef STDIO_H
  #include <stdio.h>
  #define STDIO_H
#endif
#ifndef STDLIB_H
  #include <stdlib.h>
  #define STDLIB_H
#endif
#ifndef BOOL_H
  #include <stdbool.h>
  #define BOOL_h
#endif
#ifndef ERRNO_H
  #include <errno.h>
  #define ERRNO_H
#endif
#ifndef LIMITS_H
  #include <limits.h>
  #define LIMITS_H
#endif
#ifndef MATH_H
  #include <math.h>
  #define MATH_H
#endif


#ifndef ERROR_H
  #include "error.h"
  #define ERROR_H
#endif
#ifndef ILIST_H
  #include "ilist.h"
#endif
#ifndef IAL_H
  #include "ial.h"
#endif
#ifndef STR_H
  #include "str.h"
#endif
#ifndef INTERPRET_H
  #include "interpret.h"
#endif
#ifndef SUBSTR_H
  #include "substr.h"
#endif

//#define DEBUG_INT

/***********************************************************************
 * ************************** ZASOBNIK *********************************
 * ********************************************************************/
 
 /**
  * Inicializacia zasobniku
  * zasobnik si udrzuje index vrcholu, index dna, index najvacsej dosiahnutej 
  * vysky a aktualnu velkost zasobniku
  */
 void istackInit ( tStack* s ) {

    if (s != NULL) {
		if( ( s->data = (tData *) calloc(S_SIZE,sizeof(tData)) ) == NULL) {
			fprintf(stderr,"Error: not enough memory\n");
		    exit(INTER_ERROR);
		 }
		s->top = -1;  //inicializujeme zasobnik nastavenim vrcholu na -1
		s->base = -1;
		s->highest = -1;
		s->size = S_SIZE;
	}
}

/**
 * vracia nenulovu hodnotu ak je zasobnik prazdny inak vracia 0
 */
int istackEmpty ( const tStack* s ) {

    return s->top>-1 ? 0 :1;
}

/**
 * vracia nenulovu hodnotu ak je zasobnik plny inak vracia 0
 */
int istackFull ( const tStack* s ) {
	
    return s->top == s->size-1 ? 1 :0;
}

/**
 * Po parametri @data vracia ukazatel na data na zasobniku
 */
void istackTop ( const tStack* s, void **data) {

    if (!istackEmpty(s)) {  //je zasobnik prazdny?
        *data = (void *) &((s->data[s->top]));
	}
	else {
		*data = NULL;
	}
}

/**
 * Znizi vrchol zasobniku
 */
void istackPop ( tStack* s ) {
/*   --------
** Odstraní prvek z vrcholu zásobníku. Pro ověření, zda je zásobník prázdný,
** použijte dříve definovanou funkci stackEmpty.
** 
*/

    if (!istackEmpty(s)) {  //nie je zasobnik prazdny?
        s->top=s->top-1;  //znizime vrchol zasobniku
        
	}
}

/**
 * Pushne hodnotu @data na zasobnik
 * nastavuje index na najvyssi dosiahnuty bod
 * uvolnuje string z unionu aby tam mohol zapisat inu hodnotu
 */
void istackPush ( tStack* s, tData data ) {

    if (istackFull(s))   //je zasobnik plny?
		istackRealloc(s);
			
	
	
	s->top++;
	
	if (s->top > s->highest)
		s->highest = s->top;

	if(s->data[s->top].type == _STRING)
		strFree(&(s->data[s->top].value.stringValue));
		
	if(data.type == _STRING) {
	    strInit(&(s->data[s->top].value.stringValue));
	    strCopyString(&(s->data[s->top].value.stringValue),&(data.value.stringValue));
	    s->data[s->top].type=data.type;
	}
	else
		s->data[s->top]=data;
	//printf("top is %d ",s->top);
	//printf("data type is %d\n",s->data[s->top].type);
}

/**
 * Zvacsi zasobnik
 * Vyuziva fciu callok a postupne kopirovanie starych hodnot
 * Vyuzitie realloc-u viedlo k neocakavenemu chovaniu
 */
void istackRealloc(tStack* s) {
	    
	    s->size = s->size*2;
		
		//printf("Called realloc: size is %d data address was %p\n",s->size,(void *)s->data);
		
		tData *value = NULL;
		
	    if((value = (tData *) calloc(s->size,sizeof(tData))) == NULL) {
	        fprintf(stderr,"Error: not enough memory\n");
			exit(INTER_ERROR);
		}
		else {
			int i = 0;
			//nakopirujem polozky
			while(i<=s->top) {
			    if(s->data[i].type == _STRING) {
					strInit(&(value[i].value.stringValue));
			        strCopyString(&(value[i].value.stringValue),&(s->data[i].value.stringValue));
			        strFree(&(s->data[i].value.stringValue));
			        value[i].type = s->data[i].type;
			        value[i].id = s->data[i].id;
				}
				else
				   value[i] = s->data[i];
			    i++;
		    }
		    //uvolnim stare data
			free(s->data);		
		    s->data = value;
		}
		
		//printf("data address is %p\n",(void *)s->data);
	    //printf("data on top %d\n",s->data[s->top].type);
}

/**
 * uvolni vsetky prvky v zasobniku
 */
void istackClear(tStack *s) {
	
		if(s->top > -1) { 
			int i = 0;
		    while(i<=s->highest) {
			    if(s->data[i].type == _STRING)
			        strFree(&(s->data[i].value.stringValue));
			    i++;
		    }
		    s->top = 0;
		}
		
		free(s->data);
}

/***********************************************************************
 * *************************** ZOZNAM OFFSETOV ************************
 * ********************************************************************/
 
 /**
  * Inicializacia zoznamu
  * zoznam si uchovava svoju aktualnu dlzku
  */
void inListInit(tInList *Z) {
	Z->Act = NULL;
    Z->First = NULL;	
    Z->length = 0;
}

 /**
  * Vlozi prvok na zaciatok zoznamu
  * vracia ukazatel na vlozeny prvok ak uspech inak vracia NULL
  * @Z : zoznam
  * @name : identifikator prvku
  * @index : offset prvku
  */
pTInData inListInsert(tInList *Z, char * name, int index) {
    pTInData ptrNew;
    if ((ptrNew =(pTInData) malloc(sizeof(struct inData))) != NULL) 
    {
		
		ptrNew->name = name;
        ptrNew->index = index;      
        ptrNew->ptr = Z->First;   //ukazatel na nasledujuci prvok bude ukazovat na prvok pred ktory bol vlozeny 
        Z->First = ptrNew;        //zaciatok zoznamu bude ukazovat na vlozeny prvok
        Z->length++;
        return ptrNew;
	}
	else 
	    return NULL;
}

/**
 * Vymaze prvy prvok v zozname @Z
 */
void inListDeleteFirst(tInList *Z) {
	 pTInData temp;
	 
	 temp = Z->First;
	 Z->First = Z->First->ptr;
	 free(temp);
 }

/**
 * Vyhlada prvok zoznamu podla @name
 * po ukazateli @index vracia offset prvku na zasobniku
 * ak @index == -1 prvok sa v zozname nenachadza
 */ 
void inListSearch(tInList *Z,char *name,int *index) {
	Z->Act = Z->First;  //aktivujem prvy prvok
	   
	int i = 0;
	if(Z->Act != NULL)  //zoznam nie je prazdny 
	    while(i++ != Z->length && strcmp(Z->Act->name,name) != 0) 
	        Z->Act = Z->Act->ptr;
	if(Z->Act != NULL && strcmp(Z->Act->name,name) == 0) 
	    *index = Z->Act->index;
	else *index = -1;
}

/**
 * Uvolni cely zoznam
 */
void inListClear(tInList *Z) {
	while (Z->First != NULL)
        {
			pTInData ptrNew;
	        ptrNew = Z->First;
	        Z->First = ptrNew->ptr;
	        free(ptrNew);
		}
	Z->Act = NULL;
	Z->length = 0;
}

/***********************************************************************
 * **************************** INTERPRET ******************************
 * ********************************************************************/
 
 
int readGetType(tData data)
//ziska typ polozky 
{
	int n = 2; //pocet znakov formatu
	switch(data.type) {
		case _STRING:
	      if(strCmpnConstStr(&(data.value.stringValue),"*n",n) == 0)
	          return N;
	      else if(strCmpnConstStr(&(data.value.stringValue),"*a",n) == 0)
	          return A;
	      else if(strCmpnConstStr(&(data.value.stringValue),"*l",n) == 0)
	          return L;
	      else
	        return READTYPE_ERROR;
	      break;
	    case _NUMBER:
		  return (int) data.value.doubleValue;
		  break;
		default:
		  return READTYPE_ERROR;  
		}
}

/**
 * skontroluje kde sa v skutocnosti nachadza ciel operacie
 * ciel musi byt v adrese instrukcie, pred volanim kontrolovat ci nie je NULL
 * @param src : co kontrolujem
 * @param dest : sem sa vrati adresa na skutocny ciel
 */
void destControl(void *src, void **dest, tStack *stack, tInList *inList) {
	
	int index;  // index na stack voci base
	
	 /* kontrola I->addr3 */
	  if(((tData *)src)->type == _NIL) {
		  // premenna je na zasobniku s offsetom zo zoznamu indexov
		  inListSearch(inList,((tData *)src)->id,&index);
		  *dest = (void *) &(stack->data[stack->base+index]);
	  }
	  else if(((tData *) src)->type == _ARG) {
		  // argument funkcie, je na zasobniku pod base
		  // data.value.doubleValue = [P.c. argumentu]
		  int offset = (int) (((tData *) src)->value.doubleValue) - (int) stack->data[stack->base+ARG_COUNT].value.doubleValue;
		  *dest = (void *) &(stack->data[stack->base + offset]);
	  }
	  else {
		  // semantika mi dodala svoju premennu do ktorej mam priamo ukladat
		  // alebo sa jedna o string ktory staci vypisat
		  *dest = src;
	  }
}

/**
 *  skontroluje kde sa v skutocnosti nachadza operand
 * operand nemusi byt v adrese instrukcie vobec
 * @param src : co kontrolujem
 * @param dest : sem sa vrati adresa na skutocny operand
 */
void opControl(void *src, void **dest, tStack *stack, tInList *inList) {
	
	 /* kontrola addr operandu */
	  if(src == NULL) {
		  // hodnotu uz mam na vrchole zasobniku
		  istackTop(stack,dest);
		  istackPop(stack);
	  }
	  else 
		destControl(src,dest,stack,inList);
}

double pow(double x,double y) {

	double result = 1;
	
	for(int i=0;i<(int) y;i++) 
		result = result*x;
	
	return result;
}

/**Hlavna fcia vykonavania instrukcii
 * vykonava jednotlive instrukcie zo zoznamu instrukcii,
 * kontroluje semantiku binarnych akcii
 * vracia INTERNAL_ERROR, EX_ERROR
 * alebo EOK ak prebehla interpretacia v poriadku
 */
int inter(tListOfInstr *instrList)
{
  //aktivacia prvej instrukcie
  listFirst(instrList);
  
  //priprava zasobniku a zoznamu premennych
  tStack stack;
  istackInit(&stack);  
  tData *stackData;
  tInList inList;
  inListInit(&inList);
  
  //potrebne premenne
  int c = 0;  // nacitavanie po znaku
  int Err = 0; // predavanie chyb zo vstavanych funkcii
  void *begin = NULL, *end = NULL; //hranice pre substr
  tInstr *I;
  string *tempString = NULL;
  
  //pomocne ukazatele pre pracu z operandami instrukcie
  void *addr1= NULL;
  void *addr2= NULL;
  void *addr3= NULL;
  
  while (1)
  {
    if ((I = listGetData(instrList)) == NULL) {
		inListClear(&inList);
		istackClear(&stack);
		return INTER_ERROR;
	}
    
    switch (I->instType)
    {
    case I_STOP:
      #ifdef DEBUG_INT
      printf("Running I_STOP\n");
      #endif
      // instrukce konce programu
      istackClear(&stack);  
      inListClear(&inList);
      return EOK;
      break;
    
    case I_READ:
      #ifdef DEBUG_INT
      printf("Running I_READ\n");
      #endif
      // instrukce pro nacteni hodnoty ze std. vstupu
      if (I->addr3 == NULL) {
		  fprintf(stderr,"Runtime error: uknown error while read\n");
		  istackClear(&stack);  
          inListClear(&inList);
		  return INTER_ERROR;
	  }
	  
	  // nacitavam do premennej (id v TS) ,musi byt uz v tabulke indexov	  
	  destControl(I->addr3,&addr3,&stack,&inList);
	  
	  // na vrchole zasobnika je typ toho co mam nacitat
	  istackTop(&stack,&addr1);
	  
	  int state;
      if(( state = readGetType(*((tData *)addr1)) ) == READTYPE_ERROR) {
          fprintf(stderr,"Runtime error: bad read format\n");
          istackClear(&stack);  
          inListClear(&inList);
          return SEM_ERROR;
	  }
	  // az potom ako zistim co mam nacitat mozem popnut, pri pope sa uvolnuje stringValue z unionu
	  istackPop(&stack);
	  
	  if( ((tData*)addr3)->type == _STRING ) 
		  strFree(&(((tData*)addr3)->value.stringValue));
		  
      switch (state) {
		  case N: // *n
              if (fscanf(stdin,"%lf", &(((tData *)addr3)->value.doubleValue)) != 1) {
				  ((tData *)addr3)->type = _NIL;
			  }
			  else             
				  ((tData *)addr3)->type = _NUMBER;
              break;
          case L: // *l
              strInit(&(((tData *)addr3)->value.stringValue));
		      while((c=fgetc(stdin)) != '\n' && c != EOF) {
                  if(strAddChar(&(((tData *)addr3)->value.stringValue),c) == STR_ERROR) {
				      fprintf(stderr,"Runtime error: not enough memory\n");
					  istackClear(&stack);  
					  inListClear(&inList);
					  return INTER_ERROR;
				  }
				  //printf("%s\n",strGetStr(&(((tData *)addr3)->value.stringValue)));
                  ((tData *)addr3)->type = _STRING;
			  }
			  if(c == '\n')
				  ((tData *)addr3)->type = _STRING;
              break;
		  case A: // *a
		      strInit(&(((tData *)addr3)->value.stringValue));
              while((c=fgetc(stdin)) != EOF)
                  if(strAddChar(&(((tData *)addr3)->value.stringValue),c) == STR_ERROR) {
					  fprintf(stderr,"Runtime error: not enough memory\n");
					  istackClear(&stack);  
					  inListClear(&inList);
					  return INTER_ERROR;
				  }
              //printf("%s\n",strGetStr(&(((tData *)addr3)->value.stringValue)));
              ((tData *)addr3)->type = _STRING;
              break;
		  default: // kladne cislo
		     strInit(&(((tData *)addr3)->value.stringValue));
             for(int i = 0; i < state; i++) {
				  if((c=fgetc(stdin)) != EOF) {
					  if(strAddChar(&(((tData *)addr3)->value.stringValue),c) == STR_ERROR) {
						 fprintf(stderr,"Runtime error: not enough memory\n");
						  istackClear(&stack);  
						  inListClear(&inList);
						  return INTER_ERROR;
					  }
				  ((tData *)addr3)->type = _STRING;
				  }
			  }
			  if (((tData *)addr3)->type != _STRING)
					strFree(&(((tData *)addr3)->value.stringValue));
			  //printf("%s\n",strGetStr(&(((tData *)addr3)->value.stringValue)));
		     break;
	  }
      break;
    
    case I_WRITE:
      #ifdef DEBUG_INT
      printf("Running I_WRITE\n");
      #endif
      // instrukce pro vypis hodnoty na std. vystup
      // vsetky ciselne hodnoty vypisujem pomocou %g retazce %s
      // ak v instrukcii nebola poslana ziadna adresa snazi sa vytiahnut 
      //zo zasobnika
      if(I->addr1 != NULL) {
		opControl(I->addr1,&addr1,&stack,&inList);
	
		switch (((tData *) addr1)->type) {
			case _NUMBER:
				fprintf(stdout,"%g", ((tData*) addr1)->value.doubleValue);
				break;
			case _STRING:
				fprintf(stdout,"%s", strGetStr(&(((tData*) addr1)->value.stringValue)));
				break;
			default:
				fprintf(stderr,"Runtime error: write: bad type\n");
				istackClear(&stack);  
				inListClear(&inList);
				return EX_ERROR;
				break;
			}      
		}
      break;
    
    case I_NOT:
      #ifdef DEBUG_INT
      printf("Running I_NOT\n");
      #endif
      // instrukce pro negaci (true || nil -> false, inak true)
      if (I->addr3 == NULL) {
		  fprintf(stderr,"Runtime error: uninitialized values in logical not\n");
		  istackClear(&stack);  
          inListClear(&inList);
		  return INTER_ERROR;
	  }
	  
	  /* kontrola I->addr1 */
	  opControl(I->addr1,&addr1,&stack,&inList);
	  /* kontrola I->addr3 */
	  destControl(I->addr3,&addr3,&stack,&inList);
	  /* operacia not */ 
	  if (((tData*)addr1)->type == _NIL || ((tData*)addr1)->type == _UNKNOWN) { 
		  ((tData*)addr3)->value.boolValue = false;
	  }
	  else if(((tData*)addr1)->type == _BOOL) {
	        if (((tData*)addr1)->value.boolValue == true) {
                ((tData*)addr3)->value.boolValue = false;
			}
			else
         ((tData*)addr3)->value.boolValue = true;
	  }
      else {
		  if(((tData*)addr3)->type == _STRING)
			strFree(&(((tData*)addr3)->value.stringValue));
         ((tData*)addr3)->value.boolValue = true;
	 }
         
      ((tData*)addr3)->type = _BOOL;
      break;
    
    case I_AND:
      #ifdef DEBUG_INT
      printf("Running I_AND\n");
      #endif
      // logicky and, ak prvy op == nil || false -> prvy op inak -> druhy op
      if (I->addr3 == NULL) {
		  fprintf(stderr,"Runtime error: uninitialized values in logical and\n");
		  istackClear(&stack);  
          inListClear(&inList);
		  return INTER_ERROR;
	  }
	  
	  /* kontrola I->addr1 */
	  opControl(I->addr1,&addr1,&stack,&inList);
	  
	  /* kontrola I->addr2 */
	  opControl(I->addr2,&addr2,&stack,&inList);
	  
	  /* kontrola I->addr3 - control destination */
	  destControl(I->addr3,&addr3,&stack,&inList);
	  
	  if (((tData*)addr1)->type == _NIL)
			addr3 = addr1;
	  else if (((tData*)addr1)->type == _BOOL) {
	      if (((tData*)addr1)->value.boolValue == false)
		      addr3 = addr1;
		  }
	  else
	      addr3 = addr2;
      break;
      
    case I_OR:
      #ifdef DEBUG_INT
      printf("Running I_OR\n");
      #endif
      // logicky or, prvy op != nil || false -> prvy op, inak druhy op
      if (I->addr3 == NULL) {
		  fprintf(stderr,"Runtime error: uninitialized values in logical or\n");
		  istackClear(&stack);  
          inListClear(&inList);
		  return INTER_ERROR;
	  }
	  
	  /* kontrola I->addr1 */
	  opControl(I->addr1,&addr1,&stack,&inList);
	  
	  /* kontrola I->addr2 */
	  opControl(I->addr2,&addr2,&stack,&inList);
	  
	  /* kontrola I->addr3 - control destination */
	  destControl(I->addr3,&addr3,&stack,&inList);
	  
	  if (((tData*)addr1)->type != _NIL)
			addr3 = addr1;
	  else if (((tData*)addr1)->type == _BOOL) {
	      if (((tData*)addr1)->value.boolValue != false)
		      addr3 = addr1;
		  }
	  else
	      addr3 = addr2;
      break;
    
    case I_LAB:
      // instrukce navesti
      // nic nerobim 
      break;
      
    case I_IFGOTO:
      #ifdef DEBUG_INT
      printf("Running I_IFGOTO\n");
      #endif
      // instrukce pro provedeni podmineneho skoku
      opControl(I->addr1,&addr1,&stack,&inList);
      if(addr1 != NULL) {
          if (((tData*)addr1)->value.boolValue) {
			  if(I->addr3 != NULL)
                  listGoto(instrList, I->addr3);
              else {
				  fprintf(stderr,"Runtime error: nowhere to jump\n");
				  istackClear(&stack);  
                  inListClear(&inList);
				  return INTER_ERROR;
			  }
		  }
	  }
	  else {
		  fprintf(stderr,"Runtime error: conditional jump depends on uninitialized value\n");
		  istackClear(&stack);  
          inListClear(&inList);
		  return INTER_ERROR;
	  }
      break;
      
    case I_GOTO:
      #ifdef DEBUG_INT
      printf("Running I_GOTO\n");
      #endif
      // instrukce skoku
      
      if(I->addr3 != NULL)
          listGoto(instrList, I->addr3);
      else {
		  fprintf(stderr,"Runtime error: nowhere to jump\n");
		  istackClear(&stack);  
          inListClear(&inList);
		  return INTER_ERROR;
		  }
      break;
    
    case I_ASSIGN:
      #ifdef DEBUG_INT
      printf("Running I_ASSIGN\n");
      #endif
      // instrukcia priradenia
      
      opControl(I->addr1,&addr1,&stack,&inList);
	  
	  destControl(I->addr3,&addr3,&stack,&inList);
	  
	  if( ((tData*)addr3)->type == _STRING ) 
		  strFree(&(((tData*)addr3)->value.stringValue));
	  
      switch (((tData*)addr1)->type) {
		  case _STRING:
		      strInit(&(((tData*)addr3)->value.stringValue));
		      strCopyString(&(((tData*)addr3)->value.stringValue),&(((tData*)addr1)->value.stringValue));
		      break;
			  
		  default:
		      ((tData*)addr3)->value = ((tData*)addr1)->value;
		      break;
		  }
      
      if(((tData*)addr1)->type == _UNKNOWN)
		  ((tData*)addr3)->type = _NIL;
	  else 
		  ((tData*)addr3)->type = ((tData*)addr1)->type; 
      break;
      
    case I_POWER:
	  #ifdef DEBUG_INT
      printf("Running I_POWER\n");
      #endif
      // instrukcia násobenie
      if(I->addr3 == NULL) {
		  fprintf(stderr,"Runtime error: operation power depends on unitialized values\n");
		  istackClear(&stack);  
          inListClear(&inList);
		  return INTER_ERROR;
	  }
	  
	  opControl(I->addr1,&addr1,&stack,&inList);
	  opControl(I->addr2,&addr2,&stack,&inList);
	  destControl(I->addr3,&addr3,&stack,&inList);
	  
	  if((((tData*)addr1)->type == _NUMBER) && (((tData*)addr2)->type == _NUMBER)) {
          ((tData*)addr3)->value.doubleValue = pow(((tData*)addr1)->value.doubleValue,((tData*)addr2)->value.doubleValue);
          ((tData*)addr3)->type = _NUMBER;
	  }
	  else {
		  fprintf(stderr,"Runtime error: multiplication of non number values\n");
		  istackClear(&stack);  
          inListClear(&inList);
	      return EX_ERROR;
	  }
	  
      break;
     
    case I_MULT:
      #ifdef DEBUG_INT
      printf("Running I_MULT\n");
      #endif
      // instrukcia násobenie
      if(I->addr3 == NULL) {
		  fprintf(stderr,"Runtime error: operation multiplication depends on unitialized values\n");
		  istackClear(&stack);  
          inListClear(&inList);
		  return INTER_ERROR;
	  }
	  
	  opControl(I->addr1,&addr1,&stack,&inList);
	  opControl(I->addr2,&addr2,&stack,&inList);
	  destControl(I->addr3,&addr3,&stack,&inList);
	  
      if((((tData*)addr1)->type == _NUMBER) && (((tData*)addr2)->type == _NUMBER)) {
          ((tData*)addr3)->value.doubleValue = ((tData*)addr1)->value.doubleValue * ((tData*)addr2)->value.doubleValue;
          ((tData*)addr3)->type = _NUMBER;
	  }
	  else {
		  fprintf(stderr,"Runtime error: multiplication of non number values\n");
		  istackClear(&stack);  
          inListClear(&inList);
	      return EX_ERROR;
	  }
      break;
      
    case I_DIV:
      #ifdef DEBUG_INT
      printf("Running I_DIV\n");
      #endif
      // instrukcia delenie
      if(I->addr3 == NULL) {
		  fprintf(stderr,"Runtime error: operation division depends on unitialized values\n");
		  istackClear(&stack);  
          inListClear(&inList);
		  return INTER_ERROR;
	  }
	  
	  opControl(I->addr1,&addr1,&stack,&inList);
	  opControl(I->addr2,&addr2,&stack,&inList);
	  destControl(I->addr3,&addr3,&stack,&inList);
	  
      if((((tData*)addr1)->type == _NUMBER) && (((tData*)addr2)->type == _NUMBER)) {
		  if(((tData*)addr2)->value.doubleValue != 0) {
              ((tData*)addr3)->value.doubleValue = ((tData*)addr1)->value.doubleValue / ((tData*)addr2)->value.doubleValue;
              ((tData*)addr3)->type = _NUMBER;
		  }
          else {
			  fprintf(stderr,"Runtime error: division by zero\n");
			  istackClear(&stack);  
              inListClear(&inList);
			  return EX_ERROR;
		  }
	  }
	  else {
		  fprintf(stderr,"Runtime error: division of non number values\n");
		  istackClear(&stack);  
          inListClear(&inList);
		  return EX_ERROR;
	  }
      break;
      
    case I_ADD:
      #ifdef DEBUG_INT
      printf("Running I_ADD\n");
      #endif
      // instrukcia scitavanie
      if(I->addr3 == NULL) {
		  fprintf(stderr,"Runtime error: operation addition depends on unitialized values\n");
		  istackClear(&stack);  
          inListClear(&inList);
		  return INTER_ERROR;
	  }
	  
	  opControl(I->addr1,&addr1,&stack,&inList);
	  opControl(I->addr2,&addr2,&stack,&inList);
	  destControl(I->addr3,&addr3,&stack,&inList);
	  
      if((((tData*)addr1)->type == _NUMBER) && (((tData*)addr2)->type == _NUMBER)) {
          ((tData*)addr3)->value.doubleValue = ((tData*)addr1)->value.doubleValue + ((tData*)addr2)->value.doubleValue;
          ((tData*)addr3)->type = _NUMBER;
	  }
	  else {
		  fprintf(stderr,"Runtime error: addition of non number values\n");
		  istackClear(&stack);  
          inListClear(&inList);
	      return EX_ERROR;
	  }
      break;
      
    case I_SUB:
      #ifdef DEBUG_INT
      printf("Running I_SUB\n");
      #endif
      // instrukcia odcitavanie
      if(I->addr3 == NULL) {
		  fprintf(stderr,"Runtime error: operation subtraction depends on unitialized values\n");
		  istackClear(&stack);  
          inListClear(&inList);
		  return INTER_ERROR;
	  }
	  
	  opControl(I->addr1,&addr1,&stack,&inList);
	  opControl(I->addr2,&addr2,&stack,&inList);
	  destControl(I->addr3,&addr3,&stack,&inList);
	  
      if((((tData*)addr1)->type == _NUMBER) && (((tData*)addr2)->type == _NUMBER)) {
          ((tData*)addr3)->value.doubleValue = ((tData*)addr1)->value.doubleValue - ((tData*)addr2)->value.doubleValue;
          ((tData*)addr3)->type = _NUMBER;
	  }
	  else {
		  fprintf(stderr,"Runtime error: subtraction of non number values\n");
		  istackClear(&stack);  
          inListClear(&inList);
	      return EX_ERROR;
	  }
      break;
      
    case I_ISGR:
      #ifdef DEBUG_INT
      printf("Running I_ISGR\n");
      #endif
      // instrukcia > , typ nastavit na boolean
      if(I->addr3 == NULL) {
		  fprintf(stderr,"Runtime error: operation is greater depends on unitialized values\n");
		  istackClear(&stack);  
          inListClear(&inList);
		  return INTER_ERROR;
	  }
	  
	  opControl(I->addr1,&addr1,&stack,&inList);
	  opControl(I->addr2,&addr2,&stack,&inList);
	  destControl(I->addr3,&addr3,&stack,&inList);
	  	  
      if( (((tData*)addr1)->type == _NUMBER) && (((tData*)addr2)->type == _NUMBER) ) {
          ((tData*)addr3)->value.boolValue = ((tData*)addr1)->value.doubleValue > ((tData*)addr2)->value.doubleValue;
          ((tData*)addr3)->type = _BOOL;
	  }
      else if ((((tData*)addr1)->type == _STRING) && (((tData*)addr2)->type == _STRING)) {
		  ((tData*)addr3)->value.boolValue = strCmpString(&(((tData*)addr1)->value.stringValue),&(((tData*)addr2)->value.stringValue)) > 0;
		  ((tData*)addr3)->type = _BOOL;
	  }
      else if ((((tData*)addr1)->type == _BOOL) && (((tData*)addr2)->type == _BOOL)) {
	      if( (((tData*)addr1)->value.boolValue) == true ) {
			  if( (((tData*)addr2)->value.boolValue) == false )
			      ((tData*)addr3)->value.boolValue = true;
		  }
		  else
		      ((tData*)addr3)->value.boolValue = false;
		      
		  ((tData*)addr3)->type = _BOOL;
	  }
      else if ((((tData*)addr1)->type == _NIL) || (((tData*)addr2)->type == _NIL)){
		  fprintf(stderr,"Runtime error: comparsion between uninitialized values\n");
		  istackClear(&stack);  
          inListClear(&inList);
          return EX_ERROR;
	  }
	  else {
	      ((tData*)addr3)->value.boolValue = false;   
		  ((tData*)addr3)->type = _BOOL;
	  }
      break;
      
    case I_ISLESS:
      #ifdef DEBUG_INT
      printf("Running I_ISLESS\n");
      #endif
      // instrukcia <
      if(I->addr3 == NULL) {
		  fprintf(stderr,"Runtime error: operation is less depends on unitialized values\n");
		  istackClear(&stack);  
          inListClear(&inList);
		  return INTER_ERROR;
	  }
	  
	  opControl(I->addr1,&addr1,&stack,&inList);
	  
	  opControl(I->addr2,&addr2,&stack,&inList);
	  
	  destControl(I->addr3,&addr3,&stack,&inList);
	  
	  //printf("addr1 %d addr2 %d\n",((tData*)addr1)->type,((tData*)addr2)->type);
      if((((tData*)addr1)->type == _NUMBER) && (((tData*)addr2)->type == _NUMBER)) {
          ((tData*)addr3)->value.boolValue = ((tData*)addr1)->value.doubleValue < ((tData*)addr2)->value.doubleValue;
          ((tData*)addr3)->type = _BOOL;
	  }
      else if ((((tData*)addr1)->type == _STRING) && (((tData*)addr2)->type == _STRING)) {
		  ((tData*)addr3)->value.boolValue = strCmpString(&(((tData*)addr1)->value.stringValue),&(((tData*)addr2)->value.stringValue)) < 0;
		  ((tData*)addr3)->type = _BOOL;
	  }
      else {
		  fprintf(stderr,"Runtime error: comparsion between uninitialized or bad values\n");
		  istackClear(&stack);  
          inListClear(&inList);
          return EX_ERROR;
	  }
      break;
    
    case I_ISGE:
      #ifdef DEBUG_INT
      printf("Running I_GE\n");
      #endif
      // instrukcia >=
      if(I->addr3 == NULL) {
		  fprintf(stderr,"Runtime error: operation is greater or equal depends on unitialized values\n");
		  istackClear(&stack);  
          inListClear(&inList);
		  return INTER_ERROR;
	  }
	  
	  opControl(I->addr1,&addr1,&stack,&inList);
	  opControl(I->addr2,&addr2,&stack,&inList);
	  destControl(I->addr3,&addr3,&stack,&inList);
	  
      if((((tData*)addr1)->type == _NUMBER) && (((tData*)addr2)->type == _NUMBER)) {
          ((tData*)addr3)->value.boolValue = ((tData*)addr1)->value.doubleValue >= ((tData*)addr2)->value.doubleValue;
          ((tData*)addr3)->type = _BOOL;
	  }
      else if ((((tData*)addr1)->type == _STRING) && (((tData*)addr2)->type == _STRING)) {
		  ((tData*)addr3)->value.boolValue = strCmpString(&(((tData*)addr1)->value.stringValue),&(((tData*)addr2)->value.stringValue)) >= 0;
		  ((tData*)addr3)->type = _BOOL;
	  }
      else {
		  fprintf(stderr,"Runtime error: comparsion between uninitialized or bad values\n");
		  istackClear(&stack);  
          inListClear(&inList);
          return EX_ERROR;
	  }
      break;
      
    case I_ISLE:
      #ifdef DEBUG_INT
      printf("Running I_ISLE\n");
      #endif
      // instrukcia <=
      if(I->addr3 == NULL) {
		  fprintf(stderr,"Runtime error: operation is less or equal depends on unitialized values\n");
		  istackClear(&stack);  
          inListClear(&inList);
		  return INTER_ERROR;
	  }
	  
	  opControl(I->addr1,&addr1,&stack,&inList);
	  opControl(I->addr2,&addr2,&stack,&inList);
	  destControl(I->addr3,&addr3,&stack,&inList);
	  
      if((((tData*)addr1)->type == _NUMBER) && (((tData*)addr2)->type == _NUMBER)) {
          ((tData*)addr3)->value.boolValue = ((tData*)addr1)->value.doubleValue <= ((tData*)addr2)->value.doubleValue;
          ((tData*)addr3)->type = _BOOL;
	  }
      else if ((((tData*)addr1)->type == _STRING) && (((tData*)addr2)->type == _STRING)) {
		  ((tData*)addr3)->value.boolValue = strCmpString(&(((tData*)addr1)->value.stringValue),&(((tData*)addr2)->value.stringValue)) <= 0;
		  ((tData*)addr3)->type = _BOOL;
	  }
      else {
		  fprintf(stderr,"Runtime error: comparsion between uninitialized or bad values\n");
		  istackClear(&stack);  
          inListClear(&inList);
          return EX_ERROR;
	  }
      break;
      
    case I_ISEQ:
      #ifdef DEBUG_INT
      printf("Running I_ISEQ\n");
      #endif
      // instrukcia ==
      if(I->addr3 == NULL) {
		  fprintf(stderr,"Runtime error: operation is equal depends on unitialized values\n");
		  istackClear(&stack);  
          inListClear(&inList);
		  return INTER_ERROR;
	  }
	  
	  opControl(I->addr1,&addr1,&stack,&inList);
	  opControl(I->addr2,&addr2,&stack,&inList);
	  destControl(I->addr3,&addr3,&stack,&inList);
	  
      if((((tData*)addr1)->type == _NUMBER) && (((tData*)addr2)->type == _NUMBER)) {
          ((tData*)addr3)->value.boolValue = ((tData*)addr1)->value.doubleValue == ((tData*)addr2)->value.doubleValue;
	  }
      else if ((((tData*)addr1)->type == _STRING) && (((tData*)addr2)->type == _STRING)) {
		  ((tData*)addr3)->value.boolValue = strCmpString(&(((tData*)addr1)->value.stringValue),&(((tData*)addr2)->value.stringValue)) == 0;
	  }
      else if ((((tData*)addr1)->type == _BOOL) && (((tData*)addr2)->type == _BOOL)) {
	      if( (((tData*)addr1)->value.boolValue) ==  (((tData*)addr2)->value.boolValue)) {
			      ((tData*)addr3)->value.boolValue = true;
		  }
		  else
		      ((tData*)addr3)->value.boolValue = false;
	  }
      else if ( (((tData*)addr1)->type == _NIL)) {
			  if( (((tData*)addr2)->type == _NIL))
				  ((tData*)addr3)->value.boolValue = true;
			  else {
				fprintf(stderr,"Runtime error: comparsion between uninitialized values\n");
				istackClear(&stack);  
				inListClear(&inList);
				return EX_ERROR;
			}
	  }
	  else {
	      ((tData*)addr3)->value.boolValue = false;   
	  }
	  ((tData*)addr3)->type = _BOOL;
      break;
        
    case I_FIND:
      #ifdef DEBUG_INT
      printf("Running I_FIND\n");
      #endif
      //vstavana fcia find
      if(I->addr3 == NULL) {
		  fprintf(stderr,"Runtime error: find: unitialized values\n");
		  istackClear(&stack);  
          inListClear(&inList);
		  return INTER_ERROR;
	  }
		
	  opControl(I->addr1,&addr1,&stack,&inList);
	  opControl(I->addr2,&addr2,&stack,&inList);
	  destControl(I->addr3,&addr3,&stack,&inList);
		
	  if(((tData*)addr1)->type != _STRING || ((tData*)addr2)->type != _STRING)
		  ((tData*)addr3)->type = _UNKNOWN;
	  
	  
      else if( KMPfind(&(((tData*)addr1)->value.stringValue),
								&(((tData*)addr2)->value.stringValue)) != -1) {
		((tData*)addr3)->type = _NUMBER;
		((tData*)addr3)->value.doubleValue = (double) KMPfind(&(((tData*)addr1)->value.stringValue),
																&(((tData*)addr2)->value.stringValue));
															}
	  else {
		((tData*)addr3)->type = _BOOL;
		((tData*)addr3)->value.boolValue = false;
		}
      break;
        
    case I_TYPE:
      #ifdef DEBUG_INT
      printf("Running I_TYPE\n");
      #endif
      //vstavana fcia type
      if(I->addr3 == NULL) {
		  fprintf(stderr,"Runtime error: type: unitialized values\n");
		  istackClear(&stack);  
          inListClear(&inList);
	  	  return INTER_ERROR;
	  }
		
	  opControl(I->addr1,&addr1,&stack,&inList);
	  destControl(I->addr3,&addr3,&stack,&inList);
		
      strInit(&(((tData*)addr3)->value.stringValue));
      ((tData*)addr3)->type = _STRING;
      switch (((tData*)addr1)->type) {
		  case _BOOL:
              strSetStr("boolean",&(((tData*)addr3)->value.stringValue));
              break;
          case _UNKNOWN:
          case _NIL:
              strSetStr("nil",&(((tData*)addr3)->value.stringValue));
              break;
          case _NUMBER:
              strSetStr("number",&(((tData*)addr3)->value.stringValue));
              break;
          case _STRING:
              strSetStr("string",&(((tData*)addr3)->value.stringValue));
              break;
          default:
              fprintf(stderr,"Runtime error: type: unknown type\n");
              istackClear(&stack);  
              inListClear(&inList);
              return INTER_ERROR;
	  }
      break;
        
    case I_SUBSTR:
      #ifdef DEBUG_INT
      printf("Running I_SUBSTR\n");
      #endif
      //vstavana fcia substr, v druhom kroku prijmam string a cielovu premennu
      if(I->addr3 == NULL) {
		  fprintf(stderr,"Runtime error: substr: unitialized value\n");
		  istackClear(&stack);  
          inListClear(&inList);
		  return INTER_ERROR;
	  }
	  
	  //ziskam hranice zo zasobniku
	  istackTop(&stack,&end);
	  istackPop(&stack);
	  istackTop(&stack,&begin);
	  istackPop(&stack);
		
	  opControl(I->addr1,&addr1,&stack,&inList);
	  destControl(I->addr3,&addr3,&stack,&inList);
	  
      if(((tData *) begin)->type == _NUMBER && ((tData *) end)->type == _NUMBER) {
		  tempString = substr(&(((tData*)addr1)->value.stringValue),((tData *) begin)->value.doubleValue,((tData *) end)->value.doubleValue,&Err);
		  if(tempString != NULL && Err != 5) {
			  strInit(&(((tData*)addr3)->value.stringValue)); 
			  strCopyString(&(((tData*)addr3)->value.stringValue),tempString);
		      strFree(tempString);
		      free(tempString);
		      ((tData *) addr3)->type = _STRING;
		  }
		  else {
			  ((tData *) addr3)->type = _UNKNOWN;
		  }
	  }
	  else {
		  ((tData *) addr3)->type = _UNKNOWN;
	  }
      break;
        
    case I_SORT:
      #ifdef DEBUG_INT
      printf("Running I_SORT\n");
      #endif
      //vstavana fcia sort
      if(I->addr3 == NULL) {
		  fprintf(stderr,"Runtime error: sort: unitialized values\n");
		  istackClear(&stack);  
          inListClear(&inList);
		  return INTER_ERROR;
	  }
	  
	  opControl(I->addr1,&addr1,&stack,&inList);
	  destControl(I->addr3,&addr3,&stack,&inList);
	  
	  if(((tData*)addr1)->type != _STRING) {
		  ((tData *) addr3)->type = _UNKNOWN;
	  }
      else {
		tempString = sort(&(((tData*)addr1)->value.stringValue),&Err);
		if(tempString != NULL && Err != 5) {
		  strInit(&(((tData*)addr3)->value.stringValue));
          strCopyString(&(((tData*)addr3)->value.stringValue),tempString);
          strFree(tempString);
          free(tempString);
          ((tData *) addr3)->type = _STRING;
		}
		else {
		  fprintf(stderr,"Runtime error: sort: function failure\n");
		  if(tempString != NULL) {
			strFree(tempString);
			free(tempString);
			}
		  istackClear(&stack);  
          inListClear(&inList);
          return INTER_ERROR;
		}
	  }
      break;
    
    case I_KON:
      #ifdef DEBUG_INT
      printf("Running I_KON\n");
      #endif
      //konkatenacia retazcov
      if(I->addr3 == NULL) {
			fprintf(stderr,"Runtime error: concat: unitialized values\n");
			istackClear(&stack);  
            inListClear(&inList);
			return INTER_ERROR;
		}
		
	  opControl(I->addr1,&addr1,&stack,&inList);
	  opControl(I->addr2,&addr2,&stack,&inList);
	  destControl(I->addr3,&addr3,&stack,&inList);
	  
	  if((((tData*)addr1)->type != _STRING) && (((tData*)addr2)->type != _STRING)) {
		  ((tData *) addr3)->type = _UNKNOWN;
	  }else {
      tempString = concat(&(((tData*)(addr1))->value.stringValue),&(((tData*)(addr2))->value.stringValue),&Err);
      if(tempString != NULL && Err != 5) {
		  strInit(&(((tData*)(addr3))->value.stringValue));
          strCopyString(&(((tData*)(addr3))->value.stringValue),tempString);
          strFree(tempString);
          free(tempString);
          ((tData *) addr3)->type = _STRING;
	  }
      else {
			fprintf(stderr,"Runtime error: concat: function failure\n");
			if(tempString != NULL) {
				strFree(tempString);
				free(tempString);
			}
			istackClear(&stack);  
            inListClear(&inList);
            return INTER_ERROR;
		}
	  }
      break;
    
    case I_RET:
      #ifdef DEBUG_INT
      printf("Running I_RET\n");
      #endif
      // instrukcia navratu z uzivatelskej fcie     
      // najprv ulozim navratovu hodnotu
      if((int)stack.data[stack.base+F_DEST-1].type == -1 && (stack.base+F_DEST-1) < stack.top) {
		  addr3 = stack.data[stack.base+F_DEST].value.addrValue;
      
		  if(((tData*)I->addr1) != NULL) {
			  destControl(I->addr1,&addr1,&stack,&inList);
			  switch (((tData*)addr1)->type) {
				  case _STRING:
					  strInit(&(((tData*)addr3)->value.stringValue));
					  strCopyString(&(((tData*)addr3)->value.stringValue),&(((tData*)addr1)->value.stringValue));
					  break;
				  default:
					  ((tData*)addr3)->value = ((tData*)addr1)->value;
					  break;
			  }
			  ((tData*)addr3)->type = ((tData*)addr1)->type;
		  }
	      
		  //uvolnenie lokalnych dat
		  while((stack.base + ARG_COUNT) < stack.top) 
			  istackPop(&stack); 
          
		  // popnem pocet parametrov fcie
		  istackPop(&stack);
      
		  //pop adresy s navratovou hodnotou
		  istackPop(&stack);
      
		  //ziskam adresu odkial bola fcia volana
		  istackTop(&stack,&addr2);
		  istackPop(&stack);
		  listGoto(instrList,((tData *)addr2)->value.addrValue);
      
		  //prepisat base na povodny
		  istackTop(&stack,&addr3);
		  istackPop(&stack);
		  stack.base = (int) ((tData *)addr3)->value.doubleValue;
      
		  // upravym zoznam indexov
		  while(inList.First->name != NULL) 
			  inListDeleteFirst(&inList); 
		  inListDeleteFirst(&inList); // odstranenie zarazky
	  }
	  else {// return v main
		  istackClear(&stack);  
		  inListClear(&inList);
		  if(((tData*)I->addr1) != NULL) {
			  destControl(I->addr1,&addr1,&stack,&inList);
			  return ((tData*)addr1)->value.doubleValue;
		  }
		  else return EOK;
	  }
      
      break;
    
    case I_PUSH:
      #ifdef DEBUG_INT
      printf("Running I_PUSH\n");
      #endif
      if(I->addr1 != NULL || I->addr2 != NULL) {
		  // naplnim data pre stack
		  // ak pushujem argument addr2 != NULL
		  // prvok uz moze byt niekde na zasobniku ako localna premenna
		  //s uz pridelenym typom
		  if(I->addr2 != NULL) {
		      if( ((tData *)I->addr2)->id == NULL ) {
		          addr1 = I->addr2; // nie je na zasobniku
			  }
		      else {
		          destControl(I->addr2,&addr1,&stack,&inList);
		          
			  }
		  }  
		  else 
		      addr1 = I->addr1;
		  
		  if ((stackData = malloc(sizeof(tData))) == NULL) {
		      fprintf(stderr,"Runtime error: push: not enough memory\n");
		      istackClear(&stack);  
              inListClear(&inList);
		      return INTER_ERROR;
	      }
		  stackData->type = ((tData *) addr1)->type;
		  stackData->id = ((tData *) addr1)->id;
		  if(stackData->type == _STRING) {
		      strInit(&(stackData->value.stringValue));
		      strCopyString(&(stackData->value.stringValue),&(((tData *) addr1)->value.stringValue));
		  }
		  else 
		      stackData->value = ((tData *) addr1)->value;
		      
		  // pushnem data na stack
          istackPush(&stack,*stackData);
          
          // ak je to premenna bez typu ulozim si jej index
          //iba ak to nebol argument
          if(stackData->type == _NIL) 
              if((I->addr2 != NULL && ((tData *)I->addr2)->id != NULL) || I->addr1 != NULL)
			      if (inListInsert(&inList,((tData *) addr1)->id,stack.top - stack.base) == NULL) {
					  fprintf(stderr,"Not enough memory\n");
					  istackClear(&stack);  
                      inListClear(&inList);
				      return INTER_ERROR;
				  }
			  
		  if(stackData->type == _STRING) 
		      strFree(&(stackData->value.stringValue));
           free(stackData);
      }
      else {
		  // pravdepodobne pushujem chybajuce argumenty
		  if ((stackData = malloc(sizeof(tData))) == NULL) {
		      fprintf(stderr,"Runtime error: push: not enough memory\n");
		      istackClear(&stack);  
              inListClear(&inList);
		      return INTER_ERROR;
	      }
	      
	      stackData->type = _UNKNOWN;
	      
	      // pushnem data na stack
          istackPush(&stack,*stackData);
          
          free(stackData);
	      
	  }
      break;
        
    case I_CALL:
      #ifdef DEBUG_INT
      printf("Running I_CALL\n");
      #endif
      /* ulozim aktualny index base na zasobnik */
      if ((stackData = malloc(sizeof(tData))) == NULL) {
		  fprintf(stderr,"Runtime error: call: not enough memory\n");
		  istackClear(&stack);  
          inListClear(&inList);
		  return INTER_ERROR;
	  }
      stackData->value.doubleValue = (double) stack.base;
      // typ nastavym na zapornu hodnotu aby som vedel ze s tym nemam pracovat
      stackData->type = -1;  
      istackPush(&stack,*stackData);
      stack.base = stack.top;
      free(stackData);
      
      /* ulozim si adresu aktivnej instrukcie na zasobnik */
      if ((stackData = malloc(sizeof(tData))) == NULL) {
		  fprintf(stderr,"Runtime error: call: not enough memory\n");
		  istackClear(&stack);  
          inListClear(&inList);
		  return INTER_ERROR;
	  }
      addr1 = listGetPointerActive(instrList);
      stackData->value.addrValue = addr1;
      stackData->type = -1;
      istackPush(&stack,*stackData);
      free(stackData);
      
      /* ulozim navratovu adresu na hodnoty do ktorej vraciam vysledok */
	  if ((stackData = malloc(sizeof(tData))) == NULL) {
	      fprintf(stderr,"Runtime error: call: not enough memory\n");
	      istackClear(&stack);  
          inListClear(&inList);
	      return INTER_ERROR;
	  }
      destControl(I->addr1,&addr1,&stack,&inList);
      stackData->type = ((tData *)addr1)->type;
      stackData->value.addrValue = addr1;
      istackPush(&stack,*stackData);
	  free(stackData);
	  
	  /* pushneme pocet parametrov */
	  if ((stackData = malloc(sizeof(tData))) == NULL) {
	      fprintf(stderr,"Runtime error: call: not enough memory\n");
	      istackClear(&stack);  
          inListClear(&inList);
	      return INTER_ERROR;
	  }
	  
      stackData->value.doubleValue = (double) *((int *)I->addr2);
      stackData->type = _NUMBER;
      istackPush(&stack,*stackData);
	  free(stackData);
	        
      //skocim na funkciu
      if(I->addr3 != NULL)
          listGoto(instrList, I->addr3);
      else {
		  fprintf(stderr,"Runtime error: nowhere to jump\n");
		  istackClear(&stack);  
          inListClear(&inList);
		  return INTER_ERROR;
		  }
		  
	  // vlozim zarazku do zoznamu indexov
	  inListInsert(&inList,NULL,0); 
	  
      break;
    
    }
    /* posun na dalsiu instrukciu
     * posuvam sa aj po skoku
     * v navesti sa nic nevykonava
     */
    listNext(instrList);

  }
}
