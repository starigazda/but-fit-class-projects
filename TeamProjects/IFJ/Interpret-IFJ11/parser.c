/**
 *	Project:	Interpret of the Lua programming language derivate IFJ11.
 *	File:		parser.c
 *	Module:		modul for syntax analysis
 *	Date:		Fri Dec  2 12:13:21 CET 2011
 *	Author:		Michal Srubar,	xsruba03@stud.fit.vutbr.cz	
 *	Version:	v1.2
 *
 *	Description:
 *	This module does syntax analysis by method of the recursive descent. All
 *	expressions are processed by precedence anylysis which is implement in
 *	expr.c module
 *
 */

#ifndef STDIO_H
	#include <stdio.h>
	#define STDIO_H
#endif
#ifndef BOOL_H
	#include <stdbool.h>
	#define BOOL_H
#endif
#ifndef LIMITS_H
	#include <limits.h>
	#define LIMITS_H
#endif

#ifndef PARSER_H
	#include "parser.h"
#endif
#ifndef SCANNER_H
	#include "scanner.h"
#endif
#ifndef EXPR_H
	#include "expr.h"
#endif
#ifndef IAL_H
	#include "ial.h"
#endif
#ifndef STR_H
	#include "str.h"
#endif
#ifndef ERROR_H
	#include "error.h"
#endif

/**
 * Function to generate instruction
 * @param instType -> type of instruction from ilist.h
 * @param addr1 - addr2 -> source addresses
 * @param addr3 -> destination address 
 */
void generateInstruction(int instType, void *addr1, void *addr2, void *addr3) {

   tInstr I;
   I.instType = instType;
   I.addr1 = addr1;
   I.addr2 = addr2;
   I.addr3 = addr3;
   listInsertLast(&instrList, I);
}

void initToken(tToken *p_token) {

	if (p_token == NULL) {
		master("PARSER", "initToken", "Tried to intialized token whitch points to NULL.");
	}

	p_token->type = EMPTY;
	strInit(&(p_token->key));
}

bool getToken(TParserStat *p_stat) {

	if (p_stat == NULL)
		master("PARSER", "getToken", "Tried to work with unintialized prog_stat.");

	if (p_stat->p_token->type == EMPTY) {

		if (getNextToken(p_stat) == false)
			return false;
	}

	return true;
}

/**
 * TOKEN FLUSH
 */
void flushToken(TParserStat *p_stat) {

	if (p_stat->p_token->type == STRING) {
		strFree(&(p_stat->p_token->data.stringValue));
   	}

	p_stat->p_token->type = EMPTY;
   	p_stat->p_token->data.doubleValue = 0;
}

/**
 * The function compares two token types. It doesn't do FLUSH if tokens are 
 * differ!
 *
 * @flush = token in flushed from stat
 * @next = if next == NEXT then new token is get from scanner first
 *
 * RETURN:	true = types of the tokens are equale
 * 			false = types of the tokens are diffrent
 */
bool compareTokensType(TParserStat *p_stat, int type, int flush, int next) {
		
	if (p_stat->ecode == EOK) {	

		if (next == NEXT) {
			if (getToken(p_stat) == false)
				return false;
		}
	
		if (p_stat->p_token->type == (unsigned int) type) {
	
			if (flush == FLUSH)	
				flushToken(p_stat);
	
			return true;		
		}
	}

	return false;
}

/**
 * ADD FUNCTION INTO FUNCTION TABLE AND CREATE NEW TABLE OF SYMBOLS
 *
 * RETURN:
 * - false
 *   Malloc error or identifier of the function is already declared. ErrId 
 *   tells what happend where it happend and sets automaticly ecode to
 *   SEM_ERROR.
 * - true
 *   The identifier of the function is added into the list of functions. It
 *   creates new table of symbols and sets this table as working table of
 *   symbols.
 */
bool addFuncId(TParserStat *p_stat) {

	if (checkFuncId(p_funcList, p_stat->p_token) == UNDECLARED) {

		if (insertFunc(p_funcList, p_stat->p_token, p_stat->addrOfFunLabel, p_stat->line) == false) {
			p_stat->ecode = MALLOC_ERROR;
			return false;
		}

		//set working function item
		p_stat->p_workFuncItem = searchFunc(p_funcList, p_stat->p_token);
		if (p_stat->p_workFuncItem == NULL) {
			printf("In funcList is't function which I just added.\n");
			exit(UNKNOWN_ERROR);
		}

		// initialize new table of symbols
		tHTable *table;
		table = malloc(sizeof(tHTable));
		htInit(table);
	
		// put table of symbols to function Item
		p_stat->p_workFuncItem->p_table = table;

		// set this table as working table of symbols
		p_stat->p_workTable = table;

	}
	else {
		errId(p_stat, true);
		return false;
	}

	return true;
}

/**
 * CHECK IF VARIABLE IDENTIFIER IS DECLARED. (DECLARED = IT IS IN WORKING 
 * TABLE OF SYMBOLS)
 * - it returns NULL if there is no p_token->key.str VARIABLE in working
 *   table of symbols or pointer to exist variable
 */
tHTItem *checkVarId(TParserStat *p_stat) {

	return htSearch(p_stat->p_workTable, p_stat->p_token->key);
}

/**
 * ADD VARIABLE IDENTIFIER INTO THE TABLE OF SYMBOLS
 *
 * RETURN:
 * - false = variable with same identifier is already declared
 * - true = variable was successfully added to working table of symbols
 */
bool addVarId(TParserStat *p_stat) {

	tHTItem *new = NULL;

	if ((new = checkVarId(p_stat)) == NULL) {

		if (checkFuncId(p_funcList, p_stat->p_token) == UNDECLARED) {

			tData data;
			data.id = NULL;
			data.value = p_stat->p_token->data;

			if (p_stat->inFuncBody == false) {

				data.value.doubleValue = p_stat->p_workFuncItem->params;
				data.type = _ARG;
			}
			else
				data.type = _NIL;

			// put new symbol into table of symbols
			htInsert(p_stat->p_workTable, p_stat->p_token->key, data);
	
			// set last inserted data
			p_stat->p_actData = htRead(p_stat->p_workTable,p_stat->p_token->key);
			
			return true;
		}
		else
			errId(p_stat, VAR_ID);
	}
	else {
		if (new->data.type == _ARG)
			errId(p_stat, VAR_ARG);
		else
			errId(p_stat, VAR);
	}

	return false;
}

/**
 * ============================================================================
 * RULES
 * ============================================================================
 */
 
/**
 * NON-TERMINAL: <FORMAT>
 * RULES: 	
 * <FORMAT> -> "*a"
 * <FORMAT> -> "*l"
 * <FORMAT> -> "*n"
 * <FORMAT> -> number
 */
bool format(TParserStat *p_stat) {

	bool rule_format = false;
	
	tData *readFormat = NULL;
	if ((readFormat = malloc(sizeof(tData))) == NULL) {
		perror("");	
		p_stat->ecode = MALLOC_ERROR;
		rule_format = false;
	}
	
	if ((compareTokensType(p_stat, NUMBER, ZERO, NEXT) == true) ) {
		
		readFormat->type = _NUMBER;
		readFormat->value.doubleValue = p_stat->p_token->data.doubleValue;
		
		rule_format = true;
	}
	else if(compareTokensType(p_stat, FORMAT_N, ZERO, ZERO) == true ||
			compareTokensType(p_stat, FORMAT_L, ZERO, ZERO) == true ||
			compareTokensType(p_stat, FORMAT_A, ZERO, ZERO) == true  ) {

		readFormat->type = _STRING;
		strInit(&(readFormat->value.stringValue));
		strCopyString(&(readFormat->value.stringValue),
						&(p_stat->p_token->key));
	
		rule_format = true;
	}
	else if (compareTokensType(p_stat, BOOL, ZERO, ZERO))
		p_stat->ecode = SEM_ERROR;
	else {
		free(readFormat);
		p_stat->ecode = SYNTAX_ERROR;
	}
	
	flushToken(p_stat);

	if (rule_format == true) {
		/** Generate Instruction */
		generateInstruction(I_PUSH,(void *)readFormat,NULL,NULL);
	}
	
	return rule_format;
}

/**
 * NON-TERMINAL: <INTER_FUNC>
 * RULE: 	
 * <INTER_FUNC> -> <EXPR>
 * <INTER_FUNC> -> read(<FORMAT>)
 */
bool inter_func(TParserStat *p_stat) {

	bool rule_inter_func = false;

	if (compareTokensType(p_stat, KW_READ, ZERO, NEXT) == true) {

		flushToken(p_stat);
		if (compareTokensType(p_stat, L_PAR, FLUSH, NEXT) == true) {

			if (format(p_stat) == true) {
				
				/** Generate Instruction */
				// az jak sa vo format vygeneruje instrukcia PUSH format
				// generujem READ 
				generateInstruction(I_READ,NULL,NULL,p_stat->p_actData);

				if (compareTokensType(p_stat, R_PAR, FLUSH, NEXT) == true)
					rule_inter_func = true;
				else {
					errExpected(p_stat, "token ')'");
				}
			}
			else
				errMsg(p_stat, "Internal function 'read' expect format: \"*l\", " \
						"\"*n\", \"*a\" or number", p_stat->ecode);
		}
		else
			errExpected(p_stat, "token '('");
	}
	else {

		if (p_stat->ecode != EOK)
			return rule_inter_func;

		p_stat->who = PRECEDENCE_PARSER;

		if (exprParser(p_stat) == true)
			rule_inter_func = true;

		p_stat->who = PARSER;
	}

	return rule_inter_func;
}

/**
 * NON-TERMINAL: <VAR_MAIN>
 * RULE: 	
 * <VAR_MAIN> -> main
 * <VAR_MAIN> -> id
 */
bool var_main(TParserStat *p_stat) {

	bool rule_var_main = false;

	if ((compareTokensType(p_stat, ID, FLUSH, NEXT) == true) ||
		(compareTokensType(p_stat, KW_MAIN, FLUSH, NEXT) == true)) {	

		rule_var_main = true;
	}

	return rule_var_main;
}

/**
 * NON-TERMINAL: <VAR_DEF>
 * RULE: 	
 * <VAR_DEF> -> ;
 * <VAR_DEF> -> =<EXPR>;
 */
bool var_def(TParserStat *p_stat) {
		
	bool rule_var_def = false;

	if (compareTokensType(p_stat, SEMICOLON, FLUSH, NEXT) == true) {

		p_stat->local = false;
		rule_var_def = true;				
	}
	else if (compareTokensType(p_stat, ASSIG, FLUSH, NEXT) == true) {
		
		p_stat->local = true;
		
		tData *data;
		data = p_stat->p_actData;
		
		if (getToken(p_stat) == false)
			return rule_var_def;

		p_stat->who = PRECEDENCE_PARSER;

		if (exprParser(p_stat) == true) {	
			
			generateInstruction(I_ASSIGN,(void *)p_stat->p_actData,NULL,(void *)data);

			if (compareTokensType(p_stat, SEMICOLON, FLUSH, NEXT) == true) {

				p_stat->local = false;
				rule_var_def = true;
			}
			else
				errExpected(p_stat, "token ';'");

			rule_var_def = true;
		}

		p_stat->who = PARSER;
	}
	else
		errExpected(p_stat, "tokens '=' or ';'");

	return rule_var_def;
}

/**
 * NON-TERMINAL: <VAR>
 * RULES:	
 * <VAR> -> local <VAR_MAIN><VAR_DEF><VAR>
 * <VAR> -> eps
 */
bool var(TParserStat *p_stat) {

	bool rule_var = false;

	if ((compareTokensType(p_stat, KW_END, ZERO, NEXT) == true) ||
		(compareTokensType(p_stat, KW_IF, ZERO, NEXT) == true) ||
		(compareTokensType(p_stat, KW_RETURN, ZERO, NEXT) == true) ||
		(compareTokensType(p_stat, KW_WHILE, ZERO, NEXT) == true) ||
		(compareTokensType(p_stat, KW_WRITE, ZERO, NEXT) == true) ||
		(compareTokensType(p_stat, SEMICOLON, ZERO, NEXT) == true) ||
		(compareTokensType(p_stat, KW_MAIN, ZERO, NEXT) == true) ||
		(compareTokensType(p_stat, ID, ZERO, NEXT) == true)) {

		rule_var = true;	
	}
	else if (compareTokensType(p_stat, KW_LOCAL, FLUSH, NEXT) == true) {
			
		if (var_main(p_stat) == true) {

			if (addVarId(p_stat) == true) {
				
				/** Generate Instruction  */
				// vyhladam id v ST a vygenerujem I_PUSH
				generateInstruction(I_PUSH,(void *)p_stat->p_actData,NULL,NULL);
				// ak var_def nezabudnut nastavit v ST type a hodnotu
			
				flushToken(p_stat);
			
				if (var_def(p_stat) == true) {
					if (var(p_stat) == true)
						rule_var = true;
				}
				else
					p_stat->local = false;
			}
		}
		else 
			errExpected(p_stat, "token: 'indentifier'");
	}
	else
		errExpected(p_stat, "token 'indentifier', 'main', 'local', 'write', "\
					"'return', 'while', 'if' or 'end'");

	return rule_var;
}

/**
 * NON-TERMINAL: <PARAMS_N>
 * RULE:	
 * <PARAMS_N> -> )
 * <PARAMS_N> -> , id<PARAMS_N>
 */
bool params(TParserStat *p_stat);
bool params_n(TParserStat *p_stat) {

	bool rule_params_n = false;

	if (compareTokensType(p_stat, R_PAR, ZERO, NEXT) == true) {
		
		flushToken(p_stat);
		rule_params_n = true;
	}
	else if (compareTokensType(p_stat, COMMA, FLUSH, NEXT) == true) {
		
		if (compareTokensType(p_stat, ID, ZERO, NEXT) == true) {

			if (addVarId(p_stat) == true) {

				/** Generate Instruction  */
				//MIRO: parametre se pushuju iba pred volanim fcie

				//generateInstruction(I_PUSH,NULL,NULL,NULL);
		
				if (p_stat->p_workFuncItem->params == INT_MAX) {
					fprintf(stderr, "%s:%d: Count of parameters is limited by macro INT_MAX " \
							"which you just broke.\n", p_stat->sourceFile, p_stat->line);
					return rule_params_n;
				}

				p_stat->p_workFuncItem->params++;
				flushToken(p_stat);
		
				if (params(p_stat) == true)
					rule_params_n = true;	
				}
		}
		else
			errExpected(p_stat, "token 'indentifier' or ')'");
	}
	else
		errExpected(p_stat, "token 'indentifier' or ','");

	return rule_params_n;
}

/**
 * NON-TERMINAL: <PARAMS>
 * RULE:	
 * <PARAMS> -> id<PARAMS_N>
 * <PARAMS> -> <PARAMS_N>
 */
bool params(TParserStat *p_stat) {

	bool rule_params = false;

	if ((compareTokensType(p_stat, R_PAR, ZERO, NEXT) == true) ||
		(compareTokensType(p_stat, COMMA, ZERO, NEXT) == true)) {
	
		if (params_n(p_stat) == true)
			return true;	
		else
			return false;	
	}
	else if (compareTokensType(p_stat, ID, ZERO, NEXT) == true) {
	
		if (addVarId(p_stat) == true) {

			/** Generate Instruction  */
			//MIRO: parametre se pushuju iba pred volanim fcie

			//generateInstruction(I_PUSH,NULL,NULL,NULL);
	
			if (p_stat->p_workFuncItem->params == INT_MAX) {
				//errLimits(p_stat, "Bylo dosazeno maxilamin hodnoty poctu parametru funkce a to INT_MAX");
				return rule_params;
			}

			p_stat->p_workFuncItem->params++;
			flushToken(p_stat);
	
			if (params(p_stat) == true)
				rule_params = true;	
		}
	}
	else
		errExpected(p_stat, "token ',' or ')'");

	return rule_params;
}

/**
 * NON-TERMINAL: 	<PAR_WRITE>
 * RULE: 		
 * <PAR_WRITE> -> <EPRT><EXPR_N>
 */
bool expr_n(TParserStat *p_stat);
bool par_write(TParserStat *p_stat) {

	bool rule_par_write = false;

	if (getToken(p_stat) == false)
		return rule_par_write;

	p_stat->who = PRECEDENCE_PARSER;

	if (exprParser(p_stat) == true) {
		
		/** Generate Instruction */
		generateInstruction(I_WRITE, (void *) p_stat->p_actData,NULL,NULL);

		if (expr_n(p_stat) == true)
			rule_par_write = true;
	}
	else {
		// misova specialitka
		// write( ..., (a+b));
		if ((p_stat->ecode == SYNTAX_ERROR) && (p_stat->p_token->type == R_PAR)) {
			p_stat->ecode = EOK;
			rule_par_write = true;
		}
	}

	p_stat->who = PARSER;

	return rule_par_write;
}

/**
 * NON-TERMINAL: 	<EXPR_N>
 * RULE: 			
 * <EXPR_N> -> ,<PAR_WRITE>
 * <EXPR_N> -> eps
 */
bool expr_n(TParserStat *p_stat) {

	bool rule_expr_n = false;

	if (compareTokensType(p_stat, R_PAR, ZERO, NEXT) == true)
		rule_expr_n = true;
	else if (compareTokensType(p_stat, COMMA, FLUSH, NEXT) == true) {

		if (par_write(p_stat) == true)
			rule_expr_n = true;
	}
	else
		errExpected(p_stat, "token ')' or ','");

	return rule_expr_n;
}

/**
 * NON-TERMINAL: <CMD>
 * RULES:
 * <CMD> -> eps
 * <CMD> -> if<EPXR>then<CMDS>else<CMDS>end;<CMD>
 * <CMD> -> while<EXPR>do<CMDS>end;<CMD>
 * <CMD> -> return<EXPR>;<CMD>
 * <CMD> -> write(<PAR_WRITE>);<CMD>
 * <CMD> -> <VAR_MAIN>=<INTER_FUNC>;<CMD>
 */
bool cmd(TParserStat *p_stat) {

	bool rule_cmd = false;
	
	if ((compareTokensType(p_stat, KW_END, ZERO, NEXT) == true) &&
		(compareTokensType(p_stat, KW_ELSE, ZERO, NEXT) == true)) {
		
		rule_cmd = true;
	}
	else if (compareTokensType(p_stat, KW_IF, FLUSH, NEXT) == true) {

		if (getToken(p_stat) == false)
			return rule_cmd;

		p_stat->who = PRECEDENCE_PARSER;

		if (exprParser(p_stat) == true) {

			/** Generate Instruction */
			generateInstruction(I_NOT,(void *)p_stat->p_actData,NULL,(void *)p_stat->p_actData);
			generateInstruction(I_IFGOTO,(void *)p_stat->p_actData,NULL,NULL);
			void *addrOfIfGoto = listGetPointerLast(&instrList);
			
			if (compareTokensType(p_stat, KW_THEN, FLUSH, NEXT) == true) {

				if (cmd(p_stat) == true) {

					/** Generate Instruction */
					generateInstruction(I_GOTO,NULL,NULL,NULL);
					void *addrOfGoto = listGetPointerLast(&instrList);
			
					if (compareTokensType(p_stat, KW_ELSE, FLUSH, NEXT) == true) {

						/** Generate Instruction */
					    generateInstruction(I_LAB,NULL,NULL,NULL);
					    listGoto(&instrList,addrOfIfGoto);
					    tInstr *I = listGetData(&instrList);
					    I->addr3 = listGetPointerLast(&instrList);

						if (cmd(p_stat) == true) {

							if (compareTokensType(p_stat, KW_END, FLUSH, NEXT) == true) {
						
								/** Generate Instruction */
								generateInstruction(I_LAB,NULL,NULL,NULL);
								listGoto(&instrList,addrOfGoto);
								tInstr *I1 = listGetData(&instrList);
								I1->addr3 = listGetPointerLast(&instrList);
			
								if (compareTokensType(p_stat, SEMICOLON, FLUSH, NEXT) == true) {

									if (cmd(p_stat) == true)
										rule_cmd = true;
								}
								else
									errExpected(p_stat, "token ';'");
							}
							else
								errExpected(p_stat, "token 'end'");
						}
					}
					else
						errExpected(p_stat, "token 'else'");
				}
			}
			else
				errExpected(p_stat, "token 'then'");
		}
		else {
			p_stat->who = PARSER;
			return rule_cmd;	
		}
	}
	else if (var_main(p_stat) == true) {

		// save identifier, I don't know if it's id of a function ar a variable
		tToken id_token;
		tToken tmp;
		initToken(&id_token);

		p_stat->ecode = strCopyString(&(id_token.key), &(p_stat->p_token->key));
		if (p_stat->ecode != EOK) {
			p_stat->ecode = MALLOC_ERROR;
			return false;
		}

		// save a
		id_token.type = p_stat->p_token->type;

		// flush a
		flushToken(p_stat);
		// check if ID isn't ID of a function and if it is then you can't call
		// function withou assiging its return value
		if (compareTokensType(p_stat, L_PAR, ZERO, NEXT) == true) {
			errMsg(p_stat, "If you want call a funciton then you have to assign a return value"\
					" somewhere first", SYNTAX_ERROR);

			return rule_cmd;
		}

		// save =
		tmp.type = p_stat->p_token->type;

		p_stat->ecode = strCopyString(&(p_stat->p_token->key), &(id_token.key));
		if (p_stat->ecode != EOK) {
			p_stat->ecode = MALLOC_ERROR;
			return false;
		}
		
		flushToken(p_stat);
		
		p_stat->p_token->type = id_token.type;
		strFree(&(id_token.key));			// free string for token
		if (id_token.type == STRING) {
			strFree(&(id_token.data.stringValue));
   		}

		if (checkVarId(p_stat) != NULL) {
			
			/** Generate Instruction */
			//zistim kam nacitavam
			p_stat->p_actData = htRead(p_stat->p_workTable,p_stat->p_token->key);
			tData *whereToAssign = p_stat->p_actData;

			flushToken(p_stat);

			p_stat->p_token->type = tmp.type;
	
			if (compareTokensType(p_stat, ASSIG, FLUSH, NEXT) == true) {
			
				if (inter_func(p_stat) == true) {
					
					/** Generate Instruction */
					// bud mi PA vratila co mam priradit alebo som robil read
					// a to ze to priradim znove nevadi
					if(p_stat->p_actData != whereToAssign)
						generateInstruction(I_ASSIGN,(void *)p_stat->p_actData,NULL,(void *)whereToAssign);

					if (compareTokensType(p_stat, SEMICOLON, FLUSH, NEXT) == true) {
						if (cmd(p_stat) == true)
							rule_cmd = true;
					}
					else
						errExpected(p_stat, "token ';'");
				}
				else
					return rule_cmd;
			}
			else
				errExpected(p_stat, "token: '='");
		}
		else
			errDeclaration(p_stat, false);
	}
	else if (compareTokensType(p_stat, KW_RETURN, FLUSH, NEXT) == true) {

		if (getToken(p_stat) == false)
			return rule_cmd;

		p_stat->who = PRECEDENCE_PARSER;

		if (exprParser(p_stat) == true) {

			/** Generate Instruction */
			generateInstruction(I_RET,(void *)p_stat->p_actData,NULL,NULL);
		
			if (compareTokensType(p_stat, SEMICOLON, FLUSH, NEXT) == true) {
				if (cmd(p_stat) == true)
					rule_cmd = true;
			}
			else
				errExpected(p_stat, "token ';'");
		}
		p_stat->who = PARSER;
	}

	else if (compareTokensType(p_stat, KW_WHILE, FLUSH, NEXT) == true) {

		/** Generate Instruction */
		generateInstruction(I_LAB,NULL,NULL,NULL);
		void *addrOfLab = listGetPointerLast(&instrList);
		
		if (getToken(p_stat) == false)
			return rule_cmd;

		p_stat->who = PRECEDENCE_PARSER;

		if (exprParser(p_stat) == true) {

			/** Generate Instruction */
			generateInstruction(I_NOT,(void *)p_stat->p_actData,NULL,(void *)p_stat->p_actData);
			generateInstruction(I_IFGOTO,(void *)p_stat->p_actData,NULL,NULL);
			void *addrOfIfGoto = listGetPointerLast(&instrList);

			if (compareTokensType(p_stat, KW_DO, FLUSH, NEXT) == true) {
				if (cmd(p_stat) == true) {
					if (compareTokensType(p_stat, KW_END, FLUSH, NEXT) == true) {

						/** Generate Instruction */
						generateInstruction(I_GOTO,NULL,NULL,addrOfLab);
						generateInstruction(I_LAB,NULL,NULL,NULL);
						listGoto(&instrList,addrOfIfGoto);
						tInstr *I = listGetData(&instrList);
						I->addr3 = listGetPointerLast(&instrList);
		
						if (compareTokensType(p_stat, SEMICOLON, FLUSH, NEXT) == true) {
							if (cmd(p_stat) == true)
								rule_cmd = true;
						}
						else
							errExpected(p_stat, "token ';'");
					}
					else
						errExpected(p_stat, "key word 'end'");
				}
			}
			else
				errExpected(p_stat, "key word 'do'");
		}
		p_stat->who = PARSER;
	}
	else if (compareTokensType(p_stat, KW_WRITE, FLUSH, NEXT) == true) {

		p_stat->write = true;

		if (compareTokensType(p_stat, L_PAR, FLUSH, NEXT) == true) {

			if (par_write(p_stat) == true) {

				if (compareTokensType(p_stat, R_PAR, FLUSH, NEXT) == true)

					p_stat->write = false;

					if (compareTokensType(p_stat, SEMICOLON, FLUSH, NEXT) == true) {
						if (cmd(p_stat) == true)
							rule_cmd = true;
					}
					else
						errExpected(p_stat, "token ';'");
			}
		}
		else
			errExpected(p_stat, "key word '('");
	}
	else
		rule_cmd = true;

	return rule_cmd;
}

/**
 * NON-TERMINAL: <BODY>
 * RULE:	
 * <BODY> -> <VAR><CMDS>
 */
bool body(TParserStat *p_stat) {

	bool rule_body = false;

	if (var(p_stat) == true) {

		if (cmd(p_stat) == true)
			rule_body = true;
	}

	return rule_body;
}

/**
 * NON-TERMINAL: <FUNC>
 * RULE:		
 * <FUNC> -> id (<PARAMS><BODY>end function<FUNC>
 * <FUNC> -> eps
 */
bool func(TParserStat *p_stat) {

	bool rule_func = false;

	// number of function parameters which increments <PARAMS> rule
	//int par = 0;		

	if (compareTokensType(p_stat, ID, ZERO, NEXT) == true) {
		
		/** Generate Instruction */
		// generujem navesti na funkciu s id "id"
		// [MISO] uz tu vieme adresu labelu takze mozes rovno pridat
		//do zoznamu funkcii
		generateInstruction(I_LAB,NULL,NULL,NULL);
		p_stat->addrOfFunLabel = listGetPointerLast(&instrList); //tot adresa labelu

		if (addFuncId(p_stat) == false) 
			return false;

		flushToken(p_stat);

		if (compareTokensType(p_stat, L_PAR, FLUSH, NEXT) == true) {

			// CALL <PARAMS>
			if (params(p_stat) == true) {

				p_stat->inFuncBody = true;

				// CALL <BODY>
				if (body(p_stat) == true) {

					p_stat->inFuncBody = false;

					if (compareTokensType(p_stat, KW_END, FLUSH, NEXT) == true) {
						
						void *lastInstr = listGetPointerLast(&instrList);
						listGoto(&instrList,lastInstr);
						tInstr *I = listGetData(&instrList);
						
						if(I->instType != I_RET)
							generateInstruction(I_RET,NULL,NULL,NULL);
						
						if (compareTokensType(p_stat, KW_FUNCTION, FLUSH, NEXT) == true) {

							// CALL FUNC
							if (func(p_stat) == true)
								rule_func = true;
						}
						else
							errExpected(p_stat, "key word 'function'");
					}
					else
						errExpected(p_stat, "key word 'end'");
				}
				else
					p_stat->inFuncBody = false;
			}
		}
		else
			errExpected(p_stat, "token '('");
	}
	else
		rule_func = true;

	return rule_func;
}

/**
 * NON-TERMINAL: <FUNC_MAIN>
 * RULE: 
 * <FUNC_MAIN> -> main()<BODY>end;
 */
bool func_main(TParserStat *p_stat) {

	bool rule_func_main = false;

	if (compareTokensType(p_stat, KW_MAIN, ZERO, NEXT) == true) {

		p_stat->main = true;

		if (addFuncId(p_stat) == false) 
			return false;

		flushToken(p_stat);

		if (compareTokensType(p_stat, L_PAR, FLUSH, NEXT) == true) {

			if (compareTokensType(p_stat, R_PAR, FLUSH, NEXT) == true) {

				p_stat->inFuncBody = true;

				// CALL <BODY>
				if (body(p_stat) == true) {

					p_stat->inFuncBody = false;
					
					if (compareTokensType(p_stat, KW_END, FLUSH, NEXT) == true) {

						if (compareTokensType(p_stat, SEMICOLON, FLUSH, NEXT) == true)
							rule_func_main = true;
						else
							errExpected(p_stat, "token ';'");
					}
					else
						errExpected(p_stat, "variable declaration, commands or key word 'end'");
				}
				else
					p_stat->inFuncBody = false;
			}
			else
				errMsg(p_stat, "Expected token ')', function main can NOT have parameters", 
						SYNTAX_ERROR);
		}
		else
			errExpected(p_stat, "token '('");
	}
	else
		errExpected(p_stat, "function identifier or key word 'main'");

	return rule_func_main;
}

/**
 * NON-TERMINAL: PRG
 * RULE: 
 * <PRG> -> function<FUNC><FUNC_MAIN>$
 */
bool prg(TParserStat *p_stat) {

	bool rule_prg = false;

	/** Generate Instruction */
	// prva instrukcia goto main, adresu main este nepoznam
	// neskor ju upravim pomocou listFirst a listGetData
	generateInstruction(I_GOTO,NULL,NULL,NULL);

	if (compareTokensType(p_stat, KW_FUNCTION, FLUSH, NEXT) == true) {

		// CALL <FUNC>
		if (func(p_stat) == true) {

			/** Generate Instruction */
			// teraz pride main
			generateInstruction(I_LAB,NULL,NULL,NULL);
			listFirst(&instrList);
			tInstr *I = listGetData(&instrList);
			I->addr3 = listGetPointerLast(&instrList);

			// CALL <FUNC_MAIN>	
			if (func_main(p_stat) == true) {
					
				if (compareTokensType(p_stat, END_OF_FILE, FLUSH, NEXT) == true) {

					/** Generate Instruction */
					generateInstruction(I_STOP,NULL,NULL,NULL);
				
					rule_prg = true;
				}
				else {
					
					fprintf(stderr, "%s:%d: Unexpected token '%s', expected end of file.\n", 
							p_stat->sourceFile, p_stat->line, strGetStr(&(p_stat->p_token->key)));
				}
			}
		}
	}
	else
		errExpected(p_stat, "key word 'function'");

	return rule_prg;
}

void initParserStat(TParserStat *p_stat, TProgStat *p_prgStat) {
	p_stat->inFuncBody = false;
	p_stat->write = false;
	p_stat->local = false;
	p_stat->white = false;
	p_stat->main = false;
	p_stat->sourceFile = p_prgStat->sourceName;
	p_stat->line = FIRST_LINE;
	p_stat->ecode = EOK;
	p_stat->who = PARSER;
	p_stat->addrOfFunLabel = NULL;
	p_stat->p_actData = NULL;
	p_stat->p_token = NULL;
	p_stat->p_workFuncItem = NULL;
	p_stat->p_workTable = NULL;
}

void parser(TProgStat *p_prgStat) {

	// init parser status structure
	TParserStat stat;
	initParserStat(&stat, p_prgStat);

	// init token
	tToken token;
	stat.p_token = &token;
	initToken(stat.p_token);

	// PARSER STARTS WITH FIRST RULE <PRG>
	if (prg(&stat) == false) {

		if (stat.ecode == EOK)		// syntax error = one of the rules return false
			p_prgStat->ecode = SYNTAX_ERROR;
		else {
			// malloc error or precedence analyser ...
			p_prgStat->ecode = stat.ecode;
		}
	}

	strFree(&(token.key));			// free string for token

	if (token.type == STRING) {
		strFree(&(token.data.stringValue));
   	}
}




