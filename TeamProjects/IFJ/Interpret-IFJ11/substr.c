/** 
 * Ján Porhinčák (xporhi00)
 *
 * last changes: 9.11.2010
**/

#ifndef STR_H
  #include "str.h"
#endif
#ifndef SUBSTR_H
  #include "substr.h"
#endif
#ifndef STDIO_H
  #include <stdio.h>
  #define STDIO_H
#endif
#ifndef STRING_H
  #include <string.h>
  #define STRING_H
#endif
#ifndef STDLIB_H
  #include <stdlib.h>
  #define STDLIB_H
#endif

/**
 * Funkcia pre navrat podretazca
 * @param: strIn ukazatel na vstupnu strukturu string
 * @param: begin pozicia zaciatku podretazca
 * @param: end pozicia konca podretazca
 * @param: Error v pripade chyby je prislusna chybova hodnota navratena pomocou tohoto ukazatela
 * @return: ukazatel na strukturu typu string, obsahujucu podretazec
**/
string* substr(string* strIn, double begin, double end, int* Error)
{
	//deklaracia
	int from, to, xchg;
	string* strOut;
	//inicializacia
	from = to = xchg = 0;
	strOut = NULL;
	
	//pociatocne overenie vstupov ukazatelovych premennych
	if ((strIn == NULL) || (strIn->str == NULL))
		return NULL;
	
	//upravenie hranicnych hodnot
	from = borderTransf(strIn->length, begin);
	to = borderTransf(strIn->length, end);
	if (from > to) { //zaistime aby vzdy platilo from <= to inak vratime prazdny retazec
		from = 0, to = 0;
		}
	
	if(from != 0)
		from--;
	
	//vytvorenie struktury string
	strOut = malloc(sizeof(string));
	if (strOut == NULL) {
		*Error = 5;
		return NULL;
		}
	else
		strOut->str = NULL;
	strOut->length = to - from;
	strOut->allocSize = ((to - from) +1)*sizeof(char);
	strOut->str = malloc(strOut->allocSize);
	if (strOut->str == NULL) {
		free(strOut);
		*Error = 5;
		return NULL;
		}
	
	//samotne kopirovanie podretazca
	for (int i=0; i < strOut->length; i++)
		strOut->str[i] = strIn->str[from+i];
	strOut->str[strOut->length] = '\0';
	
	return strOut;
}

/**
 * Interna funkcia pre substr, transformuje hranice na kladne cisla v intervale <1, pocet pismen+1>
 * @param: length dlzka vstupneho retazca funkcie substr
 * @param: borderIn vstupna hodnota hranice podretazca
 * @return: border transformovana hodnota hranice podretazca
**/
int borderTransf(int length, double borderIn)
{
	int border = borderIn; //orezanie prip desatinnej casi
	if (abs(border) >= length) {
		if (border >= 0)
			border = length;
		else
			border = 0;
		}
	else {
		if (border < 0) 
			border = length +1 + border;
		}
	return border;
		
}

/**
 * Interna funkcia konkatenacie dvoch retazcov
 * @param: strIn1 prvy vstupny retazec
 * @param: strIn2 druhy vstupny retazec
 * @return: novy retazec ktory vznikol konkatenaciou vstupnych retazcov
**/
string* concat(string* strIn1, string* strIn2, int* Error)
{
	//deklaracia
	int i;
	string* strOut;
	//overenie vstupnych podmienok
	if ((strIn1 == NULL) || (strIn1->str == NULL) || (strIn2 == NULL) || (strIn2->str == NULL))
		return NULL;
		
	strOut = malloc(sizeof(string));
	if (strOut == NULL) {
		*Error = 5;
		return NULL;
		}
	
	strOut->length = strIn1->length + strIn2->length;
	strOut->allocSize = (strOut->length +1) * sizeof(char);
	
	strOut->str = malloc(strOut->allocSize);
	if (strOut->str == NULL) {
		*Error = 5;
		free(strOut);
		return NULL;
		}
	
	for(i=0; i < strIn1->length; i++)
		strOut->str[i] = strIn1->str[i];
	for(i=0; i <= strIn2->length; i++)
		strOut->str[strIn1->length+i] = strIn2->str[i];
		
	return strOut;		
}
