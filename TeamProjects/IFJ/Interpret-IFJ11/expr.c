/**
 *	Project:	Interpret of the Lua programming language derivate IFJ11.
 *	File:		expr.c
 *	Module:		modul for precedence analysis
 *	Date:		13.11.2011
 *	Authors:	Michal Starigazda, xstari01@stud.fit.vutbr.cz	
 *
 *
 *	Description:
 *	
 */
 
#ifndef EXPR_H
	#include "expr.h"
#endif


#ifndef ERROR_H
	#include "error.h"
#endif
#ifndef SCANNER_H
	#include "scanner.h"
#endif

#ifndef BOOLH
	#include <stdbool.h>
	#define BOOLH
#endif 

//#define DEBUG
//#define DEBUG_ARG_PUSH
//#define DEBUG_STACK
//#define TEST
//#define TEST_ID

#define SUBSTR_ARGS 3
#define FIND_ARGS 2
#define SORT_TYPE_ARGS 1
#define FAIL -1
/**
 	PRAVIDLA:
 	
 	1. E -> (E)				
 	2. E -> i
 	3. E -> E op E				// + - * / < <= > >= == ~= .. and or not
 	4. E -> notE
 	5. E -> f()
 	6. E -> f(E)
 	7. E -> f(L)
 	8. L -> L,E
 	9. L -> E,E
 */
 
/**
 * Vycet indexovatelnych terminalov precedencnej tabulky.
 * Kvoli redukcii velkosti tabulky sa vytvorili 'skupiny terminalov' s rovnakym
 * indexom v tabulke, do skupiny sa zlucili terminaly s rovnakymi operatormi
 * precedencnej analyzy (PATSymbol).
 */
enum terminalGroups{
	arthm_addSub = 0,			// arithmetic + -
	arthm_multDiv,				// arithmetic * /
	concat,						// ..
	arthm_power,				// arithmetic ^
	relat,						// relational < > <= >= == ~= 
	and,						// and
	or,							// or
	not,						// not
	f,							// identifikator funkcie
	comma,						// ,
	l_par,						// (
	r_par,						// )
	operand,					// i
	dollar						// $ ukoncovaci znak
};
 	
/**
 * Prvky precedencnej tabulky.
 */
enum PATSymbol {
	Lt,		// <
	Gt,		// >
	Eq,		// =
	Err,	// chyba
	End		// koncovy stav
};

 /**
 * Precedencna tabulka.
 */
const int PATable[14][14] = {
//						|	0.		1.		2.		3.		4.		5.		6.		7.		8.		9.		10.		11.		12.		13.
//		 				|	+		*		..		^		<		and		or		not		f		,		(		)		i		$
//						|	-		/						>
//						|									>=
//						|									<=
//						|									==
//						|									~=
//--------------------------------------------------------------------------------------------------------------------------------------
/*	0.	arthm_addSub	*/{	Gt,		Lt,		Gt,		Lt,		Gt,		Gt,		Gt,		Lt,		Lt,		Gt,		Lt,		Gt,		Lt,		Gt,  },
/*	1.	arthm_multDiv	*/{	Gt,		Gt,		Gt,		Lt,		Gt,		Gt,		Gt,		Lt,		Lt,		Gt,		Lt,		Gt,		Lt,		Gt,  },
/*	2.	..				*/{	Lt,		Lt,		Gt,		Lt,		Gt,		Gt,		Gt,		Lt,		Lt,		Gt,		Lt,		Gt,		Lt,		Gt,  },
/*	3.	arthm_power		*/{	Gt,		Gt,		Gt,		Lt,		Gt,		Gt,		Gt,		Gt,		Lt,		Gt,		Lt,		Gt,		Lt,		Gt,  },
/*	4.	relat			*/{	Lt,		Lt,		Lt,		Lt,		Gt,		Gt,		Gt,		Lt,		Lt,		Gt,		Lt,		Gt,		Lt,		Gt,  },
/*	5.	and				*/{	Lt,		Lt,		Lt,		Lt,		Lt,		Gt,		Gt,		Lt,		Lt,		Gt,		Lt,		Gt,		Lt,		Gt,  },
/*	6.	or				*/{	Lt,		Lt,		Lt,		Lt,		Lt,		Lt,		Gt,		Lt,		Lt,		Gt,		Lt,		Gt,		Lt,		Gt,  },
/*	7.	not				*/{	Gt,		Gt,		Gt,		Lt,		Gt,		Gt,		Gt,		Err,	Lt,		Gt,		Lt,		Gt,		Lt,		Gt,  },
/*	8.	f				*/{	Err,	Err,	Err,	Err,	Err,	Err,	Err,	Err,	Err,	Err,	Eq,		Err,	Err,	Err, },
/*	9.	,				*/{	Lt,		Lt,		Lt,		Lt,		Lt,		Lt,		Lt,		Lt,		Lt,		Gt,		Lt,		Gt,		Lt,		Err, },
/*	10.	(				*/{	Lt,		Lt,		Lt,		Lt,		Lt,		Lt,		Lt,		Lt,		Lt,		Lt,		Lt,		Eq,		Lt,		Err, },
/*	11.	)				*/{	Gt,		Gt,		Gt,		Gt,		Gt,		Gt,		Gt,		Err,	Err,	Gt,		Err,	Gt,		Err,	Gt,  },
/*	12.	i				*/{	Gt,		Gt,		Gt,		Gt,		Gt,		Gt,		Gt,		Err,	Err,	Gt,		Err,	Gt,		Err,	Gt,  },
/*	13.	$				*/{	Lt,		Lt,		Lt,		Lt,		Lt,		Lt,		Lt,		Lt,		Lt,		Lt,		Lt,		Err,	Lt,		End, } 
}; 

//------------------------------- ZASOBNIK -------------------------------------

/**
 * Inicializacia zasobniku.
 * @param stack Ukazatel na zasobnik.
 * @retun chybovy kod.
 */
int initPAStack(TStack* stack)
{
	int ecode = EOK;
	
	stack->stack = (TStackPAItem *)calloc(STACK_SIZE, sizeof(TStackPAItem));
	
	if(stack->stack == NULL)
	{
		perror("");
		ecode = MALLOC_ERROR;
	}
	else
	{
		stack->top = -1;
		stack->size = STACK_SIZE;
	}
	
	return ecode;
}

/**
 * Uvolnenie pamate alokovanej zasobnikom.
 * @param stack Ukazatel na zasobnik.
 */
void deleteStack(TStack *stack)
{
	free((stack->stack));
}

/**
 * Uvolnenie dat prvku zasobnika.
 * @item Ukazatel na prvok.
 */
void freeItem(TStackPAItem *item)
{
	if (item->data && item->help_type != ID && item->help_type != FUNCT && item->help_type != POM)
		{
			if (item->type == STRING || item->help_type == STRING)
			{
				strFree(&(item->data->value.stringValue));
			}
			free(item->data);
			item->data=NULL;
		}
}

/**
 * Funkcia prechadza prvky zasobniku (pola) a uvolnuje data konstant.
 * @param stack Ukazatel na zasobnik.
 */
void clearPAStack(TStack *stack)
{
	for (int i = stack->top; i >= 0; i--)
	{
		freeItem(&(stack->stack[i]));
	}
}

/**
 * Funkcia nad zasobnikom - POP.
 * @param stack Ukazatel na zasobnik.
 */
void stackPop(TStack *stack)
{
		stack->top--;
}

/**
 * Funkcia nad zasobnikom - PUSH.
 * @param stack Ukazatel na zasobnik.
 * @return Chybovy kod (expanzia).
 */ 
int stackPush(TStack *stack, TStackPAItem item)
{
	int ecode = EOK;
	
	if (++(stack->top) > (stack->size-1))
	{// je nutne zvacsit zasobnik
		stack->size <<= 1;	//zdvojnasobime velkost zasobniku
	
		#ifdef DEBUG
			printf("realokujem zasobnik\n");
		#endif
	
		TStackPAItem *tmp = (TStackPAItem*)calloc(stack->size, sizeof(TStackPAItem));
		if (tmp == NULL)
		{
			perror("");
			return MALLOC_ERROR;
		}
			
		for (int i = 0; i <= (stack->top-1); i++)
		{
			tmp[i] = stack->stack[i];
		}
		free(stack->stack);
		stack->stack = tmp;
	
		#ifdef DEBUG
			printf("realokoval som zasobnik\n");
		#endif
	}
	stack->stack[stack->top] = item;

	return ecode;
}

/**
 * Funkcia nad zasobnikom - TOP a nasledovny POP.
 * @param stack Ukazatel na zasobnik.
 * @param item Ukazatel na prvok do ktoreho sa skopiruje vrchol zasobniku.
 */ 
void stackTopPop(TStack *stack, TStackPAItem *item)
{
	if (stack->top >= 0)
	{
		*item = stack->stack[stack->top];
		stack->top--;
	}
}

/**
 * Funkcia nad zasobnikom, ktora vlozi za najvrchnejsi terminal '<'.
 * @param stack Ukazatel na zasobnik.
 * @return Chybovy kod (vola stackPush).
 */ 
int stackPushLt(TStack *stack)	
{
	int ecode = EOK;
	
	TStackPAItem item = {LESS,EMPTY,0, false, NULL, NULL};
	
	if (stack->stack[stack->top].type == E ||		// na vrchole je E
		stack->stack[stack->top].type == L ||		// na vrchole je L
		stack->stack[stack->top].help_type == FUNCT)	// na vrchole je f
	{
		TStackPAItem tmpItem;
		stackTopPop(stack, &tmpItem);
			
		ecode = stackPush(stack, item);
		if(ecode == EOK)
		{
			ecode = stackPush(stack, tmpItem);
		}
	}
	else	
		ecode = stackPush(stack, item);

	return ecode;
}

/**
 * Funkcia nad zasobnikom, ktora vracia hodnotu najvrchnejsieho terminalu.
 * @param stack Ukazatel na zasobnik.
 * @param item Ukazatel na prvok do ktoreho sa skopiruje terminal.
 */ 
void stackTopTerm(TStack *stack, TStackPAItem *item)
{
	TStackPAItem topTerm;
	
	for(int i = stack->top; i >= 0; i--)
	{
		topTerm = stack->stack[i];
		
		if(topTerm.type != E && topTerm.type != L) 
		{
			*item = topTerm;
			break;
		}
	}
}

/**
 * Funkcia nad zasobnikom - TOP.
 * @param stack Ukazatel na zasobnik.
 * @param item Ukazatel na prvok do ktoreho sa skopiruje vrchol zasobniku.
 */ 
void stackTop(TStack *stack, TStackPAItem *item)
{
	if (stack->top >= 0)
		*item = stack->stack[stack->top];
}

//----------------------------------------------------------------------------

/**< Globalna premenna indikujuca spracovanie vyrazu v prikaze 'write();' */
bool writeFlag = false;
/**< Globalna premenna indikujuca spracovanie parametrov funkcie */
bool funcFlag = false;

/**
 * Funkcia vracia pre validny typ token (typ prvku) hodnotu indexu
 * ktoru nadobuda jeho terminal na riadku (stlpci) tabulky precedencnej analyzy
 * (stvorcova matica).
 * @param type Typ tokenu (prvku).
 * @return Index.
 */
int getIndex(tTokenType type)
{
	if (type == ID || type == NUMBER || type == STRING || type == KW_NIL ||
		type == BOOL || type == KW_MAIN)
		return operand;
	else
	if (type == KW_TYPE || type == KW_FIND || type == KW_SORT || type == KW_SUBSTR)
		return f;
	else
	if (type == KONKAT) return concat;
	else
	if (type == LESS || type == GREATER || type == LESS_EQ || type == GREATER_EQ ||
		type == NOT_EQ || type == EQ)
		return relat;
	else
	if (type == ADD || type == SUB) return arthm_addSub;
	else
	if (type == MULTIP || type == DIV) return arthm_multDiv;
	else
	if (type == POWER) return arthm_power;
	else if (type == AND ) return and;
	else if (type == OR ) return or;
	else if (type == NOT) return not;
	else if (type == COMMA) {
		return (writeFlag) ? ((funcFlag) ? comma : dollar) : comma;}
	else if (type == R_PAR) return r_par;
	else if (type == L_PAR) return l_par;
	else
		return dollar;
}

/**
 * Funkcia vracia pre prvky hodnoty (pre ich typy) polozky danej ich indexmi 
 * v precedencnej tabulke.
 * @param A Najvrchnejsi terminal zasobniku.
 * @param B Nacitany terminal (spracovany token).
 * @return Hodnota polozky danej indexmi terminalov.
 */
int getItem(TStackPAItem A, TStackPAItem B)
{
	int indexA, indexB;
	
	if ((A.type == ID || A.type == KW_MAIN) && A.help_type == FUNCT ) indexA = f;
	else indexA = getIndex(A.type);
	if ((B.type == ID || B.type == KW_MAIN)  && B.help_type == FUNCT ) indexB = f;
	else indexB = getIndex(B.type);
	return PATable[indexA][indexB];
}

/**
 * Funkcia zistuje z nacitaneho tokenu ci sa jedna o operand vyrazu.
 * @param type Typ tokenu (validne: literaly, nil, true, false, id).
 * @return Uspesnost testu na validny typ tokenu pre operand.
 */
bool isOperand(tTokenType type)
{
	if (type == ID || type == NUMBER || type == STRING || type == KW_NIL ||
		type == BOOL || type == KW_MAIN) 
		return true;
	else 
		return false;
}

/**
 * Debug funkcia - vypis obsahu zasobniku.
 * @param stack Ukazatel na zasobnik.
 */
void printPAStack(TStack *stack)
{
printf("[ ");
	for (int i = 0; i <= stack->top; i++)
	{
		if (stack->stack[i].type ==E)
			printf("E ");
		else
		if (stack->stack[i].type ==L)
			printf("L ");
		else
		if (isOperand(stack->stack[i].type))
		{
			if (stack->stack[i].help_type == FUNCT) printf("f ");
			else printf("i ");
		}
		else
		if (stack->stack[i].type ==SEMICOLON)
			printf("; ");
		else
		if (stack->stack[i].type == R_PAR)
			printf(") ");
		else
		if (stack->stack[i].type == L_PAR)
			printf("( ");
		else
		if (stack->stack[i].type == LESS)
			printf("< ");
		else
		if (stack->stack[i].type == COMMA)
			printf(", ");
		else
			printf("term ");
	}
	
	printf("]\n");
}

//-----------------------------------------------------------------------------

/**
 * Aplikacia pravidla < E > --> i.
 * Zasobnik nie je prazdny, nemoze dojst k chybe pri POP.
 * Kontrola na chybu pri PUSH nie je nutne, k realokacii nemoze dojst.
 * @param stack Ukazatel na zasobnik.
 */
void applyOperandRule(TStack *stack)
{
	TStackPAItem item;
	
	stackTopPop(stack, &item);
	stackPop(stack);	// odstranenie z vrchu < 
	
	if (item.help_type != ID && item.help_type != FUNCT)
	{
		item.help_type = item.type;
	}
	item.type = E;		// z terminalu spravime neterminal
	
	(void)stackPush(stack, item);
}

/**
 * Indikuje, ci sa prava spracovava funkcia.
 * @param stack Ukazatel na zasobnik.
 * @return True pokial sa spracovava funkcia.
 */
bool isFunc(TStack *stack)
{
	for (int i = stack->top; i > 0; i--)
	{
		if (stack->stack[i].type == L_PAR)
		{
			if (stack->stack[--i].help_type == FUNCT)
				return true;
			else break;
		}
	}
	return false;
}

/**
 * Indikuje ci sa maju spracovane parametre zahadzovat alebo sa ma generovat
 * instrukcia I_PUSH.
 * @return True pokial sa ma generovat instrukcia.
 */
bool getGenFlag(TStack *stack)
{
	for (int i = stack->top; i >= 0; i--)
	{
		if (stack->stack[i].help_type == FUNCT )
		{
			if (stack->stack[i].argCnt > 0)
				return stack->stack[i].genInst;
			else 
				return false;
		}
	}
	return true;	
}

/**
 * Funkcia vracia pocet argumentov (ocakavanych) najvrchnejsie funkcie na zasobniku.
 * Pokial sa funkcia nenasla vracia sa <0.
 * @param stack Ukazatel na zasobnik.
 * @return Pocet argumentov v pripade uspechu, inak zapornu hodnotu.
 */
int topFuncArgCnt(TStack *stack)
{
	for (int i = stack->top; i >= 0; i--)
	{
		if (stack->stack[i].help_type == FUNCT)
			return stack->stack[i].argCnt;
	}
	return FAIL;	
}

/**
 * Funkcia dekrementuje pocet nesprcovanych argumentov aktualnej funkcie.
 * @param stack Ukazatel na zasobnik.
 */
void decTopFunc(TStack *stack)
{
	for (int i = stack->top; i >= 0; i--)
	{
		if (stack->stack[i].help_type == FUNCT)
		{
			stack->stack[i].argCnt--;
			break;
		}
	}
}

/**
 * Dogeneruje instrukcie push (pushuje NULL), pokial aktualne spracovana
 * funkcia si vyzaduje viac argumentov.
 * @param stack Ukazatel na zasobnik.
 */
void generetePushNULL(TStack *stack)
{
	for (int i = topFuncArgCnt(stack); i > 0; i--)
	{
		#ifdef DEBUG_ARG_PUSH
			printf("Pushujem argument funkcie (NULL)\n");
		#endif
		generateInstruction(I_PUSH, NULL, NULL, NULL);
	}
}

/**
 * Funkcia zistuje typ aktualne spracovavanej funkcie (uzivatelska X vnutorna).
 * @param stack Ukazatel na zasobnik.
 * @return Typ funkcie.
 */
int topFuncType(TStack *stack)
{
	for (int i = stack->top; i >= 0; i--)
	{
		if (stack->stack[i].help_type == FUNCT)
		{
			return stack->stack[i].type;
		}
	}	
	return 0;
}

/**
 * Aplikacia pravidla < E > --> ( E ).
 * Zasobnik nie je prazdny, nemoze dojst k chybe pri POP.
 * Kontrola na chybu pri PUSH nie je nutne, k realokacii nemoze dojst.
  * @param p_stat Ukazatel na status suntaktickej analyzy.
 * @param stack Ukazatel na zasobnik.
 * @return True pre uspesnu redukciu podla pravidla (E).
 */
bool applyParanRule(TStack *stack, TParserStat *p_stat)
{
	TStackPAItem item = {EMPTY,EMPTY,0, false, NULL, NULL};
	TStackPAItem tmp = {EMPTY,EMPTY,0, false, NULL, NULL};
	
	stackPop(stack);				//odoberieme prvu zatvorku ')'
	
	// rozhodni kere pravidlo aplikovat:
	// 	<f()
	//	<f(E)
	//	<f(L)
	
	#ifdef DEBUG	
		printf("Testujem ci sa jedna o pravidlo funkcie\n");
	#endif
		
	if (isFunc(stack))
	{
		stackTopPop(stack, &tmp);		// ulozim a odoberieme vrchol
		
		#ifdef DEBUG	
			printf("Redukcia pravidla funkcie\n");
		#endif
		
		switch (tmp.type){
			case E:
				// generuj instrukciu pushu na zasobnik, pokial ju tam treba
				if (topFuncArgCnt(stack) > 0 && getGenFlag(stack))
				{
					#ifdef DEBUG_ARG_PUSH
						printf("Pushujem argument funkcie\n");
					#endif
					
					generateInstruction(I_PUSH, NULL, (void *)tmp.data, NULL);
					decTopFunc(stack);
				}
			case L:
				stackPop(stack);				// odberieme druhu zatvorku '('
			case L_PAR:
				// pozri do funkcie a pripadne dogeneruj NULLy, dopln chybajuce
				// argumenty
				if (getGenFlag(stack)) generetePushNULL(stack);
				stackTopPop(stack, &tmp);		// ulozim a odoberieme f
				
				if (!getGenFlag(stack)) break;
			
			#ifndef TEST
				// alokuje sa pamat pre vysledok funkcie
				item.data = (tData *)malloc(sizeof(tData));
				if (!(item.data)){p_stat->ecode = MALLOC_ERROR; perror(""); return false;}
			#endif
			
				item.data->id = NULL;
				item.data->type = _FUNCTION;		
				
				switch(tmp.type){
					case KW_SORT:
						generateInstruction(I_SORT, NULL, NULL, (void*)item.data);	
						break;
					case KW_TYPE:
						generateInstruction(I_TYPE, NULL, NULL, (void*)item.data);
						break;
					case KW_SUBSTR:
						generateInstruction(I_SUBSTR, NULL, NULL, (void*)item.data);
						break;
					case KW_FIND:
						generateInstruction(I_FIND, NULL, NULL, (void*)item.data);
						break;
					default:
						generateInstruction(I_CALL, (void*)item.data, (void *)tmp.funcArgs, (void *)tmp.data);
						break;
				}
				item.help_type = POM;
				break;
			default: 
				errUnExpected(p_stat, "somthing, this should never happen.");
				freeItem(&item);
		}
		item.type = E;
	}
	else
	{// stav zasobniku:		<(E
		stackTopPop(stack, &item);		// ulozim a odoberieme neterminal 'E'	
		if (item.type != E){
			freeItem(&item);
			errUnExpected(p_stat, "arguments, not a function.");
			return false;}
		stackPop(stack);				// odberieme druhu zatvorku '('
	}
	
	stackPop(stack);				// odoberieme '<'
	(void)stackPush(stack, item);	// na vrcholu zasobniku sa vlozi neterminal
	
	return true;
}


/**
 * Aplikacia pravidla < E > --> E op E .
 * Zasobnik nie je prazdny, nemoze dojst k chybe pri POP.
 * @param p_stat Ukazatel na status suntaktickej analyzy.
 * @param stack Ukazatel na zasobnik.
 * @return True pre uspesnu redukciu podla pravidla E op E.
 */
bool applyBinOpRule(TStack *stack, TParserStat *p_stat)
{
	// stav zasobniku: 	<E + E  
	//					<notE 
	//					<L,E
	
	// ulozime si oba operandy a operator
	TStackPAItem A = {EMPTY, EMPTY,0, false, NULL, NULL};
	TStackPAItem B = {EMPTY, EMPTY,0, false, NULL, NULL};
	TStackPAItem operator = {EMPTY, EMPTY,0, false, NULL, NULL};
	TStackPAItem item = {E, EMPTY,0, false, NULL, NULL};
	
	stackTopPop(stack, &B);
	stackTopPop(stack, &operator);

	// alokujeme pamat pre medzivysledok
#ifndef TEST
	item.data = malloc(sizeof(tData));
	if (!(item.data)){p_stat->ecode = MALLOC_ERROR; perror(""); return false;}
#endif
	
	item.data->id = NULL;
	item.data->type = _UNKNOWN;	
	// redukcia E -> not E 
	if (operator.type == NOT)
	{
	 	//[ generuj instrukciu pre not a B ]
	 	generateInstruction(I_NOT,(void*)B.data, NULL, (void*)item.data);
	 	item.type = B.type;
	 	item.help_type = POM;
	}
	else
	{
		stackTopPop(stack, &A);
		item.help_type = POM;
		
		// urcime operaciu
		int op = getIndex(operator.type);
	
		switch (op){
			case arthm_addSub:
			case arthm_multDiv:
			case arthm_power:
				// operacia je validna nad typmi NUMBER a premenna (ID)
				if ((A.help_type == NUMBER || A.help_type == ID || A.help_type == POM) &&
					(B.help_type == NUMBER || B.help_type == ID || B.help_type == POM))
				{// priznak ocakavaneho vysledku
					item.help_type = NUMBER;
					
					// [ generuj instrukciu ]
					switch(operator.type){
						case ADD:
							generateInstruction(I_ADD, (void *)A.data, (void *)B.data, (void *)item.data);
							break;
						case SUB:
							generateInstruction(I_SUB, (void *)A.data, (void *)B.data, (void *)item.data);
							break;
						case MULTIP:
							generateInstruction(I_MULT, (void *)A.data, (void *)B.data, (void *)item.data);
							break;
						case DIV:
							generateInstruction(I_DIV, (void *)A.data, (void *)B.data, (void *)item.data);
							break;
						case POWER:
							generateInstruction(I_POWER, (void *)A.data, (void *)B.data, (void *)item.data);
							break;
						default:;}
				}
				else {
					errMsg(p_stat, "nekompatibilne typu operandov", SEM_ERROR);
				}	//  [ nekompatibilne typy ]
				break;
			case concat:
				// operacia je validna nad typom STRING
				if ((A.help_type == STRING || A.help_type == ID || A.help_type == POM) &&
					(B.help_type == STRING || B.help_type == ID || B.help_type == POM))
				{
					item.help_type = STRING;
					generateInstruction(I_KON, (void *)A.data, (void *)B.data, (void *)item.data);
				}
				else {
					errMsg(p_stat, "nekompatibilne typu operandov", SEM_ERROR);
				}	// [ nekompatibilne typy ]
				break;
			case relat:
				// operacia je validna nad typmi STRING alebo NUMBER, pre '=='
				// a '~=' je to jedno
				if ( ( (A.help_type == STRING || A.help_type == NUMBER) && B.help_type == A.help_type)
					 || ( (B.help_type == STRING || B.help_type == NUMBER || B.help_type == ID || B.help_type == POM) && (A.help_type == ID || A.help_type == POM))
					 || ( (A.help_type == STRING || A.help_type == NUMBER || A.help_type == ID || A.help_type == POM) && (A.help_type == ID || A.help_type == POM))
					 || operator.type == EQ  || operator.type == NOT_EQ )
				{
					item.help_type = BOOL;
					// [ generuj instrukciu ]
					switch(operator.type){
						case LESS:
							generateInstruction(I_ISLESS, (void *)A.data, (void *)B.data, (void *)item.data);
							break;
						case GREATER:
							generateInstruction(I_ISGR, (void *)A.data, (void *)B.data, (void *)item.data);
							break;
						case LESS_EQ:
							generateInstruction(I_ISLE, (void *)A.data, (void *)B.data, (void *)item.data);
							break;
						case GREATER_EQ:
							generateInstruction(I_ISGE, (void *)A.data, (void *)B.data, (void *)item.data);
							break;
						case EQ:
							generateInstruction(I_ISEQ, (void *)A.data, (void *)B.data, (void *)item.data);
							break;
						case NOT_EQ:
							generateInstruction(I_ISEQ, (void *)A.data, (void *)B.data, (void *)item.data);
							generateInstruction(I_NOT,(void*)item.data, NULL, (void*)item.data);
							break;
						default:;}
				}
				else {
					errMsg(p_stat, "nekompatibilne typu operandov", SEM_ERROR);
				}	//  [ nekompatibilne typy ]
				break;
			case and:
				// [ generuj instrukciu ]
				generateInstruction(I_AND, (void *)A.data, (void *)B.data, (void *)item.data);
				item.help_type = POM;
				; break;
			case or:
				// [ generuj instrukciu ]
				generateInstruction(I_OR, (void *)A.data, (void *)B.data, (void *)item.data);
				item.help_type = POM;
				; break;
			case comma:
			#ifndef TEST
				// nie je potrebna pamat pre medzivysledok
				free(item.data);
				item.data = NULL;
			#endif
				// instrukcie pushu sa generuju iba pokial nie je naplneny pocet
				// ocakavanych argumentov funkcie, inak sa data uvolnuju (argument
				// sa zahadzuje)
				if (A.type == E)
				{
					if (topFuncArgCnt(stack) > 0 && getGenFlag(stack))
					{
						#ifdef DEBUG_ARG_PUSH
							printf("Pushujem argument funkcie A\n");
						#endif
						generateInstruction(I_PUSH, NULL, (void *)A.data, NULL);
						decTopFunc(stack);
					}
					else
						freeItem(&A);		
				}
				if (B.type == E)
				{
					if (topFuncArgCnt(stack) > 0  && getGenFlag(stack))
					{
						#ifdef DEBUG_ARG_PUSH
							printf("Pushujem argument funkcie B\n");
						#endif
						// [ generuj instrukciu push ]
						generateInstruction(I_PUSH, NULL, (void *)B.data, NULL);
						decTopFunc(stack);
					}
					else
						freeItem(&B);
				}
				item.type = L;
				break;
			default:
				errUnExpected(p_stat, "operator type in expression.");
		}// switch			
	}
	if (p_stat->ecode != EOK)
	{
		// je treba uvolnit nacitane operandy
		freeItem(&A);
		freeItem(&B);
		// 
		free(item.data);
		item.data = NULL;
		return false;
	}
	(void)stackPop(stack);			// odoberiem  '<'
	(void)stackPush(stack, item);	// na vrcholu zasobniku sa vlozi neterminal
	return true;
}

/**
 * Vola pravidlo podla ktoreho sa bude redukovat zasobnik.
 * @param p_stat Ukazatel na status suntaktickej analyzy.
 * @param stack Ukazatel na zasobnik.
 * @return True pre uspesnu redukciu.
 */
bool reduction(TStack *stack, TParserStat *p_stat)
{
	// pravidlo < E > --> i ak na vrchole je operand (terminal)
	if (isOperand(stack->stack[stack->top].type))
	{
		applyOperandRule(stack);
	}
	else
	//  ak na vrchole je ')'
	if (stack->stack[stack->top].type == R_PAR)
	{
		if (!applyParanRule(stack, p_stat)) return false;
	}
	else
	// pravidlo < E > --> E op E ak na vrchole je E
	if (stack->stack[stack->top].type == E)
	{
		if (!applyBinOpRule(stack, p_stat)) return false;
	}
	else{
		errMsg(p_stat, "missing operand", SYNTAX_ERROR);	
		return false;
	}
		
	return true;;
}

/**
 * Funkcia inicializuje nacitany prvok na zaklade hodnot tokenu.
 * @param p_stat Ukazatel na status suntaktickej analyzy.
 * @param stack Ukazatel na zasobnik.
 * @return True pre uspesnu inicializaciu.
 */
bool setItem(TStackPAItem *item, TParserStat *p_stat, TStack *stack)
{
	// inicializacia na zakladne hodnoty
	item->type = p_stat->p_token->type;
	item->help_type = EMPTY;
	item->argCnt = 0;
	item->genInst = false;
	item->data = NULL;
	
	#ifdef DEBUG
		printf("inicializujem nacitany item\n");
	#endif
	
	if (isOperand(item->type))
	{// pokial spracovavame operand tak sa bude alokovat
	
		// dva operandy za sebou su chyba
		if (stack && (stack->stack[stack->top].type == E ||
			stack->stack[stack->top].type == L ||
			isOperand(stack->stack[stack->top].type)))
		{
			errExpected(p_stat, "operator before operand");
			return false;
		}
	
		switch(item->type){
			case ID:
			case KW_MAIN:
				// kontrola deklaracia premennej
			#ifndef TEST_ID
				item->data = htRead ( p_stat->p_workTable, p_stat->p_token->key );
				if (!item->data)
				{
					// kontrola existencie funkcie
					item->argCnt = checkFuncId(p_funcList, p_stat->p_token);
					if (item->argCnt < 0)
					{// [ prislo nieco (ID) co nie je ani variable ani function ]
						errDeclaration(p_stat, FUNC_VAR);
						return false;
					}
					else
					{// inicializuj prvok ako funkciu
		
						item->genInst = getGenFlag(stack);
													
						#ifdef DEBUG_ARG_PUSH
							printf("ocakavany pocet argumentov funkcie '%s' : %d\n", p_stat->p_token->key.str,item->argCnt);
						#endif
					
						item->help_type = FUNCT;
					
						// ziskaj ukazatel na navesti funkcie v iliste
						item->data = getFuncLabel(p_funcList, p_stat->p_token);
						item->funcArgs = getFuncArgPtr(p_funcList, p_stat->p_token);
					}
				}
				else 
			#endif
					item->help_type = ID;
				break;
			
			default:
				// konstanta
			
			#ifndef TEST
				item->data = malloc(sizeof(tData));
				if (item->data)
				{
					if (item->type == STRING)
					{	
						strInit(&(item->data->value.stringValue));
						p_stat->ecode = strCopyString(&(item->data->value.stringValue), &(p_stat->p_token->data.stringValue));
						if (p_stat->ecode != EOK)
						{ 
							strFree(&(item->data->value.stringValue));
							free(item->data); 
							return false;
						}
						item->data->type = _STRING;
					}
					else
					{
						item->data->value = p_stat->p_token->data;
						if (item->type == NUMBER) item->data->type = _NUMBER;
						if (item->type == BOOL) item->data->type = _BOOL;
						if (item->type == KW_NIL) item->data->type = _UNKNOWN;
					}
					item->data->id = NULL;
				}
				else{
					p_stat->ecode = MALLOC_ERROR;
					perror("");
					return false;
				}
			#endif
				break;
		}// switch
	}
	else
	if (item->type == KW_TYPE || item->type == KW_FIND || item->type == KW_SORT || item->type == KW_SUBSTR)
	{// vnutorne funkcie
		item->help_type = FUNCT;
		switch (item->type){
			case KW_SUBSTR:
				item->argCnt = SUBSTR_ARGS; item->genInst = true; break;
			case KW_SORT:
				item->argCnt = SORT_TYPE_ARGS; item->genInst = true; break;
			case KW_FIND:
				item->argCnt = FIND_ARGS; item->genInst = true; break;;
			case KW_TYPE:
				item->argCnt = SORT_TYPE_ARGS; item->genInst = true; break;
			default:break;
		}
	}
	return true;
}				

/**
 * Funkcia spracovava vyraz precedencnou analyzou.
 * @param p_stat Ukazatel na status suntaktickej analyzy.
 * @param stack Ukazatel na zasobnik.
 * @return True pre uspesne ukoncenie analyzy.
 */
bool parseExpr(TParserStat *p_stat, TStack *stack)
{
	TStackPAItem itemA = {EMPTY,EMPTY,0, false, NULL, NULL};
	TStackPAItem itemB = {EMPTY,EMPTY,0, false, NULL, NULL};
	
	// vlozime na zasobnik dollarovu hodnotu, napr ';'
	itemA.type = SEMICOLON;
	p_stat->ecode = stackPush(stack, itemA);
	if (p_stat->ecode != EOK){ return false; }
	
	// nastavime hodnotu nacitaneho prvku podla dat tokenu
	if (!setItem(&itemB, p_stat, stack)) return false;
	
	do{
		// ziskaj symbol tabulky pre najvrchnejsi terminal na zasobniku a nacitany
		if (isFunc(stack)) funcFlag = true; else funcFlag = false;
		stackTopTerm(stack, &itemA);
		int state = getItem(itemA, itemB);

		#ifdef DEBUG_STACK
			printPAStack(stack);
			#ifdef DEBUG
			printf("symbol tabuly pre %d a %d je %d\n", getIndex(itemA.type), getIndex(itemB.type), state);
			#endif
		#endif
		
		switch(state){
			case Lt:
				// za najvrchnejsi terminal vlozime '<'
				p_stat->ecode = stackPushLt(stack);
				if (p_stat->ecode != EOK){ p_stat->ecode = MALLOC_ERROR; perror(""); return false;}	
			case Eq:		
				// pushneme nacitany prvok
				p_stat->ecode = stackPush(stack, itemB);
				if (p_stat->ecode != EOK){ p_stat->ecode = MALLOC_ERROR; perror(""); return false;}
					
				// nacitame dalsi token
				if (getNextToken(p_stat) == false){return false;}
				
				// nastavime hodnotu nacitaneho prvku podla dat tokenu
				if (!setItem(&itemB, p_stat, stack)) return false;
				break;			
			case Gt:				
				// redukcia podla jedneho z pravidiel
				if (!reduction(stack, p_stat)) return false;
				break;				
			case End:
				break;						 
			default:
				if ( itemB.type == R_PAR && p_stat->write){
					return true;
				}
				else{
					errUnExpected(p_stat, "token type in expression");
					return false;
				}
				break;
		}

	// v itemA aktualizujeme najvrchnejsi terminal
	stackTopTerm(stack, &itemA);
	// v itemB mame nacitany token zo vstupu
	}while (!(getIndex(itemA.type) == dollar && getIndex(itemB.type) == dollar)) ;

	return true;
}

/**
 * Precedencna analyza.
 * @param p_stat Status syntaktickej analyzy.
 * @return True pre uspesne spracovanie vyrazu. Chybovy kod pri neuspechu je
 * ulozeny v statuse.
 */
bool exprParser(TParserStat *p_stat)
{
	p_stat->ecode = EOK;
	writeFlag = p_stat->write;

	if (p_stat->local)
	{
		if ( p_stat->p_token->type == NUMBER ||
			 p_stat->p_token->type == STRING ||
			 p_stat->p_token->type == BOOL	 ||
			 p_stat->p_token->type == KW_NIL )
		{
				TStackPAItem item = {EMPTY,EMPTY,0, false, NULL, NULL};
				if(!setItem(&item, p_stat, NULL))
				{
					p_stat->p_actData = NULL;
					return false;
				}
				else
				{
					// nacitame dalsi token
					if (getNextToken(p_stat) == false){return false;}
					else{
						p_stat->p_actData = item.data;
						return true;
					}
				}
		}
		else{
			errExpected(p_stat, "literal");
			return false;
		}
	}
	
	TStack stack;
	p_stat->ecode = initPAStack(&stack);		// inicializacia zasobniku
	if (p_stat->ecode != EOK) return false;
	
	#ifdef DEBUG	
		printf("Precedencna analyza pripravena, volam parser\n");
		printf("Priznak aktivity flagWrite je na %d\n", writeFlag);
		printf("Priznak aktivity write je na %d\n", p_stat->write);
	#endif
			
	bool retValue = parseExpr(p_stat, &stack);
	if (!retValue){ 
		clearPAStack(&stack);		// pri chybe vycisti pamat
		p_stat->p_actData = NULL;}	
	else{
		if (p_stat->write)
		{
			if (stack.stack[stack.top].help_type == KW_NIL)
				errMsg(p_stat, "Invalid nil value in write.", SEM_ERROR);
			if	(stack.stack[stack.top].help_type == BOOL)
				errMsg(p_stat, "Invalid bool value in write.", SEM_ERROR);
			if (p_stat->ecode != EOK ){
				if (stack.stack[stack.top].data){
					free(stack.stack[stack.top].data);
					stack.stack[stack.top].data = NULL;
				}
				retValue = false;
			}
		}
		
		p_stat->p_actData = stack.stack[stack.top].data;
	}
	deleteStack(&stack);

	return retValue;		
}
