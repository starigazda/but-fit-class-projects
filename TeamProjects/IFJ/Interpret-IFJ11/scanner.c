/**
 *	Project:	Interpret of the Lua programming language derivate IFJ11.
 *	File:		scanner.c
 *	Module:		modul for lexical analysis
 *	Date:		17.11.2011
 *	Authors:	Michal Starigazda, xstari01@stud.fit.vutbr.cz	
 *
 *
 *	Description:
 *	
 */
 
#ifndef STDIO_H
  #include <stdio.h>
  #define STDIO_H
#endif
#ifndef STDLIB_H
  #include <stdlib.h>
  #define STDLIB_H
#endif
#ifndef CTYPE_H
  #include <ctype.h>
  #define CTYPE_H
#endif
#ifndef ERRNO_H
  #include <errno.h>
  #define ERRNO_H
#endif
#ifndef LIMITS_H
  #include <limits.h>
  #define LIMITS_H
#endif
#ifndef BOOL_H
	#include <stdbool.h>
	#define BOOL_H
#endif 

#ifndef IAL_H
  #include "ial.h"
#endif
#ifndef STR_H
  #include "str.h"
#endif
#ifndef PARSER_H
	#include "parser.h"
#endif

#ifndef SCANNER_H
  #include "scanner.h"
#endif



#ifndef ERROR_H
	#include "error.h"
#endif

#define DEBUG

FILE *f;	/**< Globalna premenna uchovava ukazatel na otvoreny zdrojovy subor.*/

/** Nastavenie zdrojoveho suboru pre nacitanie znakov. */
void setSourceFile(FILE *source)
{
	f = source;
}
 
/**
 * Funkcia konvertuje retazec na cislo typu double. Pokial je hodnota 
 * validna, ulozi sa do premennej predanej ukazatelom.
 * @param a Struktura typu string obsahujuca konvertovany retazec.
 * @param Ukazatel na premennu typu double.
 * @return Chybovy kod.
 */
int strToDouble(string a, double *val)
{
	errno = 0;
	char *endptr;
	
	double value;
	value = strtod(a.str, &endptr);
	
	if (errno == 0) {
		*val = value;
		return EOK;
	}
	else return NUMBER_ERROR;		// Tato situacia nemoze nikdy nastat vdaka
									// vdaka explicitnej kontrole retazca.
}

/**
 * Vratenie znaku na vstup, nastavenie priznaku white a aktualizacia pozicie
 * v zdrojovom subore.
 */
void myUngetc(char c, TParserStat *p_stat)
{
	if (isspace(c))
	{
		p_stat->white = true;	
		if (c == '\n') (p_stat->line)--;
	}
	else 
		p_stat->white = false;
		
	ungetc(c, f);
}

/**
 * Hlavna funkcia lexikalneho analyzatoru. Pocas behu sa riadi datami zo struk-
 * tury TParserStat, ktoru zaroven aktualizuje. 
 * @param data Ukazatel na strukturu typu TParserStat.
 * @return True pre uspesne spracovanie tokenu.
 */
bool getNextToken(TParserStat *p_stat)
{
    // inicializacia lokalnych premennych
	tScanState state = sInit;
	int c = 0;
	
	int ordValue = 0;		// ordinalna hodnota znaku v retazci dana \ddd
	int strFlag = INACTIVE;
	
	// vycistenie p_stat->p_tokenu
	strClear(&(p_stat->p_token->key));
   	if (p_stat->p_token->type == STRING) 
   		strFree(&(p_stat->p_token->data.stringValue));
   	p_stat->p_token->type = EMPTY;
   	p_stat->p_token->data.doubleValue = 0;
   	
	while(true){
		c = getc(f);
    		
      	// aktualizacia riadku, na ktorom sa nachadzame
		if (c == '\n'){(p_stat->line)++;}
		
		// FSM
		switch (state)
		{
			case sInit:
				if (isspace(c)) state = sInit; 		// biele znaky ignorujeme
				else
				if (isalpha(c) || c == '_'){
					strAddChar(&(p_stat->p_token->key), c);
					// identifikator
					state = sID;
				}
				else
				if (c >= '0' && c <='9'){
					// postupnost cisel sa docasne uklada do kluca p_stat->p_tokenu,
					// po zkonvertovani strToDouble sa hodnota konstanty ulozi
					// do unionu
					strAddChar(&(p_stat->p_token->key), c);
					// ciselna hodnota
					state = sNum;
				}
				else
				if (c == '"'){
					// inicializacia struktury string v unione !!!
					// po pouziti nezabudnut uvolnit pamat
					p_stat->ecode = strInit(&(p_stat->p_token->data.stringValue));
					if (p_stat->ecode != EOK) break;
					strFlag = ACTIVE;

					// retazec
					state = sStr;
				}
				else
				if (c == '.') state = sKonkat;					// moznost ..
				else
				if (c == '<') state = sLess;					// moznost <=
				else
				if (c == '>') state = sGreater;					// moznost >=
				else
				if (c == '=') state = sEq;						// moznost ==
				else 
				if (c == '~') state = sNotEq;					// moznost ~=
				else
				if (c == ',') p_stat->p_token->type = COMMA; 		// ,
				else
				if (c == ';') p_stat->p_token->type = SEMICOLON; 	// ;
				else
				if (c == '(') p_stat->p_token->type = L_PAR;		// (
				else
				if (c == ')') {
					c = getc(f);
					if (c == '\n'){(p_stat->line)++;}
					myUngetc(c, p_stat);
					p_stat->p_token->type = R_PAR;				// )
				}
				else
				if (c == '+') p_stat->p_token->type = ADD;		// +
				else
				if (c == '*') p_stat->p_token->type = MULTIP;	// *
				else
				if (c == '/') p_stat->p_token->type = DIV;		// /
				else
				if (c == '^') p_stat->p_token->type = POWER;	// ^
				else
				if (c == '-') state = sComSign; 		// zaciatok komentara 
														// alebo operator -
				else
				if (c == EOF) p_stat->p_token->type = END_OF_FILE;	// EOF
				else
					errMsg(p_stat, "Neplatny znak na vstupe.", LEX_ERROR);
				break;
      
			case sID:
				if (isalnum(c) || c == '_') 
					strAddChar(&(p_stat->p_token->key), c);
				else
				{				
					// kontrola na zhodu s logickym operatorom
					if (strCmpConstStr(&(p_stat->p_token->key), "and") == 0) p_stat->p_token->type = AND;
					else
					if (strCmpConstStr(&(p_stat->p_token->key), "or") == 0) p_stat->p_token->type = OR;
					else
					if (strCmpConstStr(&(p_stat->p_token->key), "not") == 0) p_stat->p_token->type = NOT;
					else
					// kontrola zhody s nejakym klucovym slovom
					if (strCmpConstStr(&(p_stat->p_token->key), "do") == 0) 
					{	
						if (p_stat->white)
							p_stat->p_token->type = KW_DO;
						else{
							errExpected(p_stat, "space before 'do'.");
						}
					}
					else
					if (strCmpConstStr(&(p_stat->p_token->key), "else") == 0) p_stat->p_token->type = KW_ELSE;
					else
					if (strCmpConstStr(&(p_stat->p_token->key), "end") == 0) p_stat->p_token->type = KW_END;
					else 
					if (strCmpConstStr(&(p_stat->p_token->key), "false") == 0) {
						p_stat->p_token->data.boolValue = false;
						p_stat->p_token->type = BOOL;}
					else 
					if (strCmpConstStr(&(p_stat->p_token->key), "function") == 0) 
						p_stat->p_token->type = KW_FUNCTION;
					else
					if (strCmpConstStr(&(p_stat->p_token->key), "main") == 0) p_stat->p_token->type = KW_MAIN;
					else
					if (strCmpConstStr(&(p_stat->p_token->key), "if") == 0) p_stat->p_token->type = KW_IF;
					else
					if (strCmpConstStr(&(p_stat->p_token->key), "local") == 0) p_stat->p_token->type = KW_LOCAL;
					else 
					if (strCmpConstStr(&(p_stat->p_token->key), "nil") == 0) p_stat->p_token->type = KW_NIL;
					else 
					if (strCmpConstStr(&(p_stat->p_token->key), "read") == 0) p_stat->p_token->type = KW_READ;
					else 
					if (strCmpConstStr(&(p_stat->p_token->key), "return") == 0) p_stat->p_token->type = KW_RETURN;
					else 
					if (strCmpConstStr(&(p_stat->p_token->key), "then") == 0) 
					{
						if (p_stat->white)
							p_stat->p_token->type = KW_THEN;
						else{
							errExpected(p_stat, "space before 'then'.");
						}
					}
					else 
					if (strCmpConstStr(&(p_stat->p_token->key), "true") == 0) {
						p_stat->p_token->data.boolValue = true;
						p_stat->p_token->type = BOOL; }
					else 
					if (strCmpConstStr(&(p_stat->p_token->key), "while") == 0) p_stat->p_token->type = KW_WHILE;
					else 
					if (strCmpConstStr(&(p_stat->p_token->key), "write") == 0) p_stat->p_token->type = KW_WRITE;
					else 
					if (strCmpConstStr(&(p_stat->p_token->key), "type") == 0) p_stat->p_token->type = KW_TYPE;
					else 
					if (strCmpConstStr(&(p_stat->p_token->key), "find") == 0) p_stat->p_token->type = KW_FIND;
					else 
					if (strCmpConstStr(&(p_stat->p_token->key), "sort") == 0) p_stat->p_token->type = KW_SORT;
					else 
					if (strCmpConstStr(&(p_stat->p_token->key), "substr") == 0) p_stat->p_token->type = KW_SUBSTR;
					else 
					// kontrola zhody s rezervovanym slovom
					if (strCmpConstStr(&(p_stat->p_token->key), "break") == 0 ||
						strCmpConstStr(&(p_stat->p_token->key), "elseif") == 0 ||
						strCmpConstStr(&(p_stat->p_token->key), "for") == 0 ||
						strCmpConstStr(&(p_stat->p_token->key), "in") == 0 ||
						strCmpConstStr(&(p_stat->p_token->key), "repeat") == 0 ||
						strCmpConstStr(&(p_stat->p_token->key), "until") == 0) 
							p_stat->p_token->type = RESERVED_WORD;
					else 
						p_stat->p_token->type = ID;
						
						// vraciame posledny nacitany znak
						myUngetc(c, p_stat);
				}// else
				break;
        
			case sNum:
				if (c >= '0' && c <='9'){
					strAddChar(&(p_stat->p_token->key), c);
				}
				else
				if (c == '.'){
					strAddChar(&(p_stat->p_token->key),c);
					// ciselny literal obsahujuci desatinnu cast
					state = sDot;
				}
				else
				if (c == 'e' || c == 'E'){
					strAddChar(&(p_stat->p_token->key),c);
					// exponent
					state = sExp;
				}
				else
				{
					// vraciame posledny nacitany znak
					myUngetc(c, p_stat);
					
					// prevod retazca na na ciselnu hodnotu
					p_stat->ecode = strToDouble(p_stat->p_token->key, &(p_stat->p_token->data.doubleValue));
					if (p_stat->ecode != EOK) break;
					
					p_stat->p_token->type = NUMBER;
				} 
				break;
				
			case sDot:
				if (c >= '0' && c <='9'){
					strAddChar(&(p_stat->p_token->key), c);
					state = sFloat;
				}
				else 
					errMsg(p_stat, "Expected number after '.'.", LEX_ERROR);
				break;
				
			case sFloat:
				if (c >= '0' && c <='9'){
					strAddChar(&(p_stat->p_token->key), c);
				}
				else
				if (c == 'e' || c == 'E'){
					strAddChar(&(p_stat->p_token->key),c);
					// exponent
					state = sExp;
				}
				else
				{
					// vraciame posledny nacitany znak
					myUngetc(c, p_stat);
					
					// prevod retazca na na ciselnu hodnotu
					p_stat->ecode = strToDouble(p_stat->p_token->key, &(p_stat->p_token->data.doubleValue));
					if (p_stat->ecode != EOK) break;
					
					p_stat->p_token->type = NUMBER;
				} 
				break;
				
			case sExp:
				if (c == '+' || c == '-'){
					strAddChar(&(p_stat->p_token->key),c);
					// za exponentom sa moze nachadzat znamienko
					state = sSign;
				}
				else
				if (c >= '0' && c <='9'){
					strAddChar(&(p_stat->p_token->key), c);
					// musi nasledovat neprazdna postupnost cisel
					state = sExpNum;
				}
				else 
					errMsg(p_stat, "Expected number or sign after 'e'.", LEX_ERROR); 
				break;
				
			case sExpNum:
				if (c >= '0' && c <='9'){
					strAddChar(&(p_stat->p_token->key), c);
				}
				else
				{
					// vraciame posledny nacitany znak
					myUngetc(c, p_stat);
					
					// prevod retazca na na ciselnu hodnotu
					p_stat->ecode = strToDouble(p_stat->p_token->key, &(p_stat->p_token->data.doubleValue)); 
					if (p_stat->ecode != EOK) break;
					
					p_stat->p_token->type = NUMBER;
				} 
				break;
				
			case sSign:
				if (c >= '0' && c <='9'){
					strAddChar(&(p_stat->p_token->key), c);
					
					// musi nasledovat neprazdna postupnost cisel
					state = sExpNum;
				}
				else 
					errMsg(p_stat, "Expected number after sign.", LEX_ERROR);
				break;
					
			case sStr:
				if (c == '"'){
					if (strCmpConstStr(&(p_stat->p_token->data.stringValue), "*n") == 0) p_stat->p_token->type = FORMAT_N;
					else
					if (strCmpConstStr(&(p_stat->p_token->data.stringValue), "*l") == 0) p_stat->p_token->type = FORMAT_L;
					else
					if (strCmpConstStr(&(p_stat->p_token->data.stringValue), "*a") == 0) p_stat->p_token->type = FORMAT_A;
					else{
						c = getc(f);
						// nastaveni priznaku white
						if (c == '\n'){(p_stat->line)++;}
						myUngetc(c, p_stat);

						p_stat->p_token->type = STRING;
					}
				}			
				else
				if (c == '\\'){
					// nevkladame do retazca pretoze moze ist o tvar \ddd
					// escape sekvencia
					state = sEsc;
				}
				else 
				if (c > 1)	// ocaka sa ordinalna hodnota >1
					strAddChar(&(p_stat->p_token->data.stringValue),c);
				else
					errMsg(p_stat, "Invalid char in string input.", LEX_ERROR);
					
				break;
				
			case sEsc:
				if (c >= '0' && c <='9'){
					// za '\' moze nasledovat znak zadany hodnotou (\ddd)
					ordValue = (c - '0');
					state = sEscNum1;
					break;
				}
				else
				if (c == '"') 
					strAddChar(&(p_stat->p_token->data.stringValue), '\"');
				else
				if (c == '\\') 
					strAddChar(&(p_stat->p_token->data.stringValue), '\\');
				else
				if (c == 'n') 
					strAddChar(&(p_stat->p_token->data.stringValue), '\n');
				else
				if (c == 't')
					strAddChar(&(p_stat->p_token->data.stringValue), '\t');
				else
					errMsg(p_stat, "Invalid format after \\ .", LEX_ERROR);
				
				state = sStr;
				break;
				
			case sEscNum1:
				if (c >= '0' && c <='9'){
					ordValue = (ordValue*BASE) + (c - '0');
					state = sEscNum2;
				}
				else
					errMsg(p_stat, "Expected number after \\ in	\\ddd.", LEX_ERROR);
				break;
				
			case sEscNum2:
				if (c >= '0' && c <='9'){
					ordValue = (ordValue*BASE) + (c - '0');
					// kontrola rozsahu, max. hodnota je 255
					if ( ordValue <= MAX_ORD_VALUE && ordValue > 1)
					{
						// vlozime znak s ordinalnou hodnotou ordValue do retazca
						strAddChar(&(p_stat->p_token->data.stringValue), (char)ordValue);
						state = sStr;
					}
					else
						errMsg(p_stat, "Value out of range for \\ddd (expected from 001 to 255).", LEX_ERROR);		
				}
				else
					errMsg(p_stat, "Expected number after \\ in	\\ddd.", LEX_ERROR);
					
					
				break;
				
			case sComSign:
				if (c == '-') state = sCommentSwitch;	// -- zaciatok komentaru
				else 
				{
					p_stat->p_token->type = SUB;					// operator -
					ungetc(c, f);
				}
				break;
			
			case sCommentSwitch:
				if (c == '[') state = sBlock;	// mozny zaciatok blokoveho komentaru
				else
				if (c == EOF) { p_stat->p_token->type = END_OF_FILE; }
				else state = sLineComment;		// riadkovy komentar
				break;

				
			case sLineComment:
				// koniec riadku == koniec komentaru
				if (c == '\n') state = sInit;
				else
				if (c == EOF) { p_stat->p_token->type = END_OF_FILE; }
				break;
				
			case sBlock:
				if (c == '[') state = sBlockComment; // --[[ zaciatok bloku
				else
				if (c == EOF) { p_stat->p_token->type = END_OF_FILE; }
				else
					state = sLineComment;
				break;
				
			case sBlockComment:
				if (c == ']') state = sBlockEnd;	// mozny koniec bloku
				else
				if (c == EOF) 
				{ 
					errMsg(p_stat, "Neukonceny blokovy komentar.", LEX_ERROR);
				}
				break;
				
			case sBlockEnd:
				if (c == ']') state = sInit;		// koniec bloku
				else
				if (c == EOF) 
				{ 
					errMsg(p_stat, "Neuplne ukoncenie blokoveho komentaru.", LEX_ERROR);
				}
				else
					state = sBlockComment;
				break;
				
			case sKonkat:
				if (c == '.') 
					p_stat->p_token->type = KONKAT;					// ..
				else 
					errMsg(p_stat, "Expected '.' for concatenation.", LEX_ERROR);
				break;
				
			case sGreater:
				if (c == '=') p_stat->p_token->type = GREATER_EQ;		// >=
				else
				{
					ungetc(c, f);
					p_stat->p_token->type = GREATER;					// >
				}
				break;
				
			case sLess:
				if (c == '=') p_stat->p_token->type = LESS_EQ;		// <=
				else
				{
					ungetc(c, f);
					p_stat->p_token->type = LESS;						// <
				}
				break;
				
			case sNotEq:
				if (c == '=') 
					p_stat->p_token->type = NOT_EQ;					// ~=
				else 
					errMsg(p_stat, "Expected '='.", LEX_ERROR);
				break;
				
			case sEq:
				if (c == '=') p_stat->p_token->type = EQ;				// ==
				else
				{
					ungetc(c, f);
					p_stat->p_token->type = ASSIG;					// priradenie =
				}
				break;
				
			default:  
				errMsg(p_stat, "Unknown error occured.", UNKNOWN_ERROR);;
				
    }//switch(state)
    
	if (p_stat->ecode != EOK && strFlag == ACTIVE) 
	{
		strFree(&(p_stat->p_token->data.stringValue));
		return false;
	}
	else
	if (p_stat->p_token->type != EMPTY) 
	{
		if ( p_stat->p_token->type == FORMAT_N ||
			 p_stat->p_token->type == FORMAT_L ||
			 p_stat->p_token->type == FORMAT_A  )
		{
			p_stat->ecode = strCopyString(&(p_stat->p_token->key), &(p_stat->p_token->data.stringValue));
			strFree(&(p_stat->p_token->data.stringValue));
			if (p_stat->ecode != EOK) return false;
		}

		break;
	}
	
	if (p_stat->ecode != EOK) return false;
  }// while

  return true;
}
