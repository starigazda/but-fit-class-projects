/**
 *	Project:	Interpret of the Lua programming language derivate IFJ11.
 *	File:		ilist.c
 *	Module:		Single and double linked linear lists.
 *	Date:		Fri Dec  2 12:13:21 CET 2011
 *	Authors:	Miroslav Slivka, xslivk02@stud.fit.vutbr.cz	
 *				Michal Srubar, xsruba03@stud.fit.vutbr.cz	
 *
 *	FIXME:
 *	- u mmalocu doplnit nejake info o tom kde a co neslo alokovat
 *
 */

#ifndef STRING_H
	#include <string.h>
	#define STRING_H
#endif

#ifndef ILIST_H
  #include "ilist.h"
#endif
#ifndef IAL_H
	#include "ial.h"
#endif
#ifndef ERROR_H
	#include "error.h"
#endif 

	
void listInit(tListOfInstr *L)
// funkce inicializuje seznam instrukci
{
  L->first  = NULL;
  L->last   = NULL;
  L->active = NULL;
}

void setNull(tListOfInstr *L, void *ad) { 
	tListItem *ptr;
	ptr = L->first;
	while(ptr != NULL) {
		if(ptr->Instruction.addr1 == ad)
			ptr->Instruction.addr1 = NULL;
		if(ptr->Instruction.addr2 == ad)
			ptr->Instruction.addr2 = NULL;
		if(ptr->Instruction.addr2 == ad)
			ptr->Instruction.addr2 = NULL;
		ptr = ptr->nextItem;
		}
}

void *clearInstr(void *addr) {
	if(addr != NULL) {
		if(((tData *) addr)->type != _NIL && ((tData *) addr)->type != _ARG) {
			if(((tData *) addr)->type == _STRING)
				strFree(&(((tData *)addr)->value.stringValue));
		void *h = addr;			
		free(addr);
		return h;
		}
	}
	return NULL;
}

void listFree(tListOfInstr *L)
// funkce dealokuje seznam instrukci
{
  tListItem *ptr = NULL;
  void *temp;
  while (L->first != NULL)
  {
    ptr = L->first;
    L->first = L->first->nextItem;
    temp = clearInstr(ptr->Instruction.addr1);
    setNull(L,temp);
    if(ptr->Instruction.instType != I_CALL) {
		temp = clearInstr(ptr->Instruction.addr2);
		setNull(L,temp);
	}
	if(ptr->Instruction.instType == I_READ) {
		temp = clearInstr(ptr->Instruction.addr3);
		setNull(L,temp);
	}
    //skontrolujeme ci nahodou nie je inicializovany string
    // uvolnime celou polozku
    free(ptr);
  }
}

void listInsertLast(tListOfInstr *L, tInstr I)
// vlozi novou instruci na konec seznamu
{
  tListItem *newItem = NULL;
  if ((newItem = malloc(sizeof (tListItem))) == NULL) {
 		printf("MALLOC FAIL\n");
	   exit(1);	
  }
  newItem->Instruction = I;
  newItem->nextItem = NULL;
  if (L->first == NULL)
     L->first = newItem;
  else
     L->last->nextItem=newItem;
  L->last=newItem;
}

void listFirst(tListOfInstr *L)
// zaktivuje prvni instrukci
{
  L->active = L->first;
}

void listNext(tListOfInstr *L)
// aktivni instrukci se stane nasledujici instrukce
{
  if (L->active != NULL)
  L->active = L->active->nextItem;
}

void listGoto(tListOfInstr *L, void *gotoInstr)
// nastavime aktivni instrukci podle zadaneho ukazatele
// POZOR, z hlediska predmetu IAL tato funkce narusuje strukturu
// abstraktniho datoveho typu
{
  L->active = (tListItem*) gotoInstr;
}

void *listGetPointerLast(tListOfInstr *L)
// vrati ukazatel na posledni instrukci
// POZOR, z hlediska predmetu IAL tato funkce narusuje strukturu
// abstraktniho datoveho typu
{
  return (void*) L->last;
}

void *listGetPointerActive(tListOfInstr *L)
// vrati ukazatel na aktivnu instrukci
// POZOR, z hlediska predmetu IAL tato funkce narusuje strukturu
// abstraktniho datoveho typu
{
  return (void*) L->active;
}

tInstr *listGetData(tListOfInstr *L)
// vrati aktivni instrukci
{
  if (L->active == NULL)
  {
    printf("Runtime error: unactive instruction list\n");
    return NULL;
  }
  else return &(L->active->Instruction);
}

void printList(tListOfInstr *L) {
	listFirst(L);
	while(L->active != NULL) {
		switch(L->active->Instruction.instType) {
			case I_STOP:
			  printf("I_STOP\n");
			  break;
	        case I_WRITE:
	          printf("I_WRITE\n");
	          break;
            case I_READ:
              printf("I_READ\n");
              break;
            case I_NOT:
              printf("I_NOT\n");
              break;
            case I_AND:
              printf("I_AND\n");
              break;
            case I_OR:
              printf("I_OR\n");
              break;
            case I_IFGOTO:
              printf("I_IFGOTO\n");
              break;
            case I_GOTO:
              printf("I_GOTO\n");
              break;
            case I_LAB:
              printf("I_LAB\n");
              break;
            case I_ASSIGN:
              printf("I_ASSIGN\n");
              break;
            case I_MULT:
              printf("I_MULT\n");
              break;
            case I_DIV:
              printf("I_DIV\n");
              break;
            case I_ADD:
              printf("I_ADD\n");
              break;
            case I_SUB:
              printf("I_SUB\n");
              break;
            case I_ISGR:
              printf("I_ISGR\n");
              break;
            case I_ISLESS:
              printf("I_ISLESS\n");
              break;
            case I_ISGE:
              printf("I_ISGE\n");
              break;
            case I_ISLE:
              printf("I_ISLE\n");
              break;
            case I_ISEQ:
              printf("I_ISEQ\n");
              break;
            case I_PUSH:
              printf("I_PUSH\n");
              break;
            case I_CALL:
              printf("I_CALL\n");
              break;
            case I_RET:
              printf("I_RET\n");
              break;
            case I_KON:
              printf("I_KON\n");
              break;
            case I_FIND:
              printf("I_FIND\n");
              break;
            case I_TYPE:
              printf("I_TYPE\n");
              break;
            case I_SUBSTR:
              printf("I_SUBSTR\n");
              break;
            case I_SORT:
              printf("I_SORT\n");
              break;
		  }
		  listNext(L);
	  }
}

void initFunctionList(TFunctionList *p_list) {

	if (p_list == NULL)
		master("ILIST.C", "initFunctionList", "You tried to initialize NULL list.\n");

	p_list->p_first = NULL;
	p_list->p_last = NULL;
}

void initFunctionItem(TFunctionItem *item) {

	if (item == NULL)
		master("ILIST.C", "initFunctionItem", "You tried to initialize NULL item.\n");

	item->line = 0;
	item->params = 0;	
	item->funcName = NULL;
	item->addr = NULL;

	item->p_table = NULL;
	item->p_next = NULL;
}

TFunctionItem *searchFunc(TFunctionList *p_list, tToken *token) {

	if (p_list == NULL)
		master("ILIST.C", "printFunctionList", "Tried to work with NULL list of functions.\n");

	TFunctionItem *tmp = NULL;
	tmp = p_list->p_first;			// set tmp to first item

	while (tmp != NULL) {

		// FIXME: uninitializet ....
		//token->key.str = "dsa";	= OK
		if (strcmp(tmp->funcName, token->key.str) == 0)
			return tmp;

		tmp = tmp->p_next;		// set first to next one
	}

	return NULL;
}

int getFuncLine(TFunctionList *p_list, tToken *token) {

	if (p_list == NULL)
		master("ILIST.C", "printFunctionList", "Tried to work with NULL list of functions.\n");

	TFunctionItem *tmp = searchFunc(p_list, token);

	if (tmp != NULL) 
		return tmp->line;

	return UNDECLARED;
}

int checkFuncId(TFunctionList *p_list, tToken *token) {

	if (p_list == NULL)
		master("ILIST.C", "printFunctionList", "Tried to work with NULL list of functions.\n");

	TFunctionItem *tmp = NULL;
	tmp = searchFunc(p_list, token);

	if (tmp != NULL)
		return tmp->params;

	return UNDECLARED;
}

void* getFuncLabel(TFunctionList *p_list, tToken *token) {

	TFunctionItem *tmp = NULL;
	tmp = searchFunc(p_list, token);

	return tmp->addr;
}

int* getFuncArgPtr(TFunctionList *p_list, tToken *token)
{
	TFunctionItem *tmp = NULL;
	tmp = searchFunc(p_list, token);
	
	return &(tmp->params);
}

int insertFunc(TFunctionList *p_list, tToken *token, void *addrOfFunLabel, int line) {
	
	if ((p_list == NULL) || (token == NULL))
		master("ILIST.C", "insertFunc", "NULL funcList or Token\n");

	TFunctionItem *new = NULL;

	#ifdef FREE
	printf(">> MALLOC: insertFunc\n");
	#endif
	if ((new = malloc(sizeof(struct functionItem))) == NULL) {
		perror("");	
		return MALLOC_ERROR;
	}

	initFunctionItem(new);

	#ifdef FREE
	printf(">> MALLOC: insertFunc\n");
	#endif
	if ((new->funcName = (char *) malloc(token->key.allocSize)) == NULL) {
		perror("");	
		return MALLOC_ERROR;
	}

	// init new item
	new->line = line;
	new->params = 0;
	new->p_next= NULL;
	new->addr = addrOfFunLabel;

//	strncpy(new->funcName, "", strlen(""));
	//printf("SSSSSS: %s\n", new->funcName);

	//token->key.str = "main\0";
	strcpy(new->funcName, token->key.str);
	//printf("SSSSSS: %s\n", new->funcName);
	//strncpy(new->funcName, token->key.str, 
	//		strGetLength(&(token->key)));
	
	if (p_list->p_first == NULL) {
		p_list->p_first = new;
		p_list->p_last = new;
	}
	else {
		p_list->p_last->p_next = new;
		p_list->p_last = new;
	}

	return true;
}

void freeFunctionList(TFunctionList *p_list) {

	if (p_list == NULL)
		master("ILIST.C", "printFunctionList", "Tried to work with NULL list of functions.\n");

	TFunctionItem *tmp = NULL;

	while (p_list->p_first != NULL) {

		tmp = p_list->p_first;				// save first item

		// free table of symbols
		htClearAll(tmp->p_table);

		#ifdef FREE
		printf(">> FREE: table of symbols of the function: %s\n", tmp->funcName);
		#endif
		free(tmp->p_table);

		#ifdef FREE
		printf(">> FREE: free memory for name of the function: %s\n", tmp->funcName);
		#endif
		free(tmp->funcName);

		#ifdef FREE
		printf(">> FREE: free the entire item of function\n");
		#endif
		p_list->p_first = tmp->p_next;		// set first to next one
		free(tmp);
		tmp = NULL;
	}
}
