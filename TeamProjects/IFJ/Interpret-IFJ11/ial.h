/** 
 *  @author hash table - Mirosslav Slivka, xslivk02
 *  @lastChange - 20.11.2011
 *  @file - ial.c
 * ---------------------------
 */

#ifndef IAL_H
  #define IAL_H

#ifndef STR_H
  #include "str.h"
#endif
#ifndef BOOL_H
	#include <stdbool.h>
	#define BOOL_H
#endif 
#ifndef STDIO_H
  #include <stdio.h>
  #define STDIO_H
#endif
#ifndef STDLIB_H
  #include <stdlib.h>
  #define STDLIB_H
#endif
#ifndef STRING_H
  #include <string.h>
  #define STRING_H
#endif

//navratove hodnoty str
#define STR_ERROR   1
#define STR_SUCCESS 0

//--------------------- Vnutorne funkcie interpretu ---------------------------

#define FALSE -1

/**
 * Funkcia sort vytvory kopiu vstupneho retazca a nasledne zavola funkciu shellsort(), ktora obstara samotne radenie
 * @param: strIn vstupny string
 * @param: Error v pripade chyby je prislusna chybova hodnota navratena pomocou tohoto ukazatela
 * @return: funkcia vracia uz zoradenu kopiu vstupneho stringu
**/
string* sort(string*, int*);

/**
 * Funkcia zoradi znaky priamo vo vstupnom retazci
 * @param: strIn vstupny retazec, ktory bude radeny
**/
void shellsort(string*);

/**
 * Funkcia pre navrat indexu zaciatku hladaneho podretazca v retazci.
 * @param: P string vzoru, ktory sa bude vyhladavat
 * @param: T string textu, ktory bude prehladavany
 * @param: Error priznak chyby, pri bezchybnom stave obsahuje hodnotu 0
 * @return: chybovy kod funkcie
**/
int KMPfind(string*, string*);

/**
 * Funkcia naplna vektor fail, hodnotamy.
 * @param: P string vzoru, ktory sa bude vyhladavat
 * @param: T string textu, ktory bude prehladavany
 * @param: fail[] ukazatel na vektor, ktory sa bude naplnat
**/
void KMPVector(string*, int*);

//-----------------------------------------------------------------------------


/* Maximální velikost pole pro implementaci
   vyhledávací tabulky. Řešené procedury však
   využívají pouze HTSIZE prvků pole (viz
   deklarace této proměnné). 
*/
#define MAX_HTSIZE 127

typedef enum itemType {
	_NIL,
	_BOOL,
	_NUMBER,
	_STRING,
	_FUNCTION,
	_ARG,    /* argument fcie */
	_UNKNOWN
} tType;

/* typ klíče */
typedef string tKey;

typedef union tValue {			
	    bool boolValue;
	    double doubleValue;/* = [P.c. argumentu] ak type = ARG */
	    void *addrValue;	/* hlavne pre stack interpretu */
	    string stringValue;
    } TValue;				/* hodnota */


/* typ obsahu */
typedef struct {
	char *id;				/* id */ // potrebujem v interprete
	tType type;				/* informacia o type */
	TValue value;
	} tData;					/* tData - caka sa na adresach instrukcii */

/*Datová položka TRP s explicitně řetězenými synonymy*/
 typedef struct tHTItem{
	tKey key;				/* klíč  */
	tData data;				/* obsah */
	struct tHTItem* ptrnext;	/* ukazatel na další synonymum */
} tHTItem;

/* TRP s explicitně zřetězenými synonymy. */
typedef tHTItem* tHTable[MAX_HTSIZE];

extern int HTSIZE;

/* Hlavičky řešených procedur a funkcí. */

int hashCode ( tKey key );

void htInit ( tHTable* ptrht );

tHTItem* htSearch ( tHTable* ptrht, tKey key );

void htInsert ( tHTable* ptrht, tKey key, tData data);

tData* htRead ( tHTable* ptrht, tKey key );

void htDelete ( tHTable* ptrht, tKey key );

void htClearAll ( tHTable* ptrht );

#endif
