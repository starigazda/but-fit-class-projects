/**
 *	Project:	Interpret of the Lua programming language derivate IFJ11.
 * 	File:		error.h
 *	Date:		Thu Dec  8 11:01:01 CET 2011
 *	Last edit: 	Michal Srubar,	xsruba03@stud.fit.vutbr.cz	
 */

#ifndef ERROR_H
	#include "error.h"
#endif



// ERRORS
// @analyse = PARSER || PRECEDENCE_PARSER
void errUnknownToken(TParserStat *p_stat) {
	/*
	if (p_stat->token.data->doubleValue != 0)
		printf("LINE%d: Unknown token: %s.\n", p_stat->line, p_stat->p_token->data->doubleValue);
	else
		printf("LINE%d: Unknown token: %s.\n", p_stat->line, p_stat->p_token->data->stringValue);
	*/
			fprintf(stderr, "%s:%d: Unknown token: ....\n", p_stat->sourceFile, p_stat->line);

}

void errMsg(TParserStat *p_stat, char *msg, int ecode) {
	
	if (p_stat->inFuncBody == true)
		errFunc(p_stat);
	
	fprintf(stderr, "%s:%d: %s.\n", p_stat->sourceFile, p_stat->line, msg);
	p_stat->ecode = ecode;
}

void errUnExpected(TParserStat *p_stat, char *msg) {

	fprintf(stderr, "%s:%d: Unexpected %s.\n", p_stat->sourceFile, p_stat->line, msg);

	if (p_stat->ecode == EOK)
		p_stat->ecode = SYNTAX_ERROR;
}

void errExpected(TParserStat *p_stat, char *msg) {

	if (msg != NULL) {
		fprintf(stderr, "%s:%d: Expected %s.\n", p_stat->sourceFile, p_stat->line, msg);

		if (p_stat->main == false)
			fprintf(stderr, "%s:%d: There is no 'main' function.\n", p_stat->sourceFile, p_stat->line);

		if (p_stat->ecode == EOK)
			p_stat->ecode = SYNTAX_ERROR;
	}
}

	void errFunc(TParserStat *p_stat) {

		fprintf(stderr, "%s:%d: In function '%s':\n", p_stat->sourceFile, p_stat->line, p_stat->p_workFuncItem->funcName);
	}

	// Undeclared id
	// @func 1=function 0=variable
	// - jedna se o vypis pri semanticke chybe... tato procedura automaticky nastavi
	// ecode na semantiky error
	void errDeclaration(TParserStat *p_stat, int func) {
		
		if (func == 1) {
			fprintf(stderr,"%s:%d: Function '%s' wasn't define yet.", \
					p_stat->sourceFile, p_stat->line, strGetStr(&(p_stat->p_token->key)));
		}
		else if (func == 0) {
			errFunc(p_stat);

			fprintf(stderr,"%s:%d: Identifier of the variable '%s' wasn't declared yet.\n", \
					p_stat->sourceFile, p_stat->line, strGetStr(&(p_stat->p_token->key)));
		}
		else {
			fprintf(stderr,"%d %s:%d: '%s' is something not a function or variable.\n", \
					p_stat->who, p_stat->sourceFile, p_stat->line, strGetStr(&(p_stat->p_token->key)));	
		}

		if (p_stat->ecode == EOK)
			p_stat->ecode = SEM_ERROR;
	}

	// Error Identifier
	// @func 1=function 0=variable
	// - jedna se o vypis pri semanticke chybe... tato procedura automaticky nastavi
	// ecode na semantiky error
	void errId(TParserStat *p_stat, int func) {
		
		errFunc(p_stat);

		if (func == 0) {
			fprintf(stderr, "%s:%d: The identifier of the function '%s' is already declared at line %d.\n", 
					p_stat->sourceFile, p_stat->line, strGetStr(&(p_stat->p_token->key)), 
					getFuncLine(p_funcList, p_stat->p_token));
		}
		else if (func == 1) {
			fprintf(stderr, "%s:%d: The identifier of the variable '%s' is already declared at line %d.\n",
					p_stat->sourceFile, p_stat->line, strGetStr(&(p_stat->p_token->key)), 
					p_stat->line);
	}
	else if (func == 3) {
		fprintf(stderr, "%s:%d: The identifier '%s' is already declared as function identifier at line %d.\n",
				p_stat->sourceFile, p_stat->line, strGetStr(&(p_stat->p_token->key)), \
				getFuncLine(p_funcList, p_stat->p_token));
	}
	else {
		fprintf(stderr, "%s:%d: The identifier '%s' is already declared as parameter of the function '%s'.\n", 
				p_stat->sourceFile, p_stat->line, strGetStr(&(p_stat->p_token->key)), 
				p_stat->p_workFuncItem->funcName);
	}

	if (p_stat->ecode == EOK)
		p_stat->ecode = SEM_ERROR;
}

/**
 * JUST FOR DEBUG AND FUN PURPOSESSSS :)
 *
 */
void master(char *module, char *funcName, char *msg) {
	fprintf(stderr, "GOD: My Kung-fu is still better then yours! Go check your programme!\n" \
			"%s(%s): %s\n" \
			"\t\t\t\t\t Your Master :)\n", module, funcName, msg);
	exit(EXIT_FAILURE);
}
