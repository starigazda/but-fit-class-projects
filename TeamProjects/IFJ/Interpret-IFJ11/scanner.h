/**
 *	Project:	Interpret of the Lua programming language derivate IFJ11.
 *	File:		scanner.h
 *	Module:		modul for lexical analysis
 *	Date:		17.11.2011
 *	Authors:	Michal Starigazda, xstari01@stud.fit.vutbr.cz	
 *
 *
 *	Description:
 *	
 */
 
#ifndef SCANNER_H
	#define SCANNER_H

	#ifndef STDIO_H
  		#include <stdio.h>
  		#define STDIO_H
	#endif
	#ifndef STDLIB_H
  		#include <stdlib.h>
  		#define STDLIB_H
	#endif
	#ifndef CTYPE_H
 		 #include <ctype.h>
 		 #define CTYPE_H
	#endif
	#ifndef ERRNO_H
		  #include <errno.h>
 		 #define ERRNO_H
	#endif
	#ifndef LIMITS_H
  		#include <limits.h>
 		 #define LIMITS_H
	#endif
	#ifndef BOOL_H
		#include <stdbool.h>
		#define BOOL_H
	#endif 

	#ifndef IAL_H
		#include "ial.h"
	#endif
	#ifndef STR_H
		#include "str.h"
	#endif
	
#define INACTIVE 0
#define ACTIVE 1
#define BASE 10
#define FIRST_LINE	1
#define MAX_ORD_VALUE 255	// max. ordinalna hodnota znaku daneho ako \ddd

/**
 * Vycet stavov konecneho automatu (lexikalneho analyzatoru).
 */
typedef enum ScannerStates{
 	sInit,
	sID,
	sNum,
	sDot,
	sFloat,
  	sExp,
  	sExpNum,
  	sSign,
  	sStr,
  	sEsc,
  	sEscNum1,
  	sEscNum2,
  	sComSign,
  	sCommentSwitch,
  	sLineComment,
  	sBlock,
  	sBlockComment,
  	sBlockEnd,
  	sKonkat,
  	sGreater,
  	sLess,
  	sNotEq,
  	sEq
} tScanState;
  
/**
 * Hlavna funkcia lexikalneho analyzatoru. Pocas behu sa riadi datami zo struk-
 * tury TParserStat, ktoru zaroven aktualizuje. 
 * @param data Ukazatel na strukturu typu TParserStat.
 * @return True pre uspesne spracovanie tokenu.
 */
bool getNextToken(TParserStat *p_stat);

/** Nastavenie zdrojoveho suboru pre nacitanie znakov. */
void setSourceFile(FILE *source);

#endif
