/**
 *	Project:	Interpret of the Lua programming language derivate IFJ11.
 * 	File:		error.h
 *	Date:		Fri Dec  2 12:13:21 CET 2011
 *	Author:		???
 *	Last edit: 	Michal Srubar,	xsruba03@stud.fit.vutbr.cz	
 *
 */

#ifndef ERROR_H
	#define ERROR_H

	#ifndef PARSER_H
		#include "parser.h"
	#endif

/**
 * Main programme errors.
 */
#define EOK 				0	// programme ends without errors
#define LEX_ERROR 			1	// scanner error (e.g. wrong structure of current lexical unit ...)
#define SYNTAX_ERROR 		2	// parser error (e.g. wrong syntax ...)
#define SEM_ERROR 			3	// semantics error (e.g. undeclared variable, operation with uncompatible types ...)
#define EX_ERROR 			4	// interpret error (e.g. zero division ...)
#define INTER_ERROR 		5	// inter error of the interpret (e.g. not enough free memory, wrong source file ...)

/**
 * Inter error
 */
#define MALLOC_ERROR 		6	// chyba pri alokacii pamate
#define NUMBER_ERROR 		7	// chyba pri konverzi retazca na number

#define UNKNOWN_ERROR 		10	// neznama chyba
#define TABLE_ITEM_EXIST 	11	// indikator duplicitneho vkladania pri pokuse o vlozenie do tabulky symbolov
#define STACK_POP_ERROR 	12	// volanie pop nad prazdnym zasobnikom
#define TABLE_ERROR 		13	// chyba tabulky symbolov
#define PARAMS_ERROR		14	// wrong count of input parametrs

/**
 * god blessed this one :) lol
 * @module 		- module where master is called from
 * @funcName 	- name of the funcntion where master is called from
 * @msg 		- messagge from master to all his slaves :)
 */
void master(char *module, char *funcName, char *msg);

void errUnknownToken(TParserStat *p_stat);

void errUnExpected(TParserStat *p_stat, char *msg);

void errExpected(TParserStat *p_stat, char *msg);

void errFunc(TParserStat *p_stat);

// Undeclared id
// @func 1=function 0=variable
// - jedna se o vypis pri semanticke chybe... tato procedura automaticky nastavi
// ecode na semantiky error
void errDeclaration(TParserStat *p_stat, int func);
	
// Error Identifier
// @func 1=function 0=variable
// - jedna se o vypis pri semanticke chybe... tato procedura automaticky nastavi
// ecode na semantiky error
void errId(TParserStat *p_stat, int func);
	
void errMsg(TParserStat *p_stat, char *msg, int ecode);
/**
 * JUST FOR DEBUG AND FUN PURPOSESSSS :)
 *
 */
void master(char *module, char *funcName, char *msg);

#endif
