/**
 *	Project:	Interpret of the Lua programming language derivate IFJ11.
 *	File:		expr.c
 *	Module:		modul for precedence analysis
 *	Date:		13.11.2011
 *	Authors:	Michal Starigazda, xstari01@stud.fit.vutbr.cz	
 *
 *
 *	Description:
 *	
 */
 
#ifndef EXPR_H
	#define EXPR_H
	#ifndef PARSER_H	
		#include "parser.h"
	#endif
	#ifndef SCANNER_H	
		#include "scanner.h"
	#endif
	#ifndef BOOL_H
		#include <stdbool.h>
	#endif 
	#ifndef ILIST_H
		#include "ilist.h"
	#endif 
	
#define STACK_SIZE	128		// inicializacna velkost zasobniku
 
/**
 * Struktura prvku zasobniku precedencnej analyzy.
 */
typedef struct stackItem{
	tTokenType type;		// typ nacitaneho terminalu (legalneho tokenu vyrazu)
	int help_type;			// pomocny typ, ocakavny typ vysledku po redukcii
	int argCnt;				// pocitadlo argumentov funkcie k PUSH
	bool genInst;
	int *funcArgs;
	tData *data;			// ukazatel na data
} TStackPAItem;

/**
 * Struktura zasobniku.
 */
typedef struct Stack{
	TStackPAItem *stack;
	int top;
	int size;
} TStack;

//-----------------------------------------------------------------------------

/**
 * Precedencna analyza.
 * @param p_stat Status syntaktickej analyzy.
 * @return True pre uspesne spracovanie vyrazu. Chybovy kod pri neuspechu je
 * ulozeny v statuse.
 */
bool exprParser(TParserStat *p_stat);
#endif
