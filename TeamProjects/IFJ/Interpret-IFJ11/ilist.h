/**
 *	Project:	Interpret of the Lua programming language derivate IFJ11.
 *	File:		ilist.h
 *	Module:		Single and double linked linear lists.
 *	Date:		Fri Dec  2 12:13:21 CET 2011
 *	Authors:	Miroslav Slivka, xslivk02@stud.fit.vutbr.cz	
 *				Michal Srubar, xsruba03@stud.fit.vutbr.cz	
 *
 *	Description:
 *	This header file contains stuctures for single linked list TFunctionList and
 *	double linked list tListOfInstr.
 */
 
#ifndef ILIST_H
  #define ILIST_H

#ifndef STDIO_H
  #include <stdio.h>
  #define STDIO_H
#endif
#ifndef STDLIB_H
	#include <stdlib.h>
	#define STDLIB_H
#endif
#ifndef BOOL_H
	#include <stdbool.h>
	#define BOOL_H
#endif

#ifndef IAL_H
  #include "ial.h"
#endif
	
#define UNDECLARED 	-1

// typy jednotlivych instrukcii
typedef enum Instructions {
	I_STOP,  //koniec programu
	I_WRITE,  //vypis
    I_READ,  //nacitanie
    I_NOT,  //negacia
    I_AND,	//logicky and
    I_OR,	//logicky or
    I_IFGOTO,  //podmieneny skok
    I_GOTO,  //nepodmieneny skok
    I_LAB,  //navesti
    I_ASSIGN,  //priradenie =
    I_POWER,
    I_MULT,  //nasobenie *
    I_DIV,  //delenie /
    I_ADD,  //scitavanie +
    I_SUB,  //odcitavanie -
    I_ISGR,  //>
    I_ISLESS,  //<
    I_ISGE,  //>=
    I_ISLE,  //<=
    I_ISEQ,  //==
    I_PUSH,
    I_CALL,  //volanie funkcie
    I_RET,  //navrat z uzivatelskej funkcie
    I_KON,  //konkatenacia retazcov
    I_FIND,  //vstavana fcia find
    I_TYPE,  //vstavana fcia type
    I_SUBSTR,  //vstavana fcia substr
    I_SORT  //vstavana fcia sort
} tInstruction;

typedef struct
{
  int instType;  // typ instrukce
  void *addr1; // adresa 1
  void *addr2; // adresa 2
  void *addr3; // adresa 3
} tInstr;

typedef struct listItem
{
  tInstr Instruction;
  struct listItem *nextItem;
} tListItem;
    
typedef struct
{
  struct listItem *first;  // ukazatel na prvni prvek
  struct listItem *last;   // ukazatel na posledni prvek
  struct listItem *active; // ukazatel na aktivni prvek
} tListOfInstr;

void listInit(tListOfInstr *L);
void listFree(tListOfInstr *L);
void listInsertLast(tListOfInstr *L, tInstr I);
void listFirst(tListOfInstr *L);
void listNext(tListOfInstr *L);
void listGoto(tListOfInstr *L, void *gotoInstr);
void *listGetPointerLast(tListOfInstr *L);
void *listGetPointerActive(tListOfInstr *L);
tInstr *listGetData(tListOfInstr *L);
void printList(tListOfInstr *L);


/**
 * Structure for single linked list of functions.
 */
typedef struct functionItem {
	int line;						// line of function declaration
	int params;						// number of function parameters
	char *funcName;					// name of the function
	void *addr;						// pointer na instrukci LABEL

	tHTable *p_table;				// pointer to table of symbols for function
	struct functionItem *p_next;	// pointer to next funcTable
} TFunctionItem;

/**
 * List of functions.
 */
typedef struct functionList {
	struct functionItem *p_first;
	struct functionItem *p_last;
} TFunctionList;

#ifndef PARSER_H
	#include "parser.h"
#endif

/**
 * Initialize list of functions.
 */
void initFunctionList(TFunctionList *p_list);

/**
 * It returns pointer to funcItem if function is in list or NULL if it's not.
 */
TFunctionItem *searchFunc(TFunctionList *p_list, tToken *token);

/**
 * It returns line where function was declared or macro UNDECLARED if the
 * function wasn't declared.
 */
int getFuncLine(TFunctionList *p_list, tToken *token);

/* Check if identifier of the function p_name is in list of function.
 *
 * RETURN:	- UNDECLARED (parser.h | ilist.h) = function wasn't declared yet
 * 			- (>= 0) = means that function was declared and return value is 
 * 						number of the function parameters
 */
int checkFuncId(TFunctionList *p_list, tToken *token);

/**
 * Funkcia vracia ukazatel (*addr) na navesti funkcie v zozname intrukci.
 */
void* getFuncLabel(TFunctionList *p_list, tToken *token);
int* getFuncArgPtr(TFunctionList *p_list, tToken *token);
/**
 * Procedure which put new function at the end of the list of function.
 *
 * @line = 	must conatain line where indentifier was declared.
 * @name =	identifier of the function
 *
 * RETURN:	- MALLOC_ERROR (perrro handle that !)
 * 			- true = function added to list
 */ 
int insertFunc(TFunctionList *p_list, tToken *token, void *addrOfFunLabel, int line);
	
/**
 * Free the whole list of function.
 */
void freeFunctionList(TFunctionList *p_list);

#endif
