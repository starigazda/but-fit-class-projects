/**
 * @author "Michal Srubar"
 * email:	xsruba03@stud.fit.vutbr.cz
 * date:	Sun Apr 15 17:42:08 CEST 2012
 *
 * FIXME: 
 * - upravit modifikatory metod
 * - pocitam s tim ze miso ostrani mezery je zacatku a konce username a password
 */

package ija.server;

import java.io.*;

/** 
 * This class manages local account. This Accoutn can be written into a server
 * account database. Can be remove from it or modified.
 *
 * All passwords for users are stored in file "usersdb". Format of the file is
 * username::password\n. Every user has its own directory where are stored all
 * his petri nets.
 */
public class Account {

	String username = null;
	String password = null;
	String newPassword = null;

	String database = "userdb";
	boolean account; 	// specifies if users directory allready exists or not
	boolean db;			// specifies if database exists 

	//File userdir = null;
	File dbfile = null;

	public Account(String username, String password) throws IOException {

		this.username = username;
		this.password = password;

	//	userdir = new File(this.username);
		dbfile = new File("../examples/"+this.database);
		

		//account = userdir.isDirectory();
		db = dbfile.exists();
		
		if (db == false) {
			File f;
			f = new File("../examples/"+this.database);
			f.createNewFile();
		}

		// FIXME:
		//verifyDatabase();
		// lock semaphore and unlock it at every 
	}

	/**
	 * @return false is username doesnt in server database
	 * 			true if it is
	 */
	private boolean checkUserName() {
		boolean result = false;
		try {
			synchronized (this) {

				// Open the database file 
				FileInputStream fstream = new FileInputStream("../examples/"+database);

				// Get the object of DataInputStream
				DataInputStream in = new DataInputStream(fstream);
				BufferedReader br = new BufferedReader(new InputStreamReader(in));
				String strLine;

				// Read database File Line By Line
				while ((strLine = br.readLine()) != null) {
					strLine.replaceAll("::.*\\n", "");
					if (strLine.equals(this.username)) {
						result = true;	
					}
				}

				//Close the input stream
				in.close();
			}
		} catch (Exception e) {
			// userdb file doestn't exists => user account doestn't exist
		}

		return result;
	}

	public String getUserName() {
		return this.username;	
	}

	/**
	 * Add a new account into the databse of the server.
	 * @return false if user account already exists or true if doesn't
	 */
	public boolean addAccIntoDb() {

		if (this.username == null || this.password == null) {
			System.err.println("ERROR: You try to add account with no username"+
							" (probably deleted before this operation ?)");	
			return false;
		}

		// user account already exists
		if (checkUserName() == true)
			return false;

		BufferedWriter out = null;
		FileWriter shadow = null;

		try {
			synchronized (this) {
				shadow = new FileWriter("../examples/"+database, true);	
				//shadow = new FileWriter(database, true);	
				// create output stream which destination is file called userdb
				out = new BufferedWriter(shadow);
				// save password into shadow file
				out.write(this.username+"::"+this.password);
				out.newLine();
				System.out.println("> password for user "+this.username+" saved into databse file: "+database);
			}
		} catch (Exception e) {
			System.err.println("Error:"+e.getMessage());	
		} finally {
			try {
				out.close();	
				shadow.close();
			} catch (Exception e) {
				System.err.println("Error:"+e.getMessage());	
			} 
		}
		return true;
	}

	/**
	 * @return true if account exist in local database
	 * 			false if account does not exists in local database
	 */
	public boolean autentificate() {

		if (this.username == null || this.password == null) {
			System.err.println("ERROR: You try to add account with no username"+
							" (probably deleted before this operation ?)");	
			return false;
		}
	
		boolean result = false;

		try {
			synchronized (this) {
				// Open the file 
				System.out.println("Working Directory = " +System.getProperty("user.dir"));
				FileInputStream fstream = new FileInputStream("../examples/"+database);
				
				//FileInputStream fstream = new FileInputStream(database);
				

				// Get the object of DataInputStream
				DataInputStream in = new DataInputStream(fstream);
				BufferedReader br = new BufferedReader(new InputStreamReader(in));
				String strLine;

				//Read File Line By Line
				while ((strLine = br.readLine()) != null) {
					// Print the content on the console
					if (strLine.equals(this.username+"::"+this.password)) {
						result = true;
					}
				}

				//Close the input stream
				in.close();
			}
		} catch (Exception e) {
			System.err.println("nepodarilo se otevrit soubor s databazi uctu.");
			// userdb file doestn't exists => user account doestn't exist
		}

		return result;
	}



	/* 
	 * Test if databse doesnt contain inconsistent data e.g. users dir without
	 * database file, or user dir without member in userdb file etc.
	boolean verifyDatabase() {

		if ((account == true) && (db == false))	{
			System.err.println("Local database is inconsistent. Username "
			+this.username+" has local dir but there is no record in userdb file");
			return false;
		}
		// pokud se v ./ nachazi slozka pojmenovana username a v db neni toto
		// jmeno, pak doslo k nekonzistenci dat na serveru	
		return true;
	}
	*/

	/**
	 * Change password of the account.
	 * @true 	= password was successfuly changed
	 * @false 	= user is not in database (this should happend)
	public synchronized boolean changePassword(String newPass) {

		String strLine;
		boolean result = false;

		try {
			// open write stream for new file
			FileWriter fw = new FileWriter("new"+database);
			BufferedWriter out = new BufferedWriter(fw);

			// Open the file that I will read from
			FileInputStream fstream = new FileInputStream(database);

			// Get the object of DataInputStream
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));

			// Read File Line By Line
			while ((strLine = br.readLine()) != null) {

				// change old password with new one
				if (strLine.equals(this.username+"::"+this.password)) {
					System.out.println(">> Change "+this.username+" password "
						+"from "+this.password+" to "+newPass);
					out.write(this.username+"::"+newPass);
					result = true;
					this.password = newPass;
				}
				else {
					out.write(strLine);
				}

				out.newLine();
			}

			//Close the input stream
			in.close();
			//Close the output stream
			out.close();

			//Catch exception if any
		} catch (Exception e) {
			System.err.println("Error: " + e.getMessage());
		}

		File f1 = new File(database);
		File f2 = new File("new"+database);

		// delete old file
    	f1.delete();

		// rename new created file to old file
		f2.renameTo(f1);

		return result;
	}
	*/

//==============================================================================
	public String getUsername() {
		return this.username;	
	}

	/*
	public boolean checkDir() {

		if (userdir.exists()) {
			System.out.println(">> user dir for "+this.username+" exists");
			return true;
		}
		else  {
			System.out.println(">> user dir for "+this.username+" DOESNT exists");
			return false;
		}
	}
	*/
	

//==============================================================================
}
