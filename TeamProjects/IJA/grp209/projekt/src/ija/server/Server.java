/*
 * Server site of simple client/server echo application.
 */

package ija.server;

import java.io.IOException;
import java.net.ServerSocket;
import org.dom4j.DocumentException;


public class Server {

	public static void main(String[] args) throws IOException, DocumentException {

		Integer port = 4444;
		ServerSocket serverSocket = null;
		boolean listening = true;

		try {
			// create new socket called s
			 serverSocket = new ServerSocket(port);	
		} catch (IOException e) {
			System.err.println("Couldn't not listen on port: "+port);
			System.exit(-1);
		}


		// wait for new client then run new thread for him
		while (listening) {
			new ServerControl(serverSocket.accept()).start();		
			//ServerControl s = new ServerControl(serverSocket.accept());
			//s.go();
		}

        serverSocket.close();
	}
}
