package ija.server;

import java.util.Calendar;
import java.text.SimpleDateFormat;
import ija.petrinet.PetriNet;
import ija.petrinet.elements.*;
import java.util.*;

public class Simulation {
	
	PetriNet net = null;

	public static final String DATE_FORMAT_NOW = "yyyy-MM-dd HH:mm:ss";
	ArrayList<Transition> trans = new ArrayList<Transition>();

	public static String now() {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
		return sdf.format(cal.getTime());
	}

	public Simulation(PetriNet n, String username) {
		this.net = n;
		this.net.addSimulationRecond(now(), username);
	}

	// just for tests...
	void printRemovedTokens(Map<Object, String> map) {
		
	    // Get Map in Set interface to get key and value
		Set s = map.entrySet();

       	// Move next key and value of Map by iterator
       	Iterator it = s.iterator();

       	while(it.hasNext()) {

           	// key=value separator this by Map.Entry to get key and value
           	Map.Entry m = (Map.Entry) it.next();

           	// getKey is used to get key of Map
           	String key = (String) m.getKey();

           	// getValue is used to get value of key in Map
           	String value= (String) m.getValue();

        	System.out.println(key+" ==> "+value);
       	}
	}

	// for tests
	void printMap(Map<String, Integer> map) {
		
	    // Get Map in Set interface to get key and value
		Set s = map.entrySet();

       	// Move next key and value of Map by iterator
       	Iterator it = s.iterator();

       	while(it.hasNext()) {

           	// key=value separator this by Map.Entry to get key and value
           	Map.Entry m = (Map.Entry) it.next();

           	// getKey is used to get key of Map
           	String key = (String) m.getKey();

           	// getValue is used to get value of key in Map
           	int value= (Integer) m.getValue();

        	System.out.println(key+" ==> "+value);
       	}
	}

	boolean isInt(String i) {
		try {
			Integer.parseInt(i);
			return true;
		} catch (NumberFormatException nfe) {
			return false;
		}
	}
	
	boolean checkConstTokens(Map<Object,String> inPlaces, PetriNet net) {
		Set s = inPlaces.entrySet();
    	Iterator it = s.iterator();
		ArrayList<Integer> tokens = null;
		
		boolean result = true;
		boolean tokenExists = false;

       	while(it.hasNext()) {
	    	Map.Entry m = (Map.Entry) it.next();
	      	Integer key = (Integer) m.getKey();
	      	String value = (String) m.getValue();

			// pokud je na hrane konstanta tak musim checkskou jestli ma misto
			// token rovny konstante

			// arc value is constant
			if (isInt(value) == true) {
				System.out.println("p"+key.intValue()+" ma na hrane konstantu="+value);
				tokens = net.getPlaceTokens(key);	

				for (int t = 0; t < tokens.size(); t++) {

					// token equal to arc constant found
					System.out.println("hrana="+value+" == tokne("+tokens.get(t)+") ??");
					if (Integer.parseInt(value) == tokens.get(t)) {
						tokenExists = true;
						break;
					}
				}
				if (tokenExists == false) {
					result = false;
					break;
				}
				// go search in new place
				tokenExists = false;
			}
       	}
		return result;
	}

	
	Integer getMappedValue(Map<String, Integer> map, String var) {
		Set s = map.entrySet();
		Iterator it = s.iterator();

       	while(it.hasNext()) {
		  	Map.Entry m = (Map.Entry) it.next();
           	String key = (String) m.getKey();
      
			if (key.equals(var))
				return (Integer) m.getValue();
       	}
		
		return null;
	}

	// pop tokens out of input places 
	// inPlace:
	//		Key = Object Place
	//		Value = String arcValue
	void popTokens(Map<Object,String> inPlaces, Map<String, Integer> vars) {
		Set s = inPlaces.entrySet();
       	Iterator it = s.iterator();

       	while(it.hasNext()) {
           	Map.Entry m = (Map.Entry) it.next();
			Place place = this.net.getPlace((Integer) m.getKey());
			String value = (String) m.getValue();

			if (isInt(value) == true)
				place.popToken(Integer.parseInt(value));
			else {
				Integer v = getMappedValue(vars, value);
				place.popToken(v.intValue());
			}
		}
	}
	
	// push mapped tokens into output places
	// outPlace:
	//		Key = Object Place
	//		Value = String arcValue
	void pushTokens(Map<Object,String> outPlaces, Map<String, Integer> vars) {
		Set s = outPlaces.entrySet();
       	Iterator it = s.iterator();

       	while(it.hasNext()) {
           	Map.Entry m = (Map.Entry) it.next();
			Place place = this.net.getPlace((Integer) m.getKey());
			String value = (String) m.getValue();

			// arc value is constant
			if (isInt(value) == true)
				place.pushToken(Integer.parseInt(value));
			else {
				Integer v = getMappedValue(vars, value);
				place.pushToken(v.intValue());
			}
		}
	}
	
	// key=variable
	boolean checkVars(Map<String, Integer> vars, String var) {
			Set s = vars.entrySet();
			Iterator it = s.iterator();

       	while(it.hasNext()) {
           	Map.Entry m = (Map.Entry) it.next();
           	String key = (String) m.getKey();			// Place ID
           	//String value= (String) m.getValue();	// variable or constatnt
			
			if (key == null ? var == null : key.equals(var))
				return true;
       	}
		return false;
	}
	
	boolean check(Map<Object,String> inputPlaces, Map<String, Integer> vars, PetriNet n) {
		Set s = inputPlaces.entrySet();
		Iterator it = s.iterator();

	 	while(it.hasNext()) {
           	Map.Entry m = (Map.Entry) it.next();
           	int key = (Integer) m.getKey();			// Place ID
           	String value= (String) m.getValue();	// variable or constatnt
			
			if (isInt(value) == false) {	// variable
				//podivat se jestli už je tato prommnena namapovana
				if (checkVars(vars, value) == false) {// promenna tam zatim neni
					Place p = n.getPlace(key);
					Integer t = p.getToken();
					
					// dosly tokeny
					if (t == null)
						return false;
					
					vars.put(value, t);
				}
			}
		}
		return true;
	}
	
	// check if all input places has at least one token
	boolean check2(Map<Object,String> inputPlaces, PetriNet net) {
		Set s = inputPlaces.entrySet();
		Iterator it = s.iterator();

	 	while(it.hasNext()) {
           	Map.Entry m = (Map.Entry) it.next();
           	
			int key = (Integer) m.getKey();			// Place ID
			
			Place p = net.getPlace(key);
			
			ArrayList<Integer> tokens = p.getTokens();
			if (tokens.isEmpty()) {
				return false;
			}
		}
		
		return true;
	}
	
	// check if all mapped value are in input places
	boolean check3(Map<String, Integer> map, Map<Object,String> inputPlaces, PetriNet net) {
		Set s = map.entrySet();
		Iterator it = s.iterator();

       	while(it.hasNext()) {
			Map.Entry m = (Map.Entry) it.next();
			
			// variable
			String key = (String) m.getKey();
			// mapped value
			int value= (Integer) m.getValue();

			s = inputPlaces.entrySet();
			it = s.iterator();

			while(it.hasNext()) {
				m = (Map.Entry) it.next();
				
				Place p = net.getPlace((Integer) m.getKey());
				
				if (key == null ? (String) m.getValue() == null : key.equals((String) m.getValue())) {
					// podivat se jestli je tam ten token
				
					ArrayList<Integer> tokens = p.getTokens();
					if (tokens.contains(value) == false)
						return false;
				}
			}
       	}
		return true;
	}
	
	// returns state of the petriNet after simulation
	// @ steps == true  --> returns state of PetriNet after one simulate step
	// @ steps == false  --> no more steps
	public boolean simulationStep() {

		boolean simulationStep = false;
		trans = net.getTransitions();
		Condition cond;

		for (int j = 0; j < trans.size(); j++) {
			boolean condition = true;
			Transition t = trans.get(j);
			System.out.println("TRANS: t"+t.getId());	// transition label

			// map of input places for current transition
			Map<Object,String> inPlaces = t.getInMap();
			// map of output places for current transition
			Map<Object,String> outPlaces = t.getOutMap();
			
			// <variable in condition, value of the variable> 
			Map<String, Integer> vars = new HashMap<String, Integer>();

			if (checkConstTokens(inPlaces, net) == false)
				condition = false;

			if ((cond = t.getCond()) != null && condition != false)
				condition = cond.eval(vars, inPlaces, net);

			if (check(inPlaces, vars, net) == false)
				condition = false;
			
			if (check2(inPlaces, net) == false)
				condition = false;
			if (check3(vars, inPlaces, net) == false)
				condition = false;
			
			// condition miss or no suitable tokens in input places, 
			// let's try another transition blok in PetriNet
			if (condition == false) {
				System.out.println("");
				System.out.println("Pro t"+t.getId()+" neni mozne provest prechod");
				continue;
			}
		
			System.out.println("");
			System.out.println("Prechod je mozne provest pro t"+t.getId());
			
			printMap(vars);
			
			// remove tokens from input places 
			popTokens(inPlaces, vars);
			
			// get transition expression
			Expression exp = null;
			if ((exp = t.getExpression()) != null) {
				// map result of expression to its variable
				vars.put(exp.getResultChar(), exp.evalExp(vars));
			}
			printMap(vars);
		
			// push tokens into output palces
			pushTokens(outPlaces, vars);
			
			simulationStep = true;
			break; 
		}
		return simulationStep;
	}
	
}
