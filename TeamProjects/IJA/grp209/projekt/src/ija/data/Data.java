package ija.data;

import java.io.*;
import ija.petrinet.PetriNet;
import java.util.ArrayList;

public class Data implements Serializable {
private static final long serialVersionUID = 1L;

	int code;
	int simulation;

	String username;
	String password;

	String name;
	double version;
	
	private ArrayList<PetriNet> petriNetList = new ArrayList<PetriNet>();

	PetriNet petriNet;
	
	public Data(int code, String userName, String passWord, PetriNet net, String name, double version, int sim) {
		this.code = code;
		this.username = userName;
		this.password = passWord;
		this.petriNet = net;
		this.simulation = sim;
		this.name = name;
		this.version = version;
	}

	public Data(int code, String userName, String passWord, 
					ArrayList<PetriNet> netList, String name, double version, int sim) {
		this.code = code;
		this.username = userName;
		this.password = passWord;
		petriNetList = netList;
		this.simulation = sim;
		this.name = name;
		this.version = version;
	}

	public void addPetriNet(PetriNet net) {
		petriNetList.add(net);
	}
	public void print() {
		System.out.println("RESPONSE:");	
		System.out.println("code = "+this.code);	
		System.out.println("username = "+this.username);	
		System.out.println("password = "+this.password);	
	}

	public int getCode() {
		return this.code;	
	}

	public String getUserName() {
		return this.username;	
	}

	public String getPassword() {
		return this.password;
	}

	/**
	 * @return 	1 one simulation step
	 * 			2 simulate until petri net is dead
	 */
	public int getSimulation() {
		return this.simulation;	
	}

	public PetriNet getPetriNet() {
		return this.petriNet;	
	}
	
	public ArrayList<PetriNet> getPetriNetList(){
	  return this.petriNetList;
	}
}
