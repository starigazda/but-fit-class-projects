/*
 * Client site of simple client/server echo application.
 */

package ija;

import ija.data.Data;
import ija.petrinet.PetriNet;
import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client {

	public static void main(String[] args) throws IOException {
	
		// autenthification request
		Data data = new Data(512, "michal", "nopass", (PetriNet) (null), "", 0.0, 0);
		
		String host = "127.0.0.1";
		Integer port = 4444;

		Socket s = null;	
		ObjectOutputStream out = null;
		ObjectInputStream ois = null;

		try {
			// create new socket
			s = new Socket(host, port);	
			OutputStream os = s.getOutputStream();
			out = new ObjectOutputStream(os);

			InputStream is = s.getInputStream();
			ois = new ObjectInputStream(is);

		} catch (UnknownHostException e) {
			System.err.println("Dont know about host: "+host);
			System.exit(1);
		} catch (IOException e) {
			System.err.println("Couldn't get I/O for the conncetion to: "+host);
			System.exit(1);
		}

		// write data into the socket
		out.writeObject(data);
		System.out.println("Posilam pozadavek o autentizaci uzivatele: "
			+data.getUserName());

		try {
			while ((data = (Data)ois.readObject()) != null) {
				if (data.getCode() == 415)
					System.out.println("accoutn created");
				else if (data.getCode() == 415)
					System.out.println("acc exists");
				
				data = null;
			}
		} catch (Exception e) {
			System.err.println(e);
			System.out.println("konec cteni ze socketu");
		}

		// the order is important you should close all streams before you close
		// the socket itself
		out.close();
		s.close();
	}
}
