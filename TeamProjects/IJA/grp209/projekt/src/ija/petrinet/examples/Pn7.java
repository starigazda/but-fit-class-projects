/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ija.petrinet.examples;

import ija.petrinet.PetriNet;
import ija.petrinet.elements.Condi2;
import ija.petrinet.elements.Condition;
import ija.petrinet.elements.Place;
import ija.petrinet.elements.Transition;

/**
 *
 * @author hitxh
 */
public class Pn7 {
		
	public Pn7(PetriNet n2) {
		
		Place p1 = new Place(n2.getPlaceId());
		Place p2 = new Place(n2.getPlaceId());
		Place p3 = new Place(n2.getPlaceId());
		Place p4 = new Place(n2.getPlaceId());

		p1.pushToken(1);
		n2.addPlace(p1);

		p2.pushToken(1);
		p2.pushToken(2);
		n2.addPlace(p2);
		
		p3.pushToken(1);
		p3.pushToken(2);
		n2.addPlace(p3);
		
		n2.addPlace(p4);
		
		Transition t1 = new Transition(n2.getTransId());
		
		Condition cond = new Condition();
		// a > b
		cond.addCondi2(new Condi2(5, "a", "b"));
		t1.addCondition(cond);	// assingn condition to t1 trasition
		t1.addExpression("x", "a   +   b");
		
		n2.addTransition(t1);
		n2.addArc(p1, t1, "b");
		n2.addArc(p2, t1, "a");
		n2.addArc(p3, t1, "a");
		n2.addArc(t1, p4, "x");
	}
}
