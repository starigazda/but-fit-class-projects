/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ija.petrinet.examples;

import ija.petrinet.PetriNet;
import ija.petrinet.elements.Condi2;
import ija.petrinet.elements.Condition;
import ija.petrinet.elements.Place;
import ija.petrinet.elements.Transition;

/**
 *
 * @author hitxh
 */
public class Pn8 {

	public Pn8(PetriNet n2) {
		
		Place p1 = new Place(n2.getPlaceId());
		Place p2 = new Place(n2.getPlaceId());
		Place p3 = new Place(n2.getPlaceId());
		
		n2.addPlace(p1);

		p2.pushToken(1);
		p2.pushToken(1);
		p2.pushToken(1);
		p2.pushToken(1);
		n2.addPlace(p2);
		
		p3.pushToken(1);
		p3.pushToken(1);
		n2.addPlace(p3);
		
		Transition t1 = new Transition(n2.getTransId());
		Transition t2 = new Transition(n2.getTransId());
		
		n2.addTransition(t1);
		n2.addTransition(t2);
		
		n2.addArc(t1, p1, "1");
		n2.addArc(p1, t2, "1");
		
		n2.addArc(p3, t1, "1");
		n2.addArc(p2, t1, "1");
		
		n2.addArc(t2, p2, "1");
	}
}
