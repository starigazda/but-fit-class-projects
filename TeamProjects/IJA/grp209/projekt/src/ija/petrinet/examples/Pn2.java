/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ija.petrinet.examples;

import ija.petrinet.PetriNet;
import ija.petrinet.elements.Place;
import ija.petrinet.elements.Transition;

/**
 *
 * @author hitxh
 */
public class Pn2 {
	
	// SIIMULATION OK
	public Pn2(PetriNet n2) {
		
		Place p1 = new Place(n2.getPlaceId());
		Place p2 = new Place(n2.getPlaceId());
		Place p3 = new Place(n2.getPlaceId());

		p1.pushToken(1);
		p1.pushToken(2);
		n2.addPlace(p1);

		p2.pushToken(3);
		n2.addPlace(p2);
		n2.addPlace(p3);
		
		Transition t1 = new Transition(n2.getTransId());
		n2.addTransition(t1);
		n2.addArc(p1, t1, "2");
		n2.addArc(p2, t1, "2");
		n2.addArc(t1, p3, "a");
	}	
}
