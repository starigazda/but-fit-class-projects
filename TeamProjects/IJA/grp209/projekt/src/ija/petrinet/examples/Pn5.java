/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ija.petrinet.examples;

import ija.petrinet.PetriNet;
import ija.petrinet.elements.Condi2;
import ija.petrinet.elements.Condition;
import ija.petrinet.elements.Place;
import ija.petrinet.elements.Transition;

/**
 *
 * @author hitxh
 */
public class Pn5 {
	
	// SIMULATION OK
	public Pn5(PetriNet n2) {
		
		Place p1 = new Place(n2.getPlaceId());
		Place p2 = new Place(n2.getPlaceId());
		
		p1.pushToken(1);
		p1.pushToken(2);
		p1.pushToken(1);
		n2.addPlace(p1);
		n2.addPlace(p2);
		
		Transition t1 = new Transition(n2.getTransId());

		n2.addTransition(t1);

		n2.addArc(p1, t1, "1");
		n2.addArc(t1, p2, "9");
	}
}
