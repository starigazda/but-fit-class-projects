package ija.petrinet;

import ija.petrinet.elements.*;
import java.io.*;
import java.util.*;
import org.dom4j.*;
import org.dom4j.dom.DOMDocument;
import org.dom4j.dom.DOMElement;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;
import org.dom4j.Node;

/**
 *
 * @authors Michal Starigazda, xstari01@stud.fit.vutbr.cz
 *			Michal Srubar, xsruba03@stud.fit.vutbr.cz
 */
public class PetriNet implements Serializable {
	private static final long serialVersionUID = 1L;

	private ArrayList<Place> places = new ArrayList<Place>();
	private ArrayList<Transition> trans = new ArrayList<Transition>();
	// <user name, date> 
	private Map<String, String> log = new HashMap<String, String>();
	
	/** Name of the Petri Net */
	private String	name;
	/** Version of the Petri Net */
	private int version;
	/** Petri Net description */
	private String description;
	
	private static int placeSequence;
	private static int transitionSequence;

	private org.dom4j.Document doc;
    private org.dom4j.Element root;
   
	public PetriNet(String name, int version, String desc) {
		PetriNet.placeSequence = 0;
		PetriNet.transitionSequence = 0;

		this.name = name;
		this.version = version;
		this.description = desc;
	}
	
	public String getDescription() {
		return this.description;
	}
	
	public void setDescription(String d) {
		this.description = d;
	}
	
	public void upgrade() {
		this.version++;
	}
	
	public String getName() {
		return this.name;
	}
	
	public int getVersion() {
		return this.version;
	}
	/**
	 * Add new simulation record.
	 * @param username user witch run simulation
	 * @param date date of the simulation
	 */
	public void addSimulationRecond(String username, String date) {
		log.put(username, date);		
	}

	/**
	 * Get next available place identifier.
	 * @return int place identifier
	 */
	public int getPlaceId() {
		return ++placeSequence;	
	}

	/**
	 * Get next available transition identifier.
	 * @return transition identifier
	 */
	public int getTransId() {
		return ++transitionSequence;	
	}
	
	/**
	 * Get log.
	 */
	public Map<String, String> getLog(){
	  return this.log;
	}
	
	/**
	 * Set log.
	 * @param log New log map.
	 */
	public void setLog(Map<String, String> log){
	  this.log = log;
	}
	
	/**
	 * Add new place into the Petri Net.
	 * @param place place Object
	 */
	public void addPlace(Place place) {
		if (place != null)
			places.add(place);
		else
			System.err.println("You tried to added null place into petri net");	
	}

	/**
	 * Add new transition into the Petri Net.
	 * @param transition transition Object
	 */
	public void addTransition(Transition transition) {
		if (transition != null)
			trans.add(transition);
		else
			System.err.println("You tried to added null place into petri net");	
	}
	
	/**
	 * Add new arc from place to the transition.
	 * @param place place
	 * @param transition transition
	 * @param arcValue arc value
	 */
	public void addArc(Place place, Transition transition, String arcValue) {
		System.out.println("pridavam place s id="+place.getId());

		if (place != null && transition != null)
			transition.bindInput(place.getId(), arcValue);
	}

	/**
	 * Add new arc from transition to the place.
	 * @param transition transition
	 * @param place place
	 * @param arcValue arc value
	 */
	public void addArc(Transition transition, Place place, String arcValue) {
		if (place != null && transition != null)
			transition.bindOutput(place.getId(), arcValue);
	}

	/**
	 * Get place Object.
	 * @param placeId place identifier
	 * @return place Object
	 */	
	public Place getPlace(int placeId) {
		Place tmp;
		for (int i = 0; i< places.size(); i++) {
			tmp = places.get(i);
			if (tmp.getId() == placeId)
				return tmp;
		}
		return null;
	}
	
	/**
	 * Get list of the place token.
	 * @param placeID place identifier
	 * @return ArrayList of tokes
	 */
	public ArrayList<Integer> getPlaceTokens(int placeID) {
		/*Place tmp;
		tmp = places.get(placeID - 1);	
		return tmp.getTokens();*/
		for (Place p : places){
		  if (p.getId() == placeID){
		    return p.getTokens();
		  }
		}
		return null;
	} 

	/**
	 * Get list of the transitions.
	 * @return ArrayList of transitions
	 */
	public ArrayList<Transition> getTransitions() {
		return this.trans;	
	}
	
	/**
	 * Get list of the placess.
	 * @return ArrayList of places
	 */
	public ArrayList<Place> getPlaces() {
		return this.places;	
	}

	/**
	 * Get place sequence value.
	 * @return int value
	 */
	public int getPlaceSeq(){
	  int tmp = placeSequence;
	  return tmp;
	}
	
	/**
	 * Get transition sequence value.
	 * @return int value
	 */
	public int getTransSeq(){
	  int tmp = transitionSequence;
	  return tmp;
	}
	
	/**
	 * Set place sequence value.
	 * @param val New int value.
	 */
	public void setPlaceSeq(int val){
	  placeSequence = val;
	}
	
	/**
	 * Set transition sequence value.
	 * @param val New int value.
	 */
	public void setTransSeq(int val){
	  transitionSequence = val;
	}
 
     
    public void iterateRootChildren() {
		root = this.doc.getRootElement();
		Iterator elementIterator = root.elementIterator();
	while(elementIterator.hasNext()){
	    Element element = (Element)elementIterator.next();
	    System.out.println(element.getName());
	    Iterator innerIt = element.elementIterator();
	    while(innerIt.hasNext()){
		Element innerElement = (Element) innerIt.next();
		System.out.println(element.getName() + "/" + innerElement.getName() + ":" + innerElement.getText());
	    }
	}
    }   
    
    public void parseWithSAX(File aFile) throws DocumentException {
		SAXReader xmlReader = new SAXReader();
        this.doc = xmlReader.read(aFile);
    }


    public void browseRootChildren() {
	XPath xpathSelector = DocumentHelper.createXPath("//author[@location='UK']/full_name");
	List results = xpathSelector.selectNodes(doc);
	for ( Iterator iter = results.iterator(); iter.hasNext(); ) {
	    Element element = (Element) iter.next();
	    System.out.println(element.getText());
	}
										     
    }

    void newJDomDocument(String file) throws IOException {
		doc = DocumentHelper.createDocument();
	
        Element root = doc.addElement( "PetriNet" );
    
        Element net = root.addElement( "net" )
			.addAttribute( "name", this.name)
            .addAttribute( "version", Integer.toString(this.version))
			.addAttribute( "description", this.description);
       
		Place p;
		for (int i = 0; i < this.places.size(); i++) {
			p = places.get(i);
			Element places = root.addElement("place")
				.addAttribute("id", p.getId()+"")
				.addAttribute("x", p.getX()+"")
				.addAttribute("y", p.getY()+"")
				.addAttribute("num", p.getTokens().size()+"");
			
			ArrayList<Integer> tokens = p.getTokens();
			for (int j = 0; j < tokens.size(); j++) {
				places.addElement("token").addAttribute("value", tokens.get(j).toString());
			}
		}
		
		Transition t;
		for (int i = 0; i < this.trans.size(); i++) {
			t = trans.get(i);
			
			Condition cond = t.getCond();
			ArrayList<Condi2> cond2 = cond.getCondition();
		
			Element trans = root.addElement("transition")
				.addAttribute("id", t.getId()+"");
			
			Integer size = t.getConditionSize();
			Element condition = trans.addElement("condition")
				.addAttribute("num", size.toString());
			
			// get all condi2 for Condition 
			for (int j = 0; j < cond2.size(); j++) {
				Condi2 c = cond2.get(j);
				Element condi2 = condition.addElement("condi2");
				condi2.addAttribute("op1", c.getOp1());
				condi2.addAttribute("op2", c.getOp1());
				Integer operator = c.getOperator();
				condi2.addAttribute("rel", operator.toString());
			}
			
			Expression exp = null;
			if ((exp = t.getExpression()) == null) {
				trans.addAttribute("resultChar", "");
				trans.addAttribute("exp","");
			}
			else {
				trans.addAttribute("resultChar", exp.getResultChar());
				trans.addAttribute("exp", exp.getExpression());
			}
				
			trans.addAttribute("x", t.getX()+"");
			trans.addAttribute("y", t.getY()+"");
		
			size = t.getInMapSize();	
			Element input = trans.addElement("inputPlaces")
				.addAttribute("num", size.toString());
			
			// print input places
			Map<Object, String> inputMap = t.getInMap();
			
			Set s = inputMap.entrySet();
			Iterator it = s.iterator();

			while(it.hasNext()) {
			  	Map.Entry m = (Map.Entry) it.next();

				Integer key = (Integer) m.getKey();
				Element pl = input.addElement("place").addAttribute("id", key.toString());
		      	
				String value= (String) m.getValue();
				pl.addAttribute("arcValue", value);
			}
			
			size = t.getOutMapSize();	
			Element output = trans.addElement("outputPlaces")
				.addAttribute("num", size.toString());
			
			// print input places
			Map<Object, String> outputMap = t.getOutMap();
			
			s = outputMap.entrySet();
			it = s.iterator();

			while(it.hasNext()) {
			  	Map.Entry m = (Map.Entry) it.next();

				Integer key = (Integer) m.getKey();
				Element pl = output.addElement("place").addAttribute("id", key.toString());
		      	
				String value= (String) m.getValue();
				pl.addAttribute("arcValue", value);
			}
		}
  
		Set s = this.log.entrySet();
       	Iterator it = s.iterator();

       	while(it.hasNext()) {
           	Map.Entry m = (Map.Entry) it.next();
			
			Element log = root.addElement("Log");
			
			// user
			String value= (String) m.getValue();
			log.addAttribute("user", value);
			
			//date 
           	String key = (String) m.getKey();
           	log.addAttribute("date", key);
			
			
		}

		OutputFormat outformat = OutputFormat.createPrettyPrint();
		//Writer out = new OutputStreamWriter(new FileOutputStream("../examples"+file));
		Writer out = new OutputStreamWriter(new FileOutputStream(file));
		XMLWriter writer = new XMLWriter(out, outformat);
		writer.write(doc);
		writer.flush();	
    }

   /**
    * This method is used to load the xml file to a document and return it
    *
    * @param xmlFileName is the xml file name to be loaded
    * @return Document
    */
   public static Document getDocument( final String xmlFileName )
   {
      Document document = null;
      SAXReader reader = new SAXReader();
      try
      {
         document = reader.read( xmlFileName );
      }
      catch (DocumentException e)
      {
      }
      return document;
   }
 
   boolean isInt(String i) {
		try {
			Integer.parseInt(i);
			return true;
		} catch (NumberFormatException nfe) {
			return false;
		}
	}
	
   /**
    * @param args
    */
   public static PetriNet load(String file)
   {
	  PetriNet net = null;
      //String xmlFileName = "../examples/test-hitxh-2.xml";
	  String xmlFileName = file;

	  System.out.println("Working Directory = " +
             System.getProperty("user.dir"));

	  //String xmlFileName = "test-hitxh-2.xml";
      String xPath = "//PetriNet/net";
      Document document = getDocument( xmlFileName );
      List<Node> nodes = document.selectNodes( xPath );
	  for (Node n : nodes) {
		  
		   String v = n.valueOf("@version");
		  // Integer.parseInt(i)
		   net = new PetriNet(n.valueOf("@name"), Integer.parseInt(v), n.valueOf("@description"));
      }
	  
	  // places
		xPath = "//PetriNet/place";
		nodes = document.selectNodes( xPath );
		int iter = 0;
		int step = 0;
		for (Node place : nodes) {
			String id = place.valueOf("@id");
			Place p = new Place(Integer.parseInt(id));
			String x = place.valueOf("@x");
			String y = place.valueOf("@y");
			String num = place.valueOf("@num");
			iter = Integer.parseInt(num);
			
			int tmp = iter;
			p.setCoords(Integer.parseInt(x), Integer.parseInt(y));

		
			// tokens of place
			xPath = "//PetriNet/place/token";
			nodes = document.selectNodes( xPath );
			if (tmp != 0) {
				int i = 0;
				for (Node t : nodes) {
					
					if (i < step) {
						i++;
						continue;
					}
					
					String token = t.valueOf("@value");
					p.pushToken(Integer.parseInt(token));
					tmp--;
					if (tmp == 0)
						break;
				}
			}
			step += iter;
			net.addPlace(p);
		}
		
		// transitions
		xPath = "//PetriNet/transition";
		nodes = document.selectNodes( xPath );
		Transition t;
		for (Node tran : nodes) {
			String id = tran.valueOf("@id");
			t = new Transition(Integer.parseInt(id));
			
			t.addExpression(tran.valueOf("@resultChar"), tran.valueOf("@exp"));
			
			String x = tran.valueOf("@x");
			String y = tran.valueOf("@y");
			
			t.setCoords(Integer.parseInt(x), Integer.parseInt(y));
		 	
			// condition
			xPath = "//PetriNet/transition/condition";
			nodes = document.selectNodes( xPath );
			Condition cond = null;
			for (Node condition : nodes) {
				
				cond = new Condition();
				
				// condi2
				xPath = "//PetriNet/transition/condition/condi2";
				nodes = document.selectNodes( xPath );
				
				for (Node condi2 : nodes) {
					String op1 = condi2.valueOf("@op1");
					String op2 = condi2.valueOf("@op2");
					String o = condi2.valueOf("@rel");
			
					
					cond.addCondi2(new Condi2(Integer.parseInt(o), op1, op2));
					
				}
				
				t.addCondition(cond);	// assingn condition to t1 trasition
				cond = null;
			}
			
			net.addTransition(t);
					
			// input places
			xPath = "//PetriNet/transition/inputPlaces/place";
			nodes = document.selectNodes( xPath );
			Place p = null;
			for (Node place : nodes) {
				String idInputPlace = place.valueOf("@id");
				String arcValue = place.valueOf("@arcValue");
				
				p = net.getPlace(Integer.parseInt(idInputPlace)); 
			
				net.addArc(p, t, arcValue);
			}
			
			// output places
			xPath = "//PetriNet/transition/outputPlaces/place";
			nodes = document.selectNodes( xPath );
			p = null;
			for (Node output : nodes) {
				String idOutput = output.valueOf("@id");
				String arcValue = output.valueOf("@arcValue");
				
				p = net.getPlace(Integer.parseInt(idOutput)); 
				net.addArc(t, p, arcValue);
			}
			
			t = null;
		
		}
		
		xPath = "//Log";
		document = getDocument( xmlFileName );
		nodes = document.selectNodes( xPath );
		
		for (Node n : nodes) {
			String user = n.valueOf("@user");
			String date = n.valueOf("@date");
			net.addSimulationRecond(date, user);
		}
		
	  return net;
   }

   /*
	public  synchronized void load(String user) {
		// File: petriNetName_creator_version
		//this.name + user + this.version;
		// load Petri net from xml file into Object
		
		String xmlFileName = "sample.xml";
		String xPath = "//Root/Address";
		Document document = getDocument( xmlFileName );
		List<Node> nodes = document.selectNodes( xPath );
		
		for (Node node : nodes) {
			String studentId = node.valueOf( "@studentId" );
			System.out.println( "Student Id : " + studentId );
		}
	}
	*/

   /**
	 * Save petri net into file.
	 * @return true = petri new save
	 *			false = petri net saved as new version
	 */
	public void savePetriNet(String fileName) {
		try {
			//newJDomDocument(this.name+"-"+creator+"-"+this.version+".xml");
			//parseWithSAX(new File(this.name+"-"+creator+"-"+this.version+".xml"));
			newJDomDocument(fileName);
			parseWithSAX(new File(fileName));
			iterateRootChildren();
		} 
		catch (DocumentException ex) {
		}
		catch (IOException ex) {
		}
	}
   
	/**
	 * Save petri net into file.
	 * @return true = petri new save
	 *			false = petri net saved as new version
	 */
	public synchronized boolean save(String creator) {
		// File: petriNetName_creator_version
		//this.name + user + this.version;
	
		//File f = new File("../examples/"+this.name+"-"+creator+"-"+this.version+".xml");
		boolean result = true;
		File f;
		
		if (new File("../examples/"+this.name+"-"+this.version+".xml").exists() == false)
			this.version = 0;
		
		while (new File("../examples/"+this.name+"-"+this.version+".xml").exists()) {
		//while (new File(/this.name+"-"+creator+"-"+this.version+".xml").exists()) {
			upgrade();
			System.out.println("UPGRADE to : petri net saved, file="+"../examples/"+this.name+"-"+this.version+".xml");
			result = false;
		}
		
		try {
			//newJDomDocument(this.name+"-"+creator+"-"+this.version+".xml");
			//parseWithSAX(new File(this.name+"-"+creator+"-"+this.version+".xml"));
			newJDomDocument("../examples/"+this.name+"-"+this.version+".xml");
			parseWithSAX(new File("../examples/"+this.name+"-"+this.version+".xml"));
			System.out.println("petri net saved, file="+"../examples/"+this.name+"-"+this.version+".xml");
			iterateRootChildren();
		} 
		catch (DocumentException ex) {
		}
		catch (IOException ex) {
		}
		
		return result;
	}
	
/******************************************************************************
 *	TEST METHODS:
 *****************************************************************************/
	
	 
	void printTokens(ArrayList<Integer> tokens) {
		System.out.print("\tTOKENS:");
		for (int i = 0; i < tokens.size(); i++) {
			System.out.print(tokens.get(i)+", ");
		}
		System.out.println("");
	}
	
	// pX; outArcs: t1:val, t2:val, ..., tX:val;
	void printPlaces(Map<Object,String> out, int tID, int o) {
		
		ArrayList<Integer> tokens = null;

	    // Get Map in Set interface to get key and value
		Set s = out.entrySet();

       	// Move next key and value of Map by iterator
       	Iterator it = s.iterator();

       	while(it.hasNext()) {

           	// key=value separator this by Map.Entry to get key and value
           	Map.Entry m = (Map.Entry) it.next();

           	// getKey is used to get key of Map
           	int key = (Integer) m.getKey();

           	// getValue is used to get value of key in Map
           	String value= (String) m.getValue();
			
			if (o == 1) {
           		System.out.println("\tt"+tID+" ==> p"+key+
							" (arcValue = "+value+")");
			}
			else
           		System.out.println("\tp"+key+" ==> t"+tID+
							" (arcValue = "+value+")");
			tokens = getPlaceTokens(key);
			printTokens(tokens);
       	}
	}

	public void printPetriNet() {
		Place p = null;
		Transition t = null;
  		ArrayList<Integer> tk = null;

		Map<Object,String> in = null;
		Map<Object,String> out = null;
		
		for (int j = 0; j < trans.size(); j++) {

			t = trans.get(j);
			System.out.println("TRANS:\tt"+t.getId());	// transition label
			System.out.println("COND:\t");
			t.printCondition();
			System.out.println("OPER:\t"+t.printExpression());

			in = t.getInMap();
			out = t.getOutMap();

			System.out.println("IN:");
			printPlaces(in, t.getId(), 0);

			System.out.println("OUT:");
			printPlaces(out, t.getId(), 1);

			p = null;
			tk = null;
			System.out.println();
		}
		
		for (Map.Entry<String, String> entry : log.entrySet())
		{
		  System.out.println("Log item: " +entry.getValue() + " : " +  entry.getKey());
		}
	}

 /*****************************************************************************/
}
