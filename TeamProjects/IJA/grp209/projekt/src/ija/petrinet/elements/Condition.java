package ija.petrinet.elements;

import ija.petrinet.elements.Condi2;
import ija.petrinet.PetriNet;
import java.io.Serializable;
import java.util.*;

/**
 *
 * @author Michal Starigazda, xstari01@stud.fit.vutbr.cz
 */
public class Condition implements Serializable{
	private static final long serialVersionUID = 1L;

	int index;
	private ArrayList<Condi2> condition = new ArrayList<Condi2>();

	public Condition() {
		index = -1;	
	}
	
	public int getNumber() {
		return condition.size();
	}
	/**
	 * Get condition array list.
	 * @return This condition array list.
	 */
	public ArrayList<Condi2> getCondition(){
	  return this.condition;
	}

	/**
	 * Add expression into transition.
	 * @param cond Condi2 witch is one part of the transition condition
	 */
	public void addCondi2(Condi2 cond) {
		if (cond != null)
			condition.add(cond);	
		else
			System.err.println("You tried to add NULL expression into"
						   		+" condition array");
	}

	/**
	 * Evaluate trasition condition
	 * @param vars Map of the tokens mapped to its variable
	 * @param inPlaces Map of the transition input places
	 * @param net Petri Net
	 * @return true = conditions is true
	 *			false = condition is false
	 */
	public boolean eval(Map<String, Integer> vars, 
					Map<Object,String> inPlaces, PetriNet net) {

		boolean result = true;

		for (int k = 0; k < condition.size(); k++) {
			Condi2 c = condition.get(k);

			/**
			 * @vars: map of variables and its values (tokens)
			 */
			if (c.evalCondi2(vars, inPlaces, net) == false) {
				result = false;
				break;
			}
		}
		return result;
	}

	// test method
	public void printCondition() {
		
		Condi2 tmp = null;

		if (condition == null)
			return;

		for (int i = 0; i < condition.size(); i++) {
			tmp = condition.get(i);

			if ((i + 1) == condition.size()) {
				System.out.print(tmp.getOp1()+" ");
				System.out.print(tmp.getOperatorChar()+" ");
				System.out.println(tmp.getOp2()+" ");
			}
			else {
				System.out.print(tmp.getOp1()+" ");
				System.out.print(tmp.getOperatorChar()+" ");
				System.out.print(tmp.getOp2()+" && ");
			}
		}
	}
}
