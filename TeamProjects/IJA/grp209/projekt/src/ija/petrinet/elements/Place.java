package ija.petrinet.elements;

import java.io.Serializable;
import java.util.*;
//import java.io.*; // needed to implements Serializable

/**
 *
 * @author Michal Starigazda, xstari01@stud.fit.vutbr.cz
 */
public class Place implements Serializable{
	private static final long serialVersionUID = 1L;

	// place identifier
	private int id;
	
	// array of values in place (tokens)
	private ArrayList<Integer> tokens = new ArrayList<Integer>();
	
	// coordinates of element at the canvas
	private int x;
	private int y;

	public Place(int id) {
		this.id = id;
	}

	public Integer getToken() {
		if (!tokens.isEmpty())
			return tokens.get(0);
		else
			return null;
	}
	/**
	 * Pust new token into the place.
	 * @param token pusked token
	 */
	public void pushToken(int token) {
		tokens.add(token); 
	}
	
	/**
	 * Pop token from the place.
	 * @param token poped token
	 */
	public void popToken(int token) {
		if (tokens.contains(token))
			this.tokens.remove(tokens.indexOf(token));
	}

	/**
	 * Get identifier of the place.
	 * @return int identifires
	 */
	public int getId() {
		return this.id;
	}
	
	/**
	 * Get token list of the place.
	 * @return tokens ArrayList 
	 */
	public ArrayList<Integer> getTokens() {
		if (this.tokens != null)
			return this.tokens;
		else
			return null;
	}
	
	/**
	 * Set coordinates of place.
	 * @param x 
	 * @param y
	 */
	public void setCoords(int x, int y){
	  this.x = x;
	  this.y = y;
	}
	
	/**
	 * Get x coordinate.
	 * @return X
	 */
	public int getX(){
	  return this.x;
	}
	
	/**
	 * Get y coordinate.
	 * @return Y
	 */
	public int getY(){
	  return this.y;
	}
}
