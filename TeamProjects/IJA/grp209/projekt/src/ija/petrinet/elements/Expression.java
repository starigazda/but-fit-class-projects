package ija.petrinet.elements;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.nfunk.jep.JEP;

/**
 *
 * @author Michal Srubar, xsruba03@stud.fit.vutbr.cz
 *
 * a = x + y / c
 *
 * var = "a";
 * exp = "x+y/c"
 */
public class Expression implements Serializable{
	private static final long serialVersionUID = 1L;

	// assigned var
	String exp;
	String resultChar;
	
	public Expression(String resultChar, String exp) {
		// remove all whitespaces
		this.exp = exp.replaceAll("\\s+", "");		
		this.resultChar = resultChar;
	}
	
	/**
	 * Get expression part.
	 * @return expression part
	 */
	public String getExpression() {
		return this.exp;
	}

	/**
	 * Get Char where result of the exression will be stored.
	 * @return result char
	 */
	public String getResultChar() {
		return this.resultChar;
	}
	
	void mapVariables(Map<String, Integer> map, JEP jep) {
		Set s = map.entrySet();
	   	Iterator it = s.iterator();

       	while(it.hasNext()) {
	       	Map.Entry m = (Map.Entry) it.next();
	        String key = (String) m.getKey();
	       	Integer value= (Integer) m.getValue();
			// map variables and its values for expression parser
			jep.addVariable(key, value.intValue());
		}
	}

	/**
	 * Evaluate expression.
	 * @param vars values mapped to its variables
	 * @return result of the expression
	 */
	public int evalExp(Map<String, Integer> vars) {
	
		JEP jep = new JEP();
		mapVariables(vars, jep);
		
		jep.parseExpression(this.exp);
		Double result = jep.getValue();

		return result.intValue();
	}
}	
