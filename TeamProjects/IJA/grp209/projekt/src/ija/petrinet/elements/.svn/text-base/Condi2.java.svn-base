package ija.petrinet.elements;

import java.util.*;
import ija.petrinet.PetriNet;
import java.io.Serializable;

/**
 * @author Michal Srubar, xsruba03@stud.fit.vutbr.cz
 * If there is a condition x < 2 && x <= y in a transition then an instance of 
 * this class will be represents first expression (x < 2) and another instance 
 * second expression (x <= y).
 */
public class Condi2 implements Serializable{
	private static final long serialVersionUID = 1L;

	/*
	 * 0 --> no operator
	 * 1 --> <
	 * 2 --> <=
	 * 3 --> >=
	 * 4 --> >
	 * 5 --> ==
	 * 6 --> !=
	 */
	int operator;

	// have to be variable
	String operand1;

	// can be variable or constant
	String operand2;

	// token list for place witch arc value is operand1
	private ArrayList<Integer> tokenListOp1 = null;
	// token list for place witch arc value is operand2
	private ArrayList<Integer> tokenListOp2 = null;

   public Condi2(int rel, String op1, String op2) {
		if (rel > 0 && rel < 7)
			this.operator = rel;	
		else
			System.err.println("(Condi2.java): You tried to add condition with wrong operator.");

		this.operand1 = op1;
		this.operand2 = op2;
	}
   
	/**
	 * Get Condi2 operator.
	 * @return int value of the operator
	 */
	public int getOperator() {
		return operator;	
	}

	/**
	 * Get first operand.
	 * @return first operand of the Condi2
	 */
	public String getOp1() {
		return this.operand1;
	}

	/**
	 * Get second operand.
	 * @return second operand of the Condi2
	 */
	public String getOp2() {
		return this.operand2;
	}

	public String getCondi2() {
		//return this.operand1;
		return this.operand1+getOperatorChar()+this.operand2;
	}
	/** 
	 * Eval Condi2.
	 * @param op1 first operand
	 * @param op2 second 
	 * @return true if Condi2 is true or false if it is not
	 */
	public boolean evalOperands(int op1, int op2) {
		if (this.operator == 1) {
			if (op1 < op2)
				return true;	
		}
		else if (this.operator == 2) {
			if (op1 <= op2)
				return true;	
		}
		else if (this.operator == 3) {
			if (op1 >= op2)
				return true;	
		}
		else if (this.operator == 4) {
			if (op1 > op2)
				return true;	
		}
		else if (this.operator == 5) {
			if (op1 == op2)
				return true;	
		}
		else if (this.operator == 6) {
			if (op1 != op2)
				return true;	
		}

		return false;
	}

	boolean isInt(String i) {
		try {
			Integer.parseInt(i);
			return true;
		} catch (NumberFormatException nfe) {
			return false;
		}
	}
	
	// returns ID of place which arc value is eval to value in condition
	// or 0 if there is no place with this arc (this case shlould handle client
	// site)
	int findPlace(Map<Object,String> places, String arcValue) {
		
	    // Get Map in Set interface to get key and value
		Set s = places.entrySet();

       	// Move next key and value of Map by iterator
       	Iterator it = s.iterator();

       	while(it.hasNext()) {

           	// key=value separator this by Map.Entry to get key and value
           	Map.Entry m = (Map.Entry) it.next();

           	// getKey is used to get key of Map
           	int key = (Integer) m.getKey();

           	// getValue is used to get value of key in Map
           	String value= (String) m.getValue();

			if (arcValue.equals(value)) {
				return (int) key;
			}
        	//System.out.println("Hledam "+arcValue+" v miste:");
        	//System.out.println("p"+key+" (arcValue = "+value+")");
       	}

		return 0;
	}


	//Map<Object,String> in = new HashMap<Object, String>();
	//in = t.getInMap();
		
	void printTokens(ArrayList<Integer> list, int placeID) {
		for (int t = 0; t < list.size(); t++) {
			System.out.println("token(p"+placeID+")="+list.get(t));
		}
	}

	boolean checkSameArc(Map<Object,String> inPlaces, String arcValue, 
						int token, PetriNet net) {

		System.out.println("(checkSameArc)> for arcVAlue="+arcValue+", token="+token);
		int placeID;
		ArrayList<Integer> tokenList = null;
		boolean result = false;

		while ((placeID = findPlace(inPlaces, arcValue)) != 0) {
		
			System.out.println("(checkSameArc)> placeID="+placeID);
			tokenList = net.getPlaceTokens(placeID);

			for (int t = 0; t < tokenList.size(); t++) {
				System.out.println("(checkSameArc)> "+tokenList.get(t)+"=="+token);
				if (tokenList.get(t) == token) {
					result = true;
					break;
				}
			}

			if (result == true)
				break;
		}

		return result;
	}

	/**
	 * @places = map of all transition's input places
	 */
	public boolean evalCondi2(Map<String, Integer> vars, 
								Map<Object,String> inPlaces, PetriNet net) {

		boolean result = false;
		int placeID1 = 0;
		int placeID2 = 0;
		int token1 = 0;
		int token2 = 0;
		ArrayList<Integer> tokenList1 = null;
		ArrayList<Integer> tokenList2 = null;

		// find first input place with arc value equal to variable in condi2
		// (condi2: x > 2; find palce with arc x)
		placeID1 = findPlace(inPlaces, this.operand1);

		// get token list of placeID1 (place with arc value equal to x)
		tokenList1 = net.getPlaceTokens(placeID1);
		if (tokenList1.isEmpty()) {
			System.out.println("(SIM)> Prechod nemuze byt proveden, p"
							+placeID1+" nebo p"+placeID2+" nema zadne tokeny");
			return result;
		}
		
		System.out.println("(SIM)> Place ID for op1("+this.operand1+
						") is "+placeID1);

		// second operand can be constant
		if (isInt(this.operand2) == false) {
			placeID2 = findPlace(inPlaces, this.operand2);
			tokenList2 = net.getPlaceTokens(placeID2);
			if (tokenList2.size() == 0) {
				System.out.println("(SIM)> Prechod nemuze byt proveden, p"
							+placeID1+" nebo p"+placeID2+" nema zadne tokeny");
				return result;
			}
			System.out.println("(SIM)> Place ID for op1("+this.operand2+
							") is "+placeID2);
		}
		else {
			System.out.println("(SIM)> Second operand is constant: "
							+this.operand2);
			placeID2 = Integer.parseInt(this.operand2);
			token2 = placeID2;
		}
		
		if (tokenList1 != null)
			printTokens(tokenList1, placeID1);
		if (tokenList2 != null)
			printTokens(tokenList2, placeID2);

		// find suitable tokens
		for (token1 = 0; token1 < tokenList1.size(); token1++) {

			if (tokenList2 != null) {
				for (token2 = 0; token2 < tokenList2.size(); token2++) {

					if (evalOperands(tokenList1.get(token1), 
						tokenList2.get(token2)) == true) {

						if ((checkSameArc(inPlaces, this.operand1, 
							tokenList1.get(token1), net) == true) &&
							(checkSameArc(inPlaces, this.operand2, 
							tokenList2.get(token2), net) == true)) {

							// map tokens to its variables in Condi2
							vars.put(this.operand1, tokenList1.get(token1));
							vars.put(this.operand2, tokenList2.get(token2));
							result = true;
						}
						break;
					}
				}

				if (result == true)
					break;
			}
			else {
				if (evalOperands(tokenList1.get(token1), token2) == true) {
					if (checkSameArc(inPlaces, this.operand1, 
						tokenList1.get(token1), net) == true) {

						// map tokens to its variables in Condi2
						vars.put(this.operand1, tokenList1.get(token1));
						result = true;
					}
				}
			}
		}
		return result;
	}

	// test method
	public String getOperatorChar() {
		if (this.operator == 1)	
			return "<";
		if (this.operator == 2)	
			return "<=";
		if (this.operator == 3)
			return ">=";
		if (this.operator == 4)
			return ">";
		if (this.operator == 5)
			return "==";
		if (this.operator == 6)
			return "!=";

		return "";
	}
	
}
