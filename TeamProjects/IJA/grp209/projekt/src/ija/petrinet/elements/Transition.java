package ija.petrinet.elements;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Michal Starigazda, xstari01@stud.fit.vutbr.cz
 */
public class Transition implements Serializable{
	private static final long serialVersionUID = 1L;

	// transition identifier
	private int id;
	
	// condition
	Condition cond;
	
	// operation witch can be performed at tokens
	Expression expression;
	
	// output value of transition
	private int output;
	
	// <place ID, arc value (constant or variable>
	private Map<Object, String> in = new HashMap<Object, String>();
	// <place ID, arc value (constant or variable>
	private Map<Object, String> out = new HashMap<Object, String>();

	// coordinates of element
	private int x;
	private int y;
	
	public Transition(int id) {
		this.id = id;	
		cond = null;
		expression = null;
	}

	public int getConditionSize() {
		return cond.getNumber();
	}
	/**
	 * Add new condition into the transition.
	 * @param c condition
	 */
	public void addCondition(Condition c) {
		this.cond = c;
	}

	/**
	 * Add new expression into the transition.
	 * @param r result char
	 * @param exp expression string
	 */
	public void addExpression(String r, String exp) {
		this.expression = new Expression(r, exp);
	}

	/**
	 * Bind input place to its transiton.
	 * @param placeID place identifier
	 * @param value arc value
	 */
	public void bindInput(int placeID, String value) {
		in.put(new Integer(placeID), value);
	}

	/**
	 * Bind output place to its transiton.
	 * @param placeID place identifier
	 * @param value arc value
	 */
	public void bindOutput(int placeID, String value) {
		out.put(new Integer(placeID), value);
	}

	/**
	 * Get map of the trasition output places.
	 * @return map the transition output places
	 */
	public Map<Object, String> getOutMap() {
		return this.out;	
	}

	public int getInMapSize() {
		return this.in.size();
	}
	/**
	 * Get map of the trasition input places.
	 * @return map the transition input places
	 */
	public Map<Object, String> getInMap() {
		return this.in;	
	}
	
	public int getOutMapSize() {
		return this.out.size();
	}
	/**
	 * Get condition of the transition.
	 * @return condition
	 */
	public Condition getCond() {
		return this.cond;	
	}

	/**
	 * Get expression of the transition.
	 * @return expression
	 */
	public Expression getExpression() {
		return this.expression;
	}
	
	// test methods
	public int getId() {
		return this.id;	
	}

	public void printCondition() {
		if (this.cond != null)
			this.cond.printCondition();	
	}
	
	public String printExpression() {
		if (this.expression != null)
			return this.expression.getExpression();
		return null;
	}
	
	/**
	 * Set coordinates of transition.
	 * @param x 
	 * @param y
	 */
	public void setCoords(int x, int y){
	  this.x = x;
	  this.y = y;
	}
	
	/**
	 * Get x coordinate.
	 * @return X
	 */
	public int getX(){
	  return this.x;
	}
	
	/**
	 * Get y coordinate.
	 * @return Y
	 */
	public int getY(){
	  return this.y;
	}
}
