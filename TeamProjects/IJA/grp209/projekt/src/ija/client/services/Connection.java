package ija.client.services;

import java.net.*;
import java.io.*;
import ija.data.Data;
import ija.petrinet.PetriNet;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class represents connection between server and client and provides methods
 * for communication and authetization.
 * @author Michal Starigazda, xstari01@stud.fit.vutbr.cz
 * @version v0.02
 */
public class Connection {

  private String user = "";
  private String password = "";

  private Socket sock;
  
  private boolean loggedIn = false;   // session activity
  private boolean connected = false;  // connection activity
  
  ObjectOutputStream out = null;
  ObjectInputStream ois = null;
		
  /** Message object. */
  Data data = null;
  
  /** Communications codes - requests. */
  private static final int AUTH_REQ		  = 511;
  private static final int CREATE_ACC_REQ	  = 512;
  private static final int LOCAL_NET_SIMULATION	  = 521;
  private static final int GET_REMOTE_NET	  = 523;
  private static final int SAVE_NET		  = 531;
  private static final int GET_LIST		  = 541;
  private static final int LOGOUT		  = 550;
  
  /** Communications codes - responses. */
  private static final int AUTH_OK		  = 411;
  private static final int AUTH_FAIL		  = 412;
  private static final int ACC_ALR_EXIST	  = 414;
  private static final int CREATE_ACC_OK	  = 415;
  private static final int WRONG_FORMAT		  = 421;
  private static final int SIMULATION_RESULT	  = 423;
  private static final int DEAD_NET  		  = 424;
  private static final int SAVE_NET_OK		  = 431;
  private static final int SAVE_NET_NEW_VERSION	  = 432;
  private static final int LIST	  		  = 442;
    
  /** 
   * Help flag, for better description of errors.
   * 0 - OK
   * 1 - UnknownHost
   * 2 - IOException
   * 3 - unexpected code
   * 4 - weird exception, IO probably
   * 5 - dead net
   */
  int flag = 0;
  
  /** Get flag. */
  public int getFlag(){
    return this.flag;
  }
  
  /** Get currently logged user. */
  public String getUser(){
    return this.user;
  }
    
  /** Session activity. */
  public boolean logged(){
    return this.loggedIn;
  }
   
  /** Connection activity. */
  public boolean connected(){
    return connected;
  }
  
  /**
   * Establishing connection between server and client.
   * @param host  Remote host.
   * @param port  Port number.
   * @return Success/fail.
   */
  public boolean createConnection(String host, int port){
    this.connected = false;
    try {
      this.sock = new Socket(host, port);
      this.connected = true;
      
      OutputStream os = this.sock.getOutputStream();
      this.out = new ObjectOutputStream(os);

      InputStream is = this.sock.getInputStream();
      this.ois = new ObjectInputStream(is);
			
      return connected;
    } 
    catch (UnknownHostException ex) {
      this.flag = 1;
      return connected;
    }
    catch (java.io.IOException ex) {
      System.err.println(ex);
      this.flag = 2;
      return connected;
    }
  }
  
  /** Closing connection. */
  public void closeConnection(){
    try {
      this.sock.close();
      this.ois.close();
      this.out.close();
      
      this.connected = false;
      this.loggedIn = false;
    } 
    catch (java.io.IOException ex) {
      System.err.println(ex);
    }
  }
  
  /**
   * Logging user to remote host.
   * @param user    User name.
   * @param passwd  Passwrod.
   * @return Success.
   */
  public boolean logIn(String user, String passwd){
    this.loggedIn = false;
    flag = 0;
    
    // init data
    this.data = new Data(AUTH_REQ, user, passwd, (PetriNet)null, null, 0.0, 0);
    
    try {
      //send data
      out.writeObject(data);
      this.data = null;
    
      // response
      while ((data = (Data)ois.readObject()) != null) {
	if (data.getCode() == AUTH_OK) {
	  // authorization OK
	  this.loggedIn = true;
	  this.user = user;
	  this.password = passwd;
	  break;
	}
	else if (data.getCode() == AUTH_FAIL) {
	  // authorization FAIL
	  break;
	}
	else{
	  flag = 3; // unexpected code occure
	  break;
	}
      }
    } catch (Exception e) {
      System.err.println(e);
      flag = 4;	  // weird exception, IO probably
    }
    
    return loggedIn;
  }
  
  /**
   * Creating new user on remote host, request established session.
   * @param user    User name
   * @param passwd  Passwrod
   * @return Success.
   */
  public boolean createAccount(String user, String passwd){
    this.loggedIn = false;
    flag = 0;
    
    // init data
    this.data = new Data(CREATE_ACC_REQ, user, passwd, (PetriNet)null, null, 0.0, 0);
    
    try {
      //send data
      out.writeObject(data);
      this.data = null;
    
      // response
      while ((data = (Data)ois.readObject()) != null) {
	if (data.getCode() == CREATE_ACC_OK) {
	  // creat account OK
	  this.user = user;
	  this.password = passwd;
	  //login
	  this.loggedIn = this.logIn(this.user, this.password);	  
	  break;
	}
	else if (data.getCode() == ACC_ALR_EXIST) {
	  // acount already exists
	  // pass
	  break;
	}
	else{
	  flag = 3; // unexpected code occure
	  break;
	}
      }
    } catch (Exception e) {
      System.err.println(e);
      flag = 4;	  // weird exception
    }
    
    return loggedIn;
  }
  
  /** Logout current user. */
  public void logOut(){
    flag = 0;
    
    // init data
    this.data = new Data(LOGOUT, this.user, null, (PetriNet)null, null, 0.0, 0);
    
    try {
      //send data
      out.writeObject(data);
      this.data = null;
      
      // not interested in response
      
    } catch (Exception e) {
      System.err.println(e);
      flag = 4;	  // weird exception
    }
          
    this.loggedIn = false;
    this.user = "";
    this.password = "";
  }
  
  /** Get simulation of Petri net. 
   * @param net Petri's net.
   * @param simflag Simulation flag( 0 -> simulet net, 1 -> one step)
   * @return Petri net after simulation
   */
  public PetriNet simulate(PetriNet net, int simflag){
    flag = 0;
    
    // init data
    this.data = new Data(LOCAL_NET_SIMULATION, null, null, net, null, 0.0, simflag);
    
    try {
      //send data
      out.writeObject(data);
      this.data = null;
    
      // response
      while ((data = (Data)ois.readObject()) != null) {
	if (data.getCode() == SIMULATION_RESULT) {
	  // csimulation OK
	  return data.getPetriNet();
	}
	else if (data.getCode() == WRONG_FORMAT) {
	  // wrong format
	  // pass
	  break;
	}
	else if (data.getCode() == DEAD_NET){
	  flag = 5;
	  return data.getPetriNet();
	}
	else{
	  flag = 3; // unexpected code occure
	  break;
	}
      }
    } catch (Exception e) {
      System.err.println(e);
      flag = 4;	  // weird exception
    }
    
    return null;
  }
  
  /**
   * Get net from server.
   * @param Name.
   * @param version.
   * @return PetriNet Petri net.
   */
  public PetriNet getRemoteNet(String name, double version){
    flag = 0;
    
    // init data
    this.data = new Data(GET_REMOTE_NET, null, null, (PetriNet)null, name, version, 0);
    
    try {
      //send data
      out.writeObject(data);
      this.data = null;
    
      // response
      while ((data = (Data)ois.readObject()) != null) {
	if (data.getCode() == SIMULATION_RESULT) {
	  // get net OK
	  return data.getPetriNet();
	}
	else if (data.getCode() == WRONG_FORMAT) {
	  // wrong format
	  // pass
	  break;
	}
	else{
	  flag = 3; // unexpected code occure
	  break;
	}
      }
    } catch (Exception e) {
      System.err.println(e);
      flag = 4;	  // weird exception
    }
    
    return null;
  }
  
  /**
   * Save net on remote host.
   * @param net Petri nett to save.
   * @return success.
   */
  public boolean saveNet(PetriNet net){
    boolean success = false;
    flag = 0;
    
    // init data
    this.data = new Data(SAVE_NET, null, null, net, null, 00, 0);
    
    try {
      //send data
      out.writeObject(data);
      this.data = null;
    
      // response
      while ((data = (Data)ois.readObject()) != null) {
	if (data.getCode() == SAVE_NET_OK) {
	   success = true;
	   break;
	}
	else if (data.getCode() == SAVE_NET_NEW_VERSION) {
	  // acount already exists
	  flag = 1; // v tomto pripade to nie je chyba
	  success =false;
	  break;
	}
	else{
	  flag = 3; // unexpected code occure
	}
	
	data = null;
      }
    } catch (Exception e) {
      System.err.println(e);
      flag = 4;	  // weird exception
    }
    
    return success;
  }
  
  /** 
   * Get list of nets stored on server.
   * @return ArrayList<Petrinet>.
   */
  public ArrayList<PetriNet> getListOfNets(){
    boolean success = false;
    flag = 0;
    
    // init data
    this.data = new Data(GET_LIST, null, null, (PetriNet)null, null, 0, 0);
    
    try {
      //send data
      out.writeObject(data);
      this.data = null;
    
      // response
      while ((data = (Data)ois.readObject()) != null) {
	if (data.getCode() == LIST) {
	   return data.getPetriNetList();
	}
	else{
	  flag = 3; // unexpected code occure
	  return null;
	}
      }
    } catch (Exception e) {
      System.err.println(e);
      flag = 4;	  // weird exception
    }
    return null;
  }
  
}
