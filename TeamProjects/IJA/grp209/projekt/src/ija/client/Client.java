/**
 * Client of Petri nets simulator (editor).
 */

package ija.client;

import ija.client.config.XMLhandler;
import ija.client.config.Config;
import ija.client.gui.MainWindow;

import org.dom4j.DocumentException;
import java.io.*;
import javax.swing.*;

/**
 * Client.
 * @author Michal Starigazda, xstari01@stud.fit.vutbr.cz
 * @version 0.01
 */
public class Client {
  public static void main(String[] args){
    Config config = new Config();
    
    // load configuration
    try{
      XMLhandler tmp = new XMLhandler();
      tmp.parseWithSAX(new File("config.xml")); 
      
      config = tmp.getConfig();
      //config.printConfig();
    }
    catch (DocumentException ex) {
      ex.printStackTrace();
      XMLhandler h = new XMLhandler();
		h.createDefault();
    }  
    // Run GUI
    JFrame gui = new MainWindow(config);
    gui.setVisible(true);
  }
}
