/**
 * Implementation of class for parsing/modifying of configuration XML file.
 *  @author Michal Starigazda
 */

package ija.client.config;

import java.util.*;
import java.io.*;
import org.dom4j.*;
import org.dom4j.dom.*;
import org.dom4j.datatype.*;
import org.dom4j.io.*;


/** 
 * Class for parsing/modifying of configuration XML file.
 * @author Michal Starigazda
 * @version 0.02
 */
public class XMLhandler {

  private org.dom4j.Document doc;
  private org.dom4j.Element root;
     
  /**
   * Parses configuration XML document.
   * @return Object representing configuration.
   */
  public Config getConfig() {
    Config conf = new Config();
    
    // root elements iterator
    root = this.doc.getRootElement();
    Iterator elementIterator = root.elementIterator();
    while(elementIterator.hasNext()){
      Element element = (Element)elementIterator.next();
      Iterator innerIt = element.elementIterator();
      // inner elements iterator - values
      while(innerIt.hasNext()){
  	Element innerElement = (Element) innerIt.next();
        if(element.getName().equals("font")) {
            if(innerElement.getName().equals("size")){
              conf.setFontSize(new Integer(innerElement.getText()));
            }
            if(innerElement.getName().equals("type")){
              conf.setFontType(innerElement.getText());
            }
            if(innerElement.getName().equals("color")){
              conf.setFontColor(innerElement.getText());
            }
        }
        if(element.getName().equals("graphics")) {
            if(innerElement.getName().equals("place")){
              conf.setPlaceVal(innerElement.getText());
            }
            if(innerElement.getName().equals("arc")){
              conf.setArcVal(innerElement.getText());
            }
            if(innerElement.getName().equals("transition")){
              conf.setTransVal(innerElement.getText());
            }
        }
        if(element.getName().equals("background")) {
            if(innerElement.getName().equals("color")){
              conf.setBgColor(innerElement.getText());
            }
        }
      }
    }
  
    return conf;
  }   
  
  /**
   * Mthod modify (save) configuration to XML document.
   * @param conf Object representing configuration. 
   */
  public void saveConfig(Config conf) throws IOException {
    root = this.doc.getRootElement();
    Iterator elementIterator = root.elementIterator();
    while(elementIterator.hasNext()){
      Element element = (Element)elementIterator.next();
      Iterator innerIt = element.elementIterator();
      while(innerIt.hasNext()){
  	Element innerElement = (Element) innerIt.next();
        if(element.getName().equals("font")) {
            if(innerElement.getName().equals("size")){
              innerElement.setText(""+conf.getFontSize());
            }
            if(innerElement.getName().equals("type")){
              innerElement.setText(conf.getFontType());
            }
            if(innerElement.getName().equals("color")){
              innerElement.setText(conf.getFontColor());
            }
        }
        if(element.getName().equals("graphics")) {
            if(innerElement.getName().equals("place")){
              innerElement.setText(conf.getPlaceVal());
            }
            if(innerElement.getName().equals("arc")){
              innerElement.setText(conf.getArcVal());
            }
            if(innerElement.getName().equals("transition")){
              innerElement.setText(conf.getTransVal());
            }
        }
        if(element.getName().equals("background")) {
            if(innerElement.getName().equals("color")){
              innerElement.setText(conf.getBgColor());
            }
        }
      }
    }
    // Writing to XML document.
    XMLWriter out = new XMLWriter(new FileWriter( new File("./config.xml") ));
    out.write(doc);
    out.close();
  }
  
  /**
   * Document's parser.
   * @param aFile XML document (File).
   * @throws DocumentException 
   */
  public void parseWithSAX(File aFile) throws DocumentException {
      SAXReader xmlReader = new SAXReader();
      this.doc = xmlReader.read(aFile);
  }

  void newJDomDocument(String file) throws IOException {
		doc = DocumentHelper.createDocument();
        Element root = doc.addElement("config");   
       
		Element font = root.addElement("font");		
			Element size = font.addElement("size");
				size.setText("12");
			Element type = font.addElement("type");
				type.setText("DejaVu Serif");	
			Element color = font.addElement("color");
				color.setText("black");	
			
		Element graphics = root.addElement("graphics");
			Element place = graphics.addElement("place");
				place.setText("white");
			Element arc = graphics.addElement("arc");
				arc.setText("black");
			Element transition = graphics.addElement("transition");
				transition.setText("gray");
			
		Element bg = root.addElement("background");
			Element c2 = bg.addElement("color");
				c2.setText("white");
  
		OutputFormat outformat = OutputFormat.createPrettyPrint();
		Writer out = new OutputStreamWriter(new FileOutputStream(file));
		XMLWriter writer = new XMLWriter(out, outformat);
		writer.write(doc);
		writer.flush();	
    }
  
     public void iterateRootChildren() {
		root = this.doc.getRootElement();
		Iterator elementIterator = root.elementIterator();
	while(elementIterator.hasNext()){
	    Element element = (Element)elementIterator.next();
	    System.out.println(element.getName());
	    Iterator innerIt = element.elementIterator();
	    while(innerIt.hasNext()){
		Element innerElement = (Element) innerIt.next();
		System.out.println(element.getName() + "/" + innerElement.getName() + ":" + innerElement.getText());
	    }
	}
    }
	 
	public void createDefault() {
		try {
			//newJDomDocument(this.name+"-"+creator+"-"+this.version+".xml");
			//parseWithSAX(new File(this.name+"-"+creator+"-"+this.version+".xml"));
			newJDomDocument("config.xml");
			parseWithSAX(new File("config.xml"));
			iterateRootChildren();
		} catch (DocumentException ex) {
		} catch (IOException ex) {
		}
	}
}