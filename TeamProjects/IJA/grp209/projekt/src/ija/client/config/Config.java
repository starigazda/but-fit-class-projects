package ija.client.config;

/**
 * Class represents font config in displayed nets.
 * @author Michal Starigazda
 */
class Font{
  public int size;
  public String type;
  public String color;
}

/**
 * Class reprezents configuration ob Petri net's element.
 * @author Michal Starigazda
 */
class GElement{
  public String color;
}

/**
 * Configuration of displayed petri nets in editor
 * @author Michal Starigazda, xstari01@stud.fit.vutbr.cz
 * @version 0.02
 */
public class Config {
  protected Font font = new Font();
  protected GElement place = new GElement();
  protected GElement transition = new GElement();
  protected GElement arc = new GElement();
  protected String bgcolor;
  
  /* See names of methods. */
  public int getFontSize(){return this.font.size;}
  public String getFontType(){return this.font.type;}
  public String getFontColor(){return this.font.color;}
  
  public String getPlaceVal(){return this.place.color;}
  public String getArcVal(){return this.arc.color;}
  public String getTransVal(){return this.transition.color;}
  
  public String getBgColor(){return this.bgcolor;}
  
  public void setFontSize(int size){this.font.size = size;}
  public void setFontType(String val){this.font.type = val;}
  public void setFontColor(String val){this.font.color = val;}
  
  public void setPlaceVal(String val){this.place.color = val;}
  public void setArcVal(String val){this.arc.color = val;}
  public void setTransVal(String val){this.transition.color = val;}
  
  public void setBgColor(String val){this.bgcolor = val;}
  
  
  /** Load default configuration. */
  public void loadDefault(){
    this.font.size = 12;
    this.font.color = "black";
    this.font.type = "DejaVu Serif";
    
    this.place.color = "white";
    this.transition.color = "gray";
    this.arc.color = "black";
    
    this.bgcolor = "white";
  }
  
  /**
   * Print class content - configuration..
   */
  public void printConfig(){
    System.out.printf("Font: %d, %s, %s\n", font.size, font.type, font.color);
    System.out.printf("Objects:\n\tPlace: %s\n\tArc: %s\n\tTransition: %s\n", place.color, arc.color, transition.color);
    System.out.printf("Background color: %s\n", bgcolor);
  }
}
