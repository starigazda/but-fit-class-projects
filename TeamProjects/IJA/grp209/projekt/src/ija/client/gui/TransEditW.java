package ija.client.gui;

import ija.petrinet.elements.Condi2;
import ija.petrinet.elements.Condition;
import ija.petrinet.elements.Expression;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Map;
import javax.swing.DefaultListModel;
import javax.swing.JDialog;
import javax.swing.JFrame;
import org.nfunk.jep.JEP;

/**
 * Simple dialog for editing conditions and expression in tansitions.
 * @author Michal Starigazda, xstari01@stud.fit.vutbr.cz
 */
public class TransEditW extends JDialog {

  private ArrayList<Condi2> condition = null;
  private Expression expression = null;
  private ArrayList<String> in;
  private ArrayList<String> out;
  DefaultListModel listModel = new DefaultListModel();
  DefaultListModel opsListModel = new DefaultListModel();
  
  /**
   * Constructor.
   */
  public TransEditW(JFrame frame, Condition cond, Expression exp,
	  ArrayList<String> in, ArrayList<String> out) {
    super(frame,true);
    this.condition = cond.getCondition();
    this.expression = exp;
    this.in = in;
    this.out = out;
    initComponents();
    if(this.expression != null){
      this.resultTextField.setText(this.expression.getResultChar());
      this.exprTextField.setText(this.expression.getExpression());
    }
    else{
      this.resultTextField.setText("");
      this.exprTextField.setText("");
    }
  }

  /** Get condition. */
  public Condition getCondition(){
    Condition tmp = new Condition();
    for(Condi2 c : this.condition){
      tmp.addCondi2(c);
    }
    return tmp;
  }
  
  /** Get expression. */
  public Expression getExpression(){
    return this.expression;
  }
  
  /** Operand validator. */
  private boolean isValidOp(String op){
    boolean tmp = false;
    for(String s : in){
      if (s.equals(op)){
	tmp = true;
	break;
      }
    }
    if (!tmp){
      try{
	Integer.parseInt(op);
	tmp = true;
      }
      catch(NumberFormatException nfe)
      {
	tmp = false;
      }
    }
    return tmp;
  }
  
  /** Result identifier validator. */
  private boolean isValidResultChar(String str){
    for(String s : out){
      if (s.equals(str)){
	return true;
      }
    }
    return false;
  }
  
  /**
   * This method is called from within the constructor to initialize the form.
   * WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jps = new javax.swing.JScrollPane();
        Condi2 tmp = null;
        String str = "";

        if (condition != null){
            for (int i = 0; i < condition.size(); i++) {
                tmp = condition.get(i);
                str += tmp.getOp1() + " " + tmp.getOperatorChar()+" "+tmp.getOp2()+" ";
                listModel.addElement(str);
                str = "";
            }
        }
        condList = new javax.swing.JList();
        removeButton = new javax.swing.JButton();
        addButton = new javax.swing.JButton();
        op1 = new javax.swing.JTextField();
        condLabel = new javax.swing.JLabel();
        exprLabel = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        resultTextField = new javax.swing.JTextField();
        equalLabel = new javax.swing.JLabel();
        exprTextField = new javax.swing.JTextField();
        statusLabel = new javax.swing.JLabel();
        statusLabel1 = new javax.swing.JLabel();
        OK = new javax.swing.JButton();
        op2 = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        opList = new javax.swing.JList();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Transition editor");
        setResizable(false);

        condList.setModel(listModel);
        jps.setViewportView(condList);

        removeButton.setText("Remove selected");
        removeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeButtonActionPerformed(evt);
            }
        });

        addButton.setText("Add condition");
        addButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addButtonActionPerformed(evt);
            }
        });

        condLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        condLabel.setText("Condition");

        exprLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        exprLabel.setText("Expression");

        resultTextField.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        equalLabel.setText("=");

        OK.setText("OK");
        OK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                OKActionPerformed(evt);
            }
        });

        opsListModel.addElement("<");
        opsListModel.addElement("<=");
        opsListModel.addElement(">");
        opsListModel.addElement(">=");
        opsListModel.addElement("==");
        opsListModel.addElement("!=");
        opList.setModel(opsListModel);
        jScrollPane1.setViewportView(opList);
        this.opList.setSelectedIndex(0);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(condLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jSeparator1)
                    .addComponent(statusLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(exprLabel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(statusLabel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jps, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(addButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(op1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(op2, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(removeButton, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 134, Short.MAX_VALUE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(OK, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(resultTextField)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(equalLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(exprTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(condLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(op1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(op2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(addButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(removeButton))
                    .addComponent(jps, javax.swing.GroupLayout.DEFAULT_SIZE, 166, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(statusLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(exprLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(exprTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(equalLabel)
                    .addComponent(resultTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(statusLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(OK)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

  private void removeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeButtonActionPerformed
    int index = condList.getSelectedIndex();
    //save content
    String toDel = ""+listModel.getElementAt(index);
    // remove from list
    listModel.remove(index);	  
    
    // remove from condition list
    Condi2 tmp = null;
    String str = "";

    if (condition != null){
      for (int i = 0; i < condition.size(); i++) {
	tmp = condition.get(i);
	str += tmp.getOp1() + " " + tmp.getOperatorChar()+" "+tmp.getOp2()+" ";
	if(str.equals(toDel)){
	  this.condition.remove(i);
	}
      }
    }

    int size = listModel.getSize();

    if (size == 0) {
        removeButton.setEnabled(false);

    } else { //Select an index.
        if (index == listModel.getSize()) {
            //removed item in last position
            index--;
        }

        condList.setSelectedIndex(index);
        condList.ensureIndexIsVisible(index);
    }
  }//GEN-LAST:event_removeButtonActionPerformed

  private void addButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addButtonActionPerformed
    Condi2 newcondi = null;
    
    if (isValidOp(op1.getText())){
      if(isValidOp(op2.getText())){
	int index = opList.getSelectedIndex(); //get selected index
	System.out.println(index);
	listModel.insertElementAt(op1.getText() + " "+opsListModel.getElementAt(index) + " "+op2.getText(), listModel.size());
	
	newcondi = new Condi2(index+1, op1.getText(), op2.getText());
	this.condition.add(newcondi);
	
	//Reset the text fields.
	op1.requestFocusInWindow();
	op1.setText("");
	op2.requestFocusInWindow();
	op2.setText("");

	//Select the new item and make it visible.
	condList.setSelectedIndex(0);
	condList.ensureIndexIsVisible(index);
	
	this.opList.setSelectedIndex(0);
      }
      else{
	this.statusLabel1.setForeground(Color.red);
	this.statusLabel1.setText("Wrong operand 2.");
	return;
      }
      
      this.statusLabel1.setForeground(Color.black);
      this.statusLabel1.setText("Condition valid.");
    }
    else{
      this.statusLabel1.setForeground(Color.red);
      this.statusLabel1.setText("Wrong operand 1.");
    }
  }//GEN-LAST:event_addButtonActionPerformed

  private void OKActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_OKActionPerformed
      
    // empty expression
    if (this.exprTextField.getText().replaceAll("\\s+", "").length() == 0 && this.resultTextField.getText().replaceAll("\\s+", "").length() == 0){
      this.statusLabel.setForeground(Color.black);
      this.statusLabel.setText("OK.");
      this.expression = null;
      setVisible(false);
      return;
    }
    
    // incomplete expresion
    if (this.in.isEmpty()|| this.out.isEmpty()){
      this.statusLabel.setForeground(Color.red);
      this.statusLabel.setText("<html>Incomplete transition - use empty expression.</html>");
      return;
    }
    
    if (isValidResultChar(this.resultTextField.getText())){
      // expression validating
      if (this.exprTextField.getText().replaceAll("\\s+", "").length() == 0){
	this.statusLabel.setForeground(Color.red);
	this.statusLabel.setText("Invalid expression.");
	return;
      }
      
      JEP j = new JEP();
      
      j.setAllowUndeclared(true);
      j.parseExpression(this.exprTextField.getText());
      
      for (Object var : j.getSymbolTable().keySet()){
	if (!this.isValidOp(""+var)){
	  this.statusLabel.setForeground(Color.red);
	  this.statusLabel.setText("Invalid identifier in expression.");
	  return;
	}
      }
      this.expression = new Expression(this.resultTextField.getText(),this.exprTextField.getText());
      this.setVisible(false);
    }
    else{
      this.statusLabel.setForeground(Color.red);
      this.statusLabel.setText("Wrong result identifier.");
    }
  }//GEN-LAST:event_OKActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton OK;
    private javax.swing.JButton addButton;
    private javax.swing.JLabel condLabel;
    private javax.swing.JList condList;
    private javax.swing.JLabel equalLabel;
    private javax.swing.JLabel exprLabel;
    private javax.swing.JTextField exprTextField;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JScrollPane jps;
    private javax.swing.JTextField op1;
    private javax.swing.JTextField op2;
    private javax.swing.JList opList;
    private javax.swing.JButton removeButton;
    private javax.swing.JTextField resultTextField;
    private javax.swing.JLabel statusLabel;
    private javax.swing.JLabel statusLabel1;
    // End of variables declaration//GEN-END:variables
}
