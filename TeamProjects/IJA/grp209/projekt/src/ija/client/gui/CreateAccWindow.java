package ija.client.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.net.*;
import java.io.*;

import ija.client.services.*;

/**
 * Simple GUI for creating new account on server, expected established connction with server.
 * Extends autentication class GUI.
 * @author Michal Starigazda, xstari01@stud.fit.vutbr.cz
 */
public class CreateAccWindow extends AuthentLogInWindow {

  /** Constructor. */
  public CreateAccWindow(JFrame frame, Connection con) {
    super(frame,con);
    setLocationRelativeTo(null);
    setTitle("Create account");
  }
  
  /** Create accoutn. */
  private void createAccount(){
    user = userTextField.getText().trim();
    passwd = new String(passwdTextField.getPassword()).trim();
    
    if (con.createAccount(user, passwd)){
      JOptionPane.showMessageDialog(this, "Account " + user + " created.");
      setVisible(false);
    }
    else{
      JOptionPane.showMessageDialog(this, "Cannot create account!","Warning",JOptionPane.WARNING_MESSAGE);
    }
  }
  
  /** Handler OK button. */
  @Override
  protected void okButtonActionPerformed(java.awt.event.ActionEvent evt) {
    if (validation()){
      createAccount();
    }
    else{
      JOptionPane.showMessageDialog(this, "Please do not leave any fields empty!");
    }
      
  }
}
